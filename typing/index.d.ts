declare module "*.json" {
  const value: any;
  export default value;
}
declare module 'react-highlight-words';

declare module 'class-autobind';

declare module 'react-truncate';

declare module 'google-map-react';

declare module 'react-infinite-scroller';

declare module 'query-string';

declare module 'react-places-autocomplete';

declare module 'uuid-js';