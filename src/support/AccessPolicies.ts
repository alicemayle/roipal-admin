import { Guardian, User } from '@xaamin/guardian';

Guardian.before(function (user, ability) {
  return user.is('root');
});

// listing actions
Guardian.define('company::page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('mission::page-access', (user: User) => {
  return user.is(['root', 'reader'])
});

Guardian.define('executive::page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('assessment::page-access', (user: User) => {
  return user.is(['root', 'reader'])
});

Guardian.define('invitation::page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('translation::page-access', (user: User) => {
  return user.is(['root', 'reader'])
});

Guardian.define('configuration::global-page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('commercial-need::page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('type-sale::page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('company-commission::page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('invoice::page-access', (user: User) => {
  return user.is(['root', 'reader']);
});

Guardian.define('executive-payment::page-access', (user: User) => {
  return user.is(['root', 'reader'])
});

Guardian.define('location::page-access', (user: User) => {
  return user.is(['company'])
});

Guardian.define('user::page-access', (user: User) => {
  return user.is(['root', 'company'])
});

Guardian.define('role::page-access', (user: User) => {
  return user.is(['root'])
});

// edit actions
Guardian.define('company::credit-edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('company::profile-edit', (user: User) => {
  return user.is(['root', 'company']);
});

Guardian.define('assessment::edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('executive::document-edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('translation::edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('configuration::global-edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('commercial-need::edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('type-sale::edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('company::commission-edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('executive::payment-edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('mission::edit', (user: User) => {
  return user.is(['root', 'company']);
});

Guardian.define('role::edit', (user: User) => {
  return user.is(['root']);
});

Guardian.define('user::edit', (user: User) => {
  return user.is(['root', 'company']);
});

Guardian.define('location::edit', (user: User) => {
  return user.is(['company']);
});

Guardian.define('company::payment-edit', (user: User) => {
  return user.is(['company']);
});

Guardian.define('company::password-edit', (user: User) => {
  return user.is(['company']);
});

// create actions
Guardian.define('mission::create', (user: User) => {
  return user.is(['root', 'company']);
});

Guardian.define('translation::create', (user: User) => {
  return user.is(['root']);
});

Guardian.define('configuration::create', (user: User) => {
  return user.is(['root']);
});

Guardian.define('company::comission-create', (user: User) => {
  return user.is(['root']);
});

Guardian.define('location::create', (user: User) => {
  return user.is(['company']);
});

Guardian.define('user::create', (user: User) => {
  return user.is(['root','company']);
});

Guardian.define('role::create', (user: User) => {
  return user.is(['root']);
});

// download actions
Guardian.define('executive::payment-download', (user: User) => {
  return user.is(['root']);
});

Guardian.define('company::invoice-download', (user: User) => {
  return user.is(['root']);
});

Guardian.define('company::invoice-retry', (user: User) => {
  return user.is(['root']);
});

Guardian.define('mission::executive-reject', (user: User) => {
  return user.is(['company']);
});

Guardian.define('mission::executive-rate', (user: User) => {
  return user.is(['company']);
});

Guardian.define('company::account-edit', (user: User) => {
  return user.is(['company']);
});