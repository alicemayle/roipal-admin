const typeCard = {
  VISA: 'visa',
  MASTERCARD: 'mastercard',
  AMERICAN_EXPRESS: 'american-express',
  DISCOVER: 'discover'
}

export {
  typeCard
}