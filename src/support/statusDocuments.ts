const statusDocuments = {
  MISSION_ON_HOLD: 0,
  MISSION_REJECTED: -1,
  MISSION_APPROVED: 1
}

export {
  statusDocuments
}
