import Lang from './Lang';

class ValidateLanguage {

  public setLanguage(language: string) {
    localStorage.setItem('language', language);
    Lang.setLocale(language);
  }


  public async loadLanguage() {
    const language = localStorage.getItem('language')
    const lang = typeof language === 'string' ? language : 'es'

    Lang.setLocale(lang);
  }
}

export default new ValidateLanguage();