import missions from './modules/missions'
import generic from './generic'
import parametrization from './modules/parametrization'
import payments from './modules/payments'
import charges from './modules/charges'
import invitations from './modules/invitations'
import users from './modules/users'
import companies from './modules/companies'
import documents from './modules/documents'
import assessments from './modules/assestments'
import translations from './modules/languages'
import access from './modules/access'
import locations from './modules/locations'
import settings from './modules/settings'
import register from './modules/register'
import reset from './modules/reset-password'
import signin from './modules/signin'
import map from './modules/map'

const dictionary:{} = {
   ...missions,
   ...generic,
   ...parametrization,
   ...payments,
   ...charges,
   ...invitations,
   ...users,
   ...companies,
   ...documents,
   ...assessments,
   ...translations,
   ...access,
   ...locations,
   ...settings,
   ...register,
   ...reset,
   ...signin,
   ...map
}

export default dictionary