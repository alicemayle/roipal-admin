import button from './buttons';
import language from './language';
import policies from './policies';
import scope from './scope';
import legends from './legends';
import notifications from './notifications'

const generic = {
  'es.generic': {
    'legends': legends.es,
    'button': button.es,
    'language': language.es,
    'policies': policies.es,
    'scope':scope.es,
    'notifications': notifications.es
  },
  'en.generic': {
    'legends': legends.en,
    'button': button.en,
    'language': language.en,
    'policies': policies.en,
    'scope': scope.en,
    'notifications': notifications.en
  },
  'fr.generic': {
    'legends': legends.fr,
    'button': button.fr,
    'language': language.fr,
    'policies': policies.fr,
    'scope': scope.fr,
    'notifications': notifications.fr
  }
}

export default generic;