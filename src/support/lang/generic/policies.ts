const policies = {
    'en': {
      'denied': 'Permits denied',
    },
    'es': {
      'denied': 'Permisos denegados',
    },
    'fr': {
      'denied': 'Permis refusés',
    }
  }
  
  export default policies