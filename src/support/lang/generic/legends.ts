const legends = {
  'en': {
    'confirm': 'Confirmation',
    'required': 'It is required',
    'read_more': 'See more',
    'read_less': 'See Less',
    'start': 'Initiated',
    'finish': 'Finished',
    'comment': 'Comment'
  },
  'es': {
    'confirm': 'Confirmación',
    'required': 'Es requerido',
    'read_more': 'Ver más',
    'read_less': 'Ver menos',
    'start': 'Iniciada',
    'finish': 'Finalizada',
    'comment': 'Comentario'
  },
  'fr': {
    'confirm': 'Confirmation',
    'required': 'Est requis',
    'read_more': 'Voir plus',
    'read_less': 'Voir moins',
    'start': 'Initié',
    'finish': 'Terminé',
    'comment': 'Commentaire'
  }
}

export default legends