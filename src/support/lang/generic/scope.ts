const scope = {
  'en': {
    'admin': 'Admin',
    'company': 'Company',
    'select_scope': 'Select an option',
  },
  'es': {
    'admin': 'Admin',
    'company': 'Compañía',
    'select_scope': 'Seleccione una opción',
  },
  'fr': {
    'admin': 'Admin',
    'company': 'Entreprise',
    'select_scope': 'Sélectionnez une option',
  }
}

export default scope