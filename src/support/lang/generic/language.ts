const language = {
  'en': {
    'english': 'English',
    'spanish': 'Spanish',
    'french': 'French',
    'select_language': 'Select language'
  },
  'es': {
    'english': 'Inglés',
    'spanish': 'Español',
    'french': 'Francés',
    'select_language': 'Seleccionar idioma'
  },
  'fr': {
    'english': 'Anglais',
    'spanish':' Espagnol',
    'french': 'Français',
    'select_language': 'Choisir la langue'
  }
}

export default language