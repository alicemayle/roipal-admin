const notifications = {
    'en': {
      'error': 'Error',
      'success': 'Successful'
    },
    'es': {
      'error': 'Error',
      'success': 'Exitoso'
    },
    'fr': {
      'error': 'Erreur',
      'success': 'Succès'
    }
  }
  
  export default notifications