import user from './user'
import button from './buttons'

const users = {
  'es.users': {
    'button': button.es,
    'user': user.es
  },
  'en.users': {
    'button': button.en,
    'user': user.en
  },
  'fr.users': {
    'button': button.fr,
    'user': user.fr
  }
}

export default users;