import invitations from './invitations';

const invitation = {
  'es.invitation': {
    'title_menu': 'Invitaciones',
    'content': 'Ver listado de invitaciones generadas a los prestadores de servicio',
    ...invitations.es,
  },
  'en.invitation': {
    'title_menu': 'Invitations',
    'content': 'View list of invitations generated to service providers',
    ...invitations.en,
  },
  'fr.invitation': {
    'title_menu': 'Invitations',
    'content': 'Voir la liste des invitations générées pour les fournisseurs de services',
    ...invitations.fr,
  }
}

export default invitation;