const invitations = {
    'en' : {
      'description': 'Description',
      'company': 'Company',
      'details': 'Invitations details',
      'executive': 'Service provider',
      'mission': 'Mission',
      'title:': 'Invitations',
      'date': 'Creation date',
      'status': 'Status'
    },
    'es': {
      'description': 'Descripción',
      'company': 'Compañia',
      'details': 'Detalles de invitación',
      'executive': 'Prestador de servicio',
      'mission': 'Misión',
      'title': 'Invitaciones',
      'date': 'Fecha de creación',
      'status': 'Estatus'
    },
    'fr': {
      'description': 'Portrait',
      'company': 'Maison de commerce',
      'details': "Détails de l'invitation",
      'executive': 'Prestataire de services',
      'mission': 'Mission',
      'title': 'Invitations',
      'date': 'Date de creation',
      'status': 'Statut'
    }
}

export default invitations;