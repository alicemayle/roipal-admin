import companies from './companies';
import location from './location';
import missions from './missions';
const company = {
  'es.company': {
    ...companies.es,
    'location': location.es,
    'mission': missions.es
  },
  'en.company': {
    ...companies.en,
    'location': location.en,
    'mission': missions.en
  },
  'fr.company': {
    ...companies.fr,
    'location': location.fr,
    'mission': missions.fr
  }
}

export default company;