const location = {
  'en': {
    'title': 'Location',
    'address': 'Address'
  },
  'es': {
    'title': 'Ubicación',
    'address': 'Dirección'
  },
  'fr': {
    'title': 'Lieu',
    'address': 'Adresse'
  }
}

export default location
