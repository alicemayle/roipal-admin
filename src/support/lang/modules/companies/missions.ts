const missions = {
  'en': {
    'title': 'Missions',
    'total': 'Total missions',
    'created': 'Created',
    'started': 'Started',
    'completed': 'Completed',
    'canceled': 'Canceled'
  },
  'es': {
    'title': 'Misiones',
    'total': 'Total de misiones',
    'created': 'Creadas',
    'started': 'Iniciadas',
    'completed': 'Completadas',
    'canceled': 'Canceladas'
  },
  'fr': {
    'title': 'Missions',
    'total': 'Total des missions',
    'created': 'Créé',
    'started': 'Lancé',
    'completed': 'Fini',
    'canceled': 'Annulé'
  }
}

export default missions