import location from './location'
import button from './buttons'

const locations = {
  'es.locations': {
    'button': button.es,
    'location': location.es
  },
  'en.locations': {
    'button': button.en,
    'location': location.en
  },
  'fr.locations': {
    'button': button.fr,
    'location': location.fr
  }
}

export default locations;