const language = {
  'en': {
    'title': 'Language',
    'selected': 'Selected language',
    'change': 'Change'
  },
  'es': {
    'title': 'Idioma',
    'selected': 'Idioma seleccionado',
    'change': 'Cambiar'
  },
  'fr': {
    'title': 'Langue',
    'selected': 'Langue sélectionnée',
    'change': 'Changer'
  }
}

export default language