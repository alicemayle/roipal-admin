const settings = {
  'en': {
    'title': 'Settings',
    'personal': 'Personal information',
    'update_error': 'Error updating profile',
    'name': 'Name',
    'content': 'Company profile settings',
    'log_out': 'Are you sure you want to log out?'
  },
  'es': {
    'title': 'Configuración',
    'personal': 'Información Personal',
    'update_error': 'Error al actualizar perfil',
    'name': 'Nombre',
    'content': 'Configuraciones de perfil de compañía',
    'log_out': '¿Estás seguro que deseas cerrar sesión?'
  },
  'fr': {
    'title': 'Paramètres',
    'personal': 'Information personnelle',
    'update_error': 'Erreur lors de la mise à jour du profil',
    'name': 'Nom',
    'content': "Paramètres du profil de l'entreprise",
    'log_out': 'Vous êtes sûr de vouloir vous déconnecter?'
  }
}

export default settings