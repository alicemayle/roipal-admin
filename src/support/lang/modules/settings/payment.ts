const payment = {
  'en': {
    'title': 'Update payment method',
    'error_update': 'Error updating card',
    'error_payment': 'Error getting payment method information',
    'required': 'It is required',
    'name': 'Name',
    'info_number': 'The card number or CVV is invalid',
    'invalid_exp': 'The expiration date is invalid',
    'exp': 'Expiration date',
    'invalid_ccv': 'The Code field is invalid',
  },
  'es': {
    'title': 'Actualizar método de pago',
    'error_update': 'Error al actualizar la tarjeta',
    'error_payment': 'Error al obtener la información del método de pago',
    'required': 'Es requerido',
    'name': 'Nombre',
    'info_number': 'El numero de tarjeta o CVV es invalido',
    'invalid_exp': 'La fecha de expiración no es válida',
    'exp': 'Fecha de expiración',
    'invalid_ccv': 'El código CVV no es válido',
  },
  'fr': {
    'title': 'Mettre à jour le mode de paiement',
    'error_update': 'Erreur lors de la mise à jour de la carte',
    'error_payment': "Erreur lors de l'obtention des informations sur le mode de paiement",
    'required': 'Est requis',
    'name': 'Nom',
    'info_number': 'Le numéro de carte ou ccv est invalide.',
    'invalid_exp': "La date d'expiration est invalide",
    'exp': "Date d'expiration",
    'invalid_ccv': "La case Code n'est pas valide",
  }
}

export default payment