const disable = {
  'en': {
    'disable_button': 'Disable',
    'enable_button': 'Enable',
    'enable_message': 'Reactivate your account to continue enjoying the services and benefits of ROIPAL',
    'message_one': 'Disabling the account will cancel all missions that are on hold. We are not responsible for payments made for such missions',
    'message_two': 'You will then be able to re-enable your account',
    'title': 'Disable / Enable account'
  },
  'es': {
    'disable_button': 'Deshabilitar',
    'enable_button': 'Habilitar',
    'enable_message': 'Reactiva tu cuenta para continuar disfrutando de los servicios y beneficios de ROIPAL',
    'message_one': 'Al deshabilitar la cuenta se cancelarán todas las misiones que se encuentren en espera. No nos hacemos responsables por los pagos efectuados correspondientes a dichas misiones',
    'message_two': 'Posteriormente podrás habilitar nuevamente tu cuenta',
    'title': 'Deshabilitar / Habilitar cuenta'
  },
  'fr': {
    'disable_button': 'Désactiver',
    'enable_button': 'Activer',
    'enable_message': 'Réactivez votre compte pour continuer à bénéficier des services et avantages de ROIPAL',
    'message_one': 'La désactivation du compte annule toutes les missions en attente. Nous ne sommes pas responsables des paiements effectués pour de telles missions',
    'message_two': 'Vous pourrez alors réactiver votre compte',
    'title': 'Désactiver / Activer le compte'
  }
}

export default disable