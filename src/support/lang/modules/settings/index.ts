import buttons from './buttons';
import settings from './settings';
import language from './language';
import disable from './disableAccount';
import password from './password';
import payment from './payment'

const setting = {
  'es.setting': {
    'settings': settings.es,
    'button': buttons.es,
    'language': language.es,
    'disable_account': disable.es,
    'password': password.es,
    'payment': payment.es
  },
  'en.setting': {
    'settings': settings.en,
    'button': buttons.en,
    'language': language.en,
    'disable_account': disable.en,
    'password': password.en,
    'payment': payment.en
  },
  'fr.setting': {
    'settings': settings.fr,
    'button': buttons.fr,
    'language': language.fr,
    'disable_account': disable.fr,
    'password': password.fr,
    'payment': payment.fr
  }
}

export default setting;