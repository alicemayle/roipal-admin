const password = {
  'en': {
    'update_error': 'Error updating password',
    'current_pass': 'Enter current password',
    'curent_pass_small': 'Current password',
    'new_pass_small': 'New Password',
    'new_pass': 'Enter new Password',
    'confirm_pass': 'Confirm Password',
    'info_pass': 'Enter a valid password, it must have the following, characteristics:',
    'info_pass2':'*Minimum 8 characters',
    'info_pass3':'*At least one uppercase letter',
    'info_pass4':'*At least one lowercase letter',
    'info_pass5':'*At least one digit',
    'info_pass6': 'Passwords must match',
    'title': 'Change Password'
  },
  'es': {
    'update_error': 'Error al actualizar contraseña',
    'current_pass': 'Ingrese contraseña actual',
    'curent_pass_small': 'Contraseña actual',
    'new_pass_small': 'Nueva Contraseña',
    'new_pass': 'Ingrese nueva Contraseña',
    'confirm_pass': 'Confirmar contraseña',
    'info_pass': 'Ingresar una contraseña válida, debe tener las siguientes características:',
    'info_pass2':'*Mínimo 8 caracteres',
    'info_pass3':'*Al menos una letra mayúscula',
    'info_pass4':'*Al menos una letra minúscula',
    'info_pass5':'*Al menos un dígito',
    'info_pass6': 'Las contraseñas deben coincidir',
    'title': 'Cambiar Contraseña'
  },
  'fr': {
    'update_error': 'Erreur lors de la mise à jour du mot de passe',
    'current_pass': 'Entrez le mot de passe actuel',
    'curent_pass_small': 'Mot de passe actuel',
    'new_pass_small': 'Nouveau mot de passe',
    'new_pass': 'Entrez un nouveau mot de passe',
    'confirm_pass': 'Confirmer mot de passe',
    'info_pass': 'Entrez un mot de passe valide, il doit avoir les caractéristiques suivantes:',
    'info_pass2':'*Au moins 8 caractères',
    'info_pass3':'*Au moins une lettre majuscule',
    'info_pass4':'*Au moins une lettre minuscule',
    'info_pass5':'*Au moins un chiffre',
    'info_pass6': 'Les mots de passe doivent correspondre',
    'title': 'Changer le mot de passe'
  }
}

export default password