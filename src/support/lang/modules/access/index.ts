import user from './user';
import role from './role';
import button from './button';

const access = {
  'en.access': {
    'title': 'Access control',
    user: user.en,
    role: role.en,
    button: button.en
  },
  'es.access': {
    'title': 'Control de acceso',
    user: user.es,
    role: role.es,
    button: button.es
  },
  'fr.access': {
    'title': "Contrôle d'accès",
    user: user.fr,
    role: role.fr,
    button: button.fr
  }
}

export default access;