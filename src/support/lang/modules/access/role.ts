const role = {
    'en': {
      'list':'See list of roles',
      'upsert': 'Create/edit roles',
      'title': 'Roles',
      'error_group': 'Error loading groups',
      'error_permissions': 'Error loading permissions',
      'select_group': 'Select a group',
      'date': 'Creation date',
      'name': 'Name',
      'group': 'Group',
      'permissions': 'Permissions',
      'edit': 'Edit',
      'select_permission': 'Permission Selection',
      'name_rol': 'Role name',
      'select_rol': 'Enter the name of the role',
      'new_role': 'New role',
      'list_permissions': 'List of permissions assigned to the role',
      'rol_signin': 'Select a role',
      'success': 'Edited role'
    },
    'es': {
      'list':'Ver listado de roles',
      'upsert': 'Crear/editar roles',
      'title': 'Roles',
      'error_group': 'Error al cargar lo grupos',
      'error_permissions': 'Error al cargar los permisos',
      'select_group': 'Selecciona un grupo',
      'date': 'Fecha de creación',
      'name': 'Nombre',
      'group': 'Grupo',
      'permissions': 'Permisos',
      'edit': 'Editar',
      'select_permission': 'Selección de permisos',
      'name_rol': 'Nombre del rol',
      'select_rol': 'Ingrese el nombre del rol',
      'new_role': 'Nuevo Rol',
      'list_permissions': 'Lista de permisos asignados al rol ',
      'rol_signin': 'Seleccione un rol',
      'success': 'Rol editado'
    },
    'fr': {
      'list':'Voir la liste des rôles',
      'upsert': 'Création/modification des rôles',
      'title': 'Rôles',
      'error_group': 'Erreur lors du chargement des groupes',
      'error_permissions': 'Erreur al cargar los permisos',
      'select_group': 'Sélectionner un groupe',
      'date': 'Date de creation',
      'name': 'Nom',
      'group': 'Groupe',
      'permissions': 'Les permissions',
      'edit': 'Éditer',
      'select_permission': 'Sélection de permission',
      'name_rol': 'Nom du rôle',
      'select_rol': 'Entrez le nom du rôle',
      'new_role': 'Nouveau rôle',
      'list_permissions': 'Liste des autorisations attribuées au rôle',
      'rol_signin': 'Sélectionnez un rôle',
      'success': 'Rôle édité'
    }
  }

  export default role