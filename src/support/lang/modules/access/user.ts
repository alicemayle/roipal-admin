const user = {
    'en': {
      'list':'See list of users',
      'upsert': 'Create/edit users',
      'title': 'Users',
      'date': 'Date of creation',
      'email': 'E-mail',
      'name': 'Name',
      'roles': 'Roles',
      'permissions': 'Permissions',
      'validations': {
        'name': 'Must enter a name',
        'email': 'Must enter an e-mail address'
      },
      'success_update': 'User created',
      'success_create': 'Edited user'
    },
    'es': {
      'list':'Ver listado de usuarios',
      'upsert': 'Crear/editar usuarios',
      'title': 'Usuarios',
      'date': 'Fecha de creación',
      'email': 'Correo electrónico',
      'name': 'Nombre',
      'roles': 'Roles',
      'permissions': 'Permisos',
      'validations': {
        'name': 'Debe introducir un nombre',
        'email': 'Debe introducir un correo electrónico',
      },
      'success_update': 'Usuario creado',
      'success_create': 'Usuario editado'
    },
    'fr': {
      'list':'Voir la liste des utilisateurs',
      'upsert': 'Créer/modifier des utilisateurs',
      'title': 'Utilisateurs',
      'date': 'Date de création',
      'email': 'Adresse électronique',
      'name': 'Prénom',
      'roles': 'Rôles',
      'permissions': 'Permis',
      'validations': {
        'name': 'Vous devez entrer un nom',
        'email': 'Vous devez entrer une adresse e-mail'
      },
      'success_update': 'Utilisateur créé',
      'success_create': 'Utilisateur édité'
    }
  }
  
  export default user