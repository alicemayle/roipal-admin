import language from './language'
import validations from './validations'

const translations = {
  'es.translations': {
    ...language.es,
    'validations': validations.es
  },
  'en.translations': {
    ...language.en,
    'validations': validations.en
  },
  'fr.translations': {
    ...language.fr,
    'validations': validations.fr
  }
}

export default translations;