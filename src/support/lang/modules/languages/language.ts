const language = {
  'en': {
    'title':'Translations',
    'content':'View and edit translations of different applications',
    'language': 'Language',
    'group': 'Group',
    'translate': 'Translate',
    'select': 'Select app',
    'edit_translation': 'Update translation',
    'create_translation': 'Create a translation'
  },
  'es': {
    'title':'Traducciones',
    'content':'Ver y editar traducciones de las diferentes aplicaciones',
    'language':'Idioma',
    'group': 'Grupo',
    'translate': 'Traducción',
    'select': 'Seleccione una app',
    'edit_translation': 'Editar traducción',
    'create_translation': 'Crear una traducción'
  },
  'fr': {
    'title':'Traductions',
    'content':'Visualiser et éditer les traductions de différentes applications',
    'language': 'Langage',
    'group': 'Groupe',
    'translate': 'Traduire',
    'select': "Sélectionnez l'application",
    'edit_translation': 'Mise à jour de la traduction',
    'create_translation': 'Create a translation'
  }
}

export default language