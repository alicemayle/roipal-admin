const validations = {
  'en': {
    'translate': 'Must enter the translation',
    'item': 'Item of translation',
    'group': 'Must enter the group',
    'namespace': 'Must enter the namespace',
    'language': 'Must enter the language'
  },
  'es': {
    'translate': 'Debe introducir la traducción',
    'item': 'Indique item de la traducción',
    'group': 'Debe ingresar el grupo',
    'namespace': 'Debe introducir la app correspondiente',
    'language': 'Debe introducir el idioma'
  },
  'fr': {
    'translate': 'Vous devez saisir la traduction',
    'item': 'Item de la traduction',
    'group': 'Vous devez entrer dans le groupe',
    'namespace': "Vous devez saisir l'application correspondante",
    'language': 'Vous devez saisir la langue'
  }
}

export default validations