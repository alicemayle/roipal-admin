const typesSales = {
  'en': {
    'title': 'Types of sales',
    'name': 'Name',
    'zone_A': 'Tariff zone A - North',
    'zone_B': 'Tariff zone B - Centre',
    'zone_C': 'Tariff zone C - South',
    'bonus': 'Minimum Bonus',
    'actions': 'Actions',
    'validations': {
      'zone_A': 'Must enter the fare for zone A',
      'zone_B': 'Must enter the fare for zone B',
      'zone_C': 'Must enter the fare for zone C',
      'bonus': 'Must enter the minimum rate for bonuses',
    },
    'success': 'Type of sale updated'
  },
  'es': {
    'title': 'Tipos de ventas',
    'name': 'Nombre',
    'zone_A': 'Tarifa zona A - Norte',
    'zone_B': 'Tarifa zona B - Centro',
    'zone_C': 'Tarifa zona C - Sur',
    'bonus': 'Bono mínimo',
    'actions': 'Acciones',
    'validations': {
      'zone_A': 'Debe ingresar la tarifa para la zona A',
      'zone_B': 'Debe ingresar la tarifa para la zona B',
      'zone_C': 'Debe ingresar la tarifa para la zona C',
      'bonus': 'Debe ingresar la tarifa mínima para bonos',
    },
    'success': 'Tipo de venta actualizado'
  },
  'fr': {
    'title': 'Types de ventes',
    'name': 'Name',
    'zone_A': 'Zone tarifaire A - Nord',
    'zone_B': 'Zone tarifaire B - Centre',
    'zone_C': 'Zone tarifaire C - Sud',
    'bonus': 'Bonus minimum',
    'actions': 'Actions',
    'validations': {
      'zone_A': 'Vous devez saisir le tarif de la zone A',
      'zone_B': 'Vous devez saisir le tarif de la zone B',
      'zone_C': 'Vous devez saisir le tarif de la zone C',
      'bonus': 'Vous devez saisir le taux minimum pour les bonus',
    },
    'success': 'Type de vente mis à jour'
  }
}

export default typesSales