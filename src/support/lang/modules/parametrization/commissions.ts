const commissions = {
  'en': {
    'title': 'Commissions',
    'name': 'Name',
    'company': 'Business name',
    'value': 'Value',
    'type': 'Type',
    'actions': 'Actions',
    'percentage': 'Percentage',
    'landline': 'Landline',
    'commission': 'Commission',
    'other': 'Other',
    'validations': {
      'name': 'Must enter a name',
      'company': 'Must select a company',
      'value': 'Must enter a value',
      'type': 'Must select an option',
      'commission': 'Must select an option'
    },
    'success': 'Updated company commissions'
  },
  'es': {
    'title': 'Comisiones',
    'name': 'Nombre',
    'company': 'Razón social',
    'value': 'Valor',
    'type': 'Tipo',
    'actions': 'Acciones',
    'percentage': 'Porcentaje',
    'landline': 'Fijo',
    'commission': 'Comisión',
    'other': 'Otro',
    'validations': {
      'name': 'Debe ingresar un nombre',
      'company': 'Debe seleccionar una compañía',
      'value': 'Debe ingresar un valor',
      'type': 'Debe seleccionar una opción',
      'commission': 'Debe seleccionar una opción'
    },
    'success': 'Comisiones de compañía actualizadas'
  },
  'fr': {
    'title': 'Commissions',
    'name': 'Prénom',
    'comany': 'Raison sociale',
    'value': 'Mérite',
    'type': 'Taux',
    'actions': 'Actions',
    'percentage': 'Pourcentage',
    'landline': 'Fixe',
    'commission': 'Commission',
    'other': 'Autre',
    'validations': {
      'name': 'Vous devez entrer un nom',
      'company': 'Vous devez sélectionner une société',
      'value': 'Vous devez saisir une valeur',
      'type': 'Vous devez sélectionner une option',
      'commission': 'Vous devez sélectionner une option'
    },
    'success': 'Mise à jour des tarifs des entreprises'
  }
}

export default commissions