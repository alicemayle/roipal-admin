import buttons from './../../generic/buttons'

const button = {
  'es': {
    ...{ ...buttons.es }
  },
  'en': {
    ...{ ...buttons.en }
  },
  'fr': {
    ...{ ...buttons.fr }
  }
}

export default button;