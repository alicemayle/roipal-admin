const commercialNeeds = {
  'en': {
    'title':'Commercial needs',
    'name': 'Name',
    'min_time': 'Minimum time',
    'time_unit': 'Unit of time',
    'actions': 'Actions',
    'days': 'Days',
    'hours': 'Hours',
    'validations': {
      'min_time': 'Must enter the minimum time',
      'time_unit': 'Must select the time unit',
      'name': 'Must enter the name'
    },
    'success': 'Updated commercial need'
  },
  'es': {
    'title':'Necesidades comerciales',
    'name': 'Nombre',
    'min_time': 'Tiempo mínimo',
    'time_unit': 'Unidad de tiempo',
    'actions': 'Acciones',
    'days': 'Días',
    'hours': 'Horas',
    'validations': {
      'min_time': 'Debe ingresar el tiempo mínimo',
      'time_unit': 'Debe seleccionar la unidad de tiempo',
      'name': 'Debe ingresar un nombre'
    },
    'success': 'Necesidad comercial actualizada'
  },
  'fr': {
    'title':'Besoins commercial',
    'name': 'Prénom',
    'min_time': 'Temps minimum',
    'time_unit': 'Unité de temps',
    'actions': 'Actions',
    'days': 'Jours',
    'hours': 'Heures',
    'validations': {
      'min_time': 'Vous devez entrer le temps minimum',
      'time_unit': "Vous devez sélectionner l'unité de temps",
      'name': 'Vous devez entrer le prénom'
    },
    'success': 'Besoins actuels des entreprises'
  }
}

export default commercialNeeds