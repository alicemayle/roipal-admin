import global from './global';
import commissions from './commissions';
import typesSales from './typesSales';
import commercialNeeds from './commercialNeeds';
import buttons from './buttons';

const parametrization = {
  'es.parametrization': {
    'title': 'Parametrización',
    'content': 'Configuraciones',
    'global': global.es,
    'commissions': commissions.es,
    'types_sales': typesSales.es,
    'commercial_needs': commercialNeeds.es,
    'button': buttons.es
  },
  'en.parametrization': {
    'title': 'Parametrization',
    'content': 'Settings',
    'global': global.en,
    'commissions': commissions.en,
    'types_sales': typesSales.en,
    'commercial_needs': commercialNeeds.en,
    'button': buttons.en
  },
  'fr.parametrization': {
    'title': 'Paramétrage',
    'content': 'Paramètres de modification',
    'global': global.fr,
    'commissions': commissions.fr,
    'types_sales': typesSales.fr,
    'commercial_needs': commercialNeeds.fr,
    'button': buttons.fr
  }
}

export default parametrization;