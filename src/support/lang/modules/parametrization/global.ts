const global = {
  'en': {
    'title':'Global',
    'concept': 'Concept',
    'value': 'Value',
    'unit': 'Unity',
    'actions': 'Actions',
    'validations': {
      'key': 'Must enter the concept',
      'value': 'Must enter the value',
      'type': 'must enter the unit'
    },
    'success': 'Updated global configuration'
  },
  'es': {
    'title':'Globales',
    'concept': 'Concepto',
    'value': 'Valor',
    'unit': 'Unidad',
    'actions': 'Acciones',
    'validations': {
      'key': 'Debe ingresar el concepto',
      'value': 'Debe ingresar el valor',
      'type': 'Debe ingresar la unidad'
    },
    'success': 'Configuración global actualizada'
  },
  'fr': {
    'title':'Globale',
    'concept': 'Idée',
    'value': 'Mérite',
    'unit': 'Unité',
    'actions': 'Actions',
    'validations': {
      'key': 'Vous devez saisir le concept',
      'value': 'Vous devez saisir la valeur',
      'type': "Vous devez entrer l'unité"
    },
    'success': 'Mise à jour de la configuration globale'
  }
}

export default global