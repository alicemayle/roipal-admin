import map from './map'

const maps = {
  'es.map': {
    ...map.es,
  },
  'en.map': {
    ...map.en,
  },
  'fr.map': {
    ...map.fr,
  }
}

export default maps;