const map = {
  'en': {
    'search': 'Search Places...',
    'name_location': 'Location name',
    'location': 'Location'
  },
  'es': {
    'search': 'Buscar lugares...',
    'name_location': 'Nombre de ubicación',
    'location': 'Ubicación'
  },
  'fr': {
    'search': 'Rechercher des lieux...',
    'name_location': 'Nom du lieu',
    'location': 'Emplacement'
  }
}

export default map;