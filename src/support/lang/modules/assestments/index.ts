import assesment from './assesment'

const assesments = {
  'es.assesments': {
    ...assesment.es
  },
  'en.assesments': {
    ...assesment.en
  },
  'fr.assesments': {
    ...assesment.fr
  }
}

export default assesments;