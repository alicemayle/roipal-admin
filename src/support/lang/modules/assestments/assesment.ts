const assesment = {
  'en': {
    'title':'Assessments',
    'title_menu':'Assessments',
    'content':'View and edit ROIPAL evaluations',
    'upload': 'Click to Upload Assessments',
    'date': 'Creation date',
    'error_message': 'Indicate an answer',
    'language': 'Language',
    'edit': 'Edit',
    'edit_assessment': 'Edit assessment',
    'question': 'Question',
    'assessment': 'Assessment',
    'success_update': 'Updated assessment'
  },
  'es': {
    'title':'Evaluaciones',
    'title_menu':'Evaluaciones',
    'content':'Ver y editar evaluaciones ROIPAL',
    'upload': 'Clic para subir evaluaciones',
    'date': 'Fecha de creación',
    'error_message': 'Indique una respuesta',
    'language': 'Idioma',
    'edit': 'Editar',
    'edit_assessment': 'Editar evaluación',
    'question': 'Pregunta',
    'assessment': 'Evaluación',
    'success_update': 'Evaluación actualizada'
  },
  'fr': {
    'title':'Évaluations',
    'title_menu':'Tests',
    'content':'Afficher et modifier les évaluations ROIPAL',
    'upload': 'Cliquez pour télécharger les évaluations',
    'date': 'Date de creation',
    'error_message': 'Indiquez une réponse',
    'language': 'Langage',
    'edit': 'Éditer',
    'edit_assessment': "Éditer l'évaluation",
    'question': 'Question',
    'assessment': 'Évaluation',
    'success_update': 'Évaluation actualisée'
  },
}

export default assesment