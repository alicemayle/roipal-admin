import charge from './charges'
import button from './buttons'

const charges = {
  'es.charges': {
    'button': button.es,
    'charge': charge.es
  },
  'en.charges': {
    'button': button.en,
    'charge': charge.en
  },
  'fr.charges': {
    'button': button.fr,
    'charge': charge.fr
  }
}

export default charges;