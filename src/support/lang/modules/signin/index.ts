import buttons from './buttons';
import login from './signin';

const signin = {
  'en.signin': {
    'signin': login.en,
    'button': buttons.en
  },
  'es.signin': {
    'signin': login.es,
    'button': buttons.es
  },
  'fr.signin': {
    'signin': login.fr,
    'button': buttons.fr
  }
}

export default signin;