const signing = {
  'en': {
    'error_email': 'The email is not valid',
    'error_user': 'The user is invalid',
    'user': 'User',
    'error_pass': 'The password is not valid.',
    'password': 'Password',
    'signin': 'Sign In',
    'forgot_pass': 'Did you forget your password?',
    'account': 'You do not have an account?',
    'sign': ' sign up here'

  },
  'es': {
    'error_email': 'El correo electrónico no es válido',
    'error_user': 'El usuario no es válido',
    'user': 'Usuario',
    'error_pass': 'La contraseña no es valida',
    'password': 'Contraseña',
    'signin': 'Entrar',
    'forgot_pass': '¿Olvidaste tu contraseña?',
    'account': ' ¿No tienes cuenta?',
    'sign': ' Registrate aquí',

  },
  'fr': {
    'error_email': "L'email n'est pas valide",
    'error_user': "L'utilisateur n'est pas valide.",
    'user': 'Identifiant',
    'error_pass': "Le mot de passe n'est pas valide.",
    'password': 'Mot de passe',
    'signin': 'Se Connecter',
    'forgot_pass': 'Vous avez oublié votre mot de passe?',
    'account': "Vous n'avez pas de compte?",
    'sign': ' Inscrivez-vous ici',

  }
}

export default signing