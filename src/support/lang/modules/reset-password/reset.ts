const reset = {
  'en': {
    'error': 'Failed to send email',
    'recover': 'Recover password',
    'email': 'Email',
    'error_email': 'Enter your email',
    'reset_password': 'Enter the link we sent to your email to recover your password'
  },
  'es': {
    'error': 'Error al enviar el correo electrónico',
    'recover': 'Recuperar contraseña',
    'email': 'Correo electrónico',
    'error_email': 'Ingresa tu correo electrónico',
    'reset_password': 'Ingresa al enlace que enviaremos a tu email para recuperar tu contraseña'
  },
  'fr': {
    'error': "Erreur lors de l'envoi de l'e-mail",
    'recover': 'Récupérer le mot de passe',
    'email': 'Courrier électronique',
    'error_email': 'Entrez votre email',
    'reset_password': 'Entrez dans le lien que nous enverrons à votre courriel' +
    'pour récupérer votre mot de passe'
  }
}

export default reset