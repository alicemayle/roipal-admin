import buttons from './buttons';
import reset from './reset';

const resetPass = {
  'es.reset': {
    'reset': reset.es,
    'button': buttons.es,
  },
  'en.reset': {
    'reset': reset.en,
    'button': buttons.en,
  },
  'fr.reset': {
    'reset': reset.fr,
    'button': buttons.fr,
  }
}

export default resetPass;