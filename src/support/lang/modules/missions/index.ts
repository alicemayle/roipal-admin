import button from './buttons';
import mission from './mission';
import charges from './charges';
import edit from './edit';
import add from './add'
import score from './score';

const buses = {
  'es.buses': {
    'button': button.es,
    'mission': {
      ...mission.es,
      charges: charges.es,
      edit: edit.es,
      add: add.es
    },
    'score': score.es
  },
  'en.buses': {
    'button': button.en,
    'mission': {
      ...mission.en,
      charges: charges.en,
      edit: edit.en,
      add: add.en
    },
    'score': score.en
  },
  'fr.buses': {
    'button': button.fr,
    'mission': {
      ...mission.fr,
      charges: charges.fr,
      edit: edit.fr,
      add: add.fr
    },
    'score': score.fr
  }
}

export default buses;