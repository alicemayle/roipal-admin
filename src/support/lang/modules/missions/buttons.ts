import buttons from './../../generic/buttons'

const button = {
  'es': {
    ...{
      ...buttons.es,
      'start': 'Iniciar',
      'finish': 'Finalizar',
      'cancel': 'Cancelar'
    }
  },
  'en': {
    ...{ 
      ...buttons.en,
      'start': 'Start',
      'finish': 'Finish',
      'cancel': 'Cancel'
    }
  },
  'fr': {
    ...{ 
      ...buttons.fr,
      'start': 'Commencer',
      'finish': 'Finir',
      'cancel': 'Annuler'
    }
  }
}

export default button;