const charges = {
    'en': {
      'title': 'Charges',
      'create_date': 'Creation date',
      'executives': 'Service providers',
      'status': 'Status',
      'amount': 'Transaction Amount'
    },
    'es': {
      'title': 'Cargos',
      'create_date': 'Fecha de creación',
      'executives': 'Prestadores de servicio',
      'status': 'Status',
      'amount': 'Monto de transacción'
    },
    'fr': {
      'title': 'Charges',
      'create_date': 'Date de création',
      'executives': 'Fournisseurs de services',
      'status': 'Statut',
      'amount': 'Montant de la transaction'
    }
  }
  
  export default charges