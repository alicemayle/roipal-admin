const score = {
    'en': {
      'info_bono':
      'Do you want to add a bonus to the executive for his performance in the mission?',
      'yes': 'Yes',
      'no': 'No',
      'commissions': 'Commissions',
      'iva': 'VAT',
      'total': 'Total',
    },
    'es': {
      'info_bono':
      '¿Deseas agregar una bonificación al ejecutivo por su desempeño en la misión?',
      'yes': 'Si',
      'no': 'No',
      'commissions': 'Comisiones',
      'iva': 'IVA',
      'total': 'Total',
    },
    'fr': {
      'info_bono':
      "Voulez-vous ajouter un bonus à l'exécutif pour sa performance dans la mission?",
      'yes': 'Oui',
      'no': 'Non',
      'commissions': 'Les commissions',
      'iva': 'TVA',
      'total': 'Total',
    }
  }

  export default score