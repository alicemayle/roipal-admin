const mission = {
  'en': {
    'accepted': 'Invitations accepted',
    'activities': 'Activities',
    'amount': 'Total inversion',
    'cancel_mission': 'Are you sure you want to cancel the mission?',
    'cancel_question': "Are you sure you want to cancel the executive's mission?",
    'cancelled': 'Cancelled',
    'comment': 'Commentary',
    'commercial_need': 'Commercial Need',
    'content': 'See list of created missions',
    'country': 'Country',
    'create_date': 'Creation date',
    'currency': 'Currency',
    'date': 'Start of the mission',
    'description': 'Description',
    'email': 'Email',
    'error_activity': 'Error bringing executive activities',
    'error_score': 'Error qualifying executive',
    'estimationMessage1': 'A good mission of',
    'estimationMessage2': 'requires',
    'estimationMessage3': 'minimum',
    'estimationMessage4': 'The salary for',
    'estimationMessage5': 'varies according to the total number of',
    'estimationMessage6': 'required',
    'estimationMessage7': 'Average rate required for',
    'estimationMessage8': '+ VAT',
    'finish_question': 'Are you sure you want to finish the mission?',
    'finished': 'Finished',
    'inProcess': 'In process',
    'invi_sent': 'Invitations sent',
    'list_executives': 'List of executives',
    'location': 'Locations',
    'mision': 'Mission',
    'name': 'Name',
    'onHold': 'On hold',
    'optCountryMx': 'Mexico',
    'optCurrencyMx': 'MX',
    'profile': 'Profile',
    'rate_executive': 'Rate',
    'reject_button': 'Reject',
    'reject_executive': 'Executive Reject Error',
    'reject_question': 'Are you sure you want to reject the executive?',
    'requested': 'Executives requested',
    'score': 'Score',
    'see_activity': 'See Activities',
    'selectCountry': 'Select the country',
    'selectCurrency': 'Select the currency',
    'selectHours': 'Select hours',
    'select_date': 'Select date',
    'start': 'Mission start',
    'start_question': 'Are you sure you want to start the mission?',
    'status': 'Status',
    'status_canceled': 'Canceled',
    'status_completed': 'Completed',
    'status_created': 'Created',
    'status_started': 'Started',
    'subtotal': 'Subtotal:',
    'success_cancel_executive': 'Executive mission cancelled',
    'success_change_status': 'Updated mission status',
    'success_edit_locations': 'Updated mission locations',
    'success_reject_executive': 'Executive rejected',
    'success_score': 'Qualified executive',
    'title': 'Missions',
    'titleTab': 'Missions',
    'title_menu': 'Missions',
    'type_sale': 'Type of sale'
  },
  'es': {
    'accepted': 'Invitaciones aceptadas',
    'activities': 'Actividades',
    'amount': 'Inversión total',
    'cancel_mission': '¿Estas seguro que quieres cancelar la misión?',
    'cancel_question': '¿Estas seguro que quieres cancelar la misión del ejecutivo?',
    'cancelled': 'Cancelada',
    'comment': 'Comentario',
    'commercial_need': 'Necesidad comercial',
    'content': 'Ver listado de misiones creadas',
    'country': 'País',
    'create_date': 'Fecha de creación',
    'currency': 'Moneda',
    'date': 'Inicio de la misión',
    'description': 'Descripción',
    'email': 'Correo electrónico',
    'error_activity': 'Error al traer las actividades del ejecutivo',
    'error_score': 'Error al calificar ejecutivo',
    'estimationMessage1': 'Una buena misión de',
    'estimationMessage2': 'requiere de',
    'estimationMessage3': 'mínimo',
    'estimationMessage4': 'El sueldo por',
    'estimationMessage5': 'varía en función del número total de',
    'estimationMessage6': 'requeridas',
    'estimationMessage7': 'Tarifa promedio requerida por',
    'estimationMessage8': '+ IVA',
    'finish_question': '¿Estas seguro que quieres finalizar la misión?',
    'finished': 'Finalizada',
    'inProcess': 'En Proceso',
    'invi_sent': 'Invitaciones enviadas',
    'list_executives': 'Listado de ejecutivos',
    'location': 'Ubicaciones',
    'mision': 'Misión',
    'name': 'Nombre',
    'onHold': 'En Espera',
    'optCountryMx': 'México',
    'optCurrencyMx': 'MX',
    'profile': 'Perfil',
    'rate_executive': 'Calificar',
    'reject_button': 'Rechazar',
    'reject_executive': 'Error al rechazar ejecutivo',
    'reject_question': '¿Estas seguro que quieres rechazar al ejecutivo?',
    'requested': 'Ejecutivos solicitados',
    'score': 'Calificación',
    'see_activity': 'Ver Actividades',
    'selectCountry': 'Seleccione el país',
    'selectCurrency': 'Seleccione la moneda',
    'selectHours': 'Seleccionar horas',
    'select_date': 'Seleccionar fecha',
    'start': 'Inicio de la misión',
    'start_question': '¿Estas seguro que quieres iniciar la misión?',
    'status': 'Estatus',
    'status_canceled': 'Cancelada',
    'status_completed': 'Completada',
    'status_created': 'Creada',
    'status_started': 'Iniciada',
    'subtotal': 'Subtotal:',
    'success_cancel_executive': 'Misión de ejecutivo cancelada',
    'success_change_status': 'Estatus de misión actualizado',
    'success_edit_locations': 'Ubicaciones de misión actualizadas',
    'success_reject_executive': 'Ejecutivo rechazado',
    'success_score': 'Ejecutivo calificado',
    'title': 'Misiones',
    'titleTab': 'Misiones',
    'title_menu': 'Misiones',
    'type_sale': 'Tipo de venta'
  },
  'fr': {
    "accepted": "Invitations acceptées",
    "activities": "Les activités",
    "amount": "Investissement total",
    'cancel_mission': 'Voulez-vous vraiment annuler la mission?',
    'cancel_question': "Voulez-vous vraiment annuler la mission de l'exécutif?",
    "cancelled": "Annulé",
    "comment": "Commentaire",
    'commercial_need': 'Besoin commercial',
    "content": "Voir la liste des missions créées",
    "country": "Pays",
    "create_date": "Date de creation",
    "currency": "Monnaie",
    "date": "Début de la vie",
    "description": "La description",
    "email": "Courrier électronique",
    "error_activity": "Erreur lors de l'exécution des activités exécutives",
    "error_score": "Erreur lors de la qualification du cadre",
    "estimationMessage1": "Une bonne mission de",
    "estimationMessage2": "nécessite",
    "estimationMessage3": "minimum",
    "estimationMessage4": "Le salaire pour les",
    "estimationMessage5": "varie en fonction du nombre total d'",
    "estimationMessage6": "requis",
    "estimationMessage7": "Taux moyen requis pour",
    "estimationMessage8": "+ TVA",
    'finish_question': 'Êtes-vous sûr de vouloir terminer la mission?',
    "finished": "Fini",
    "inProcess": "En procès",
    "invi_sent": "Invitations envoyées",
    "list_executives": "Liste des l'exécutif",
    "location": "Emplacements",
    "mision": "Mission",
    "name": "Nom",
    "onHold": "En attente",
    "optCountryMx": "Mexique",
    "optCurrencyMx": "MX",
    "profile": "Profil",
    "rate_executive": "Tarif",
    "reject_button": "Rejeter",
    "reject_executive": "Erreur de rejet de l'exécutif",
    'reject_question': "Voulez-vous vraiment rejeter l'exécutif?",
    "requested": "Providers demandés",
    "score": "Évaluation",
    "see_activity": "Voir les activités",
    "selectCountry": "Sélectionnez le pays",
    "selectCurrency": "Sélectionnez la devise",
    "selectHours": "Sélectionnez les heures",
    "select_date": "Sélectionner la date",
    "start": "Début de mission",
    'start_question': 'Êtes-vous sûr de vouloir commencer la mission?',
    "status": "Statut",
    'status_canceled': 'Annulé',
    'status_completed': 'Terminé',
    'status_created': 'Créé',
    'status_started': 'Démarré',
    'subtotal': 'Sous-total:',
    'success_cancel_executive': 'Mission exécutive annulée',
    'success_change_status': 'Mise à jour du statut de la mission',
    'success_edit_locations': 'Lieux de mission mis à jour',
    'success_reject_executive': "L'exécutif a rejeté",
    'success_score': "L'exécutif qualifié",
    "title": "Missions",
    "titleTab": "Missions",
    "title_menu": "Missions",
    'type_sale': 'Type de vente'
  }
}

export default mission;


