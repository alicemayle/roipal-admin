const edit = {
    'en': {
      'title': 'Edit locations',
      'subtotal': 'Subtotal',
      'tax': 'Tax',
      'total': 'Total',
      'commissions': 'Commission',
      'locations_list': 'Locations',
      'location_selected': 'Selected locations',
      'price_locations': 'Total location price:'
    },
    'es': {
      'title': 'Editar ubicaciones',
      'subtotal': 'Subtotal',
      'tax': 'Impuesto',
      'total': 'Total',
      'commissions': 'Comisiones',
      'locations_list': 'Ubicaciones',
      'location_selected': 'Ubicaciones seleccionadas',
      'price_locations': 'Precio total de la ubicación:'
    },
    'fr': {
      'title': 'Modification des emplacements',
      'subtotal': 'Sous-total',
      'tax': 'Taxe',
      'total': 'Total',
      'commissions': 'Commission',
      'locations_list': 'Lieux',
      'location_selected': 'Lieux choisis',
      'price_locations': "Prix total de l'emplacement:"
    }
  }

  export default edit