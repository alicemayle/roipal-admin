import activation from './activation';
import profile from './profile';
import contact from './contact';
import payment from './payment';
import registration from './register';
import button from './button';

const register = {
  'es.register': {
    ...registration.es,
    'activation': activation.es,
    'profile': profile.es,
    'contact': contact.es,
    'payment': payment.es,
    'button': button.es
  },
  'en.register': {
    ...registration.en,
    'activation': activation.en,
    'profile': profile.en,
    'contact': contact.en,
    'payment': payment.en,
    'button': button.en
  },
  'fr.register': {
    ...registration.fr,
    'activation': activation.fr,
    'profile': profile.fr,
    'contact': contact.fr,
    'payment': payment.fr,
    'button': button.fr
  }
}

export default register;