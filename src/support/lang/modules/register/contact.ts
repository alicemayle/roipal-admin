const contact = {
    'en': {
      'title': 'Contact information',
      'error_register': 'Error registering contact',
      'full_name': 'Full Name',
      'add_name': 'You must enter a name',
      'name': 'Name',
      'job_title': 'Job title',
      'error_job': 'You must enter a charge',
      'phone': 'Phone',
      'error_phone': 'You must enter a phone'
    },
    'es': {
      'title': 'Datos de contacto',
      'error_register': 'Error al registrar el contacto',
      'full_name': 'Nombre Completo',
      'add_name': 'Debe ingresar un nombre',
      'name': 'Nombre',
      'job_title': 'Cargo',
      'error_job': 'Debe ingresar un cargo',
      'phone': 'Telefono',
      'error_phone': 'Debe ingresar un teléfono'
    },
    'fr': {
      'title': 'Coordonnées de contact',
      'error_register': "Erreur lors de l'enregistrement du contact",
      'full_name': 'Nom complet',
      'add_name': 'Vous devez saisir un nom',
      'name': 'Nom',
      'job_title': 'Profession',
      'error_job': 'Vous devez saisir des frais',
      'phone': 'Téléphone',
      'error_phone': 'Vous devez entrer un téléphone'
    }
  }

  export default contact