const profile = {
  'en': {
    'title': 'Complete profile',
    'description': 'Description',
    'website': 'Website',
    'slogan': 'Slogan',
    'vertical': 'Industry sector',
    'address': 'Address',
    'validations': {
      'description': 'Please enter a description',
      'vertical': 'Please select industry sector',
      'address': 'Please enter an address'
    }
  },
  'es': {
    'title': 'Completar perfil',
    'description': 'Descripción',
    'website': 'Sitio web',
    'slogan': 'Slogan',
    'vertical': 'Giro',
    'address': 'Dirección',
    'validations': {
      'description': 'Por favor introduzca una descripción',
      'vertical': 'Por favor seleccione un giro',
      'address': 'Por favor introduzca una dirección'
    }
  },
  'fr': {
    'title': 'Profil complet',
    'description': 'Description',
    'website': 'Site Web',
    'slogan': 'Slogan',
    'vertical': "Secteur d'activité",
    'address': 'Adresse',
    'validations': {
      'description': 'Veuillez entrer une description',
      'vertical': "Veuillez choisir un secteur d'activité",
      'address': 'Veuillez entrer une adresse'
    }
  }
}

export default profile