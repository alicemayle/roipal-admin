const activation = {
    'en': {
      'title': 'Activate account',
      'verify': 'Enter the link we sent to your email to verify your account',
      'resend': 'If you have received an email with the confirmation link, click ',
      'here': 'here',
      'verified_account': 'If you have already verified your account click ',
      'check': 'to check'
    },
    'es': {
      'title': 'Activar cuenta',
      'verify': 'Ingresa al enlace que hemos enviado a tu email para verificar tu cuenta',
      'resend': 'Si no has recibido un correo electrónico con el enlace de confirmación, haz clic ',
      'here': 'aquí',
      'verified_account': 'Si ya has verificado tu cuenta haz clic ',
      'check': 'para comprobarlo'
    },
    'fr': {
      'title': 'Activer le compte',
      'verify': 'Entrez le lien que nous avons envoyé à votre courriel pour vérifier votre compte.',
      'resend': "Si vous n'avez pas reçu d'e-mail avec le lien de confirmation, cliquez sur ",
      'here': 'ici',
      'verified_account': 'Si vous avez déjà vérifié votre compte, cliquez ',
      'check': 'pour le vérifier'
    }
  }
  
  export default activation