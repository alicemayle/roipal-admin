const registration = {
  'en': {
    'title': 'Register',
    'legal_name': 'Company name',
    'trade_name': 'Trade name',
    'name': 'Name',
    'email': 'E-mail',
    'password': 'Password',
    'confirm_password': 'Confirm password',
    'validations': {
      'legal_name': 'Please enter your company name',
      'trade_name': 'Please enter your trade name',
      'name': 'Please enter your name',
      'rfc': 'Please enter your RFC',
      'email': 'Please enter your email address',
      'password': 'Please enter a password'
    },
    'info_pass': 'Enter a valid password, it must have the following, characteristics:',
    'info_pass2':'*Minimum 8 characters',
    'info_pass3':'*At least one uppercase letter',
    'info_pass4':'*At least one lowercase letter',
    'info_pass5':'*At least one digit',
    'have_account': 'Do you already have an account?',
    'login': 'Log in',
    'confirm_account': 'Confirm account'
  },
  'es': {
    'title': 'Registro',
    'legal_name': 'Razón social',
    'trade_name': 'Nombre comercial',
    'name': 'Nombre',
    'email': 'Correo electrónico',
    'password': 'Contraseña',
    'confirm_password': 'Confirmar contraseña',
    'validations': {
      'legal_name': 'Por favor introduzca su Razón social',
      'trade_name': 'Por favor introduzca su Nombre comercial',
      'name': 'Por favor introduzca su Nombre',
      'rfc': 'Por favor introduzca su RFC',
      'email': 'Por favor introduzca su correo electrónico',
      'password': 'Por favor introduzca una contraseña'
    },
    'info_pass': 'Ingresar una contraseña válida, debe tener las siguientes características:',
    'info_pass2':'*Mínimo 8 caracteres',
    'info_pass3':'*Al menos una letra mayúscula',
    'info_pass4':'*Al menos una letra minúscula',
    'info_pass5':'*Al menos un dígito',
    'have_account': '¿Ya tienes una cuenta?',
    'login': 'Iniciar sesión',
    'confirm_account': 'Confirmar cuenta'
  },
  'fr': {
    'title': "S'inscrire",
    'legal_name': "Nom de l'entreprise",
    'trade_name': 'Nom commercial',
    'name': 'Nom',
    'email': 'Courriel',
    'password': 'Mot de passe',
    'confirm_password': 'Confirmer le mot de passe',
    'validations': {
      'legal_name': 'Veuillez entrer le nom de votre société',
      'trade_name': 'Veuillez entrer votre nom commercial',
      'name': 'Veuillez entrer votre nom',
      'rfc': 'Veuillez entrer votre RFC',
      'email': 'Veuillez entrer votre adresse email',
      'password': 'Veuillez entrer un mot de passe'
    },
    'info_pass': 'Entrez un mot de passe valide, il doit avoir les caractéristiques suivantes:',
    'info_pass2':'*Au moins 8 caractères',
    'info_pass3':'*Au moins une lettre majuscule',
    'info_pass4':'*Au moins une lettre minuscule',
    'info_pass5':'*Au moins un chiffre',
    'have_account': 'Vous avez déjà un compte?',
    'login': 'Commencer la session',
    'confirm_account': 'Confirmer le compte'
  }
}

export default registration