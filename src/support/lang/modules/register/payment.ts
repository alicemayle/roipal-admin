const payment = {
  'en': {
    'title': 'Payment information',
    'register_error': 'Error registering card',
    'required': 'It is required',
    'name': 'Name',
    'info_number': 'The card number or CVV is invalid',
    'invalid_exp': 'The expiration date is invalid',
    'exp': 'Expiration date',
    'invalid_ccv': 'The Code field is invalid',
  },
  'es': {
    'title': 'Datos de pago',
    'register_error': 'Error al registrar la tarjeta',
    'required': 'Es requerido',
    'name': 'Nombre',
    'info_number': 'El numero de tarjeta o CVV es invalido',
    'invalid_exp': 'La fecha de expiración no es válida',
    'exp': 'Fecha de expiración',
    'invalid_ccv': 'El código CVV no es válido',
  },
  'fr': {
    'title': 'Information de paiement',
    'register_error': "Erreur lors de l'enregistrement de la carte",
    'required': 'Est requis',
    'name': 'Nom',
    'info_number': 'Le numéro de carte ou ccv est invalide.',
    'invalid_exp': "La date d'expiration est invalide",
    'exp': "Date d'expiration",
    'invalid_ccv': "La case Code n'est pas valide",
  }
}

export default payment