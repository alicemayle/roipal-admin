const document = {
  'en': {
    "approve_document": "Approve Document",
    "contact": "Contact:",
    "empty_docs": "The Provider has not registered any documents",
    "info": "The Document has already been reviewed",
    "info_motive": "You must select a reason for rejection",
    "info_rejected": "The Document was reviewed please wait for the update.",
    "missing_review": "Missing Document Review",
    "ready": "Verified User",
    "reason_rejection": "Reason for rejection",
    "reject_document": "Reject Document",
    "reviewed": "Document Reviewed by:",
    "submit_review": "Submit Review",
    "verified_user": "Verified User"
  },
  'es': {
    "approve_document": "Aprobar Documento",
    "contact": "Contacto:",
    "empty_docs": "El Prestador no ha registrado ningún documento",
    "info": "El Documento ya fue revisado",
    "info_motive": "Debe seleccionar un motivo de rechazo",
    "info_rejected": "El Documento fue revisado porfavor espera por la actualización.",
    "missing_review": "Falta Revisión de Documentos",
    "ready": "Usuario Verificado",
    "reason_rejection": "Motivo de Rechazo",
    "reject_document": "Rechazar Documento",
    "reviewed": "Documento Revisado por:",
    "submit_review": "Enviar Revisión",
    "verified_user": "Usuario Verificado"
  },
  'fr': {
    "approve_document": "Approuver le document",
    "contact": "Contact:",
    "empty_docs": "Le fournisseur n'a enregistré aucun document",
    "info": "Le document a déjà été examiné",
    "info_motive": "Vous devez sélectionner un motif de rejet",
    "info_rejected": "Le document a été examiné. Veuillez attendre la mise à jour.",
    "missing_review": "Examen de document manquant",
    "ready": "Utilisateur vérifié",
    "reason_rejection": "Raison du rejet",
    "reject_document": "Rejeter le document",
    "reviewed": "Document révisé par:",
    "submit_review": "Soumettre un avis",
    "verified_user": "Utilisateur vérifié"
  }
}

export default document;