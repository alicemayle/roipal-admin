import document from './document'
import button from './buttons'

const documents = {
  'es.documents': {
    'button': button.es,
    'document': document.es
  },
  'en.documents': {
    'button': button.en,
    'document': document.en
  },
  'fr.documents': {
    'button': button.fr,
    'document': document.fr
  }
}

export default documents;