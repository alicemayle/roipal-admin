import payment from './payment'
import button from './buttons'

const payments = {
  'es.payments': {
    'button': button.es,
    'payment': payment.es
  },
  'en.payments': {
    'button': button.en,
    'payment': payment.en
  },
  'fr.payments': {
    'button': button.fr,
    'payment': payment.fr
  }
}

export default payments;