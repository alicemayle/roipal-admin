class NumberFormat {
    public formatByCountry(num: number, country?: string) {
      let format = 'en-US';

      if (country === 'FR') {
        format = 'de-DE' 
      }

      const _num = new Intl.NumberFormat(format).format(num);
      
      return _num
    }
  }
  
  export default new NumberFormat();
  