// import messages from './lang/messages.json';
import dictionary from './lang/index';
import LangJS from 'lang.js';

const Lang = new LangJS({});
Lang.setMessages(dictionary);
Lang.setLocale('es');

Lang.setFallback('es');

export default Lang;