const statusCharge = {
    CHARGE_REJECTED: 'rejected',
    CHARGE_APPROVED: 'approved',
    CHARGE_EXPIRATION: 'expiration',
  }
  
  export {
    statusCharge
  }