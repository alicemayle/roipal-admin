const statusPayment = {
  MISSION_PENDING: 0,
  MISSION_IN_PROCESS: 1,
  MISSION_APPROVED: 2
}

export {
  statusPayment
}