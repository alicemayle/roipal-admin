class Event {
    public listeners:{};
    constructor() {
      this.listeners = {}
    }
  
    public listen(event:any, callback:any) {
      this.listeners[event] = this.listeners[event] || [];
  
      this.listeners[event].push(callback);
    }
  
    public subscribe(event:any, callback:any) {
      this.listen(event, callback);
    }
  
    public remove(event:any, callback:any) {
      if (this.listeners[event]) {
        for (const i in this.listeners[event]) {
          if (this.listeners[event][i] === callback) {
            this.listeners[event].splice(i, 1);
            break;
          }
        };
      }
    }
  
    public unsubscribe(event:any, callback:any) {
      this.remove(event, callback);
    }
  
    public notify(event:any, data:{}) {
      if (this.listeners[event]) {
        this.listeners[event].forEach(function (listener:any) {
          listener(data);
        });
      }
    }
  
    public emit(event:any, data:{}) {
      this.notify(event, data);
    }
  }
  
  export default new Event();
  