import { typeCard } from './TypeCardConstants';

class TypeCardManagement {

 public isVisaCard(type:any) {
    return typeCard.VISA === type;
  }

  public isMasterCard(type:any) {
    return typeCard.MASTERCARD === type;
  }

  public isAmericanExpressCard(type:any) {
    return typeCard.AMERICAN_EXPRESS === type;
  }

  public  isDiscoverCard(type:any) {
    return typeCard.DISCOVER === type;
  }
}

export default new TypeCardManagement;