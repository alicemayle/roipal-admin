import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import PropTypes from 'prop-types'
import SignIn from '../../auth/signin'
import { CompaniesList } from '../../pages/companies'
import { MissionsList, MissionsUpsert } from '../../pages/missions'
import { AssestmentsList } from '../../pages/assestments'
import { InvitationsList } from '../../pages/invitations'
import { UsersList } from '../../pages/users'
import { LanguageList } from '../../pages/languages'
import { AssestmentsDiscList } from '../../pages/assestments-disc'
import AssestmentsUpsert from '../../pages/assestments-disc/Upsert'
import { ParametrizationTabs } from '../../pages/parametrization'
import { ChargesList } from '../../pages/charges'
import { PaymentExecutiveList } from '../../pages/payment-executive'
import { ControlTabs } from '../../pages/access-control'
import { LocationsList } from '../../pages/locations'
import { Menu } from '../../pages/profile-settings'
export class AppNavigation extends Component {
  static propTypes = {

  }

  render() {
    return (
      <Switch>
        <Route path="/missions" component={MissionsList} />
        <Route path="/mission/create" component={MissionsUpsert} />
        <Route path="/companies" component={CompaniesList} />
        <Route path="/assestments-disc/detail" component={AssestmentsUpsert} />
        <Route path="/assestmentss" component={AssestmentsList} />
        <Route path="/assestments-disc" component={AssestmentsDiscList} />
        <Route path="/parametrization" component={ParametrizationTabs} />
        <Route path="/languages" component={LanguageList} />
        <Route path="/invitations" component={InvitationsList} />
        <Route path="/charges" component={ChargesList} />
        <Route path="/payment" component={PaymentExecutiveList} />
        <Route path="/users" component={UsersList} />
        <Route path="/signin" component={SignIn} />
        <Route path="/access" component={ControlTabs} />
        <Route path="/locations" component={LocationsList} />
        <Route path="/settings" component={Menu} />
      </Switch>
    )
  }
}

export default AppNavigation
