import React from 'react';
import { Layout, Menu, Icon, Popconfirm } from 'antd';
import { Link, Redirect } from 'react-router-dom';
import Lang from '../support/Lang';
import Auth from '../auth/User';
import AppNavigation from './navigation/AppNavigation'
import Autobind from 'class-autobind';
import './full-layout.scss';
import { Home } from '../pages/home';
import { Gate } from '@xaamin/guardian'

const { Sider, Content } = Layout;

export interface State {
  collapsed: boolean,
  closeSession: boolean
}
class FullLayout extends React.Component<any, State>  {
  public state: State;

  public constructor(props?: any) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onCollapse = this.onCollapse.bind(this);

    this.state = {
      collapsed: true,
      closeSession: false
    }
    Autobind(this);
  }

  public componentWillUnmount() {
    this.setState({
      closeSession: false
    })
  }

  private async handleCloseSession() {
    await Auth.logout();
    await this.setState({
      closeSession: true
    })
    return <Redirect to="/signin" />
  }

  private toggle() {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  private onCollapse(collapsed: boolean) {
    this.setState({ collapsed });
  }

  public render() {
    const isActive = Auth.getData();

    const { pathname } = this.props.location;

    const permissionConfiguration = Gate.allows('configuration::global-page-access')
      || Gate.allows('commercial-need::page-access')
      || Gate.allows('type-sale::page-access')
      || Gate.allows('company-commission::page-access')
      ? true : false

    const permissionAccess = Gate.allows('user::page-access')
      || Gate.allows('role::page-access')
      ? true : false

    if (!isActive) {
      return <Redirect to="/signin" />
    }

    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider
          collapsible={true}
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <div className="logo" style={{ backgroundColor: 'transparent' }}>
            <Link to="/">
              {this.state.collapsed &&
                <img
                  src={require('../images/Imagotipo_ROIPAL_Perforado_09.png')}
                  width="50" height="50"
                />
              }
              {
                !this.state.collapsed &&
                <img
                  src={require('../images/Logo_ROIPAL_Perforado_08.png')}
                  height="60"
                />
              }
            </Link>
          </div>
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            {
              Gate.allows('company::page-access') &&
              <Menu.Item key="companies">
                <Link to="/companies">
                  <Icon type="global" />
                  <span>{Lang.get('company.title')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('executive::page-access') &&
              <Menu.Item key="users">
                <Link to="/users">
                  <Icon type="team" />
                  <span>{Lang.get('users.user.title_menu')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('location::page-access') &&
              <Menu.Item key="locations">
                <Link to="/locations">
                  <Icon type="environment" />
                  <span>{Lang.get('locations.location.menu')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('mission::page-access') &&
              <Menu.Item key="missions">
                <Link to="/missions">
                  <Icon type="reconciliation" />
                  <span>{Lang.get('buses.mission.title_menu')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('invitation::page-access') &&
              <Menu.Item key="invitations">
                <Link to="/invitations">
                  <Icon type="mail" />
                  <span>{Lang.get('invitation.title_menu')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('invoice::page-access') &&
              <Menu.Item key="charges">
                <Link to="/charges">
                  <Icon type="dollar" />
                  <span>{Lang.get('charges.charge.menu')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('executive-payment::page-access') &&
              <Menu.Item key="payment">
                <Link to="/payment">
                  <Icon type="calculator" />
                  <span>{Lang.get('payments.payment.menu')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('assessment::page-access') &&
              <Menu.Item key="assestments">
                <Link to="/assestments-disc">
                  <Icon type="ordered-list" />
                  <span>{Lang.get('assesments.title_menu')}</span>
                </Link>
              </Menu.Item>
            }
            {
              Gate.allows('translation::page-access') &&
              <Menu.Item key="languages">
                <Link to="/languages">
                  <Icon type="flag" />
                  <span>{Lang.get('translations.title')}</span>
                </Link>
              </Menu.Item>
            }
            {
              permissionConfiguration &&
              <Menu.Item key="parametrization">
                <Link to="/parametrization">
                  <Icon type="control" />
                  <span>{Lang.get('parametrization.title')}</span>
                </Link>
              </Menu.Item>
            }
            {
              permissionAccess &&
              <Menu.Item key="access">
                <Link to="/access">
                  <Icon type="idcard" />
                  <span>{Lang.get('access.title')}</span>
                </Link>
              </Menu.Item>
            }
            <Menu.Item key="settings">
              <Link to="/settings">
                <Icon type="setting" />
                <span>{Lang.get('setting.settings.title')}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="logout">
              <Popconfirm title={Lang.get('setting.settings.log_out')}
                onConfirm={this.handleCloseSession}
                okText={Lang.get('generic.button.yes')}
                cancelText={Lang.get('generic.button.no')}>
                <Icon type="logout" />
                <span>{Lang.get('generic.button.sign_out')}</span>
              </Popconfirm>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="container">
          <Content style={{
            margin: '24px 16px', padding: 24, background: '#fff', minHeight: '100vH',
          }}
          >
            {pathname === '/' && <Home />}
            <AppNavigation />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default FullLayout;