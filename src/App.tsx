import * as React from 'react';
import { Provider } from 'unstated';
import { Route, HashRouter, Switch } from 'react-router-dom';
import { Logger, Persist } from 'unstated-enhancers'
import LocalForage from 'localforage'
import LoginPage from './auth/signin/index';
import './App.scss';
import FullLayout from './layouts/FullLayout';
import GuardedRoute from './components/hocs/GuardedRoute';
import { Register } from './pages/register'
import { UserConfirmation } from './auth/user-confirmation'
import ValidateLanguage from './support/ValidateLanguage'

Logger.start();

const options = {
  key: 'persist::roipal-pwa',
  version: 1.0,
  storage: LocalForage,
  debounce: 250
}

Persist.start(options);

ValidateLanguage.loadLanguage()

class App extends React.Component {
  public render() {
    return (
      <Provider>
         <HashRouter>
            <Switch>
              <Route path="/user-confirmation" component = { UserConfirmation } />
              <Route path="/signin" component={ LoginPage } />
              <Route path="/register" component={ Register } />
              <GuardedRoute path="/" component={ FullLayout }/>
            </Switch>
        </HashRouter>
      </Provider>
    );
  }
}

export default App;
