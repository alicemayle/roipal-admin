import React, { PureComponent } from 'react';
import { Input, Button,  } from 'antd';
import autobind from 'class-autobind';
import Lang from '../../support/Lang'
import '../../index.scss'

interface FilterSearchProps {
  onSelectedKeys: (selected:string[])=>void,
  selectedKeys: number[],
  confirm: boolean,
  handleReset: (clearFilters: () => {})=>void,
  onSearchInput: (node:any)=>void,
  onSearch:(selectedKeys:any, confirm:any)=>void,
  clearFilters: () => {},
  dataIndex: any,
}

class FilterSearch extends PureComponent<FilterSearchProps> {
    constructor(props: FilterSearchProps) {
        super(props);
        autobind(this);
    }

    public handleSearch () {
      const { onSearch, confirm,  selectedKeys} = this.props;
      onSearch(selectedKeys, confirm);
    }

    private handleSelectedKeys(e: any) {
      const { onSelectedKeys } = this.props;
      onSelectedKeys(e.target.value ? [e.target.value] : []);
    }
    private handleSearchInput(node: any) {
      const { onSearchInput } = this.props;
      onSearchInput(node);
    }

    private handleReset() {
      const { handleReset,clearFilters } = this.props;
      handleReset(clearFilters);
    }

    public render() {
      const { selectedKeys } = this.props;
        return(
          <div style={{ padding: 8 }}>
            <Input
              ref={node => { this.handleSearchInput(node) }}
              // placeholder={`${Lang.get('generic.button.search')} ${dataIndex}`}
              value={selectedKeys[0]}
              onChange={ this.handleSelectedKeys}
              onPressEnter={this.handleSearch}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Button
              type="primary"
              onClick={ this.handleSearch}
              icon="search"
              size="small"
              style={{
                 width: 90,
                 marginRight: 8,
                }}
            >
              {Lang.get('generic.button.search')}
            </Button>
            <Button
              onClick={this.handleReset}
              size="small"
              style={{ width: 90 }}
            >
              {Lang.get('generic.button.reset')}
            </Button>
        </div>
        );
    }
}

export default FilterSearch;