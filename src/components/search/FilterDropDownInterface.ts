interface FilterDropDownInterface {
    setSelectedKeys: (selected: string[]) => {},
    selectedKeys: number[],
    clearFilters: () => {},
    confirm: any
}

export default FilterDropDownInterface;