interface FilterInterface {
    setSelectedKeys: (selected: string[]) => {},
    selectedKeys: number[],
    clearFilters: () => {},
    confirm: any
}

export default FilterInterface;