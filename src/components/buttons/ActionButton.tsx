import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Button } from 'antd';

interface ActionButtonProps {
  label: string,
  field: string,
  onHandleClick: (value: string) => void
}

class ActionButton extends PureComponent <ActionButtonProps>{
  constructor(props: ActionButtonProps) {
    super(props);
    Autobind(this);
  }

  private handleClick() {
    const { onHandleClick, field } = this.props;

    if ( onHandleClick ) {
      onHandleClick(field)
    }
  }

  public render() {
    const { label } = this.props;

    return (
      <Button
        type="primary"
        onClick={this.handleClick}
        style={{marginRight: 10}}
      >
        {label}
      </Button>
    );
  }
}

export default ActionButton;