import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import { Button } from 'antd';

interface ShowProps {
  item: {},
  onFuction: (item: any) => void
  colorIcon: any
  size: number
  name: string,
  disable?: boolean
}

class IconButton extends PureComponent<ShowProps> {
  constructor(props: ShowProps) {
    super(props);
    autobind(this);
  }

  private handleShow() {
    this.props.onFuction(this.props.item);
  }

  public render() {
    const { colorIcon, size, name, disable } = this.props

    return (
      <div>
        <Button
          type="link"
          icon={name}
          style={{
            borderColor: 'transparent',
            color: disable ? 'lightgrey' : colorIcon,
            fontSize: size
          }}
          onClick={this.handleShow}
          disabled={disable ? true : false}
          size="default" />
      </div>
    )
  }
}

export default IconButton;