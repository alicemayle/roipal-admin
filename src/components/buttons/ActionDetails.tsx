import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import Lang from '../../support/Lang';

interface ShowDetailsProps {
    item: {},
    onShowDetails:(item: any) => void
}

class ActionDetails extends PureComponent<ShowDetailsProps> {
  constructor(props: ShowDetailsProps) {
    super(props);
    autobind(this);
  }

  private handleShow() {
    this.props.onShowDetails(this.props.item);
  }

  public render() {
    return (
      <div>
        <a onClick={this.handleShow}>{Lang.get('generic.button.details')}</a>
      </div>
    )
  }
}

export default ActionDetails;