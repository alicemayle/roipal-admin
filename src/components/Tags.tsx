import React, { PureComponent } from 'react';
import { Tag } from 'antd';
import autobind from 'class-autobind';

interface TagsProps {
    // onClose:(
    //     field:{
    //         name: string,
    //         status?: boolean
    //     }
    // )=>void,
    index: number,
    field: {
        name: string,
        status?: boolean
    }
}

class Tags extends PureComponent<TagsProps> {
    constructor(props: TagsProps) {
        super(props);
        autobind(this);
    }

    // private handleClose() {
    //     this.props.onClose(this.props.field);
    // }

    public render() {
        const { index,field } = this.props;

        return (
            <Tag color="#52bebb"
            key={index}
            visible={field.status}
            // closable={true}
            // afterClose={this.handleClose}
            >
                {field.name}
            </Tag>
        );
    }
}

export default Tags;