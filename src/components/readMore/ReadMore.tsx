import React, { PureComponent } from 'react';
import Truncate from 'react-truncate';
import autobind from 'class-autobind';
import './styles.scss'
import Lang from 'src/support/Lang';

class ReadMore extends PureComponent<any> {
  constructor(props: any) {
    super(props);

    autobind(this);
  }

  public state: any = {
    expanded: false,
  };

  private toggleLines(event: any) {
    event.preventDefault();

    this.setState({
      expanded: !this.state.expanded
    });
  }


  public render() {
    const { description } = this.props;
    const { expanded } = this.state;

    return (
      <div className="card-content">
        <Truncate
          lines={!expanded && 2}
          ellipsis={(
            <span>... <a href="#" onClick={this.toggleLines}>
              {Lang.get('generic.legends.read_more')}
            </a></span>
          )}
        >
          {description}
        </Truncate>
        {expanded && (
          <span> <a href="#" onClick={this.toggleLines}>
            {Lang.get('generic.legends.read_less')}
          </a></span>
        )}
      </div>
    );
  }
}

export default ReadMore;