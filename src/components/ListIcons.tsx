import React, { PureComponent } from 'react';
import { Icon } from 'antd';

interface ListIconProps {
    label:string,
    icon: string,
    type: string,
    colorIcon?: string
}

class ListVerticalIcons extends PureComponent<ListIconProps> {
    constructor(props: ListIconProps) {
        super(props);
    }

    public render() {
        const { label, icon, type, colorIcon = '' } = this.props;

        return(
            <div >
              {
                label &&
                <div  className={type==='vertical'? 'list-icon-vertical': 'list-icon-horizontal'}>
                    <Icon
                        className={type==='vertical'? 'icon-vertical': 'icon-horizontal'}
                        type={ icon }
                        style = {{color: colorIcon}}
                    />
                    <p> {label}</p>
                  </div>
              }
            </div>
        );
    }
}

export default ListVerticalIcons;