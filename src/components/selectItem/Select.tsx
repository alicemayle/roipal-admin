import React, { Component } from 'react';
import autobind from 'class-autobind';

import { Select as SelectComponent } from 'antd';

const { Option } = SelectComponent;

interface ItemProps {
  id: string,
  value: string,
  label: string,
}

interface SelectItemProps {
  defaultValue?: any,
  disable?: any,
  options: any[],
  field: string,
  showSearch: boolean,
  placeholder: string,
  onChange: (value:any, field: any) =>void,
}

class Select extends Component <SelectItemProps> {
  constructor(props: SelectItemProps) {
    super(props);
    autobind(this);
  }

  private handleChange (value: any) {

    const { onChange, field } = this.props;

    if (onChange) {
      onChange(field, value)
    }
  }

  public renderItem(item: ItemProps) {
    return(
      <Option key={item.id} value={item.value}>
        {item.label}
     </Option>
    )
  }

  public render() {

    const { options = [], defaultValue, disable } = this.props;

    return (
      <SelectComponent
      showSearch={true}
      onChange={this.handleChange}
      defaultValue={defaultValue}
      disabled={disable}
      >
        {
          options.map(this.renderItem)
        }
      </SelectComponent>
    )
  }

}

export default Select