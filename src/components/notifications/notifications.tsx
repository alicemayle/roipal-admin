import { notification } from 'antd';
import Lang from '../../support/Lang'

class Notifications{
  public openNotification(data:any){
      notification[data.type]({
        message: data.type === 'error'
          ?  Lang.get('generic.notifications.error') 
          : Lang.get('generic.notifications.success'),
        description: data.message
      });
  }
}

export default Notifications;