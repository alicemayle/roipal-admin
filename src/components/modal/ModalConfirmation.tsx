import React, { Component } from 'react';
import { Modal, Button } from 'antd';
import autobind from 'class-autobind';
import Text from 'antd/lib/typography/Text';
import Lang from 'src/support/Lang';

interface ListState {
  visible: any
}

class ModalConfirmation extends Component<any, ListState> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public render() {

    const { onVisible, onOk, onFunction, onClose, onText } = this.props

    return (
      <div>
        <Modal
          title={Lang.get('generic.legends.confirm')}
          visible={onVisible}
          onOk={onOk}
          destroyOnClose={true}
          centered={true}
          onCancel={onClose}
          footer={[
            <Button
              key="cancel"
              onClick={onClose}
            >
              {Lang.get('generic.button.cancel')}
            </Button>,
            <Button
              key="submit"
              type="primary"
              onClick={onFunction}
            >
              {Lang.get('generic.button.accept')}
            </Button>
          ]}
        >
          <div style={{ textAlign: 'center' }}>
            <Text>{onText}</Text>
          </div>
        </Modal>
      </div>
    );
  }
}

export default ModalConfirmation;