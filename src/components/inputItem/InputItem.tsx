import React, { Component } from 'react';
import autobind from 'class-autobind';

import { Input } from 'antd';
import HandleChange from 'src/pages/missions/models/Upsert/HandleChange';

interface InputItemProps {
  field: string,
  placeholder: string,
  onChange: (field: string, value:any) =>void,
  location?: any
}

class InputItem extends Component <InputItemProps> {
  constructor(props: InputItemProps) {
    super(props);
    autobind(this);
  }

  private handleChange (e: HandleChange) { 

    const value = e.target.value;

    const { onChange, field } = this.props;

    if (onChange) {
      onChange(field, value)
    }
  }

  public render() {

    const { placeholder, location } = this.props;

    return (
      <Input 
        placeholder={placeholder} 
        onChange={this.handleChange}
        defaultValue={location ? location.name : ''}
      />
    )
  }

}

export default InputItem