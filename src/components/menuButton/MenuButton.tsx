import React, { Component } from 'react';
import { Col, Icon, Button, Tooltip } from 'antd';
import autobind from 'class-autobind';
import { Link } from 'react-router-dom';

class MenuButton extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this);
  }

  public render() {
    const { link, title, icon, button_text } = this.props

    return (
      <Link to={link}>
        <Tooltip title={title}>
          <Col className="home-column">
            <Icon
              type={icon}
              className="home-icon"
            />
            <Button className="home-button">
              {button_text}
            </Button>
          </Col>
        </Tooltip>
      </Link>
    )
  }
}

export default MenuButton