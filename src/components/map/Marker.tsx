import React, { Component } from 'react';
import { Icon, Tag } from 'antd';
import Autobind from 'class-autobind';

interface MarkerState {
  showCard: boolean
}

interface MarkerProps {
  nextProps?: {
    marker: any,
  },
  marker: any,
  lat: number,
  lng: number
}

class Marker extends Component<MarkerProps, MarkerState> {
  public margin1: number = 20;
  public margin2: number = 50;
  constructor(props: MarkerProps) {
    super(props);
    Autobind(this);
    this.state = {
      showCard: false,
    }
  }

  public shouldComponentUpdate(nextProps: MarkerProps, nextState: MarkerState) {
    return (nextProps.marker !== this.props.marker
      || nextState.showCard !== this.state.showCard)
  }

  public render() {
    return (
      <div>
        <div
          style={{
            cursor: 'pointer',
          }}>
          <Tag
            style={{
              width: '35px',
              height: '35px',
              borderRadius: '50% 50% 0% 50%',
              background: '#89849b',
              position: 'relative',
              transform: 'rotate(45deg)',
              margin: `-${20}px 0 ${50}px`,
              animationName: 'bounce',
              animationFillMode: 'both',
              animationDuration: '1s',
              animation: 'pulse 1s ease 1s 3'
            }}
          >
            <Icon
              style={{
                fontSize: '24px',
                position: 'absolute',
                marginLeft: '-3px',
                marginTop: '4px',
                transform: 'rotate(-87deg)',
              }}
              type="pushpin" />
          </Tag>
        </div>
      </div>
    );
  }
}

export default Marker;