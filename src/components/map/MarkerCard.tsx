import React, { PureComponent } from 'react';
import './styles.css';

interface MarkerCardProps {
    marker: {
        name?: string,
        address?: string
    }
}

class MarkerCard extends PureComponent<MarkerCardProps> {
    
    public render() {
        const { marker } = this.props;

        return (
            <div
            style={{
                marginLeft: '40px',
                marginTop: '-104px',
                position: 'absolute',
                zIndex: 1,
                width: '300px'
            }}>
                <div className="showInfoMarker">
                    <span>
                        {marker.name}
                        <br/>
                        {marker.address}
                    </span>
                </div>                
            </div>
        )
    }
}

export default MarkerCard;