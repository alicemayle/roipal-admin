import React from 'react';
import Auth from '../../auth/User';
import { Route, Redirect } from 'react-router-dom';

const GuardedRoute = ({ component: Component, ...rest }:any) => {
    const wrapped = (props:any) => {
        
        return Auth.isAuthenticated()
        ? <Component { ...props } />
        : <Redirect to={ { pathname: '/signin', state: { from: props.location } } } />
    }

    return <Route { ...rest } render={ wrapped } />
}

export default GuardedRoute;
