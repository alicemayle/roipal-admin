import React, { Component } from 'react';
import Autobind from 'class-autobind';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import { Logger } from 'unstated-enhancers';
import { AddressSelected } from '../../pages/missions/components/Upsert/Upsert';
import { Input } from 'antd';
import Lang from 'src/support/Lang';

class SelectAddress extends Component<any, any> {

  private searchOptions: any ;

  constructor(props:any) {
    super(props);
    this.state = { address: '' };
    Autobind(this);
  }
  private handleChange = (address: any) => {
    this.setState({ address });
 };
  private async handleSelect (address: any) {
    try {
      const addressName = await geocodeByAddress(address);

      const latLng = await getLatLng(addressName[0]);

      const addressResult: AddressSelected = {
        latLng: {...latLng},
        address: addressName[0].formatted_address
      }

      const { onChangeAddress } = this.props;

      this.setState({ address: addressName[0].formatted_address});

      if (onChangeAddress) {
        onChangeAddress(addressResult);
      }

    } catch (error) {
        // TODO: Tratar error
        Logger.dispatch(error)
    }

  }

  public componentDidMount() {
    const { location, onChangeAddress } = this.props

    if(location) {
      this.searchOptions = {
        location: new google.maps.LatLng(location.longitude, location.latitude),
        radius: 2000,
        types: ['address']
      }

      const addressResult: AddressSelected = {
        latLng: {
          lat: location.latitude,
          lng: location.longitude
        },
        address: location.address
      }

      if (onChangeAddress) {
        onChangeAddress(addressResult);
      }

      this.setState({
        address: location.address
      })

    } else {
      this.searchOptions = {
        location: new google.maps.LatLng(-99.1832562, 19.3665953),
        radius: 2000,
        types: ['address'],
        country: 'mx'
      }
    }
  }

  public render() {
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
        searchOptions={this.searchOptions}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }:any) => (
          <div>
            <Input
              {...getInputProps({
                placeholder: Lang.get('map.search'),
                className: 'location-search-input',
              })}
            />
            <div className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map((suggestion: any) => {
                const className = suggestion.active
                  ? 'suggestion-item--active'
                  : 'suggestion-item';
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                  : { backgroundColor: '#ffffff', cursor: 'pointer' };
                return (
                  <div key={suggestion.id}
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

export default SelectAddress;