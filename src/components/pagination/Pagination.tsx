import React, { Component } from 'react';
import PaginationNavigate from './components/PaginationNavigate';
import Autobind from 'class-autobind';

interface PaginationProps {
    meta: {
        current: number,
        prev: number,
        next: number,
        count: number
    },
    limit?: number,
    onChange:(limit:number, page: number) =>void,
}

class Pagination extends Component<PaginationProps> {
    constructor(props:PaginationProps) {
        super(props);
        Autobind(this);
    }

    public handlePrevPage() {
        let page = this.props.meta.current;
        page = page - 1;
        const { count } = this.props.meta;
        const { limit } = this.props;
        const _limit = limit ? limit : count
        this.props.onChange(_limit, page);
    }

    public handleNextPage() {
        let page = this.props.meta.current;
        page = page + 1;
        const { count } = this.props.meta;
        const { limit } = this.props;
        const _limit = limit ? limit : count
        this.props.onChange(_limit, page);
    }

    public handleChangeLimit(limit: number) {
        const { current } = this.props.meta;

        this.props.onChange(limit, current);
    }

    public render() {
        const {meta, limit } = this.props;       

        return (
            <PaginationNavigate 
            paginate={meta}
            limit={limit ? limit : meta.count}
            onPrevPage={this.handlePrevPage}
            onNextPage={this.handleNextPage}
            onChangeLimit={this.handleChangeLimit}
            />
        )
    }
    
}

export default Pagination;