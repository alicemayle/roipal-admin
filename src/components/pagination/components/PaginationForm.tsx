import React, { Component } from 'react';
import { Select } from 'antd';
import Autobind from 'class-autobind';

interface PaginationFormProps {
    limit: number,
    onChangeLimit: (limit:number)=>void
}

const Option = Select.Option;

class PaginationForm extends Component<PaginationFormProps> {
    constructor(props: PaginationFormProps) {
        super(props);
        Autobind(this);
    }

    private handleChange(value: number) {
        this.props.onChangeLimit(value);
    }

    public render() {
        const { limit } = this.props;
        
        return(
            <Select
            value={limit}
            onChange={this.handleChange}
            >
                <Option value={50}>{50}</Option>
                <Option value={100}>{100}</Option>
                <Option value={250}>{250}</Option>
                <Option value={500}>{500}</Option>
            </Select>
        )
    }

}

export default PaginationForm;