import React, { PureComponent } from 'react';
import { Button, Row, Col, BackTop } from 'antd';
import PaginationPages from './PaginationForm';
import Autobind from 'class-autobind';
import './styles.scss';

interface PaginationNavigateProps {
  paginate: {
    current: number | null,
    prev: number | null,
    next: number | null,
    count: number
  },
  limit: number
  onPrevPage: () => void,
  onNextPage: () => void,
  onChangeLimit: (limit: number) => void
}

class PaginationNavigate extends PureComponent<PaginationNavigateProps> {
  constructor(props: PaginationNavigateProps) {
    super(props);
    Autobind(this);
  }

  public handleChangeLimit(limit: number) {
    this.props.onChangeLimit(limit);
  }

  public handlePrevPage() {
    this.props.onPrevPage();
  }

  public handleNextPage() {
    this.props.onNextPage();
  }

  public render() {
    const { paginate, limit } = this.props;

    return (
      <Row
        gutter={16}>
        <Col
          className={'paginateAling'}
          xs={{ span: 8 }}
          lg={{ span: 2, offset: 16 }}>
          <PaginationPages
            onChangeLimit={this.handleChangeLimit}
            limit={limit} />
        </Col>
        <Col
          className="paginateAling"
          xs={{ span: 5 }}
          lg={{ span: 1 }}>
          <Button
            icon="left"
            disabled={paginate.prev ? false : true}
            onClick={this.handlePrevPage} />
        </Col>
        <Col
          className="paginateAling"
          xs={{ span: 5 }}
          lg={{ span: 1 }}>
          <Button>{paginate.current}</Button>
        </Col>
        <Col
          className="paginateAlingRigth"
          xs={{ span: 5 }}
          lg={{ span: 1 }}>
          <Button
            icon="right"
            disabled={paginate.count === 0 || paginate.count < limit ? true : false}
            onClick={this.handleNextPage} />
        </Col>
        <Col
          className="paginateAlingRigth"
          xs={{ span: 5 }}
          lg={{ span: 1 }}>
          <BackTop>
            <div className="ant-back-top-inner">
              <Button
                icon="up"
              />
            </div>
          </BackTop>
        </Col>
      </Row>
    )
  }
}

export default PaginationNavigate;