import axios from 'axios';
import User from '../auth/User';
import Language from '../support/Lang';

import { AxiosInstance, AxiosRequestConfig } from 'axios';

class Http {

  private axios: AxiosInstance

  public constructor() {
    this.axios = axios.create();

    this.registerResponseInterceptor();
    this.registerRequestInterceptor();
  }

  public request(config: AxiosRequestConfig) {
    return this.axios.request(config);
  }

  public get(url: string, config: AxiosRequestConfig = {}) {
    return this.axios.get(url, this.getConfig(config));
  }

  public delete(url: string, config: AxiosRequestConfig = {}) {
    return this.axios.delete(url, this.getConfig(config));
  }

  public head(url: string, config: AxiosRequestConfig = {}) {
    return this.axios.head(url, this.getConfig(config));
  }

  public options(url: string, config: AxiosRequestConfig = {}) {
    return (this.axios as any).options(url, this.getConfig(config));
  }

  public post(url: string, data = {}, config: AxiosRequestConfig = {}) {
    return this.axios.post(url, data, this.getConfig(config))
  }

  public put(url: string, data = {}, config: AxiosRequestConfig = {}) {
    return this.axios.put(url, data, this.getConfig(config))
  }

  public patch(url: string, data = {}, config: AxiosRequestConfig = {}) {
    return this.axios.patch(url, data, this.getConfig(config))
  }

  private getConfig(config: AxiosRequestConfig) {
    config = config ? { ...config } : {};

    config.baseURL = process.env.REACT_APP_API_URL
    return config;
  }

  private registerRequestInterceptor() {
    this.axios.interceptors.request.use((config: any) => {
      const token = User.getAccessToken(false);

      config.headers['X-Preferred-Language'] = Language.getLocale();
      config.headers['Accept-Language'] = Language.getLocale();

      if (token !== null && config.url.indexOf('http') === -1) {
        config.headers.Authorization = 'Bearer ' + token;
      }

      return config;
    }, (error) => {
      return Promise.reject(error);
    });
  }

  private registerResponseInterceptor() {
    this.axios.interceptors.response.use((response) => {

      return Promise.resolve(response.data);
    }, (error) => {
      const response = error.response || {};

      if (!response.data) {
        let message = error.status ? (error.status + ' ' + error.statusText) : null;
        message = message || error.message;

        response.data = {
          message: message || 'Http request uncaught error'
        }
      }

      return Promise.reject(response.data);
    });
  }
}

export default new Http();