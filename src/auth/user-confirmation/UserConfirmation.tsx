import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Layout, Row, Col, Card, Form, Input, Button } from 'antd';
import Lang from 'src/support/Lang';
import Text from 'antd/lib/typography/Text';
import UserConfirmationContainer from '../state/UserConfirmationContainer'
import { connect, Logger } from 'unstated-enhancers';
import SignInContainer from '../state/SignInContainer';

class UserConfirmation extends Component<any, any> {
  public form: any = null;

  constructor(props: any) {
    super(props);

    autobind(this);
    this.state = {
      password: null,
      confirmPassword: null,
      flagerror: false
    };
  }

  private changeText(e: any) {
    this.setState({
      flagerror: false,
      [e.target.name]: e.target.value
    })
  }

  private async handleSubmit() {
    const { user } = this.props.containers;

    this.props.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        const { confirmPassword, password } = this.state;

        const newPass = password;
        const regex = /(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}.+$)/;
        const valor = regex.test(newPass);

        if (password === confirmPassword && user.state.data) {
          if (valor) {
            const token = this.props.location.search.split('=');

            const _data = {
              token: token[1],
              password: newPass,
              uuid: user.state.data.uuid
            }

            await user.updateUser(_data)
            const { error } = user.state

            if (!error) {
              this.setState({
                password: null,
                confirmPassword: null,
              })

              const credentials = {
                email: user.state.data.email,
                password: newPass,
                scope: 'admin'
              }
      
              const { signin } = this.props.containers;
      
              await signin.login(credentials);

              this.props.history.push({
                pathname: '/signin',
              })
            }
          } else {
            this.setState({
              flagerror: true
            })
            return;
          }
        } else {
          this.setState({
            flagerror: true
          })
        }
      }
    })
  }

  public async componentDidMount() {
    const token = this.props.location.search;
    const { user } = this.props.containers;
    Logger.dispatch('props', this.props)

    if (token) {
      await user.validateUser(token);
    }
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { flagerror } = this.state;
    const { httpBusy, data } = this.props.user;

    return (
      <Layout className="home-background">
        <div className="Divider" />
        <Row
          align="middle"
          justify="center"
        >
          <Col
            xs={{ span: 18, offset: 3 }}
            md={{ span: 12, offset: 9 }}>

            <Card className="cardBox" style={{ width: '50%' }}>
              <img
                src={require('../../images/logo_colores.png')}
                width="260" height="60"
                className="img"
              />
              <br />

              <Form>
                <Form.Item>
                  {getFieldDecorator('name', {
                    initialValue: data ? data.name : '',
                    rules: [{
                      required: true,
                      message: Lang.get('register.validations.name')
                    }],
                  })(
                    <Input
                      placeholder={Lang.get('register.name')}
                      disabled={data && data.name ? true : false}
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('email', {
                    initialValue: data ? data.email : '',
                    rules: [{
                      required: true,
                      message: Lang.get('register.validations.email')
                    }],
                  })(
                    <Input
                      placeholder={Lang.get('register.email')}
                      disabled={data && data.email ? true : false}
                    />
                  )}
                </Form.Item>
                <Form.Item
                  validateStatus={flagerror ? 'error' : undefined}
                >
                  {getFieldDecorator('password', {
                    rules: [
                      {
                        required: true,
                        message: Lang.get('register.validations.password')
                      },
                    ],
                  })(
                    <Input.Password
                      placeholder={Lang.get('register.password')}
                      name="password"
                      onChange={this.changeText}
                    />
                  )}
                </Form.Item>
                <Form.Item
                  validateStatus={flagerror ? 'error' : undefined}
                  help={flagerror ? <Text style={{ color: 'red' }}>
                    {Lang.get('register.info_pass')}
                    <br />
                    {Lang.get('register.info_pass2')}
                    <br />
                    {Lang.get('register.info_pass3')}
                    <br />
                    {Lang.get('register.info_pass4')}
                    <br />
                    {Lang.get('register.info_pass5')}
                  </Text> : ''}
                >
                  {getFieldDecorator('confirmPass', {
                    rules: [
                      {
                        required: true,
                        message: Lang.get('register.confirm_password')
                      },
                    ],
                  })(
                    <Input.Password
                      placeholder={Lang.get('register.confirm_password')}
                      onChange={this.changeText}
                      name="confirmPassword"
                    />
                  )}
                </Form.Item>

                <Button
                  type="primary"
                  className="login-form-button"
                  loading={httpBusy}
                  onClick={this.handleSubmit}
                >
                  {Lang.get('register.confirm_account')}
                </Button>
              </Form>
            </Card>
          </Col>
        </Row>
      </Layout>
    )
  }
}

const container = {
  user: UserConfirmationContainer,
  signin: SignInContainer
}

const mapStateToProps = (containers: any) => {
  return {
    user: containers.user.state,
    signin: containers.signin.state
  }
}

const UserConfirmationForm: any = Form.create()(UserConfirmation);

export default connect(container, mapStateToProps)(UserConfirmationForm);
