import User from '../User'
import { Http } from '../../shared';
class Auth {

  public async login(credentials: any): Promise<any> {

    const params = {
      'grant_type': 'password',
      'client_id': '923a69f6-c2c7-401c-802c-0fff3c67e448',
      'client_secret': 'owQmd6i7YM9ofDS41gZ8bJN8E9tBta5ZVshom5l6',
      'username': credentials.email,
      'password': credentials.password,
      'scope': credentials.scope,
      'includes': 'roles,permissions,company.profile,checklist'
    }

    let response;

    try {
      response = await Http.post('/oauth/token?', params);

      if (response.data.roles && response.data.roles.length > 0) {
        const found = response.data.roles.find((roles: any) =>
          roles.name === 'root' || roles.name === 'reader' || roles.name === 'company'
        );

        if (found === undefined) {
          throw new Error('Invalid User');
        }
      }

      const user = {
        ...response.data,
        scope: credentials.scope
      }
      User.setData(user)
    } catch (error) {
      throw error;
    }

    return Promise.resolve(response);
  }
}

export default new Auth;