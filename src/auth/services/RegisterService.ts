import { Http } from '../../shared';

class RegisterService {
  public register(data: any): any {
    return Http.post('api/auth/companies/', data);
  }
}


export default new RegisterService;