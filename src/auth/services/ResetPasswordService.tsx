import { Http } from '../../shared';

class RegisterService {

  public resetPassword(data: any): any {
    return Http.post('/api/password/recovery', data);
  }
}


export default new RegisterService;