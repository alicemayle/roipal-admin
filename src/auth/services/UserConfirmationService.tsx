import { Http } from '../../shared';

class UserConfirmationService {

  public validateUser(params: any): any {
    return Http.get(`api/token/validate${params}`,);
  }

  public updateUser(data: any): any {
    return Http.put(`api/token/update`, data,);
  }
}


export default new UserConfirmationService;