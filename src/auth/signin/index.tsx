import React, { Component } from 'react'
import {
  Form, Icon, Input, Button, Layout, message, Row, Col, Card, Select
} from 'antd';
import { MessageType } from 'antd/lib/message';
import { connect, Logger } from 'unstated-enhancers'
import { Redirect } from 'react-router-dom';
import './signin.scss'
import User from '../User'
import SignInContainer from '../state/SignInContainer';
import SignInState from '../models/SignInState';
import Auth from '../User';
import Lang from '../../support/Lang';
import LanguageContainer from '../state/LanguageContainer';
import RegisterCompanyForm from './components/RegisterCompanyForm';
import autobind from 'class-autobind';
import ResetPassword from './components/ResetPassword'
import ValidateLanguage from '../../support/ValidateLanguage'

const FormItem = Form.Item;
const { Option } = Select;

interface LoginProps extends SignInState {
  form: any,
  getFieldDecorator: any,
  containers: any,
  history?: any,
  visible: any,
  change: any
}

class NormalLoginForm extends Component<LoginProps> {
  private hide: MessageType;

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    visible: false,
    change: true
  }

  public componentDidMount() {
    const { data } = this.props.containers.signin.state
    if (data) {
      User.setData(data);
    }
  }

  private handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);

        this.hide = message.loading('Wait...', 0);

        ValidateLanguage.setLanguage(Lang.getLocale())
        
        const credentials = {
          email: values.email,
          password: values.password,
          scope: values.scope
        }

        const { signin } = this.props.containers;

        await signin.login(credentials);

        this.hide();

        const { error } = this.props.containers.signin.state;

        if (error) {
          message.error(error.message);
        }

      }
    });
  }

  public handleChangeLanguage = (value: string) => {
    const { language } = this.props.containers;

    language.setLanguage(value)
  }

  public handleRegister() {
    this.props.history.push({
      pathname: '/register',
    })
  }

  private showModal() {
    this.setState({
      visible: !this.state.visible
    })
  }

  public changeStatus() {
    this.setState({
      change: !this.state.change
    })
  }

  public render() {
    const { httpBusy } = this.props.containers.signin.state;
    const { getFieldDecorator } = this.props.form;
    const isActive = Auth.isAuthenticated();
    const { change } = this.state
    Logger.count(this)

    if (isActive) {
      const scope = Auth.getData().scope

      if (scope === 'company') {
        const checklist = Auth.getData().checklist
        if (checklist.length < 4) {
          return <Redirect to="/register" />
        } else {
          const incompleted = checklist.filter((item: any) => {
            if (!item.completed) {
              return item
            }
          })

          if (incompleted.length > 0) {
            return <Redirect to="/register" />
          }
        }
      }
      return <Redirect to="/" />
    }

    return (
      <Layout className="home-background">
        <div className="Divider" />
        <Row
          align="middle"
          justify="center"
        >
          <Col
            xs={{ span: 18, offset: 3 }}
            md={{ span: 12, offset: 9 }}>

            <Card className="cardBox" style={{ width: '50%' }}>
              <img
                src={require('../../images/logo_colores.png')}
                width="260" height="60"
                className="img"
              />
              <br />
              {
                change &&
                <Form
                  className="login-form"
                  onSubmit={this.handleSubmit}
                >
                  <FormItem>
                    {getFieldDecorator('email', {
                      rules: [{
                        type: 'email', message: Lang.get('signin.signin.error_email'),
                      }, { required: true, message: Lang.get('signin.signin.error_user') }],
                    })(
                      <Input
                        prefix={<Icon type="user"
                          style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder={Lang.get('signin.signin.user')} />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('password', {
                      rules: [{ required: true, message: Lang.get('signin.signin.error_pass') }],
                    })(
                      <Input.Password
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder={Lang.get('signin.signin.password')} />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('scope', {
                      rules: [{ required: true, message: Lang.get('generic.scope.select_scope') }]
                    })(
                      <Select
                      placeholder={Lang.get('access.role.rol_signin')}
                      >
                        <Option value="admin">{Lang.get('generic.scope.admin')}</Option>
                        <Option value="company">{Lang.get('generic.scope.company')}</Option>
                      </Select>
                    )}
                  </FormItem>
                  <FormItem>
                    <Select
                      placeholder={Lang.get('generic.language.select_language')}
                      defaultValue={Lang.getLocale()}
                      onChange={this.handleChangeLanguage}>
                      <Option value="en">{Lang.get('generic.language.english')}</Option>
                      <Option value="es">{Lang.get('generic.language.spanish')}</Option>
                      <Option value="fr">{Lang.get('generic.language.french')}</Option>
                    </Select>
                  </FormItem>
                  <FormItem>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                      loading={httpBusy}
                    >
                      {Lang.get('signin.signin.signin')}
                    </Button>
                    <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
                      <p>{}</p>
                      <a style={{color: '#3babd4'}} onClick={this.showModal}>
                        {Lang.get('signin.signin.forgot_pass')}
                      </a>
                      <p>{}</p>
                    </div>
                  </FormItem>
                  <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
                    <p>{}</p>
                    <p>
                    {Lang.get('signin.signin.account')}
                    <a style={{color: '#3babd4'}} onClick={this.changeStatus}>
                      {Lang.get('signin.signin.sign')}
                    </a>
                    </p>
                    <p>{}</p>
                  </div>
                </Form>
              }
              {
                !change &&
                <RegisterCompanyForm
                  onRegister={this.handleRegister}
                  onChange={this.changeStatus} />
              }
            </Card>
          </Col>
        </Row>
        <ResetPassword
          visible={this.state.visible}
          cancel={this.showModal}
        />
      </Layout>
    );
  }
}

const config = {
  signin: SignInContainer,
  language: LanguageContainer
}

const mapStateToProps = (containers: any) => {
  return {
    signin: containers.signin.state,
    language: containers.language.state
  }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);

export default connect(config, mapStateToProps)(WrappedNormalLoginForm);