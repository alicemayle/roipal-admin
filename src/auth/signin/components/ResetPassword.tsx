import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Modal, Button, Form, Input, Icon } from 'antd';
import PasswordContainer from 'src/auth/state/PasswordContainer';
import { connect } from 'unstated-enhancers';
import Notifications from 'src/components/notifications/notifications';
import Lang from 'src/support/Lang';
import Text from 'antd/lib/typography/Text';

class ResetPassword extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  private handleSubmit(e: any) {
    e.preventDefault();
    this.props.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        const { reset } = this.props.containers;

        const data = {
          ...values
        }

        if (data) {
          await reset.resetPassword(data);
          const { error } = reset.state;

          if (error) {
            const notification = new Notifications;

            const msj = {
              type: 'error',
              message: Lang.get('reset.reset.error')
            }

            notification.openNotification(msj)
          }
        }
      }
    })
  }

  private onClose() {
    const { cancel } = this.props

    if (cancel) {
      this.setState({
        email: null
      })
      cancel()
    }
  }

  public render() {
    const { visible } = this.props
    const { getFieldDecorator } = this.props.form;
    const { httpBusy } = this.props.containers.reset.state

    return (
      <div>
        <Modal
          title={Lang.get('reset.reset.recover')}
          visible={visible}
          destroyOnClose={true}
          centered={true}
          width={'40%'}
          onCancel={this.onClose}
          footer={[
            <Button
              key="cancel"
              onClick={this.onClose}
            >
              {Lang.get('reset.button.cancel')}
            </Button>,
            <Button
              key="submit"
              type="primary"
              onClick={this.handleSubmit}
              loading={httpBusy}
            >
              {Lang.get('reset.button.send')}
            </Button>
          ]}
        >
          <div>
            <p>{Lang.get('reset.reset.email')}</p>
            <Form>
              <Form.Item>
                {getFieldDecorator('email', {
                  rules: [{
                    required: true,
                    message: Lang.get('reset.reset.error_email')
                  }],
                })(
                  <Input
                    prefix={<Icon type="user"
                      style={{ color: 'rgba(0,0,0,.25)' }} />}
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Text
                  style={{ fontSize: 12 }}>
                  {Lang.get('reset.reset.reset_password')}
                </Text>
              </Form.Item>
            </Form>
          </div>
        </Modal>
      </div>
    )
  }
}

const config = {
  reset: PasswordContainer
}

const mapStateToProps = (containers: any) => {
  return {
    reset: containers.reset.state,
  }
}

const WrappedResetPassword: any =
  Form.create({ name: 'RecoveryResetForm' })(ResetPassword);

export default connect(config, mapStateToProps)(WrappedResetPassword);