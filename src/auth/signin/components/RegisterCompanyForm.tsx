import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Form, Input, Button, message } from 'antd';
import LanguageContainer from '../../state/LanguageContainer';
import RegisterContainer from '../../state/RegisterContainer';
import SignInContainer from '../../state/SignInContainer';
import { connect } from 'unstated-enhancers';
import User from '../../User'
import Lang from 'src/support/Lang';
import Text from 'antd/lib/typography/Text';

class RegisterCompanyForm extends Component<any, any> {
  public form: any = null;
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    flag: false
  }

  private handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);

        const pass = values.password;

        const regex = /(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}.+$)/;
        const valor = regex.test(pass);

        if (valor) {
          const { register, signin } = this.props.containers
          const { selected } = this.props.language
          const _data = {
            ...values,
            language: selected,
          }

          await register.register(_data);

          const { error, data } = register.state;

          if (!error) {
            const _groups: any[] = []
            // TODO: CORREGIR AL ESTAR LISTO EL REGISTRO DE COMPAÑIA CON ROL
            if (data.roles) {
              data.roles.map((role: any) => {
                const groupFound = _groups.find((item: string) => item === role.group)

                if (!groupFound) {
                  _groups.push(role.group)
                }
              });
            } else {
              _groups.push('roipal')
            }

            const newData = {
              ...data,
              groups: _groups,
              scope: 'company'
            }

            await signin.setData(newData)

            const user = {
              ...data,
              scope: 'company'
            }
            User.setData(user)

            this.props.onRegister()
          } else {
            message.error(error.message);
          }
        } else {
          this.setState({
            flag: true
          })
        }
      }
    });
  }

  private setPass(e: any) {
    this.setState({
      flag: false
    })
  }

  private change() {
    const { onChange } = this.props

    if (onChange) {
      onChange()
    }
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { httpBusy } = this.props.register;
    const { flag } = this.state

    return (
      <Form
        className="login-form"
        onSubmit={this.handleSubmit}
      >
        <Form.Item>
          {getFieldDecorator('legal_name', {
            rules: [
              {
                required: true,
                message: Lang.get('register.validations.legal_name')
              }],
          })(
            <Input
              placeholder={Lang.get('register.legal_name')} />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('name', {
            rules: [{
              required: true,
              message: Lang.get('register.validations.trade_name')
            }],
          })(
            <Input
              placeholder={Lang.get('register.trade_name')} />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('rfc', {
            rules: [{
              required: true,
              message: Lang.get('register.validations.rfc')
            }],
          })(
            <Input
              placeholder="RFC" />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('email', {
            rules: [{
              required: true,
              message: Lang.get('register.validations.email')
            }],
          })(
            <Input
              placeholder={Lang.get('register.email')} />
          )}
        </Form.Item>
        <Form.Item
          validateStatus={flag ? 'error' : undefined}
          help={flag ? <Text style={{ color: 'red' }}>
            {Lang.get('register.info_pass')}
            <br />
            {Lang.get('register.info_pass2')}
            <br />
            {Lang.get('register.info_pass3')}
            <br />
            {Lang.get('register.info_pass4')}
            <br />
            {Lang.get('register.info_pass5')}
          </Text> : ''}
        >
          {getFieldDecorator('password', {
            rules: [{
              required: true,
              message: Lang.get('register.validations.password')
            }],
          })(
            <Input.Password
              type="password"
              name="password"
              placeholder={Lang.get('register.password')}
              onChange={this.setPass} />,
          )}
        </Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          className="login-form-button"
          loading={httpBusy}
        >
          {Lang.get('register.button.register')}
        </Button>
        <div style={{ display: 'flex', 'justifyContent': 'space-between', marginTop: 40 }}>
          <p>{}</p>
          <p>
          {Lang.get('register.have_account')}
           <a style={{color: '#3babd4'}} onClick={this.change}> {Lang.get('register.login')}</a>
          </p>
          <p>{}</p>
        </div>
      </Form>
    )
  }
}

const container = {
  language: LanguageContainer,
  register: RegisterContainer,
  signin: SignInContainer
}

const mapStateToProps = (containers: any) => {
  return {
    language: containers.language.state,
    register: containers.register.state,
    signin: containers.signin.state
  }
}

const WrappedRegisterForm: any = Form.create()(RegisterCompanyForm);

export default connect(container, mapStateToProps)(WrappedRegisterForm);