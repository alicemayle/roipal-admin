import { Container } from 'unstated';
import LanguageState from '../models/LanguageState';
import Lang from '../../support/Lang';
import ValidateLanguage from '../../support/ValidateLanguage';

const initialState = {
  data: null,
  selected: Lang.getLocale()
}

class LanguageContainer extends Container<LanguageState> {
  public state: LanguageState = {
    ...initialState,
  }

  public setLanguage(value: string) {
    this.setState({
      selected: value
    })

    ValidateLanguage.setLanguage(value)
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }
}

export default LanguageContainer;