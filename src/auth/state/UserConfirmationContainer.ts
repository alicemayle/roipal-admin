import { Container } from 'unstated';
import UserConfirmationService from '../services/UserConfirmationService';

const State = {
  httpBusy: false,
  message: null,
  data: null,
  error: null,
}

class UserConfirmationContainer extends Container<any> {
  public state: any = {
    ...State
  };

  public name: string = 'User Confirmation Container ';

  public async validateUser(params: any) {
    try {
      await this.setState({
        __action: 'User Confirmation validate starts'
      })

      const response = await UserConfirmationService.validateUser(params);

      this.setState({
        data: response.data,
        __action: 'User Confirmation validate success'
      });
    } catch (error) {
      this.setState({
        error,
        message: error.message,
        __action: 'User Confirmation validate error'
      });
    }
  }

  public async updateUser(data: any) {
    try {
      await this.setState({
        httpBusy: true,
        __action: 'User Confirmation update starts'
      })

      const response = await UserConfirmationService.updateUser(data);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'User Confirmation update success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'User Confirmation update error'
      });
    }
  }
}

export default UserConfirmationContainer;