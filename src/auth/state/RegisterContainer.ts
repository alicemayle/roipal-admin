import { Container } from 'unstated';
import RegisterService from '../services/RegisterService';

const State = {
  httpBusy: false,
  message: null,
  data: null,
  error: null,
}

class RegisterContainer extends Container<any> {
  public state: any = {
    ...State
  };

  public name: string = 'Register';

  public async register(data: any) {
    try {
      await this.setState({
        httpBusy: true,
        __action: 'Register company starts'
      })

      const response = await RegisterService.register(data);

        this.setState({
          httpBusy: false,
          data: response.data,
          __action: 'Register company success'
        });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Register company error'
      });
    }
  }
}

export default RegisterContainer;