import { Container } from 'unstated';

import Auth from '../services/Auh';
import { Queue } from '../../shared';
import SignInState from '../models/SignInState'

const State = {
  httpBusy: false,
  message: null,
  data: null,
  error: null,
}

class SignInContainer extends Container<SignInState> {
  public state: SignInState = {
    ...State
  };

  public name: string = 'SignIn';

  public persist = {
    key: 'login'
  }

  public async login(credentials: any) {
    let result: any;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Sign in starts'
      })

      result = await Auth.login(credentials)
      const _groups: any[] = []
      const newData = {
        ...result.data,
        scope: credentials.scope
      }

      if (result.data.roles) {
        result.data.roles.map((role: any) => {
          const groupFound = _groups.find((item: string) => item === role.group)
  
          if(!groupFound) {
            _groups.push(role.group)
          }
        });
  
        newData.groups = _groups
      }

      await this.setState({
        data: newData,
        __action: 'Sign in success'
      })
    } catch (error) {
      result = { error }

      await this.setState({
        error,
        __action: 'Sign in error'
      })
    } finally {
      if (result.data || (result.error && !result.error.errors)) {
        Queue.next(() => {
          this.setState({
            httpBusy: false,
            error: null,
            message: null,
            __action: 'Sign in ends ' + (result.error ? 'with error' : '')
          })
        })
      } else {
        Queue.next(() => {
          this.setState({
            httpBusy: false,
            message: null,
            __action: 'Sign in ends ' + (result.error ? 'with error' : '')
          })
        })
      }
    }
  }

  public setProfile(_profile: any) {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        profile: _profile
      },
      __action: 'Set profile sigin'
    })
  }

  public setData(_data: any) {
    this.setState({
      ...this.state,
      data: _data,
      __action: 'Set data sigin'
    })
  }

}

export default SignInContainer;