import { Container } from 'unstated';
import reset from '../services/ResetPasswordService';

const State = {
  httpBusy: false,
  message: null,
  data: null,
  error: null,
}

class PasswordContainer extends Container<any> {
  public state: any = {
    ...State
  };

  public name: string = 'Reset Password Container ';

  public async resetPassword(data: any) {
    try {
      await this.setState({
        httpBusy: true,
        __action: 'Reset password starts'
      })

      const response = await reset.resetPassword(data);

        this.setState({
          httpBusy: false,
          data: response.data,
          __action: 'Reset password success'
        });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Reset password error'
      });
    }
  }
}

export default PasswordContainer;