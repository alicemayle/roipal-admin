import Http from '../shared/Http';
import { AxiosResponse } from 'axios';
import LocalForage from 'localforage';
import { Guardian, User } from '@xaamin/guardian'

class Customer {
  private _isAuthenticated: boolean
  private data: any

  constructor() {
    this._isAuthenticated = false;
  }

  public async configure() {
    await LocalForage.getItem('persist::roipal-pwa').then((value) => {
      const response: any = value;
      const login = JSON.parse(response);
      if (login && login.login) {
        this.data = login.login.data;
        this.setData(this.data);
      }
    });
  }

  public async setData(data: any) {
    const dataUser = data || {}
    const _profile = dataUser.profile || {}
    const _roles = dataUser.roles || []
    const _permissions = dataUser.permissions || []
    const _groups: any[] = []

    const roles = _roles.map((role: any) => {
      const _role = {
        group: role.group,
        name: role.name,
        role: role.name
      }

      const groupFound = _groups.find((item: string) => item === role.group)

      if(!groupFound) {
        _groups.push(role.group)
      }
      return _role
    });

    const permissions = _permissions.map((permission: any) => {
      const _permission = {
        group: permission.group,
        granted: true,
        permission: permission.definition
      }

      return _permission
    });

    const user = new User({
      id: _profile.uuid,
      name: _profile.name,
      email: _profile.email,
      roles: [...roles],
      permissions: [...permissions]
    })

    this.data = {
      ...dataUser,
      groups: _groups
    }
    Guardian.setUser(user);
  }

  public attempt(credentials: any): Promise<AxiosResponse> {
    return Http.post('/oauth/token', credentials);
  }

  public isAuthenticated() {
    this._isAuthenticated = this.data ? this.data.token : null;

    return this._isAuthenticated;
  }

  public getUser(): any | null {
    if (this.isAuthenticated()) {
      return this.data.user;
    }

    throw new Error('User is not logged in');

  }

  public getData() {
    return this.data
  }

  public getUserId(): string {
    return this.data.user.id;
  }

  public getToken(): any | null {
    if (this.isAuthenticated()) {
      return this.data.token;
    }

    throw new Error('User is not logged in');
  }

  public getAccessToken(throwError: boolean = true): string | null {
    if (this.isAuthenticated()) {
      return this.data.token.access_token;
    }

    return null;
  }

  public logout() {
    this.data = null;
    this._isAuthenticated = false;

    LocalForage.removeItem('persist::roipal-pwa').then(() => {
      this.data = null;
      this.setData(this.data);
      this._isAuthenticated = false;
    });
    LocalForage.clear();
  }
}

export default new Customer();