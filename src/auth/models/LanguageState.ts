interface LanguageState {
  data?: any,
  selected?: string,
  __action?: string
}

export default LanguageState;