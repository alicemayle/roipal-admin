import { ResponseError } from '../../roipal/models'

interface SignInState {
  httpBusy?: boolean,
  message: string | null,
  data?: any,
  error?: ResponseError | null,
  __action?: string
}

export default SignInState;