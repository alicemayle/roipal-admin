import React, { Component } from 'react';
import { Row, Col } from 'antd';
import Lang from '../../support/Lang';
import './Home.scss';
import { Gate } from '@xaamin/guardian';
import MenuButton from '../../components/menuButton/MenuButton'

class Home extends Component<any> {

  constructor(props: any) {
    super(props);
  }

  public render() {
    const permissionConfiguration = Gate.allows('configuration::global-page-access')
      || Gate.allows('commercial-need::page-access')
      || Gate.allows('type-sale::page-access')
      || Gate.allows('company-commission::page-access')
      ? true : false

    const permissionAccess = Gate.allows('user::page-access')
      || Gate.allows('role::page-access')
      ? true : false

    const contentPayment =
      <p>
        <p>{Lang.get('payments.payment.content')}</p>
        <p>{Lang.get('payments.payment.pay')}</p>
        <p>{Lang.get('payments.payment.verify')}</p>
      </p>

    const contentMission =
      <p>
        <p>{Lang.get('buses.mission.content')}</p>
        <p>{Lang.get('buses.mission.add.title')}</p>
      </p>

    const contentAccess =
      <p>
        <p>{Lang.get('access.user.list')}</p>
        <p>{Lang.get('access.user.upsert')}</p>
        <p>{Lang.get('access.role.list')}</p>
        <p>{Lang.get('access.role.upsert')}</p>
      </p>

    const contentLocations =
      <p>
        <p>{Lang.get('locations.location.content')}</p>
        <p>{Lang.get('locations.location.content_2')}</p>
      </p>

    return (
      <div style={{ padding: '30px', minHeight: '100vH', backgroundColor: '#eff4f4' }}>
        <Row gutter={40} type="flex" justify={'center'}
          style={{ margin: 30, justifyContent: 'center' }}>
          {
            Gate.allows('company::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('company.title')}
                icon={'global'}
                link={'/companies'}
                title={Lang.get('company.content')}
              />
            </Col>
          }
          {
            Gate.allows('executive::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('users.user.title_menu')}
                icon={'team'}
                link={'/users'}
                title={Lang.get('users.user.content')}
              />
            </Col>
          }
          {
            Gate.allows('location::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('locations.location.menu')}
                icon={'environment'}
                link={'/locations'}
                title={contentLocations}
              />
            </Col>
          }
          {
            Gate.allows('mission::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('buses.mission.titleTab')}
                icon={'reconciliation'}
                link={'/missions'}
                title={contentMission}
              />
            </Col>
          }
          {
            Gate.allows('invitation::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('invitation.title')}
                icon={'mail'}
                link={'/invitations'}
                title={Lang.get('invitation.content')}
              />
            </Col>
          }
          {
            Gate.allows('invoice::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('charges.charge.menu')}
                icon={'dollar'}
                link={'/charges'}
                title={Lang.get('charges.charge.content')}
              />
            </Col>
          }
          {
            Gate.allows('executive-payment::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('payments.payment.menu')}
                icon={'calculator'}
                link={'/payment'}
                title={contentPayment}
              />
            </Col>
          }
          {
            Gate.allows('assessment::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('assesments.title_menu')}
                icon={'ordered-list'}
                link={'/assestments-disc'}
                title={Lang.get('assesments.content')}
              />
            </Col>
          }
          {
            Gate.allows('translation::page-access') &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('translations.title')}
                icon={'flag'}
                link={'/languages'}
                title={Lang.get('translations.content')}
              />
            </Col>
          }
          {
            permissionConfiguration &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('parametrization.title')}
                icon={'control'}
                link={'/parametrization'}
                title={Lang.get('parametrization.content')}
              />
            </Col>
          }
          {
            permissionAccess &&
            <Col span={6}>
              <MenuButton
                button_text={Lang.get('access.title')}
                icon={'idcard'}
                link={'/access'}
                title={contentAccess}
              />
            </Col>
          }
          <Col span={6}>
            <MenuButton
              button_text={Lang.get('setting.settings.title')}
              icon={'setting'}
              link={'/settings'}
              title={Lang.get('setting.settings.content')}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export default Home;