import { Container } from 'unstated';
import CommissionUpsertState from '../models/CommissionUpsertState';
import ParametrizationService from '../services/Parametrization';
import Event from '../../../support/Event';
import Lang from '../../../support/Lang';

const initialState = {
  httpBusy: false,
  data: null,
  error: null,
  message: '',
  companies: null,
  commissions: null
}

class CommissionUpsertContainer extends Container<CommissionUpsertState> {
  public state: CommissionUpsertState = {
    ...initialState
  }

  public name: string = 'ParametrizationCommissionUpsert';

  public async upsert(data: {
    company: {
      business_name?: string,
      uuid: string
    },
    name?: string,
    value: number,
    type: string,
    uuid?: string,
    commission_uuid?: string,
    commission_name?: string,
    company_uuid: string
  }) {
    let result;
    try {
      this.setState({
        httpBusy: true,
        __action: 'Parametrization commission Upsert Starts'
      });

      let response;

      if (data.uuid) {
        const _data = {
          value: data.value,
          type: data.type
        }
        response = await ParametrizationService.updateCommission(data.uuid, _data);
      } else {
        const _data = {
          value: data.value,
          type: data.type,
          commission_uuid: data.commission_uuid,
          commission_name: data.name,
        }

        if (data.commission_uuid === '0') {
          delete _data.commission_uuid
        }

        response = await ParametrizationService.createCommission(data.company_uuid, _data);
      }
      result = {
        message: Lang.get('parametrization.commissions.success')
      }

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Parametrization commission Upsert Success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Parametrization commission Upsert Error'
      })

      result = {
        error,
      }
    }
    Event.notify('parametrizationCommission::upsert', result);
  }

  public async getCommissions(params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Parametrization commissions list starts'
      });

      const response = await ParametrizationService.getCommissionsSelect(params);

      const other = {
        name: Lang.get('parametrization.commissions.other'),
        uuid: '0'
      }

      response.data.push(other)

      this.setState({
        httpBusy: false,
        commissions: response.data,
        __action: 'Parametrization commissions list success'
      });

    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Parametrization commissions list error'
      });
    }
  }

  public async getCompanies(params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Parametrization companies list starts'
      });

      const response = await ParametrizationService.getCompaniesSelect(params);

      this.setState({
        httpBusy: false,
        companies: response.data,
        __action: 'Parametrization companies list success'
      });

    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Parametrization companies list error'
      });
    }
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: ''
    })
  }

  public reset() {
    this.setState({
      ...initialState
    })
  }
}

export default CommissionUpsertContainer;