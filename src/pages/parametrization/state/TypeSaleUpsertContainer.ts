import { Container } from 'unstated';
import ListProps from '../models/ListProps'
import ParametrizationService from '../services/Parametrization';
import Event from '../../../support/Event';
import Lang from 'src/support/Lang';

const initialState = {
  httpBusy: false,
  data: null,
  error: null,
  message: ''
}

class TypeSaleUpsertContainer extends Container<ListProps> {
  public state: ListProps = {
    ...initialState
  }

  public name: string = 'ParametrizationTypeSaleUpsert';

  public async upsert(data: {
    uuid: string,
    bonus_minimum: number
    zone_A: number
    zone_B: number
    zone_C: number
  }) {
    let result;
    try {
      this.setState({
        httpBusy: true,
        __action: 'Parametrization Type Sale Upsert Starts'
      });

      let response;

      response = await ParametrizationService.updateTypeSale(data.uuid, data);
      result = {
        message: Lang.get('parametrization.types_sales.success')
      }

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Parametrization Type Sale Upsert Success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Parametrization Type Sale Upsert Error'
      })

      result = {
        error,
      }
    }
    Event.notify('parametrizationTypeSale::upsert', result);
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: ''
    })
  }

  public reset() {
    this.setState({
      ...initialState
    })
  }
}

export default TypeSaleUpsertContainer;