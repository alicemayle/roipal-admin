import { Container } from 'unstated';

import ListProps from '../models/ListProps';
import ParametrizationService from '../services/Parametrization'

const initialState = {
  httpBusy: false,
  message: '',
  data: null,
  error: null,
  meta: null
}

class CommercialListContainer extends Container<ListProps> {
  public state: ListProps = {
    ...initialState
  };

  public name: string = 'ParametrizationCommercialList';

  public async fetch(params: any) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Parametrization Commercial Needs list starts'
      });

      const response = await ParametrizationService.getCommercialNeeds(params);

      response.data.pop()

      const newResponse:{} = response.data.map((data:{ uuid:string, key: string}) => {
        data = { ...data }
        data.key = data.uuid;
        return data;
      })

      this.setState({
        httpBusy: false,
        data: newResponse,
        meta: response.meta,
        __action: 'Parametrization Commercial Needs list success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Parametrization Commercial Needs list error'
      });
    }
  }
}

export default CommercialListContainer;