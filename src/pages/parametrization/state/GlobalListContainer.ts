import { Container } from 'unstated';

import ListProps from '../models/ListProps';
import ParametrizationService from '../services/Parametrization';

const initialState = {
  httpBusy: false,
  message: '',
  data: null,
  error: null,
  meta: null
}

class GlobalListContainer extends Container<ListProps> {
  public state: ListProps = {
    ...initialState
  };

  public name: string = 'ParametrizationGlobalList';

  public async fetch(params: any) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Parametrization Global list starts'
      });

      const response = await ParametrizationService.getGlobal(params);

      this.setState({
        httpBusy: false,
        data: response.data,
        meta: response.meta,
        __action: 'Parametrization Global list success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Parametrization Global list error'
      });
    }
  }

  public reset() {
    this.setState({
      httpBusy: false,
      data: null,
      error: null,
      message: '',
      __action: 'Parametrization Global list reset'
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Parametrization Global list clear error'
    });
  }
}

export default GlobalListContainer;