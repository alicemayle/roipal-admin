import { Container } from 'unstated';
import ListProps from '../models/ListProps';
import ParametrizationService from '../services/Parametrization';
import Event from '../../../support/Event';
import Lang from 'src/support/Lang';

const initialState = {
  httpBusy: false,
  data: null,
  error: null,
  message: ''
}

class GlobalUpsertContainer extends Container<ListProps> {
  public state: ListProps = {
    ...initialState
  }

  public name: string = 'ParametrizationGlobalUpsert';

  public async upsert(data: {
    uuid?: string
    key: string,
    value: number,
    type: string
  }) {
    let result;
    try {
      this.setState({
        httpBusy: true,
        __action: 'Parametrization global Upsert Starts'
      });

      let response;

      if (data.uuid) {
        response = await ParametrizationService.updateGlobal(data.uuid, data);
      } else {
        response = await ParametrizationService.createGlobal(data);
      }
      result = {
        message: Lang.get('parametrization.global.success')
      }

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Parametrization global Upsert Success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Parametrization global Upsert Error'
      })

      result = {
        error,
      }
    }
    Event.notify('parametrizationGlobal::upsert', result);
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: ''
    })
  }

  public reset() {
    this.setState({
      ...initialState
    })
  }
}

export default GlobalUpsertContainer;