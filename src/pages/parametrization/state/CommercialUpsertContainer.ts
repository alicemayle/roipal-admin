import { Container } from 'unstated';
import ListProps from '../models/ListProps'
import ParametrizationService from '../services/Parametrization';
import Event from '../../../support/Event';
import Lang from 'src/support/Lang';

const initialState = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
}

class CommercialUpsertContainer extends Container<ListProps> {
    public state: ListProps = {
        ...initialState
    }

    public name: string = 'ParametrizationCommercialUpsert';

    public async upsert(data: {
        uuid: string
        min_time: number,
        time_unit: string,
        time_unit_cod: string
      }) {
        let result;
        try {
          this.setState({
            httpBusy: true,
            __action: 'Parametrization Commercial Need Upsert Starts'
          });
    
          let response;
    
            response = await ParametrizationService.updateCommercial(data.uuid, data);
          result = {
            message: Lang.get('parametrization.commercial_needs.success')
          }
    
          this.setState({
            httpBusy: false,
            data: response.data,
            __action: 'Parametrization Commercial Need Upsert Success'
          });
        } catch (error) {
          this.setState({
            httpBusy: false,
            error,
            __action: 'Parametrization Commercial Need Upsert Error'
          })
    
          result = {
            error,
          }
        }
        Event.notify('parametrizationCommercialNeed::upsert', result);
      }

    public clearError() {
        this.setState({
            ...this.state,
            error: null,
            message: ''
        })
    }

    public reset() {
        this.setState({
            ...initialState
        })
    }
}

export default CommercialUpsertContainer;