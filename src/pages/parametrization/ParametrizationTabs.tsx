import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import { Tabs } from 'antd';

import Lang from '../../support/Lang';
import GlobalList from './components/GlobalList';
import CommercialList from './components/CommercialList';
import CommissionList from './components/CommissionList';
import TypeSaleList from './components/TypeSaleList';
import { Gate } from '@xaamin/guardian';

const { TabPane } = Tabs;


class ParametrizationTabs extends Component<any> {

  constructor(props: any) {
    super(props);
    autobind(this);
  }

  public render() {
    Logger.count(this)
    return (
      <div>
        <h1>{Lang.get('parametrization.title')}</h1>
        <Tabs defaultActiveKey="1">
          {
            Gate.allows('configuration::global-page-access') &&
            <TabPane tab={Lang.get('parametrization.global.title')} key="1">
              <GlobalList />
            </TabPane>
          }
          {
            Gate.allows('commercial-need::page-access') &&
            <TabPane tab={Lang.get('parametrization.commercial_needs.title')} key="2">
              <CommercialList />
            </TabPane>
          }
          {
            Gate.allows('type-sale::page-access') &&
            <TabPane tab={Lang.get('parametrization.types_sales.title')} key="3">
              <TypeSaleList />
            </TabPane>
          }
          {
            Gate.allows('company-commission::page-access') &&
            <TabPane tab={Lang.get('parametrization.commissions.title')} key="4">
              <CommissionList />
            </TabPane>
          }
        </Tabs>
      </div>
    )
  }
}

export default (ParametrizationTabs);