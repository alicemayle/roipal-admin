import { Http } from '../../../shared';

class Parametrization {

  public getGlobal( params: any) : any {
    return Http.get(`/api/configuration/`, { params })
  }

  public createGlobal(data: any) {
    return Http.post(`/api/configuration/`, data);
  }

  public updateGlobal(uuid: string, data: any) {
    return Http.put(`/api/configuration/${uuid}`, data);
  }

  public getCommercialNeeds( params: any) : any {
    return Http.get(`api/commercial-needs/?includes=group`, { params })
  }

  public updateCommercial(uuid: string, data: any) {
    return Http.put(`/api/commercial-need-group/${uuid}`, data);
  }

  public getCommissions( params: any): any {
    return Http.get(`/api/company-commission?includes=company`, { params })
  }

  public updateCommission(uuid: string, data: any) {
    return Http.put(`/api/company-commission/${uuid}`, data);
  }

  public createCommission(uuid: string, data: any) {
    return Http.post(`/api/company-commission/${uuid}/commission`, data);
  }

  public getCommissionsSelect( uuid: string) {
    return Http.get(`/api/commission/${uuid}/does-not-have`)
  }

  public getCompaniesSelect( params: any) {
    return Http.get(`/api/companies`, { params })
  }

  public getTypesSales( params: any) : any {
    return Http.get(`/api/commercial-type-sale`, { params })
  }

  public updateTypeSale(uuid: string, data: any) {
    return Http.put(`/api/commercial-type-sale/${uuid}`, data);
  }

}

export default new Parametrization;