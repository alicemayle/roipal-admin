interface GlobalState {
  openUpsert: boolean
  record?: {
    key?: string,
    value?: number,
    type?: string,
    uuid?: string,
  }
  searchText: string,
  fields: any,
  limit: number,
  page: number
}

export default GlobalState;