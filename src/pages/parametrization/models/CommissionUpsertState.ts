interface CommissionUpsertState {
    data?: any,
    httpBusy?: boolean,
    error?: any   
    message?: string,
    __action?: string,
    meta?: any,
    containers?: any,
    companies?: any,
    commissions?: any
}

export default CommissionUpsertState;