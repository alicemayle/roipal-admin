interface GlobalUpsertProps {
  openUpsert: boolean
  record?: {
    key?: string,
    value?: number,
    type?: string,
    uuid?: string,
  }
  onOpenUpsert?: (record: {}) => void,
  onClose: () => void,
  onChange: (value:string, field: string) =>void,
  onComplete: (type: string) =>void,
  containers?: any
}

export default GlobalUpsertProps;