interface CommissionUpsertProps {
  openUpsert: boolean
  record?: {
    company?: {
      business_name?: string,
      uuid?: string
    },
    name?: string,
    value?: number,
    type?: string,
    uuid?: string,
  }
  onOpenUpsert?: (record: {}) => void,
  onClose: () => void,
  onChange: (value: string, field: string) => void,
  onChangeCompany: (value: string, field: string) => void,
  onComplete: (type: string) => void,
  commissions?: any,
  companies?: any,
  containers?: any
}

export default CommissionUpsertProps;