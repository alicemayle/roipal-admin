interface CommissionState {
    openUpsert: boolean
    record?: {
      company?: {
        business_name?: string,
        uuid?: string
      },
      name?: string,
      value?: number,
      type?: string,
      uuid?: string,
      commission_uuid?: string,
      commission_name?: string
    }
    searchText: string,
    fields: any,
    commissions?: any,
    companies?: any,
    limit: number,
    page: number
  }
  
  export default CommissionState;