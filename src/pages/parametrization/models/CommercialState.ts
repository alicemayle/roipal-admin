interface CommercialState {
  openUpsert: boolean
  record: {
    commercial_need?: string,
    group?: {
      uuid?: string,
      time_unit?: string,
      time_unit_cod?: string,
      min_time?: number,
    }
    uuid: string,
  }
  searchText: string,
  fields: any,
  limit: number,
  page: number
}

export default CommercialState;