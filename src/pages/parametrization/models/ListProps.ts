interface ListProps {
    data?: any,
    httpBusy?: boolean,
    error?: any   
    message?: string,
    __action?: string,
    meta?: any,
    containers?: any,
}

export default ListProps;