interface CommercialUpsertProps {
  openUpsert: boolean
  record?: {
    commercial_need?: string,
    group?: {
      time_unit?: string,
      time_unit_cod?: string,
      min_time?: number,
      uuid?: string
    }
    uuid?: string,
  }
  onOpenUpsert?: (record: {}) => void,
  onClose: () => void,
  onChange: (value: string, field: string) => void,
  onChangeTime: (value: string, field: string) => void,
  onComplete: (type: string) =>void,
  containers?: any
}

export default CommercialUpsertProps;