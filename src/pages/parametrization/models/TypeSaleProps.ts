interface TypeSaleProps {
  openUpsert: boolean
  record?: {
    uuid: string,
    bonus_minimum: number
    description?: string,
    type: string
    zone_A: number
    zone_B: number
    zone_C: number
  }
  onOpenUpsert?: (record: {}) => void,
  onClose: () => void,
  onChange: (value: string, field: string) => void,
  onComplete: (type: string) => void,
  containers?: any
}

export default TypeSaleProps;