interface TypeSaleState {
    openUpsert: boolean
    record: {
      uuid: string,
      bonus_minimum: number
      description?: string,
      type: string
      zone_A: number
      zone_B: number
      zone_C: number
    }
    searchText: string,
    fields: any,
    limit: number,
    page: number
  }
  
  export default TypeSaleState;