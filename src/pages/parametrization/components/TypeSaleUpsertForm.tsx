import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import Lang from '../../../support/Lang';
import { Form, Input, InputNumber } from 'antd';

class TypeSaleUpsertForm extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }

  private handleChange(e: any) {
    const value = e.target.value;
    const field = e.target.name;

    this.props.onChange(value, field)
  }

  private handleChangeZoneA(value: number) {
    this.props.onChange(value, 'zone_A')
  }

  private handleChangeZoneB(value: number) {
    this.props.onChange(value, 'zone_B')
  }

  private handleChangeZoneC(value: number) {
    this.props.onChange(value, 'zone_C')
  }

  private handleChangeBonus(value: number) {
    this.props.onChange(value, 'bonus_minimum')
  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { record } = this.props;
    return (
      <Form  >
        <Form.Item label={Lang.get('parametrization.types_sales.name')}>
          {getFieldDecorator('name', {
            initialValue: record.type,
          })(
            <Input
              placeholder={Lang.get('parametrization.types_sales.name')}
              name="type"
              disabled={record.uuid ? true : false}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.types_sales.zone_A')}>
          {getFieldDecorator('zone_A', {
            initialValue: record.zone_A,
            rules: [{
              required: true,
              message: Lang.get('parametrization.types_sales.validations.zone_A')
            }],
          })(
            <InputNumber
              placeholder={Lang.get('parametrization.types_sales.zone_A')}
              name="zone_A"
              onChange={this.handleChangeZoneA}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.types_sales.zone_B')}>
          {getFieldDecorator('zone_B', {
            initialValue: record.zone_B,
            rules: [{
              required: true,
              message: Lang.get('parametrization.types_sales.validations.zone_B')
            }],
          })(
            <InputNumber
              placeholder={Lang.get('parametrization.types_sales.zone_B')}
              name="zone_B"
              onChange={this.handleChangeZoneB}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.types_sales.zone_C')}>
          {getFieldDecorator('zone_C', {
            initialValue: record.zone_C,
            rules: [{
              required: true,
              message: Lang.get('parametrization.types_sales.validations.zone_C')
            }],
          })(
            <InputNumber
              placeholder={Lang.get('parametrization.types_sales.zone_C')}
              name="zone_C"
              onChange={this.handleChangeZoneC}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.types_sales.bonus')}>
          {getFieldDecorator('bonus_minimum', {
            initialValue: record.bonus_minimum,
            rules: [{
              required: true,
              message: Lang.get('parametrization.types_sales.validations.bonus')
            }],
          })(
            <InputNumber
              placeholder={Lang.get('parametrization.types_sales.bonus')}
              name="bonus_minimum"
              onChange={this.handleChangeBonus}
            />
          )}
        </Form.Item>

      </Form>
    );
  }
}

const WrapperTypeSaleUpsertForm: any = Form.create({
  name: 'TypeSaleUpsertForm'
})(TypeSaleUpsertForm);

export default WrapperTypeSaleUpsertForm;