import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import Lang from '../../../support/Lang';
import { Form, Input, InputNumber, Select } from 'antd';
// import {Logger} from 'unstated-enhancers';

const { Option } = Select;

class CommissionUpsertForm extends PureComponent<any, {showName: boolean}> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
      showName: false
    }
  }

  private handleChange(e: any) {
    const value = e.target.value;
    const field = e.target.name;

    this.props.onChange(value, field)
  }

  private handleChangeValue(value: number) {
    this.props.onChange(value, 'value')
  }

  private handleChangeType(value: string) {
    this.props.onChange(value, 'type')
  }

  private handleChangeCommission(value: string) {
    this.props.onChange(value, 'commission_uuid')

    if (value === '0') {
      this.setState({
        showName: true
      })
    } else {
      this.setState({
        showName: false
      })
    }
  }

  private handleChangeCompany(value: string) {
    this.props.onChangeCompany(value, 'company_uuid')
  }

  private renderOptionsCommissions(item: any) {
    return <Option key={item.uuid} value={item.uuid}>{item.name}</Option>
  }

  private renderOptionsCompanies(item: any) {
    return <Option key={item.uuid} value={item.uuid}>{item.business_name}</Option>
  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { record, commissions, companies } = this.props;

    return (
      <Form>
        <Form.Item label={Lang.get('parametrization.commissions.company')}>
          {getFieldDecorator('business_name', {
            initialValue: record.company.uuid,
            rules: [{
              required: true,
              message: Lang.get('parametrization.commissions.validations.company')
            }],
          })(
            <Select
              placeholder={Lang.get('parametrization.commissions.company')}
              disabled={record.uuid ? true : false}
              onChange={this.handleChangeCompany}
            >
              {companies.map(this.renderOptionsCompanies)}
            </Select>
          )}
        </Form.Item>
        
        { 
          !record.uuid && commissions &&     
          <Form.Item label={Lang.get('parametrization.commissions.commission')}>
            {getFieldDecorator('commission_uuid', {
              initialValue: record.commission_uuid,
              rules: [{
                required: true,
                message: Lang.get('parametrization.commissions.validations.commission')
              }],
            })(
              <Select
                placeholder={Lang.get('parametrization.commissions.commission')}
                onChange={this.handleChangeCommission}
              >
                {commissions.map(this.renderOptionsCommissions)}
              </Select>
            )}
          </Form.Item>
        }

        { 
          (record.uuid || this.state.showName) &&    
          <Form.Item label={Lang.get('parametrization.commissions.name')}>
            {getFieldDecorator('name', {
              initialValue: record.name,
              rules: [{
                required: true,
                message: Lang.get('parametrization.commissions.validations.name')
              }],
            })(
              <Input
                placeholder={Lang.get('parametrization.commissions.name')}
                name="name"
                disabled={record.uuid ? true : false}
                onChange={this.handleChange}
              />
            )}
          </Form.Item>
        }

        <Form.Item label={Lang.get('parametrization.commissions.value')}>
          {getFieldDecorator('value', {
            initialValue: record.value,
            rules: [{
              required: true,
              message: Lang.get('parametrization.commissions.validations.value')
            }],
          })(
            <InputNumber
              placeholder={Lang.get('parametrization.commissions.value')}
              name="value"
              onChange={this.handleChangeValue}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.commissions.type')}>
          {getFieldDecorator('type', {
            initialValue: record.type,
            rules: [{
              required: true,
              message: Lang.get('parametrization.commissions.validations.type')
            }],
          })(
            <Select
              placeholder={Lang.get('parametrization.commissions.type')}
              onChange={this.handleChangeType}
            >
              <Option value="%">{Lang.get('parametrization.commissions.percentage')}</Option>
              <Option value="F">{Lang.get('parametrization.commissions.landline')}</Option>
            </Select>
          )}
        </Form.Item>
      </Form>
    );
  }
}

const WrapperCommissionUpsertForm: any = Form.create({
    name: 'CommissionUpsertForm'
})(CommissionUpsertForm);

export default WrapperCommissionUpsertForm;