import React, { PureComponent } from 'react';
import { Modal, Button, message } from 'antd';
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';

import CommercialUpsertContainer from '../state/CommercialUpsertContainer';
import CommercialUpsertProps from '../models/CommercialUpsertProps';
import CommercialUpsertForm from './CommercialUpsertForm';
import Lang from 'src/support/Lang';
import Event from '../../../support/Event';
import Notifications from '../../../components/notifications/notifications';
import { Gate } from '@xaamin/guardian';
class CommercialUpsert extends PureComponent<CommercialUpsertProps> {
  public form: any = null;
  constructor(props: CommercialUpsertProps) {
    super(props);
    autobind(this);
    Event.listen('parametrizationCommercialNeed::upsert', this.listeForUpsert);
  }

  private handleClose() {
    this.props.onClose();
  }

  private handleChange(value: string, field: string) {
    this.props.onChange(value, field);
  }

  private handleChangeTime(value: string, field: string) {
    this.props.onChangeTime(value, field);
  }

  public handleSubmit() {
    this.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        if(Gate.allows('commercial-need::edit')) {
          const { upsert } = this.props.containers;
          const { record } = this.props;
          let data;

          if (record && record.group) {
            data = {
              uuid: record.group.uuid,
              time_unit: record.group.time_unit,
              time_unit_cod: record.group.time_unit_cod,
              min_time: record.group.min_time
            }
          }
          upsert.upsert(data);
        } else {
          message.error(Lang.get('generic.policies.denied'));
        }
      }
    });
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.props.onComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.props.onComplete('error');
    }

    notification.openNotification(msj)
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  public componentWillUnmount() {
    Event.remove('parametrizationCommercialNeed::upsert', this.listeForUpsert);
  }

  public render() {
    const { record, openUpsert } = this.props;
    const { httpBusy } = this.props.containers.upsert

    return (
      <Modal
        title={Lang.get('parametrization.button.edit')}
        style={{ top: 20 }}
        visible={openUpsert}
        centered={true}
        onCancel={this.handleClose}
        destroyOnClose={true}
        footer={[
          <Button
            key="cancel"
            onClick={this.handleClose}>
            {Lang.get('parametrization.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={httpBusy}
            onClick={this.handleSubmit}>
            {Lang.get('parametrization.button.accept')}
          </Button>
        ]}
      >
        <CommercialUpsertForm
          record={record}
          onChange={this.handleChange}
          onChangeTime={this.handleChangeTime}
          onValidate={this.handleValidateForm}
        />
      </Modal>
    );
  }
}

const container = {
  upsert: CommercialUpsertContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    upsert: containers.upsert.state,
  }
}

export default connect(container, mapStateToProps)(CommercialUpsert);