import React, { Component } from 'react';
import { Table, Button, Icon } from 'antd';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';

import ListProps from '../models/ListProps';
import CommissionListContainer from '../state/CommissionListContainer';
import CommissionState from '../models/CommissionState'
import Lang from '../../../support/Lang';
import CommissionUpsert from './CommissionUpsert';
import FilterInterface from '../../../components/search/FilterInterface';
import FilterSearch from '../../../components/search/FilterSearch';
import Pagination from '../../../components/pagination/Pagination'
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';

const { Column } = Table;
class CommissionList extends Component<ListProps, CommissionState> {
  public searchInput: any = null;
  public searchField: string = '';
  constructor(props: ListProps) {
    super(props);
    autobind(this);
  }

  public state: CommissionState = {
    searchText: '',
    openUpsert: false,
    record: {
      company: {
        business_name: '',
        uuid: ''
      },
      name: '',
      value: 0,
      type: '',
    },
    fields: [],
    limit: 50,
    page: 1
  }

  private renderActions(record: any) {
    return (
      <IconButton
        item={record}
        onFuction={this.handleOpenUpsert}
        size={18}
        colorIcon={'#29a3a3'}
        name={'edit'}
      />
    );
  }

  private handleOpenUpsert(record: any = {}) {
    let _record;
    if (record && record.key) {
      _record = record;
    } else {
      _record = { ...this.state.record };
    }

    this.setState({
      record: _record,
      openUpsert: true
    })
  }

  private handleCloseUpsert() {
    this.setState({
      openUpsert: false,
      record: {
        company: {
          business_name: '',
          uuid: ''
        },
        name: '',
        value: 0,
        type: '',
      }
    })
  }

  public handleChange(value: string, field: string) {
    const { record } = this.state;
    const _record = {
      ...record,
      [field]: value
    }
    this.setState({
      record: _record
    })
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  });

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.handleCloseUpsert();
      this.handleFetch();
    }
  }

  public handleFetch(limit?: number, page?: number) {
    const { commissionList } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    commissionList.fetch(meta);
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  public componentDidMount() {
    this.handleFetch()
  }

  public render() {
    const { record, openUpsert, limit } = this.state;
    const { data, httpBusy, meta } = this.props.containers.commissionList.state;

    return (
      <div>
        <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
          <h1>{}</h1>
          {
            Gate.allows('company::comission-create') &&
            <Button
              type="primary"
              style={{ marginBottom: 16 }}
              icon="plus-square"
              onClick={this.handleOpenUpsert}
            >
              {Lang.get('parametrization.button.add')}
            </Button>
          }
        </div>
        <Table
          dataSource={data}
          loading={httpBusy}
          pagination={false}
          className="parametrization-commission-table"
        >
          <Column
            title={Lang.get('parametrization.commissions.name')}
            dataIndex="name"
            key="name"
            {...this.getColumnSearchProps('name')}
            align={'center'}
          />
          <Column
            title={Lang.get('parametrization.commissions.company')}
            dataIndex="company.business_name"
            key="company.business_name"
          />
          <Column
            title={Lang.get('parametrization.commissions.value')}
            dataIndex="value"
            key="value"
            align={'center'}
          />
          <Column
            title={Lang.get('parametrization.commissions.type')}
            dataIndex="type"
            key="type"
            align={'center'}
          />
          {
            Gate.allows('company::commission-edit') &&
            <Column
              title={Lang.get('parametrization.commissions.actions')}
              dataIndex=""
              key="0"
              render={this.renderActions}
              align={'center'}
            />
          }
        </Table>
        {
          meta &&
          <Pagination
            limit={limit}
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
          />
        }
        <CommissionUpsert
          record={record}
          openUpsert={openUpsert}
          onClose={this.handleCloseUpsert}
          onChange={this.handleChange}
          onComplete={this.handleOnComplete}
        />
      </div>
    );
  }
}

const container = {
  commissionList: CommissionListContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    commissionList: containers.commissionList.state,
  }
}

export default connect(container, mapStateToProps)(CommissionList);

