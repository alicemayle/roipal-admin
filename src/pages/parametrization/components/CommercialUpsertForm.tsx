import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import Lang from '../../../support/Lang';
import { Form, Input, InputNumber, Select } from 'antd';
const { Option } = Select;

class CommercialUpsertForm extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }

  private handleChange(e: any) {
    const value = e.target.value;
    const field = e.target.name;

    this.props.onChange(value, field)
  }

  private handleChangeValue(value: number) {
    this.props.onChangeTime(value, 'min_time')
  }

  private handleChangeTimeUnit(value: string) {
    this.props.onChangeTime(value, 'time_unit_cod')
  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { record } = this.props;
    return (
      <Form  >
        <Form.Item label={Lang.get('parametrization.commercial_needs.name')}>
          {getFieldDecorator('commercial_need', {
            initialValue: record.commercial_need,
            rules: [{
              required: true,
              message: Lang.get('parametrization.commercial_needs.validations.name')
            }],
          })(
            <Input
              placeholder={Lang.get('parametrization.commercial_needs.name')}
              name="commercial_need"
              disabled={record.uuid ? true : false}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.commercial_needs.min_time')}>
          {getFieldDecorator('min_time', {
            initialValue: record.group.min_time > 0 ? record.group.min_time : '',
            rules: [{
              required: true,
              message: Lang.get('parametrization.commercial_needs.validations.min_time')
            }],
          })(
            <InputNumber
              placeholder={Lang.get('parametrization.commercial_needs.min_time')}
              name="min_time"
              onChange={this.handleChangeValue}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.commercial_needs.time_unit')}>
          {getFieldDecorator('time_unit', {
            initialValue: record.group.time_unit_cod,
            rules: [{
              required: true,
              message: Lang.get('parametrization.commercial_needs.validations.time_unit')
            }],
          })(
            <Select
              placeholder={Lang.get('parametrization.commercial_needs.time_unit')}
              onChange={this.handleChangeTimeUnit}
            >
              <Option value="D">{Lang.get('parametrization.commercial_needs.days')}</Option>
              <Option value="H">{Lang.get('parametrization.commercial_needs.hours')}</Option>
            </Select>
          )}
        </Form.Item>
      </Form>
    );
  }
}

const WrapperCommercialUpsertForm: any = Form.create({
  name: 'CommercialUpsertForm'
})(CommercialUpsertForm);

export default WrapperCommercialUpsertForm;