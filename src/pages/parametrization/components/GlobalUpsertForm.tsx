import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import Lang from '../../../support/Lang';
import { Form, Input, InputNumber } from 'antd';

class GlobalUpsertForm extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }

  private handleChange(e: any) {
    const value = e.target.value;
    const field = e.target.name;

    this.props.onChange(value, field)
  }

  private handleChangeValue(value: number) {

    this.props.onChange(value, 'value')
  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { record } = this.props;

    return (
      <Form  >
        <Form.Item label={Lang.get('parametrization.global.concept')}>
          {getFieldDecorator('key', {
            initialValue: record.key,
            rules: [{
              required: true,
              message: Lang.get('parametrization.global.validations.key')
            }],
          })(
            <Input
              placeholder={Lang.get('parametrization.global.concept')}
              name="key"
              disabled={record.uuid ? true : false}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.global.value')}>
          {getFieldDecorator('value', {
            initialValue: record.uuid ? record.value : '',
            rules: [{
              required: true,
              message: Lang.get('parametrization.global.validations.value')
            }],
          })(
            (record.key === 'translations_business' || record.key === 'translations_executive')
            ? <Input
              placeholder={Lang.get('parametrization.global.value')}
              name="value"
              onChange={this.handleChange}
            />
            : <InputNumber
              placeholder={Lang.get('parametrization.global.value')}
              name="value"
              onChange={this.handleChangeValue}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('parametrization.global.unit')}>
          {getFieldDecorator('type', {
            initialValue: record.type,
            rules: [{
              required: true,
              message: Lang.get('parametrization.global.validations.type')
            }],
          })(
            <Input
              placeholder={Lang.get('parametrization.global.unit')}
              name="type"
              disabled={record.uuid ? true : false}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>
      </Form>
    );
  }
}

const WrapperGlobalUpsertForm: any = Form.create({ name: 'GlobalUpsertForm' })(GlobalUpsertForm);

export default WrapperGlobalUpsertForm;