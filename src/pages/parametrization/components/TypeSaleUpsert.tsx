import React, { PureComponent } from 'react';
import { Modal, Button, message } from 'antd';
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';

import TypeSaleProps from '../models/TypeSaleProps';
import TypeSaleUpsertForm from './TypeSaleUpsertForm';
import Lang from '../../../support/Lang';
import Event from '../../../support/Event';
import TypeSaleUpsertContainer from '../state/TypeSaleUpsertContainer';
import Notifications from '../../../components/notifications/notifications';
import { Gate } from '@xaamin/guardian';
class TypeSaleUpsert extends PureComponent<TypeSaleProps> {
  public form: any = null;
  constructor(props: TypeSaleProps) {
    super(props);
    autobind(this);
    Event.listen('parametrizationTypeSale::upsert', this.listeForUpsert);
  }

  private handleClose() {
    this.props.onClose();
  }

  private handleChange(value: string, field: string) {
    this.props.onChange(value, field);
  }

  public handleSubmit() {
    this.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        if(Gate.allows('type-sale::edit')) {
          const { typeSaleUpsert } = this.props.containers;
          const { record } = this.props;

          typeSaleUpsert.upsert(record);
        } else {
          message.error(Lang.get('generic.policies.denied'));
        }
      }
    });
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.props.onComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.props.onComplete('error');
    }

    notification.openNotification(msj)
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  public componentWillUnmount() {
    Event.remove('parametrizationTypeSale::upsert', this.listeForUpsert);
  }

  public render() {
    const { record, openUpsert } = this.props;
    const { httpBusy } = this.props.containers.typeSaleUpsert;

    return (
      <Modal
        title={Lang.get('parametrization.button.edit')}
        style={{ top: 20 }}
        visible={openUpsert}
        centered={true}
        onCancel={this.handleClose}
        destroyOnClose={true}
        footer={[
          <Button
            key="cancel"
            onClick={this.handleClose}>
            {Lang.get('parametrization.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={httpBusy}
            onClick={this.handleSubmit}>
            {Lang.get('parametrization.button.accept')}
          </Button>
        ]}
      >
        <TypeSaleUpsertForm
          record={record}
          onChange={this.handleChange}
          onValidate={this.handleValidateForm}
        />
      </Modal>
    );
  }
}

const container = {
  typeSaleUpsert: TypeSaleUpsertContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    typeSaleUpsert: containers.typeSaleUpsert.state,
  }
}

export default connect(container, mapStateToProps)(TypeSaleUpsert);