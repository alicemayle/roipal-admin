import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';

import ListProps from '../models/ListProps';
import CommercialListContainer from '../state/CommercialListContainer';
import CommercialState from '../models/CommercialState'
import Lang from '../../../support/Lang';
import CommercialUpsert from './CommercialUpsert';
import FilterInterface from '../../../components/search/FilterInterface';
import FilterSearch from '../../../components/search/FilterSearch';
import Pagination from '../../../components/pagination/Pagination'
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';

const { Column } = Table;
class CommercialList extends Component<ListProps, CommercialState> {
  public searchInput: any = null;
  public searchField: string = '';
  constructor(props: ListProps) {
    super(props);
    autobind(this);
  }

  public state: CommercialState = {
    searchText: '',
    openUpsert: false,
    record: {
      commercial_need: '',
      group: {
        uuid: '',
        time_unit: '',
        time_unit_cod: '',
        min_time: 0,
      },
      uuid: ''
    },
    fields: [],
    limit: 50,
    page: 1
  }

  private renderActions(record: any) {
    return (
      <IconButton
      item={record}
      onFuction={this.handleOpenUpsert}
      size={18}
      colorIcon={'#29a3a3'}
      name={'edit'}
    />
    );
  }

  private handleOpenUpsert(record: any = {}) {
    let _record;
    if (record && record.uuid) {
      _record = record;
    } else {
      _record = { ...this.state.record };
    }

    this.setState({
      record: _record,
      openUpsert: true
    })
  }

  private handleCloseUpsert() {
    this.setState({
      openUpsert: false,
      record: {
        commercial_need: '',
        group: {
          uuid: '',
          time_unit: '',
          time_unit_cod: '',
          min_time: 0,
        },
        uuid: ''
      },
    })
  }

  public handleChange(value: string, field: string) {
    const { record } = this.state;
    const _record = {
      ...record,
      [field]: value
    }

    this.setState({
      record: _record
    })
  }

  public handleChangeTime(value: string, field: string) {
    const { record } = this.state;
    let _unit;

    if(field === 'time_unit_cod') {
      _unit = value === 'D' ? 'days' : 'hours'
    }

    const _group = record && record.group ? record.group : {}
    const _record = {
      ...record,
      group: {
        ..._group,
        [field]: value,
        time_unit: _unit
      }
    }

    this.setState({
      record: _record
    })
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  });

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  public handleOnComplete(type: string) {
    if(type==='success') {
      this.handleCloseUpsert();
      this.handleFetch();
    }
  }

  public handleFetch(limit?:number, page?:number) {
    const { commercialList } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

      const meta = {
        includes: 'group',
        limit: limit || _limit.toString(),
        page: page || _page,
      }

    commercialList.fetch(meta);
  }

  public handleChangeLimit(_limit:number, _page:number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  public componentDidMount() {
    this.handleFetch()
  }

  public render() {
    const { record, openUpsert, limit } = this.state;
    const { httpBusy, data, meta } = this.props.containers.commercialList.state;

    return (
      <div>
        <Table
          dataSource={data}
          loading={httpBusy}
          pagination={false}
          className="parametrization-commercial-needs-table"
        >
          <Column
            title={Lang.get('parametrization.commercial_needs.name')}
            dataIndex="commercial_need"
            key="commercial_need"
            {...this.getColumnSearchProps('commercial_need')}
            align={'center'}
          />
          <Column
            title={Lang.get('parametrization.commercial_needs.min_time')}
            dataIndex="group.min_time"
            key="group.min_time"
            align={'center'}
          />
          <Column
            title={Lang.get('parametrization.commercial_needs.time_unit')}
            dataIndex="group.time_unit"
            key="group.time_unit"
            align={'center'}
          />
          {
            Gate.allows('commercial-need::edit') &&
            <Column
              title={Lang.get('parametrization.commercial_needs.actions')}
              dataIndex=""
              key="0"
              render={this.renderActions}
              align={'center'}
            />
          }
        </Table>
        {
          meta &&
          <Pagination
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
            limit={limit}
          />
        }
        <CommercialUpsert
          record={record}
          openUpsert={openUpsert}
          onClose={this.handleCloseUpsert}
          onChange={this.handleChange}
          onChangeTime={this.handleChangeTime}
          onComplete={this.handleOnComplete}
        />
      </div>
    );
  }
}

const container = {
  commercialList: CommercialListContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    commercialList: containers.commercialList.state,
  }
}

export default connect(container, mapStateToProps)(CommercialList);

