import React, { PureComponent } from 'react';
import { Modal, Button, message } from 'antd';
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';

import CommissionUpsertProps from '../models/CommissionUpsertProps';
import CommissionUpsertForm from './CommissionUpsertForm';
import Lang from '../../../support/Lang';
import Event from '../../../support/Event';
import CommissionUpsertContainer from '../state/CommissionUpsertContainer';
import Notifications from '../../../components/notifications/notifications';
import { Gate } from '@xaamin/guardian';
class CommissionUpsert extends PureComponent<CommissionUpsertProps> {
  public form: any = null;
  constructor(props: CommissionUpsertProps) {
    super(props);
    autobind(this);
    Event.listen('parametrizationCommission::upsert', this.listeForUpsert);
  }

  private handleClose() {
    this.props.onClose();
  }

  private handleChange(value: string, field: string) {
    this.props.onChange(value, field);
  }

  private handleChangeCompany(value: string, field: string) {
    this.props.onChange(value, field);
    this.handleGetCommissionsSelect(value)
  }

  public handleSubmit() {
    const { record = {} } = this.props;
    const permission = record.uuid
      ? Gate.allows('company::commission-edit')
      : Gate.allows('company::comission-create')

    this.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        if (permission) {
          const { commissionUpsert } = this.props.containers;
          let _value;

          if (record && record.type === '%' && record.value) {
            if (record.value < 1) {
              _value = record.value * 100;

              record.value = _value
            }
          }
          commissionUpsert.upsert(record);
        } else {
          message.error(Lang.get('generic.policies.denied'));
        }
      }
    });
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.props.onComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.props.onComplete('error');
    }

    notification.openNotification(msj)
  }

  public handleGetCompaniesSelect(limit?: number, page?: number) {
    const { commissionUpsert } = this.props.containers;

    const meta = {
      limit: limit || '100',
      page: page || 1,
    }

    commissionUpsert.getCompanies(meta);
  }

  public async handleGetCommissionsSelect(uuid: string) {
    const { commissionUpsert } = this.props.containers;

    await commissionUpsert.getCommissions(uuid);
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  public componentDidMount() {
    this.handleGetCompaniesSelect()
  }

  public componentWillUnmount() {
    Event.remove('parametrizationCommission::upsert', this.listeForUpsert);
  }

  public render() {
    const { record, openUpsert } = this.props;
    const { httpBusy, companies, commissions } = this.props.containers.commissionUpsert.state;

    return (
      <Modal
        title={record && record.uuid
          ? Lang.get('parametrization.button.edit')
          : Lang.get('parametrization.button.add')}
        style={{ top: 20 }}
        visible={openUpsert}
        centered={true}
        onCancel={this.handleClose}
        destroyOnClose={true}
        footer={[
          <Button
            key="cancel"
            onClick={this.handleClose}>
            {Lang.get('parametrization.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={httpBusy}
            onClick={this.handleSubmit}>
            {Lang.get('parametrization.button.accept')}
          </Button>
        ]}
      >
        <CommissionUpsertForm
          record={record}
          onChange={this.handleChange}
          onChangeCompany={this.handleChangeCompany}
          commissions={commissions}
          companies={companies}
          onValidate={this.handleValidateForm}
        />
      </Modal>
    );
  }
}

const container = {
  commissionUpsert: CommissionUpsertContainer
}

const mapStateToProps = (containers: any) => {
  return {
    commissionUpsert: containers.commissionUpsert.state
  }
}

export default connect(container, mapStateToProps)(CommissionUpsert);