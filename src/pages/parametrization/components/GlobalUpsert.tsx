import React, { PureComponent } from 'react';
import { Modal, Button, message } from 'antd';
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';

import GlobalUpsertProps from '../models/GlobalUpsertProps';
import GlobalUpsertForm from './GlobalUpsertForm';
import Lang from 'src/support/Lang';
import Event from '../../../support/Event';
import GlobalUpsertContainer from '../state/GlobalUpsertContainer';
import Notifications from '../../../components/notifications/notifications';
import { Gate } from '@xaamin/guardian';
class GlobalUpsert extends PureComponent<GlobalUpsertProps> {
  public form: any = null;
  constructor(props: GlobalUpsertProps) {
    super(props);
    autobind(this);
    Event.listen('parametrizationGlobal::upsert', this.listeForUpsert);
  }

  private handleClose() {
    this.props.onClose();
  }

  private handleChange(value: string, field: string) {
    this.props.onChange(value, field);
  }

  public handleSubmit() {
    const { record = {} } = this.props;
    const permission = record.uuid
      ? Gate.allows('configuration::global-edit')
      : Gate.allows('configuration::create')

    this.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        if(permission) {
          const { globalUpsert } = this.props.containers;

          if(record && record.type === '%' && record.value) {
            if (record.value >= 1) {
              let decimal;

              if (record.value.toString().length === 1) {
                decimal = '0.0'
              } else {
                decimal = '0.'
              }

              const _value = decimal.concat(record.value.toString())
              record.value = parseFloat(_value)
            }
          }
          globalUpsert.upsert(record);
        } else {
          message.error(Lang.get('generic.policies.denied'));
        }
      }
    });
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.props.onComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.props.onComplete('error');
    }

    notification.openNotification(msj)
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  public componentWillUnmount() {
    Event.remove('parametrizationGlobal::upsert', this.listeForUpsert);
  }

  public render() {
    const { record, openUpsert } = this.props;
    const { httpBusy } = this.props.containers.globalUpsert;

    return (
      <Modal
        title={record && record.uuid
          ? Lang.get('parametrization.button.edit')
          : Lang.get('parametrization.button.add')}
        style={{ top: 20 }}
        visible={openUpsert}
        centered={true}
        onCancel={this.handleClose}
        destroyOnClose={true}
        footer={[
          <Button
            key="cancel"
            onClick={this.handleClose}>
            {Lang.get('parametrization.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={httpBusy}
            onClick={this.handleSubmit}>
            {Lang.get('parametrization.button.accept')}
          </Button>
        ]}
      >
        <GlobalUpsertForm
          record={record}
          onChange={this.handleChange}
          onValidate={this.handleValidateForm}
        />
      </Modal>
    );
  }
}

const container = {
  globalUpsert: GlobalUpsertContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    globalUpsert: containers.globalUpsert.state,
  }
}

export default connect(container, mapStateToProps)(GlobalUpsert);