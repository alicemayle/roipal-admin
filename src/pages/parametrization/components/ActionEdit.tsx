import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import EditProps from '../models/EditProps';
import Lang from '../../../support/Lang';

class ActionEdit extends PureComponent<EditProps> {
  constructor(props: EditProps) {
    super(props);
    autobind(this);
  }

  private handleEdit() {
    this.props.onOpenEdit(this.props.record);
  }

  public render() {
    return (
      <div>
        <a onClick={this.handleEdit}>{Lang.get('parametrization.button.edit')}</a>
      </div>
    )
  }
}

export default ActionEdit;