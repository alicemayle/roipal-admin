import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';

import ListProps from '../models/ListProps';
import TypeSaleListContainer from '../state/TypeSaleListContainer';
import TypeSaleState from '../models/TypeSaleState'
import Lang from '../../../support/Lang';
import TypeSaleUpsert from './TypeSaleUpsert';
import FilterInterface from '../../../components/search/FilterInterface';
import FilterSearch from '../../../components/search/FilterSearch';
import Pagination from '../../../components/pagination/Pagination'
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';

const { Column } = Table;
class TypeSaleList extends Component<ListProps, TypeSaleState> {
  public searchInput: any = null;
  public searchField: string = '';
  constructor(props: ListProps) {
    super(props);
    autobind(this);
  }

  public state: TypeSaleState = {
    searchText: '',
    openUpsert: false,
    record: {
      uuid: '',
      bonus_minimum: 0,
      description: '',
      type: '',
      zone_A: 0,
      zone_B: 0,
      zone_C: 0
    },
    fields: [],
    limit: 50,
    page: 1
  }

  private renderActions(record: any) {
    return (
      <IconButton
        item={record}
        onFuction={this.handleOpenUpsert}
        size={18}
        colorIcon={'#29a3a3'}
        name={'edit'}
      />
    );
  }

  private handleOpenUpsert(record: any = {}) {
    let _record;
    if (record && record.uuid) {
      _record = record;
    } else {
      _record = { ...this.state.record };
    }

    this.setState({
      record: _record,
      openUpsert: true
    })
  }

  private handleCloseUpsert() {
    this.setState({
      openUpsert: false,
      record: {
        uuid: '',
        bonus_minimum: 0,
        description: '',
        type: '',
        zone_A: 0,
        zone_B: 0,
        zone_C: 0
      },
    })
  }

  public handleChange(value: string, field: string) {
    const { record } = this.state;
    const _record = {
      ...record,
      [field]: value
    }

    this.setState({
      record: _record
    })
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  });

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.handleCloseUpsert();
      this.handleFetch();
    }
  }

  public handleFetch(limit?: number, page?: number) {
    const { typeSaleList } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    typeSaleList.fetch(meta);
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  public componentDidMount() {
    this.handleFetch()
  }

  public render() {
    const { record, openUpsert, limit } = this.state;
    const { httpBusy, data, meta } = this.props.containers.typeSaleList.state;

    return (
      <div>
        <Table
          dataSource={data}
          loading={httpBusy}
          pagination={false}
          className="parametrization-types-sales-table"
        >
          <Column
            title={Lang.get('parametrization.types_sales.name')}
            dataIndex="type"
            key="type"
            {...this.getColumnSearchProps('type')}
            align={'center'}
          />
          <Column
            title={Lang.get('parametrization.types_sales.zone_A')}
            dataIndex="zone_A"
            key="zone_A"
            align={'right'}
          />
          <Column
            title={Lang.get('parametrization.types_sales.zone_B')}
            dataIndex="zone_B"
            key="zone_B"
            align={'right'}
          />
          <Column
            title={Lang.get('parametrization.types_sales.zone_C')}
            dataIndex="zone_C"
            key="zone_C"
            align={'right'}
          />
          <Column
            title={Lang.get('parametrization.types_sales.bonus')}
            dataIndex="bonus_minimum"
            key="bonus_minimum"
            align={'right'}
          />
          {
            Gate.allows('type-sale::edit') &&
            <Column
              title={Lang.get('parametrization.types_sales.actions')}
              dataIndex=""
              key="0"
              render={this.renderActions}
              align={'center'}
            />
          }
        </Table>
        {
          meta &&
          <Pagination
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
            limit={limit}
          />
        }
        <TypeSaleUpsert
          record={record}
          openUpsert={openUpsert}
          onClose={this.handleCloseUpsert}
          onChange={this.handleChange}
          onComplete={this.handleOnComplete}
        />
      </div>
    );
  }
}

const container = {
  typeSaleList: TypeSaleListContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    typeSaleList: containers.typeSaleList.state,
  }
}

export default connect(container, mapStateToProps)(TypeSaleList);

