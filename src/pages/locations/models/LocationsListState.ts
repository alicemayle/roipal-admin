import { ListState } from '../../../roipal/models';

interface LocationsListState extends ListState {
  __action?: string
}

export default LocationsListState;