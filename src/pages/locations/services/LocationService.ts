import { Http } from '../../../shared';

class LocationService {

  public paginate(company: string, params: any): any {
    return Http.get(`api/companies/${company}/locations`, { params })
  }

  public storeLocation(data: any, company: any) {
    return Http.post(`api/companies/${company}/locations`, data)
  }

  public updateLocation(data: any, company: string, location: string) {
    return Http.put(`api/companies/${company}/locations/${location}`, data)
  }
}

export default new LocationService;