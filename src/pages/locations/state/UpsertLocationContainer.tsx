import { Container } from 'unstated';
import Locations from '../services/LocationService';
import Event from 'src/support/Event';
import Lang from 'src/support/Lang';

class UpsertLocationContainer extends Container<any> {
  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
  }

  public name: string = 'Locations Upsert List';

  public async storeLocation(data: any, company: any) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Locations upsert starts'
      });

      const response = await Locations.storeLocation(data, company);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Locations upsert success'
      });

      result = {
        message: Lang.get('locations.location.success_create')
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Locations upsert error'
      });
      result = {
        error,
      }
    }
    Event.notify('location::create', result);
  }

  public async updateLocation(data: any, company: any, location: string) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Locations upsert starts'
      });

      const response = await Locations.updateLocation(data, company, location);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Locations upsert success'
      });
      result = {
        message: Lang.get('locations.location.success_update')
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Locations upsert error'
      });
      result = {
        error,
      }
    }
    Event.notify('location::create', result);
  }
}

export default UpsertLocationContainer;