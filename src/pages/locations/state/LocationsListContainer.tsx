import { Container } from 'unstated';
import Locations from '../services/LocationService';
import LocationsListState from '../models/LocationsListState';
import moment from 'moment';

class LocationsListContainer extends Container<LocationsListState> {
  public state: LocationsListState = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
  }

  public name: string = 'Locations List';

  public async fetch(companyUuid: string, params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Locations list starts'
      });

      const response = await Locations.paginate(companyUuid, params);

      const newResponse = response.data.map((data: any) => {
        data = { ...data }
        data.key = data.uuid;
        data.created_at = moment(new Date(data.created_at)).format('DD-MM-YYYY')
        return data;
      })

      this.setState({
        httpBusy: false,
        data: newResponse,
        meta: response.meta,
        __action: 'Locations list success'
      });

    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Locations list error'
      });
    }
  }

  public reset() {
    this.setState({
      httpBusy: false,
      data: null,
      error: null,
      message: '',
      __action: 'Locations list reset'
    });
  }
}

export default LocationsListContainer;