import React, { Component } from 'react';
import Autobind from 'class-autobind';
import GoogleMap from 'google-map-react';
import { Col, Row, Card } from 'antd';
import Marker from 'src/components/map/Marker';

interface MapProps {
  addressSelected?: AddressSelected,
  onChangeAddress: (data: AddressSelected) => void
}

interface AddressSelected {
  latLng: {lat: number, lng: number}
  address: string
}

class Map extends Component<MapProps, any> {
  constructor(props: MapProps) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }

  public render() {
    const { addressSelected } = this.props;
    const center = addressSelected ?
    [addressSelected.latLng.lat, addressSelected.latLng.lng]
    : undefined

    return (
      <Card>
        <Row>
          <Col span={24}>
            <div style={{ height: '22vh' }}>
              <GoogleMap
                bootstrapURLKeys={{ key: 'AIzaSyCzTCLGCeoIPtGng_7IhV4aZRgL-dYyPaw' }}
                zoom={14}
                center={center}>
                {
                addressSelected &&
                <Marker
                  lat={addressSelected.latLng.lat}
                  lng={addressSelected.latLng.lng}
                  marker={{name:'title', address:'address'}}
                />
                }
                </GoogleMap>
            </div>
          </Col>
        </Row>
      </Card>
    )
  }
}

export default Map