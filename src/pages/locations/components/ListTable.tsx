import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { Table, Icon } from 'antd';
import FilterDropdDownInterface from 'src/components/search/FilterDropDownInterface'
import FilterSearch from 'src/components/search/FilterSearch';
import Lang from 'src/support/Lang';
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';

const { Column } = Table;

class ListTable extends Component<any> {
  public searchInput: any = null;
  public searchField: string = '';
  public constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public click(data: {}) {
    this.props.onDetail(data);
  }

  public state: any = {
    searchText: '',
    fields: [],
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterDropdDownInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  })

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private renderActions(record: any) {
    return (
      <IconButton
        item={record}
        onFuction={this.props.onUpsert}
        size={18}
        colorIcon={'#29a3a3'}
        name={'edit'}
      />
    );
  }

  public render() {
    const { data, busy } = this.props;

    return (
      <div>
        <Table
          loading={busy}
          dataSource={data}
          pagination={false}
          className="locations-table"
        >
          <Column
            title={Lang.get('locations.location.date')}
            dataIndex={'created_at'}
            key={'created_at'}
            align={'center'}
          />
          <Column
            title={Lang.get('locations.location.name')}
            dataIndex={'name'}
            key={'name'}
            {...this.getColumnSearchProps('name')}
          />
          <Column
            title={Lang.get('locations.location.address')}
            dataIndex={'address'}
            key={'address'}
            {...this.getColumnSearchProps('address')}
          />
          {
            Gate.allows('location::edit') &&
            <Column
              title={Lang.get('generic.button.actions')}
              dataIndex=""
              key="0"
              render={this.renderActions}
              align={'center'}
            />
          }
        </Table>
      </div>
    )
  }
}

export default ListTable;