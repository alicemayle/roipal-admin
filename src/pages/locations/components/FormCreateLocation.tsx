import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Form } from 'antd';
import SelectAddress from '../../../components/selectAddress/SelectAddress';
import InputItem from '../../../components/inputItem/InputItem';
import Lang from 'src/support/Lang';

interface AddressSelected {
  latLng: { lat: number, lng: number }
  address: string
}

interface ItemPropsI {
  onChangeAddress: (data: AddressSelected) => void,
  onHandleNameAddress: (field: string, value: any) => void,
  location?: any
}

class FormCreateLocation extends PureComponent<ItemPropsI, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }

  private handleChange(field: string, value: any) {
    const { onHandleNameAddress } = this.props;

    if (onHandleNameAddress) {
      onHandleNameAddress(field, value);
    }
  }

  public render() {
    const { location } = this.props

    return (
      <div>
        <Form.Item label={Lang.get('locations.location.name_address')}>
          <InputItem
            field="addressName"
            placeholder={Lang.get('locations.location.info_address')}
            onChange={this.handleChange}
            location={location}/>
        </Form.Item>
        <Form.Item label={Lang.get('locations.location.address')}>
          <SelectAddress
            onChangeAddress={this.props.onChangeAddress}
            location={location}
          />
        </Form.Item>
      </div>
    )
  }
}

export default FormCreateLocation