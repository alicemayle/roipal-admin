import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { Modal, Button, message } from 'antd';
import FormCreateLocation from '../components/FormCreateLocation'
import { connect } from 'unstated-enhancers';
import Map from '../components/Map'
import UpsertLocationContainer from '../state/UpsertLocationContainer';
import Lang from 'src/support/Lang';
import Notifications from 'src/components/notifications/notifications';
import { Gate } from '@xaamin/guardian';

interface AddressSelected {
  latLng: { lat: number, lng: number }
  address: string
}

class UpsertLocation extends Component<any> {
  public constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public state: any = {
    addressSelected: null,
    addressName: null
  }

  private handleChangeAddress(data: AddressSelected) {
    this.setState({
      addressSelected: data
    })
  }

  private handleChange(field: string, value: any) {
    this.setState({
      [field]: value
    })
  }

  private async handleSaveLocation() {
    const { company, close, location = {} } = this.props

    if ((location && location.uuid && Gate.allows('location::edit'))
        || Gate.allows('location::create')) {
      const { addressSelected, addressName } = this.state;
      const { upsert } = this.props.containers
      let _name = null

      if (location && location.uuid && !addressName) {
        _name = location.name
      } else {
        _name = addressName
      }


      const newData = {
        name: _name,
        address: addressSelected.address,
        position: {
          latitude: addressSelected.latLng.lat,
          longitude: addressSelected.latLng.lng,
        }
      };

      if (newData) {

        if(location && location.uuid) {
          await upsert.updateLocation(newData, company, location.uuid);
        } else {
          await upsert.storeLocation(newData, company);
        }

        const { error } = this.props.containers.upsert.state

        if(error){
          const notification = new Notifications;

          const msj = {
            type: 'error',
            message: Lang.get('locations.location.error_list')
          }

          notification.openNotification(msj)
        }else{
          close()
        }
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  private onClose() {
    const { close } = this.props
    this.setState({
      addressSelected: null
    })

    if (close) {
      close()
    }

  }

  public render() {
    const { visible, location } = this.props
    const { addressSelected, addressName } = this.state;
    const { httpBusy } = this.props.containers.upsert.state
    const disable = addressSelected && addressName ? false : true

    return (
      <div>
        <Modal
          title={ location && location.uuid
            ? Lang.get('locations.location.edit')
            : Lang.get('locations.location.new_location')}
          visible={visible}
          width={'50%'}
          destroyOnClose={true}
          onCancel={this.onClose}
          footer={[
            <Button
              key="cancel"
              onClick={this.onClose}>
              {Lang.get('locations.button.cancel')}
            </Button>,
            <Button
              key="submit"
              type="primary"
              onClick={this.handleSaveLocation}
              loading={httpBusy}
              disabled={disable}
            >
              {Lang.get('locations.button.save')}
            </Button>
          ]}
        >
          <div>
            <FormCreateLocation
              onChangeAddress={this.handleChangeAddress}
              onHandleNameAddress={this.handleChange}
              location={location}
            />
            {
              addressSelected &&
              <Map
                addressSelected={addressSelected}
                onChangeAddress={this.handleChangeAddress}
              />}
          </div>
        </Modal>
      </div>
    )
  }
}

const container = {
  upsert: UpsertLocationContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    upsert: containers.upsert.state,
  }
}

export default connect(container, mapStateToProps)(UpsertLocation);
