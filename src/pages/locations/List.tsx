import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';
import LocationsListContainer from './state/LocationsListContainer';
import ListTable from './components/ListTable';
import { Divider, Button, Select } from 'antd';
import SignInContainer from 'src/auth/state/SignInContainer';
import Pagination from 'src/components/pagination/Pagination';
import UpsertLocation from './components/UpsertLocation';
import Lang from 'src/support/Lang';
import Notifications from 'src/components/notifications/notifications';
import Event from 'src/support/Event';
import { Gate } from '@xaamin/guardian';
import CompaniesContainer from '../companies/state/CompaniesListContainer';
const { Option } = Select;
interface ListState {
  limit: number,
  page: number,
  visible: boolean,
  uuidCompany: string,
  location?: any
}

class List extends Component<any, ListState> {
  public constructor(props: any) {
    super(props);
    Autobind(this);
    Event.listen('location::create', this.listeForUpsert);
  }

  public state: any = {
    limit: 50,
    page: 1,
    visible: false,
    uuidCompany: null
  }

  public handleFetch(limit?: number, page?: number) {
    const { locations } = this.props.containers
    const _limit = this.state.limit;
    const _page = this.state.page;
    const { uuidCompany } = this.state;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page
    }

    locations.fetch(uuidCompany, meta);
  }

  public getCompany() {
    const { signing } = this.props.containers
    const { company } = signing.state.data;

    if (company) {
      this.setState({
        uuidCompany: company.uuid
      }, () => this.handleFetch())

    } else {
      const { companies } = this.props.containers;

      const params = {
        includes: 'profile,payments_profiles',
        limit: 100
      }
      companies.fetch(params);
    }
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  private openModal(record?: any) {
    const _location = record && record.uuid ? record : null

    this.setState({
      visible: !this.state.visible,
      location: _location
    })
  }

  private renderOptions(data: any) {
    const value = data.business_name

    return (
      <Option key={data.key} value={data.uuid}>
        {value}
      </Option>
    );
  }

  private onChangeCompany(value: string) {
    this.setState({
      uuidCompany: value
    }, () => this.handleFetch())
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }
      this.handleOnComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }
      this.handleOnComplete('error');
    }
    notification.openNotification(msj)
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.handleFetch();
    }
  }

  public componentWillUnmount() {
    Event.remove('location::create', this.listeForUpsert);

    const { companies, locations } = this.props.containers;
    companies.reset()
    locations.reset()
  }

  public componentDidMount() {
    this.getCompany()
  }

  public render() {
    const { locations } = this.props.containers
    const { data, httpBusy, meta } = locations.state;
    const { location, visible, uuidCompany, limit } = this.state;
    const companies = this.props.companies.data;

    return (
      <div>
        <h1>{Lang.get('locations.location.title')}</h1>
        <Divider />
        <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
          { 
            !companies && <h1>{}</h1>
          }
          {
            companies &&
              <Select
                showSearch={true}
                placeholder={Lang.get('buses.mission.add.description.select_company.label')}
                optionFilterProp="children"
                onChange={this.onChangeCompany}
                style={{width: '30%'}}
              >
                {companies.map(this.renderOptions)}
              </Select>
          }
          {
            Gate.allows('location::create') &&
            <Button type="primary"
              icon="plus-square"
              onClick={this.openModal}
            >
              {Lang.get('locations.button.add')}
            </Button>
          }
        </div>
        <br />
        {uuidCompany &&
          <ListTable
            busy={httpBusy}
            data={data}
            meta={meta}
            onUpsert={this.openModal}
          />
        }
        {
          meta &&
          <Pagination
            limit={limit}
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
          />
        }
        {
          <UpsertLocation
            visible={visible}
            close={this.openModal}
            company={uuidCompany}
            location={location}
          />
        }
      </div>
    );
  }
}

const config = {
  locations: LocationsListContainer,
  signing: SignInContainer,
  companies: CompaniesContainer
}

const mapStateToProps = (containers: any) => {
  return {
    locations: containers.locations.state,
    signing: containers.signing.state,
    companies: containers.companies.state
  }
}

export default connect(config, mapStateToProps)(List);
