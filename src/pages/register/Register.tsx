import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Divider, Steps, Layout, Menu, Popconfirm, Icon } from 'antd';
import LanguageContainer from '../../auth/state/LanguageContainer'
import { connect } from 'unstated-enhancers';
import Lang from '../../support/Lang'
import './register.scss'
import { Redirect } from 'react-router-dom';
import Auth from '../../auth/User';
import Activation from './components/Activation';
import ChecklistContainer from './state/ChecklistContainer';
import Profile from './components/Profile';
import Payment from './components/Payment'
import Contact from './components/Contact';
import '../../styles/steps.scss'

const { Step } = Steps;
const { Sider, Content } = Layout;

const steps = [
  {
    title: Lang.get('register.activation.title'),
    id: 0,
    step: 'activation',
    completed: false
  },
  {
    title: Lang.get('register.profile.title'),
    id: 1,
    step: 'profile',
    completed: false
  },
  {
    title: Lang.get('register.contact.title'),
    id: 2,
    step: 'contact',
    completed: false
  },
  {
    title: Lang.get('register.payment.title'),
    id: 3,
    step: 'payment',
    completed: false
  }
];
export interface State {
  collapsed: boolean,
  closeSession: boolean,
  current: number,
  checklist: any[]
}

class Register extends Component<any, State> {
  public state: State;

  constructor(props: any) {
    super(props);

    autobind(this);
    this.state = {
      current: 0,
      collapsed: true,
      closeSession: false,
      checklist: steps
    };
  }

  private next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  private async handleCloseSession() {
    await Auth.logout();
    await this.setState({
      closeSession: true
    })

    return <Redirect to="/signin" />
  }

  private onCollapse(collapsed: boolean) {
    this.setState({ collapsed });
  }

  private async getChecklist() {
    const { checklist } = this.props.containers;

    await checklist.getChecklist();

    const { data } = checklist.state;
    const _steps = this.state.checklist

    data.map((item: any) => {
      if (item.completed) {
        const index = _steps.findIndex((ele: any) => ele.step === item.step)

        if (index > -1) {
          const step = {
            ..._steps[index],
            completed: item.completed
          }
          _steps.splice(index, 1, step)
        }
      }
    })

    const _current = _steps.findIndex((step: any) => step.completed === false)

      this.setState({
        checklist: _steps,
        current: _current
      })
  }

  private handleGoHome() {
    this.props.history.push({
      pathname: '/',
    })
  }

  public componentWillUnmount() {
    this.setState({
      closeSession: false
    })
  }

  public componentDidMount() {
    this.getChecklist()
  }

  public render() {
    const isActive = Auth.getData();
    const { current, checklist } = this.state;

    if (!isActive) {
      return <Redirect to="/signin" />
    }

    if(isActive.scope === 'admin') {
      return <Redirect to="/" />
    }

    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider
          collapsible={true}
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <div className="logo" style={{ backgroundColor: 'transparent' }}>
            {this.state.collapsed &&
              <img
                src={require('../../images/Imagotipo_ROIPAL_Perforado_09.png')}
                width="50" height="50"
              />
            }
            {
              !this.state.collapsed &&
              <img
                src={require('../../images/Logo_ROIPAL_Perforado_08.png')}
                height="60"
              />
            }
          </div>
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="logout">
              <Popconfirm title={Lang.get('setting.settings.log_out')}
                onConfirm={this.handleCloseSession}
                okText={Lang.get('generic.button.yes')}
                cancelText={Lang.get('generic.button.no')}>
                <Icon type="logout" />
                <span>{Lang.get('generic.button.sign_out')}</span>
              </Popconfirm>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="container">
          <Content style={{
            margin: '24px 16px', padding: 24, background: '#fff', minHeight: '100vH',
          }}
          >
            <h1>
              {Lang.get('register.title')}
            </h1>
            <Divider />
            <Steps progressDot={true} current={current}>
              {
                checklist.map(item => (
                  <Step key={item.step} title={item.title} />
                ))
              }
            </Steps>
            <div>
              {
                current === 0 &&
                <Activation
                  onGetChecklist={this.getChecklist}
                />
              }
              {
                current === 1 &&
                <Profile
                  current={current}
                  totalSteps={checklist.length}
                  next={this.next}
                />
              }
              {
                current === 2 &&
                <Contact
                  current={current}
                  totalSteps={checklist.length}
                  next={this.next}
                />
              }
              {
                current === 3 &&
                <Payment
                  current={current}
                  totalSteps={checklist.length}
                  next={this.next}
                  onGoHome={this.handleGoHome}
                />
              }
            </div>
          </Content>
        </Layout>
      </Layout>
    )
  }
}

const container = {
  language: LanguageContainer,
  checklist: ChecklistContainer
}

const mapStateToProps = (containers: any) => {
  return {
    language: containers.language.state,
    checklist: containers.checklist.state
  }
}

export default connect(container, mapStateToProps)(Register);
