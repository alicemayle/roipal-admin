import { Container } from 'unstated';
import RegisterService from '../services/RegisterService'

class UpdateProfileContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
  }

  public name: string = 'Update profile Container';

  public async updateProfile(data:any, company:string) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update Profile Starts'
      })

      const response = await RegisterService.update(data, company);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Update Profile Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Update Profile Error'
      })
    }
  }

 public async createCustomer(uuid:string, data:any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Create customer starts'
      })

      const response = await RegisterService.createCustomer(uuid, data);

      if (response) {
        await this.setState({
          data: response,
          __action: 'Create customer success'
        })
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Create customer Error'
      })
    }
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Update contact business clear error'
    });
  }
}

export default UpdateProfileContainer;