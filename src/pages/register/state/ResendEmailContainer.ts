import { Container } from 'unstated';
import RegisterService from '../services/RegisterService'

class ResendEmailContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
  }

  public name: string = 'Resend email Container';

  public async sendEmail() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Resend email Starts'
      })

      const response = await RegisterService.sendEmail();

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Resend email Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Resend email Error'
      })
    }
  }
}

export default ResendEmailContainer;