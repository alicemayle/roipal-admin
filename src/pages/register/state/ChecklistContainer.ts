import { Container } from 'unstated';
import RegisterService from '../services/RegisterService'

class ChecklistContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
  }

  public name: string = 'Get Checklist Container';

  public async getChecklist() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Checklist Starts'
      })

      const params = {
        includes: 'checklist'
      }

      const response = await RegisterService.getChecklist(params);

      this.setState({
        httpBusy: false,
        data: response.data.checklist,
        __action: 'Get Checklist Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Get Checklist Error'
      })
    }
  }
}

export default ChecklistContainer;