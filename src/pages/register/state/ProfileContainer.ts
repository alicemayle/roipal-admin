import { Container } from 'unstated';
import RegisterService from '../services/RegisterService'

class ProfileContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
  }

  public name: string = 'Profile Container';

  public async getLineBusiness() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Line business Starts'
      })

      const response = await RegisterService.getLineBusiness();

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Get Line business Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Get Line business Error'
      })
    }
  }

  public async updateProfile(data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update profile business Starts'
      })

      const response = await RegisterService.updateProfile(data);

      this.setState({
        httpBusy: false,
        profile: response.data,
        __action: 'Update profile business Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Update profile business Error'
      })
    }
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Update profile business clear error'
    });
  }
}

export default ProfileContainer;