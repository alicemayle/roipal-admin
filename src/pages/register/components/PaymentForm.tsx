import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Form, Input, Col, Row } from 'antd';
import Lang from 'src/support/Lang';
import '../register.scss'
import creditCardType from 'credit-card-type';
import typeCard from '../../../support/TypeCardManagement';

let lastValue = null;

class PaymentForm extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
  }

  private changeText(e: any) {
    this.setState({
      flagName: false,
      [e.target.name]: e.target.value
    }, () => this.setData())
  }

  private changeNumbCard(e: any) {
    const value = e.target.value
    const re = /-/g;
    const { ccv } = this.state
    let complete = value

    this.setState({
      flagCard: false
    })

    if (value.length === 4 && lastValue.length !== 5) {
      complete = value.concat('-')
    }

    if (value.length === 9 && lastValue.length !== 10) {
      complete = value.concat('-')
    }

    if (value.length === 14 && lastValue.length !== 15) {
      complete = value.concat('-')
    }

    lastValue = complete

    const cardNumber = complete.toString().replace(re, '');

    const cardType = cardNumber === undefined
      || cardNumber === ''
      || creditCardType(cardNumber).length === 0 ? undefined : creditCardType(cardNumber)[0].type;

    if (typeCard.isAmericanExpressCard(cardType) && value.length <= 18) {
      this.setState({
        flagCard: false,
        [e.target.name]: complete,
        card: cardNumber
      }, () => this.setData())
      if (ccv) {
        if (ccv.length === 4) {
          this.setState({
            ccvValidate: false
          })
        }
      }
    }

    if (!typeCard.isAmericanExpressCard(cardType) && value.length <= 19) {
      this.setState({
        flagCard: false,
        [e.target.name]: complete,
        card: cardNumber
      }, () => this.setData())
      if (ccv) {
        if (ccv.length === 4) {
          this.setState({
            ccvValidate: true
          })
        }
      }
    }
  }

  private changeTextDate(e: any) {

    this.setState({
      flagExp: false
    })

    let value = e.target.value
    const oldValue = this.state.oldValue ? this.state.oldValue : 0
    const { invalidExp } = this.state;

    if (invalidExp) {
      this.setState({
        invalidExp: false
      }, () => this.setData())
    }

    if (value.length > 1) {
      if (value.length === 2 && oldValue.length !== 3) {
        value += '/';
      }
    }

    if (value.length <= 5) {
      this.setState({
        [e.target.name]: value
      }, () => this.setData())
    } else {
      this.setState({
        oldValue: value
      }, () => this.setData())
    }
  }

  private changeCCV(e: any) {
    const { card_number } = this.state
    const value = e.target.value

    if (card_number) {
      const cardType = card_number === undefined
        || card_number === ''
        || creditCardType(card_number).length === 0 ? undefined
        : creditCardType(card_number)[0].type;

      if (!typeCard.isAmericanExpressCard(cardType) ? value.length <= 3 : value.length <= 4) {
        this.setState({
          [e.target.name]: value
        }, () => this.setData())
      }
    } else {
      if (value.length <= 4) {
        this.setState({
          [e.target.name]: value
        }, () => this.setData())
      }
    }
  }

  private setData() {
    const { setData } = this.props

    const data = this.state

    if (setData) {
      setData(data)
    }
  }

  public render() {
    const { flagName, flagCard, cardnumbervalidate,
      flagExp, invalidExp, flagCcv, ccvValidate,
    } = this.props

    return (
      <div>
        <Form style={{ margin: 50 }}>
          <Form.Item
            validateStatus={flagName ? 'error' : undefined}
            help={flagName ? Lang.get('register.payment.required') : ''}>
            <Input
              placeholder={Lang.get('register.payment.name')}
              name="name"
              onChange={this.changeText}
            />
          </Form.Item>
          <Form.Item
            validateStatus={flagCard ? 'error' : undefined}
            help={cardnumbervalidate === false ?
              Lang.get('register.payment.info_number') : flagCard ?
                Lang.get('register.payment.required') : ''}>
            <Input
              name="card_number"
              placeholder={'XXXX XXXX XXXX XXXX'}
              onChange={this.changeNumbCard}
              value={this.state.card_number}
            />
          </Form.Item>
          <Row gutter={12}>
            <Col span={10}>
              <Form.Item
                validateStatus={flagExp ? 'error' : undefined}
                help={invalidExp ? Lang.get('register.payment.invalid_exp') : flagExp ?
                  Lang.get('register.payment.required') : ''}>
                <Input
                  placeholder={Lang.get('register.payment.exp')}
                  name="exp"
                  style={{ width: 200 }}
                  onChange={this.changeTextDate}
                  value={this.state.exp}
                />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item
                validateStatus={flagCcv ? 'error' : undefined}
                help={ccvValidate ?
                  Lang.get('register.payment.invalid_ccv')
                  : flagCcv ? Lang.get('register.payment.required') : ''}>
                <Input
                  placeholder={'ccv'}
                  name="ccv"
                  onChange={this.changeCCV}
                  style={{ width: 100 }}
                  value={this.state.ccv}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

export default PaymentForm;