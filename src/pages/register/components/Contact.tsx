import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Form, Input } from 'antd';
import Notifications from 'src/components/notifications/notifications';
import { connect } from 'unstated-enhancers';
import SignInContainer from 'src/auth/state/SignInContainer';
import UpdateProfileContainer from '../state/UpdateProfileContainer'
import Lang from 'src/support/Lang';
import StepButtons from './StepButtons';

class Contact extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    name: null,
    job_title: null,
    phone: null
  }

  private handlePhoneChange(e: any) {
    const value = e.target.value
    const parse = Number(value)

    if(Number.isInteger(parse) && value.length <= 10){
      this.setState({
        flagPhone: false,
        [e.target.name]: value
      })
    }
  }

  private handleValueChange(e: any) {
    const flag = e.target.name === 'name' ? 'flagName' : 'flagJob'

    this.setState({
      [flag]: false,
      [e.target.name]: e.target.value
    });
  }

  private async updateDataContact() {
      const { contact } = this.props.containers;
      const { profileCompany, name, job_title, phone } = this.state

      if (name && job_title && phone) {
        const data = {
          contact: {
            name: this.state.name,
            job_title: this.state.job_title,
            phone: this.state.phone
          }
        }

        if (data) {
          await contact.updateProfile(data, profileCompany.uuid);
          const { error } = contact.state;

          if (error) {
            const notification = new Notifications;

            const msj = {
              type: 'error',
              message: Lang.get('register.contact.error_register')
            }

            notification.openNotification(msj)

            contact.clearError()

          } else {
            this.props.next()
          }
        }
      } else {
        this.setState({
          flagName: name ? false : true,
          flagJob: job_title ? false : true,
          flagPhone: phone ? false : true
        })
      }
  }

  private async getDataCompany() {
    const { signing } = this.props.containers

    this.setState({
      profileCompany: signing.state.data.company,
      profileUser: signing.state.data.profile
    })
  }

  public componentDidMount() {
    this.getDataCompany()
  }

  public render() {
    const { flagName, flagJob, flagPhone } = this.state
    const { current, totalSteps } = this.props
    return (
      <div>

        <Form style={{ margin: 50 }}>
          <Form.Item
            label={Lang.get('register.contact.name')}
            validateStatus={flagName ? 'error' : undefined}
            help={flagName ? Lang.get('register.contact.add_name') : ''}
          >
            <Input
              placeholder={Lang.get('register.contact.full_name')}
              name="name"
              onChange={this.handleValueChange}
            />
          </Form.Item>
          <Form.Item
            label={Lang.get('register.contact.job_title')}
            validateStatus={flagJob ? 'error' : undefined}
            help={flagJob ? Lang.get('register.contact.error_job') : ''}
          >
            <Input
              placeholder={Lang.get('register.contact.job_title')}
              name="job_title"
              onChange={this.handleValueChange}
            />
          </Form.Item>
          <Form.Item
            label={Lang.get('register.contact.phone')}
            validateStatus={flagPhone ? 'error' : undefined}
            help={flagPhone ? Lang.get('register.contact.error_phone') : ''}
          >
            <Input
              placeholder={Lang.get('register.contact.phone')}
              name="phone"
              onChange={this.handlePhoneChange}
              value={this.state.phone}
            />
          </Form.Item>
        </Form>
        <StepButtons
          current={current}
          totalSteps={totalSteps}
          next={this.updateDataContact}
        />
      </div>
    )
  }
}

const container = {
  signing: SignInContainer,
  contact: UpdateProfileContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    signing: containers.signing.state,
    contact: containers.contact.state,
  }
}

export default connect(container, mapStateToProps)(Contact);