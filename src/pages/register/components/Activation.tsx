import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Icon, Card } from 'antd';
import Lang from 'src/support/Lang';
import ResendEmailContainer from '../state/ResendEmailContainer'
import { connect } from 'unstated-enhancers';

class Activation extends Component<any, any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public async verify() {
    await this.setState({
      httpBusy: true
    })

    const { resend } = this.props.containers;

    await resend.sendEmail();
  }

  public render() {

    return (
      <div>
        <Card style={{ textAlign: 'center', margin: 50 }}>
          <Icon type="mail" style={{ fontSize: 60, color: '#5bc1be', margin: 20 }} />
          <p>{Lang.get('register.activation.verify')}</p>
          <br />
          <p>
            {Lang.get('register.activation.resend')}
            <a onClick={this.verify}>{Lang.get('register.activation.here')}</a>
          </p>
          <p>
            {Lang.get('register.activation.verified_account')}
            <a onClick={this.props.onGetChecklist}>{Lang.get('register.activation.here')}</a>
            , {Lang.get('register.activation.check')}
          </p>
        </Card>
      </div>
    )
  }
}

const container = {
  resend: ResendEmailContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    resend: containers.resend.state,
  }
}

export default connect(container, mapStateToProps)(Activation);

