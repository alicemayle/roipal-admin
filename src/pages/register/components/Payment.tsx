import React, { Component } from 'react';
import autobind from 'class-autobind';
import Lang from 'src/support/Lang';
import '../register.scss'
import PaymentForm from '../components/PaymentForm'
import Notifications from 'src/components/notifications/notifications';
import creditCardType from 'credit-card-type';
import typeCard from '../../../support/TypeCardManagement';
import { connect } from 'unstated-enhancers';
import SignInContainer from 'src/auth/state/SignInContainer';
import UpdateProfileContainer from '../state/UpdateProfileContainer';
import StepButtons from './StepButtons';
import { Row, Col, Card } from 'antd';
import Cards from 'react-credit-cards';

class Payment extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    flagName: false,
    flagCard: false,
    cardnumbervalidate: null,
    card_number: '',
    flagExp: false,
    invalidExp: false,
    exp: '',
    flagCcv: false,
    ccvValidate: false,
    ccv: '',
    name: ''
  }

  private async handleSubmit() {
    const { name, card, ccv, exp, profileCompany } = this.state
    const { register } = this.props.containers

    this.setState({
      flagName: name ? false : true,
      flagCard: card ? false : true,
      flagExp: exp ? false : true,
      flagCcv: ccv ? false : true
    })

    if (name) {
      const data = {
        payment: {
          owner: name,
          exp: this.state.exp,
          ccv: this.state.ccv,
          card_number: card,
        }
      }

      const cardType = card === undefined
        || card === ''
        || creditCardType(card).length === 0
        ? undefined : creditCardType(card)[0].type;

      if ((!typeCard.isAmericanExpressCard(cardType) && card.length === 16
        && ccv.length === 3)
        || (typeCard.isAmericanExpressCard(cardType) && card.length === 15
          && ccv.length === 4)) {
        this.setState({
          cardnumbervalidate: true
        })

        if ((!typeCard.isAmericanExpressCard(cardType) && this.state.ccv.length !== 3)
          || (typeCard.isAmericanExpressCard(cardType) && this.state.ccv.length !== 4)) {
          this.setState({
            ccvValidate: false
          })
          return;
        }
      }

      const validExp = await this.validateExp();

      if (validExp) {
        await register.updateProfile(data, profileCompany.uuid);
        const { error } = register.state

        if (error) {
          const notification = new Notifications;

          const msj = {
            type: 'error',
            message: Lang.get('register.payment.register_error')
          }

          notification.openNotification(msj)

          register.clearError()
        } else {
          this.props.onGoHome()

          const random = Number(Math.random() * (9999999999 - 1000000000) + 1000000000);
          const str = 'test_payer_'
          const emailTest = str.concat(random.toString(), '@testuser.com');

          const customer = {
            payment: {
              email: emailTest,
              card_number: card,
              exp: this.state.exp,
              ccv: this.state.ccv,
              owner: register.state.data.owner.name
            }
          }
          if (customer) {
            await register.createCustomer(profileCompany.uuid, customer);
            const registerError = this.props.containers.register.state.error
            const mess = this.props.containers.register.state.message

            if (registerError) {
              const notification = new Notifications;

              const msj = {
                type: 'error',
                message: mess
              }

              notification.openNotification(msj)

              register.clearError()
            }
          }
        }
      }
    }
  }

  private async validateExp() {
    const { exp } = this.state;

    const month = new Date().getMonth();
    const year = new Date().getFullYear();

    if (exp) {
      const arrayDate = exp.split('/');

      const mm = Number(arrayDate[0])
      const yy = Number(arrayDate[1])

      if (mm > 12 || mm < 1) {
        this.setState({
          invalidExp: true
        })
        return false;
      }

      if (yy < Number(year.toString().substr(2, 3))) {
        this.setState({
          invalidExp: true
        })
        return false;
      } else if (yy === Number(year.toString().substr(2, 3))) {
        if (mm <= (month + 1)) {
          this.setState({
            invalidExp: true
          })
          return false;
        } else {
          this.setState({
            invalidExp: false
          })
          return true;
        }
      } else {
        this.setState({
          invalidExp: false
        })
        return true;
      }
    }
    return false
  }

  private async getDataCompany() {
    const { signing } = this.props.containers

    this.setState({
      profileCompany: signing.state.data.company,
      profileUser: signing.state.data.profile
    })
  }

  private setData(data: any) {
    this.setState({
      ...data
    })
  }

  public componentDidMount() {
    this.getDataCompany()
  }

  public render() {
    const { flagName, flagCard, cardnumbervalidate, flagExp, invalidExp,
      exp, flagCcv, ccvValidate, ccv, name, card } = this.state
    const { current, totalSteps } = this.props
    const cardCondicion = card ? card : ''

    return (
      <div>
        <Row gutter={20} type="flex" justify="center">
          <Col span={8}>
            <Card style={{ marginTop: 35, border: 0 }}>
              <Cards
                number={cardCondicion}
                name={name}
                expiry={exp}
                cvc={ccv}
                focused={name}
                preview={true}
              />
            </Card>
          </Col>
          <Col span={13}>
            <PaymentForm
              flagName={flagName}
              flagCard={flagCard}
              cardnumbervalidate={cardnumbervalidate}
              flagExp={flagExp}
              invalidExp={invalidExp}
              flagCcv={flagCcv}
              ccvValidate={ccvValidate}
              setData={this.setData}
            />
          </Col>
        </Row>
        <StepButtons
          current={current}
          totalSteps={totalSteps}
          next={this.handleSubmit}
        />
        <br />
        <br />
      </div>
    )
  }
}

const container = {
  signing: SignInContainer,
  register: UpdateProfileContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    signing: containers.signing.state,
    register: containers.register.state,
  }
}

export default connect(container, mapStateToProps)(Payment);