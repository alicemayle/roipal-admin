import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Button } from 'antd';

class StepButtons extends Component<any, any> {
  constructor(props: any) {
    super(props);
    autobind(this);
    this.state = {
      current: 0,
    };
  }

  private next() {
    const current = this.props.current + 1;
    this.props.next(current);
  }

  public render() {
    const { current, totalSteps } = this.props;

    return (
          <div className="steps-action">
            {current < totalSteps - 1 && current > 0 && (
              <Button type="primary"
              onClick={this.next}>
                Next
            </Button>
            )}
            {current === totalSteps - 1 && (
              <Button type="primary"
              onClick={this.next}>
                Done
            </Button>
            )}
          </div>
    )
  }
}

export default (StepButtons);
