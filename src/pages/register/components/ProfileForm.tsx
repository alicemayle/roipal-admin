import React, { Component } from 'react';
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';
import { Form, Input, Select } from 'antd';
import Lang from '../../../support/Lang';
import ProfileContainer from '../state/ProfileContainer'
import SelectAddress from 'src/components/selectAddress/SelectAddress';
import Map from 'src/pages/locations/components/Map';

const { Option } = Select;

interface HandleChange {
  target: HTMLInputElement
}

interface AddressSelected {
  latLng: { lat: number, lng: number }
  address: string
}

class ProfileForm extends Component<any, any> {
  constructor(props: any) {
    super(props);
    autobind(this)

    this.state = {
      data: {
        country: 'México'
      },
    }
  }

  public async getLineBusiness() {
    const { profile } = this.props.containers;

    await profile.getLineBusiness();
  }

  private renderOptions(data: any) {
    return (
      <Option key={data} value={data}>
        {data}
      </Option>
    );
  }

  private onChangeText(e: HandleChange) {
    const value = e.target.value;
    const name = e.target.name;

    this.setState({
      data: {
        ...this.state.data,
        [name]: value
      }
    }, () => this.props.onChangeData(this.state.data))
  }

  private onChangeWebsite(e: HandleChange) {
    const value = e.target.value;
    const name = e.target.name;
    const website = 'https://'.concat(value)

    this.setState({
      data: {
        ...this.state.data,
        [name]: website
      }
    }, () => this.props.onChangeData(this.state.data))
  }

  public onChangeLineBusiness(value: string) {
    this.setState({
      data: {
        ...this.state.data,
        vertical: value
      }
    }, () => this.props.onChangeData(this.state.data))
  }

  private onChangeAddress(data: AddressSelected) {
    this.setState({
      data: {
        ...this.state.data,
        address: data
      }
    }, () => this.props.onChangeData(this.state.data))
  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
    this.getLineBusiness()
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { data } = this.props.profile
    const address = this.state.data.address
    const { validateAddress } = this.props

    return (
      <Form style={{ marginTop: 15 }}>
        <Form.Item label={Lang.get('register.profile.description')}>
          {getFieldDecorator('description', {
            rules: [
              {
                required: true,
                message: Lang.get('register.profile.validations.description')
              }],
          })(
            <Input
              placeholder={Lang.get('register.profile.description')}
              onChange={this.onChangeText}
              name={'bio'}
            />
          )}

        </Form.Item>
        <Form.Item label={Lang.get('register.profile.website')}>
          <Input
            addonBefore="https://"
            placeholder={Lang.get('register.profile.website')}
            onChange={this.onChangeWebsite}
            name={'website'}
          />
        </Form.Item>
        <Form.Item label={Lang.get('register.profile.slogan')}>
          <Input
            placeholder={Lang.get('register.profile.slogan')}
            onChange={this.onChangeText}
            name={'slogan'}
          />
        </Form.Item>
        <Form.Item label={Lang.get('register.profile.vertical')}>
          {getFieldDecorator('vertical', {
            rules: [
              {
                required: true,
                message: Lang.get('register.profile.validations.vertical'),
              },
            ],
          })(
            <Select
              showSearch={true}
              placeholder={Lang.get('register.profile.vertical')}
              onChange={this.onChangeLineBusiness}
            >
              {
                data && data.map(this.renderOptions)
              }
            </Select>
          )}
        </Form.Item>
        <Form.Item
          validateStatus={!validateAddress ? 'error' : undefined}
          help={!validateAddress ? Lang.get('register.profile.validations.address') : undefined}
          label={Lang.get('register.profile.address')}>
          {getFieldDecorator('address', {
            rules: [
              {
                required: true,
              },
            ],
          })(
          <SelectAddress
            onChangeAddress={this.onChangeAddress}
            location={address}
          />
          )}
        </Form.Item>
        {
          address &&
          <Map
            addressSelected={address}
            onChangeAddress={this.onChangeAddress}
          />
        }
      </Form>
    )
  }
}

const container = {
  profile: ProfileContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    profile: containers.profile.state,
  }
}

const WrappedProfileForm: any = Form.create()(ProfileForm);

export default connect(container, mapStateToProps)(WrappedProfileForm);

