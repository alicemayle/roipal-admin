import React, { Component } from 'react';
import autobind from 'class-autobind';
import '../register.scss'
import { Upload } from 'antd';
import avatar from '../../../images/user_business.png'

class Avatar extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
  };

  public render() {
    const { beforeUpload, onChange, imageUrl,
      uploadButton, cambio, bio_ima } = this.props

    return (
      <div>
        {
          !cambio &&
          <img
            alt="example"
            src={imageUrl ? imageUrl : bio_ima ? bio_ima : avatar}
            style={{ width: '100%', height: '100%' }}
          />
        }
        {
          cambio &&
          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={beforeUpload}
            onChange={onChange}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          >
            {imageUrl ? <img src={imageUrl} alt="avatar"
              style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        }
      </div>
    )
  }
}

export default Avatar;