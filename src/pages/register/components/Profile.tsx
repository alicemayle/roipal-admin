import React, { Component } from 'react';
import autobind from 'class-autobind';
import SignInContainer from '../../../auth/state/SignInContainer';
import { connect } from 'unstated-enhancers';
import { Card, Icon, message, Row, Col, Form } from 'antd';
import Avatar from './Avatar';
import Meta from 'antd/lib/card/Meta';
import ProfileForm from './ProfileForm'
import StepButtons from './StepButtons';
import ProfileContainer from '../state/ProfileContainer';
import Notifications from 'src/components/notifications/notifications';

class Profile extends Component<any, any> {
  public form: any = null;

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    loading: false,
    cambio: false,
    data: {},
    validateAddress: true
  };

  private beforeUpload(file: any) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  }

  private handleChangeAvatar(info: any) {
    if (info.file.status === 'uploading') {
      this.setState({
        loading: true
      });
      return;
    }
    if (info.file.status === 'done') {
      let fileList = [...info.fileList];
      fileList = fileList.slice(-1);
      fileList = fileList.map(file => {
        if (file.response) {
          this.setState({
            data: {
              ...this.state.data,
              avatar: file.response
            }
          })
        }
        return file;
      });
      this.getBase64(info.file.originFileObj, (imageUrl: any) =>
        this.setState({
          imageUrl,
          loading: false,
          cambio: false,
        }),
      );
    }
  }

  private getBase64(img: any, callback: any) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  private set() {
    this.setState({
      cambio: true
    })
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  private async onSaveProfile() {
    const { data } = this.state
    const { profile } = this.props.containers
    let newData = {}

    if (!data.address) {
      this.setState({
        validateAddress: false
      })
    }

    this.form.validateFields(async (err: any, values: any) => {
      if (!err) {

          newData = {
            profile: {
              ...data,
              address: data.address.address,
              position: {
                latitude: data.address.latLng.lat,
                longitude: data.address.latLng.lng
              }
            }
          }

          await profile.updateProfile(newData)

          const { error } = profile.state

          if(!error) {
            this.props.next()
          } else {
            const notification = new Notifications;
            const msj = {
              type: 'error',
              message:  error.message
            }
            notification.openNotification(msj)
    
            profile.clearError()
          }
      }
    });
  }

  private handleChangeData(_data: any) {
    let _validateAddress = this.state.validateAddress

    if (_data.address) {
      _validateAddress = true
    }

    this.setState({
      data: _data,
      validateAddress: _validateAddress
    })
  }

  public render() {
    const user = this.props.signin.data.profile;
    const { imageUrl, cambio, validateAddress } = this.state;
    const { current, totalSteps } = this.props

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' :
          imageUrl ? 'plus' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    return (
      <div>
        <Row gutter={20} style={{ margin: 50 }}>
          <Col span={7} >
            <Card
              style={{ width: 250, margin: 25 }}
              cover={
                <Avatar
                  bio_ima={user.avatar}
                  beforeUpload={this.beforeUpload}
                  onChange={this.handleChangeAvatar}
                  imageUrl={imageUrl}
                  uploadButton={uploadButton}
                  cambio={cambio}
                />
              }
              actions={[
                <Icon type="upload" key="upload" onClick={this.set} />
              ]}
            >
              <Meta
                title={user.email}
              />
            </Card>
          </Col>
          <Col span={14} >
            <ProfileForm
              onValidate={this.handleValidateForm}
              onChangeData={this.handleChangeData}
              validateAddress={validateAddress}
            />
          </Col>
        </Row>
        <StepButtons
          current={current}
          totalSteps={totalSteps}
          next={this.onSaveProfile}
        />
      </div>
    )
  }
}

const container = {
  signin: SignInContainer,
  profile: ProfileContainer
}

const mapStateToProps = (containers: any) => {
  return {
    signin: containers.signin.state,
    profile: containers.profile.state
  }
}

const WrappedProfileForm: any = Form.create()(Profile);

export default connect(container, mapStateToProps)(WrappedProfileForm);

