import { Http } from '../../../shared';
import Auth from '../../../auth/User'
class Register {

  public sendEmail() : any {
    return Http.post('/api/users/email-verification/');
  }

  public getChecklist(params: any): any {
    const company = Auth.getData().profile.uuid;

    return Http.get(`/api/users/${company}`, { params });
  }

  public getLineBusiness() {
    return Http.get(`/api/translations/catalogs?namespace=catalogs&group=line_business`);
  }

  public  updateProfile(data: any) {
    const company = Auth.getData().company.uuid;

    return Http.put(`/api/companies/${company}`, data);
  }

  public update(data:any, uuid:any): any {
    return Http.put(`/api/companies/${uuid}?includes=profile,payments_profiles,owner`, data)
  }

  public createCustomer(uuid:string, data:any) {
    return Http.post(`api/payment/create-customer/${uuid}`, data)
  }

}

export default new Register;