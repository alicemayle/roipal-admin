import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import LanguajesListContainer from './containers/LanguageListContainer';
import ListTable from './components/ListTable';
import ListProps from './models/ListProps';
import { Button, notification, Select, Divider, Row, Col } from 'antd';
import Autobind from 'class-autobind';
import './styles.scss';
import Upsert from './Upsert';
import Pagination from '../../components/pagination/Pagination';
import Lang from '../../support/Lang';
import { Gate } from '@xaamin/guardian';
const { Option } = Select;
interface ListState {
  openUpsert: boolean
  profile: {
    text: string,
    item: string,
    group: string,
    locale: string,
    namespace: string,
    id?: number,
  }
  filter: boolean,
  namespace: string,
  limit: number,
  page: number
}

const openNotification = (type: string, title: string, message: string) => {
  notification[type]({
    message: title,
    description: message
  });
};

class List extends Component<ListProps, ListState> {
  public form: any = null;
  constructor(props: ListProps) {
    super(props);
    this.state = {
      openUpsert: false,
      profile: {
        text: '',
        item: '',
        group: '',
        locale: '',
        namespace: '*'
      },
      filter: false,
      namespace: '',
      limit: 50,
      page: 1
    }
    Autobind(this);
  }

  public componentDidMount() {
    this.handleFetch();
  }

  public handleFetch(limit?: number, page?: number) {
    const { languages } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    languages.fetch(meta);
  }

  public handleFilterNamespace(app?: string, limit?: number, page?: number) {
    const { languages } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      namespace: app || this.state.namespace,
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    languages.filterNamespace(meta);
  }

  public handleOnComplete(type: string, title: string, message: string) {
    openNotification(type, title, message);

    if (type === 'success') {
      this.handleCloseUpsert();

      if (this.state.filter) {
        this.handleFilterNamespace()
      } else {
        this.handleFetch();
      }
    }
  }

  private handleOpenUpsert(record: any = {}) {
    let _profile;
    if (record && record.text) {
      _profile = record;
    } else {
      _profile = { ...this.state.profile };
    }
    this.setState({
      profile: _profile,
      openUpsert: true
    })
  }

  private handleCloseUpsert() {
    this.setState({
      openUpsert: false,
      profile: {
        text: '',
        item: '',
        group: '',
        locale: '',
        namespace: '*'
      }
    })
  }

  public handleChange(value: string, field: string) {
    const { profile } = this.state;
    const _profile = {
      ...profile,
      [field]: value
    }
    this.setState({
      profile: _profile
    })
  }

  public handleChangeSelect(value: string) {
    this.setState({
      filter: true,
      namespace: value
    })
    this.handleFilterNamespace(value)
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    if (this.state.filter) {
      this.handleFilterNamespace(this.state.namespace, _limit, _page)
    } else {
      this.handleFetch(_limit, _page)
    }
  }

  public render() {
    const { data, httpBusy, meta } = this.props.languages;

    const { profile } = this.state;

    return (
      <div>
        <h1>{Lang.get('translations.title')}</h1>
        <Divider />
        <Row gutter={16}>
          <Col className="gutter-row" span={16}>
            <h1>{}</h1>
          </Col>
          <Col className="gutter-row"
            span={8}
            style={{ textAlign: 'right', marginBottom: 16 }}>
            {
              Gate.allows('translation::create') &&
              <Button
                type="primary"
                onClick={this.handleOpenUpsert}
              >
                {Lang.get('generic.button.add')}
              </Button>
            }
            <Select
              placeholder={Lang.get('translations.select')}
              style={{ marginLeft: 20, width: 200 }}
              onChange={this.handleChangeSelect}
            >
              <Option value="business">Business</Option>
              <Option value="executive">Executive</Option>
              <Option value="admin">Web-Admin</Option>
              <Option value="*">*</Option>
            </Select>
          </Col>
        </Row>
        <ListTable
          data={data}
          meta={meta}
          httpBusy={httpBusy}
          onOpenUpsert={this.handleOpenUpsert} />
        {meta &&
          <Pagination
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
            limit={this.state.limit}
          />
        }
        <Upsert
          profile={profile}
          open={this.state.openUpsert}
          onClose={this.handleCloseUpsert}
          onChange={this.handleChange}
          onComplete={this.handleOnComplete}
        />
      </div>
    )
  }
}

const container = {
  languages: LanguajesListContainer
}

const mapStateToProps = (containers: any) => {
  return {
    languages: containers.languages.state
  }
}

export default connect(container, mapStateToProps)(List);