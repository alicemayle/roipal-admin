import { Container } from 'unstated';
import Language from '../services/Languages';
import LanguageListLanguageState from '../models/containers/LanguageListLanguageState';

const initialState = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
}

class LanguageListLanguageContainer extends Container<LanguageListLanguageState> {
    public state:LanguageListLanguageState = {
        ...initialState
    }

    public async fetch(params:{} = {}) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'LIST LANGUAGE STARTS'
            });

            const response = await Language.getLanguages(params);

            this.setState({
                httpBusy: false,
                data: response.data,
                __action: 'LIST LANGUAGE SUCCESS'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'LIST LANGUAGE ERROR'
            });
        }
    }

    public reset() {
        this.setState({
            ...initialState
        });
    }

    public clearError() {
        this.setState({
            ...this.state,
            error: null,
            message: ''
        })
    }
}

export default LanguageListLanguageContainer;