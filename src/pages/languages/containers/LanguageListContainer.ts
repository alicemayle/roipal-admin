import { Container } from 'unstated';
import Languages from '../services/Languages';
import LanguagesList from '../models/containers/LanguagesList';

const initialState = {
    httpBusy: true,
    data: null,
    meta: null,
    error: null,
    message: ''
}

class LanguagesListContainer extends Container<LanguagesList> {
    public state: LanguagesList = {
        ...initialState
    }

    public async fetch(params:{}) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'LANGUAGES LIST STARTS'
            });

            const response = await Languages.list(params);

            response.data.map((language:any, index:number) =>{
                response.data[index].key = language.id;
            })

            this.setState({
                data: response.data,
                meta: response.meta,
                httpBusy: false,
                
                __action: 'LANGUAGES LIST SUCCESS'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'LANGUAGES LIST ERROR'
            })
        }
    }

    public async filterNamespace(params:{}) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'LANGUAGES LIST FILTER NAMESPACE STARTS'
            });

            const response = await Languages.filterNamespace(params);

            response.data.map((language:any, index:number) =>{
                response.data[index].key = language.id;
            })

            this.setState({
                data: response.data,
                meta: response.meta,
                httpBusy: false,
                
                __action: 'LANGUAGES LIST FILTER NAMESPACE SUCCESS'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'LANGUAGES LIST FILTER NAMESPACE ERROR'
            })
        }
    }

    public reset() {
        this.setState({
            ...initialState
        });
    }

    public clearError() {
        this.setState({
            ...this.state,
            message: '',
            error: null
        })
    }
}

export default LanguagesListContainer;