import { Container } from 'unstated';
import Language from '../services/Languages';
import Event from '../../../support/Event';
import LanguageUpsertState from '../models/containers/LanguageUpsertState';

const initialState = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
}

class LanguageUpsertContainer extends Container<LanguageUpsertState> {
    public state:LanguageUpsertState = {
        ...initialState
    }

    public async upsert(data:{
        id?: string
        text: string,
        item: string,
        group: string,
        namespace: string,
        lang?: number,
        locale?: string
    }) {
        let result;
        let _action;
        try {
            this.setState({
                httpBusy: true,
                __action: 'LANGUAGE UPSERT STARTS'
            });
            
            let response;

            if (data.id) {
                response = await Language.update(data.id, data);
                _action = 'CREATE LANGUAGE';
            } else {
                response = await Language.create(data);
                _action = 'UPDATE LANGUAGE';
            }
            result = {
                ...response,
                action: _action
            }

            this.setState({
                httpBusy: false,
                data: response.data,
                __action: 'LANGUAGE UPSERT SUCCESS'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'LANGUAGE UPSERT ERROR'
            })

            result = {
                error,
                action: 'LANGUAGE UPSERT ERROR'
            }
        }
        Event.notify('language::upsert', result);
    }

    public clearError() {
        this.setState({
            ...this.state,
            error: null,
            message: null
        })
    }

    public reset() {
        this.setState({
            ...initialState
        })
    }
}

export default LanguageUpsertContainer;