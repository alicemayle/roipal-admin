interface UpserProps {
    open: boolean,
    onClose: ()=>void,
    onChange: (value:string, field: string) =>void,
    onComplete: (type: string,
                title: string,
                message: string
                ) =>void,
    profile: {
        id?: number,
        text: string,
        item: string,
        group: string,
        locale: string,
        namespace: string,
        lang?: number
    },
    containers: {
        upsert: {
            upsert:(data:{})=>void
        },
        list: {
            fetch:(params:{}) =>void
        }
    },
    upsert: {
        httpBusy: boolean
    },
    nextProps: {
        open: boolean,
        profile: {},
        upsert: {
            httpBusy: boolean
        }
    },
    list: {
        data: {}
    }
}

export default UpserProps;