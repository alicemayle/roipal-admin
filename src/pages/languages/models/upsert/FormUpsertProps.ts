interface FormUpsertProps {
    form?:any,
    name?:string,
    profile: {
        id?: number,
        text: string,
        item: string,
        group: string,
        locale: string,
        namespace: string,
        lang?: number
    },
    language: [],
    onValidate: (form:any)=>void,
    onChange: (value: string|number, field: string) =>void,
    nextProps: {
        profile: {},
        language: []
    }
}

export default FormUpsertProps;