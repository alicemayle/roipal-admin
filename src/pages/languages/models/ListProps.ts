interface ListProps {
  containers: any,
  languages: {
    data:{},
    meta: {
      cursor:{
        current: number,
        prev: number,
        next: number,
        count: number
      }
    },
    httpBusy: boolean
  },
  data: {},
  httpBusy: boolean,
  meta: {
    cursor:{
        current: number,
        prev: number,
        next: number,
        count: number
    }
},
}

export default ListProps;