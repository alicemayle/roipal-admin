interface ListTableProps {
  data: any,
  httpBusy: boolean,
  meta?: {},
  onOpenUpsert:(record:{})=>void,
  nextProps?: {
    data: {},
    httpBusy: boolean
  }
}

export default ListTableProps;