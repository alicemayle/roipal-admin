interface LanguageUpsertState {
    httpBusy: boolean,
    data: {} | null,
    error: {} | null,
    message: string | null,
    __action?: string
}

export default LanguageUpsertState;