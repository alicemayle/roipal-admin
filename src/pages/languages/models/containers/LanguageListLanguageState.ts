interface LanguageListLanguageState {
    httpBusy: boolean,
    data: {} | null,
    error: {} | null,
    message: string,
    __action?: string
}

export default LanguageListLanguageState;