interface LanguagesList {
    httpBusy: boolean,
    data: {} | null,
    error: {} | null,    
    message: string,
    __action?: string,
    meta: {} | null,
}

export default LanguagesList;