import { Http } from '../../../shared';
// import { AxiosResponse } from 'axios';

class Languages {

  public list( params: any): any {// Promise<AxiosResponse>
    return Http.get(`/api/translations`, { params })
  }
  
  public filterNamespace( params: any): any {// Promise<AxiosResponse>
    return Http.get(`/api/translations/catalogs?`, { params })
  }

  public create( data:{
    text: string,
    item: string,
    group: string,
    locale?: string,
    namespace: string,
    lang?: number
  }) {
    const lang = data.lang;
    delete data.lang;
    return Http.post(`/api/translations/${lang}`, data);
  }

  public update(id:string, data: {
    text: string,
    item: string,
    group: string,
    namespace: string
  }) {
    return Http.put(`/api/translations/${id}`, data);
  }

  public getLanguages(params:{} = {}) {
    return Http.get(`/api/languages`, params);
  }



}

export default new Languages;