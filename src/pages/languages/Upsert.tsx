import React, { Component } from 'react';
import { Modal, Button, message } from 'antd';
import Autobind from 'class-autobind';
import FormUpsert from './components/upsert/FormUpsert';
import LanguageUpsertContainer from './containers/LanguageUpsertContainer';
import LanguageListLanguageContainer from './containers/LanguageListLanguageContainer';
import { connect } from 'unstated-enhancers';
import Event from '../../support/Event';
import UpserProps from './models/upsert/UpserProps';
import Lang from '../../support/Lang';
import { Gate } from '@xaamin/guardian';

class Upsert extends Component<UpserProps> {
  public form: any = null;
  constructor(props: UpserProps) {
    super(props);
    Autobind(this);
    Event.listen('language::upsert', this.listeForUpsert);
  }

  public componentDidMount() {
    const { list } = this.props.containers;
    const meta = {
      limit: '1'
    }
    list.fetch(meta);
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {

    if (!result.error) {
      this.props.onComplete('success', result.action, result.message);
      this.form.resetFields();
    } else {
      this.props.onComplete('error', result.action, result.error.message);
    }

  }

  private handleClose() {
    this.props.onClose();
    this.form.resetFields();
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  public handleSubmit() {
    const { profile } = this.props;
    const permission = profile.id 
      ? Gate.allows('translation::edit')
      : Gate.allows('translation::create')
    
    this.form.validateFields(
      (err: any,
        values: {
          group: string,
          item: string,
          text: string
        }
      ) => {
        if (!err) {
          if(permission) {
            const data = {
              namespace: this.props.profile.namespace,
              group: values.group,
              item: values.item,
              text: values.text,
              id: this.props.profile.id,
              lang: this.props.profile.lang
            }
            const { upsert } = this.props.containers;
            upsert.upsert(data);
          } else {
            message.error(Lang.get('generic.policies.denied'));
          }
        }
      })
  }

  public componentWillUnmount() {
    Event.remove('language::upsert', this.listeForUpsert);
  }

  private handleChange(value: string, field: string) {
    this.props.onChange(value, field);
  }

  public shouldComponentUpdate(nextProps: UpserProps) {
    return (nextProps.open !== this.props.open
      || nextProps.profile !== this.props.profile
      || nextProps.upsert.httpBusy !== this.props.upsert.httpBusy);
  }

  public render() {
    const { open, profile } = this.props;
    const { httpBusy } = this.props.upsert;
    const { data } = this.props.list;
    return (
      <Modal
        title={profile.id
          ? Lang.get('translations.edit_translation')
          : Lang.get('translations.create_translation')}
        style={{ top: 20 }}
        visible={open}
        centered={true}
        onCancel={this.handleClose}
        footer={[
          <Button
            key="cancel"
            onClick={this.handleClose}>
            {Lang.get('generic.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={httpBusy}
            onClick={this.handleSubmit}>
            {Lang.get('generic.button.accept')}
                    </Button>,
        ]}
      >
        <FormUpsert
          profile={profile}
          onValidate={this.handleValidateForm}
          onChange={this.handleChange}
          language={data}
        />
      </Modal>
    );
  }
}

const container = {
  upsert: LanguageUpsertContainer,
  list: LanguageListLanguageContainer
}

const mapStateToProps = (containers: any) => {
  return {
    upsert: containers.upsert.state,
    list: containers.list.state
  }
}

export default connect(container, mapStateToProps)(Upsert);