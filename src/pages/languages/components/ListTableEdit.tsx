import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import ListTableEditProps from '../models/ListTableEditProps';
import Lang from '../../../support/Lang';

class ListTableEdit extends PureComponent<ListTableEditProps> {
    constructor(props: ListTableEditProps) {
        super(props);
        Autobind(this);
    }

    private handleEdit() {
        this.props.onOpenEdit(this.props.record);        
    }

    public render() {
        return(
            <div>
                <a onClick={this.handleEdit}>{Lang.get('generic.button.edit')}</a>
            </div>
        )
    }
}

export default ListTableEdit;