import React, { Component } from 'react';
import {  Table, Icon } from 'antd';
import ListTableProps from '../models/ListTableProps';
import autobind from 'class-autobind';
import VerifyJSON from '../helpers/VerifyJSON';
import Lang from '../../../support/Lang';
import FilterInterface from 'src/components/search/FilterInterface';
import FilterSearch from 'src/components/search/FilterSearch';
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';
import ReadMore from 'src/components/readMore/ReadMore';
import flagUSA from '../../../images/united-states.svg'
import flagMEX from '../../../images/mexico.svg'
import flagFRA from '../../../images/france.svg'

const { Column } = Table;

class ListTable extends Component<ListTableProps, any> {
  public searchInput: any = null;
  public searchField: string = '';

  constructor(props: ListTableProps) {
    super(props);
    this.state = {
      editingKey: false,
      searchText: '',
      fields: [],
    };
    autobind(this)
  }

  private renderColumn(record: any) {
    return (
      <IconButton
      item={record}
      onFuction={this.handleOpenEdit}
      size={18}
      colorIcon={'#52bebb'}
      name={'edit'}
    />
    );
  }

  private renderTextColumn(record: any) {
    const isJSON =  VerifyJSON.isJSON(record);
    const parseData = isJSON ? JSON.stringify(record) : record;

    return(
      <ReadMore description={parseData} />
    );
  }

  private handleOpenEdit(record:any) {
    this.props.onOpenUpsert(record);
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  })

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private renderFlag(flag: any){

    const url = flag === 'en' ? flagUSA : flag === 'es' ? flagMEX : flagFRA

    return(
      <img src={url} width="20" height="20" />
    )
  }

  public render() {
    const { data = {}, httpBusy } = this.props;

    return (
      <Table
        dataSource={data}
        loading={httpBusy}
        pagination={false}
        scroll={{ x: 420 }}
      >
        <Column
          title={Lang.get('translations.language')}
          dataIndex="locale"
          key="uuid"
          align={'center'}
          render={this.renderFlag}
          {...this.getColumnSearchProps('locale')}
        />
        <Column
          title="App"
          dataIndex="namespace"
          key="namespace"
        />
        <Column
          title={Lang.get('translations.group')}
          dataIndex="group"
          key="group"
          {...this.getColumnSearchProps('group')}
          align={'center'}
        />
        <Column
          title="Item"
          dataIndex="item"
          key="item"
          {...this.getColumnSearchProps('item')}
          align={'center'}
        />
        <Column
          title={Lang.get('translations.translate')}
          dataIndex="text"
          key="text"
          {...this.getColumnSearchProps('text')}
          render={this.renderTextColumn}
        />
        {
          Gate.allows('translation::edit') &&
          <Column
            title={Lang.get('generic.button.actions')}
            key="0"
            dataIndex=""
            render={this.renderColumn}
            align={'center'}
          />
        }

      </Table>
    );
  }

}

export default ListTable;

