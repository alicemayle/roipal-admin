import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { Form, Icon, Input, Select } from 'antd';
const { TextArea } = Input;

import FormUpsertProps from '../../models/upsert/FormUpsertProps';
import VerifyJSON from '../../helpers/VerifyJSON';
import Lang from '../../../../support/Lang';

type validateStatus = '' | 'error' | 'success' | 'warning' | 'validating' | undefined;

interface FormUpsertState {
  validationParse: validateStatus;
}


const Option = Select.Option;

class FormUpsert extends Component<FormUpsertProps, FormUpsertState> {
  constructor(props: FormUpsertProps) {
    super(props);
    Autobind(this);
    this.state = {
      validationParse: 'success'
    }

  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
  }

  private handleChange(e: any) {
    const value = e.target.value;
    const field = e.target.name;

    if (VerifyJSON.isJSON(value)) {
      const parse = VerifyJSON.parseJSON(value);
      this.setState({
        validationParse: parse ? 'success' : 'error'
      });
    }

    this.props.onChange(value, field)
  }

  // private renderLanguage(lang: { id: number, name: string }, index: number) {
  //   return (
  //     <Option value={lang.id} key={index}>{lang.name}</Option>
  //   )
  // }

  private handleChangeLanguage(value: number) {
    this.props.onChange(value, 'lang')
  }

  public handleChangeNamespace(value: string) {
    this.props.onChange(value, 'namespace')
  }

  public shouldComponentUpdate(nextProps: FormUpsertProps) {
    return (nextProps.profile !== this.props.profile
      || nextProps.language !== this.props.language)
  }

  public render() {
    const {
      getFieldDecorator
    } = this.props.form;

    const { profile } = this.props;
    const { validationParse } = this.state;

    const isJSON = VerifyJSON.isJSON(profile.text);
    const parseText = isJSON ? JSON.stringify(profile.text) : profile.text;

    return (
      <Form  >
        <Form.Item validateStatus={validationParse} >
          {getFieldDecorator('text', {
            initialValue: parseText,
            rules: [{
              required: true,
              message: Lang.get('translations.validations.translate')
            }],
          })(
            <TextArea rows={4}
              placeholder={Lang.get('translations.translate')}
              name="text"
              onChange={this.handleChange}
            />
          )}
        </Form.Item>

        <Form.Item >
          {getFieldDecorator('item', {
            initialValue: profile.item,
            rules: [{
              required: true,
              message: Lang.get('translations.validations.item')
            }],
          })(
            <Input prefix={<Icon type="lock"
              style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="item"
              disabled={profile.id ? true : false}
              name="item"
              onChange={this.handleChange} />
          )}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('group', {
            initialValue: profile.group,
            rules: [{
              required: true,
              message: Lang.get('translations.validations.group')
            }],
          })(
            <Input prefix={<Icon type="usergroup-add"
              style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder={Lang.get('translations.group')}
              disabled={profile.id ? true : false}
              name="group"
              onChange={this.handleChange} />
          )}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('namespace', {
            initialValue: profile.namespace,
            rules: [{
              required: true,
              message: Lang.get('translations.validations.namespace')
            }],
          })(
            <Select
              placeholder={Lang.get('translations.select')}
              onChange={this.handleChangeNamespace}
              disabled={profile.id ? true : false}
            >
              <Option value="business">Business</Option>
              <Option value="executive">Executive</Option>
              <Option value="admin">Web-Admin</Option>
              <Option value="*">*</Option>
            </Select>
          )}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('locale', {
            initialValue: profile.locale,
            placeholder: 'Select a lang',
            rules: [{
              required: true,
              message: Lang.get('translations.validations.language')
            }],
          })(
            <Select
              placeholder={Lang.get('translations.language')}
              onChange={this.handleChangeLanguage}
              disabled={profile.id ? true : false}
            >
              {/*language && language.map(this.renderLanguage)*/}
              <Option value={2}>{Lang.get('generic.language.english')}</Option>
              <Option value={1}>{Lang.get('generic.language.spanish')}</Option>
              <Option value={3}>{Lang.get('generic.language.french')}</Option>
            </Select>
          )}
        </Form.Item>
      </Form>
    );
  }
}

const WrapperFormUpsert: any = Form.create({ name: 'formUpsert' })(FormUpsert);

export default WrapperFormUpsert;