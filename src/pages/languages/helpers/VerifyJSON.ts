class VerifyJSON {

  public isJSON(data: any) {
    const type = typeof (data)
    const exp = /{{1}.*}{1}/
    const checkText = JSON.stringify(data);
    const text = checkText.match(exp);

    if (type === 'object' || text) {
      return true;
    }
    else {
      return false;
    }
  }

  public parseJSON(data: any) {
    try {
      const convert = JSON.parse(data)
      return convert;

    } catch (error) {
      return false;
    }

  }

}


export default new VerifyJSON();