import { ItemState } from '../../../roipal/models';

interface AssestmentsUpsertState extends ItemState {
  __action?: string
}

export default AssestmentsUpsertState;