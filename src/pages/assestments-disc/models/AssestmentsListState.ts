import { ListState } from '../../../roipal/models';

interface AssestmentsListState extends ListState {
  __action?: string
}

export default AssestmentsListState;