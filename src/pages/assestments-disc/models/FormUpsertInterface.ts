interface FormUpsertInterface {
    onSubmit:()=>void,
    data:{
        questions:[],
        name: string
    },
    onChange:(data:{})=>void,
    onChangeQuestion:(name:string, value: string)=>void,
    form:any
}

export default FormUpsertInterface;