import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';
import AssestmentsListContainer from './state/AssestmentsListContainer';
import ListTable from './components/ListTable';
import { Upload, message, Button, Divider } from 'antd';
import Lang from '../../support/Lang';
import Pagination from 'src/components/pagination/Pagination';

const propsUpload = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info: any) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

class List extends Component<any> {
  public constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public state = {
    limit: 50,
    page: 1
  }

  public handleFetch(limit?: number, page?: number) {
    const { assestments } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    assestments.fetch(meta);
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  public componentDidMount() {
    this.handleFetch()
  }

  private handleOpenInfo(data: {}) {
    this.props.history.push({
      pathname: '/assestments-disc/detail',
      state: { data }
    })
  }

  public render() {
    const { data, httpBusy, meta } = this.props.assestments;
    return (
      <div>
        <h1>{Lang.get('assesments.title')}</h1>
        <Divider />
        <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
          <h1>{}</h1>
          <Upload {...propsUpload}>
            <Button type="primary" icon="upload">
              {Lang.get('assesments.upload')}
            </Button>
          </Upload>
        </div>
        <br />
        <ListTable
          busy={httpBusy}
          onDetail={this.handleOpenInfo}
          data={data}
        />
        {
          meta &&
          <Pagination
            limit={this.state.limit}
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
          />
        }
      </div>
    );
  }
}

const config = {
  assestments: AssestmentsListContainer
}

const mapStateToProps = (containers: any) => {
  return {
    assestments: containers.assestments.state
  }
}

export default connect(config, mapStateToProps)(List);
