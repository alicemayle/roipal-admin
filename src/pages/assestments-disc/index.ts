import AssestmentsDiscList from './List';
import AssestmentsDiscUpsert from './Upsert';

export {
    AssestmentsDiscList,
    AssestmentsDiscUpsert
}