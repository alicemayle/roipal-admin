import React, { Component } from 'react';
import Autobind from 'class-autobind';
import ActionsProps from '../models/ActionsProps';
import Lang from '../../../support/Lang';

class Actions extends Component<ActionsProps> {
  public constructor(props: ActionsProps) {
    super(props);
    Autobind(this);
  }

  public click() {
    const { data, onDetail } = this.props;
    onDetail(data);
  }

    public render() {
        return (
          <a onClick={this.click}>{Lang.get('generic.button.edit')}</a>
        )
    }
}

export default Actions;
