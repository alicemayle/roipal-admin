import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { Table } from 'antd';
import Lang from '../../../support/Lang';
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';

const { Column } = Table;

class ListTable extends Component<any> {
  public constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public click(data: {}) {
    this.props.onDetail(data);
  }

  private renderActions(text: string, record: {}) {

    return (
      <IconButton
          item={record}
          onFuction={this.click}
          size={18}
          colorIcon={'#52bebb'}
          name={'edit'}
        />
    )
  }

  public render() {
    const { data, busy } = this.props;

    return (
      <div>
        <Table
          loading={busy}
          scroll={{ x: 500 }}
          dataSource={data}
          pagination={false}
          >
          <Column
            title={Lang.get('assesments.date')}
            dataIndex={'created_at'}
            key={'created_at'}
            align={'center'}
          />
          <Column
            title={Lang.get('assesments.assessment')}
            dataIndex={'name'}
            key={'name'}
            align={'center'}
          />
          <Column
            title={Lang.get('assesments.language')}
            dataIndex={'lang'}
            key={'lang'}
            align={'center'}
          />
          {
            Gate.allows('assessment::edit') &&
            <Column
              title={Lang.get('assesments.edit')}
              dataIndex={'actions'}
              key={'actions'}
              render={this.renderActions}
              align={'center'}
            />
          }
        </Table>
      </div>
    )
  }
}

export default ListTable;