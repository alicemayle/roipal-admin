import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { Form, Input } from 'antd';
import FormUpsertOptionsInterface from '../../models/FormUpsertOptionsInterface';
import Lang from '../../../../support/Lang';

interface HandleNameChangeInterface {
  target: HTMLInputElement;
}

class FormUpsertOptions extends Component<FormUpsertOptionsInterface> {
  constructor(props: FormUpsertOptionsInterface) {
    super(props);
    Autobind(this);
  }

  private handleChange(e: HandleNameChangeInterface) {
    const { question, onChange } = this.props;
    const name = e.target.name;
    const value = e.target.value;
    const keys = Object.keys(question.options)
    let data = {};

    keys.map((key) => {

      if (key === name) {
        data = {
          ...data,
          [key]: value
        }
      } else {
        data = {
          ...data,
          [key]: question.options[key]
        }
      }
    })
    const _question = {
      ...question,
      options: {
        ...data
      }
    }
    onChange(_question);
  }

  private renderOptions(record: {}, index: number) {
    const { getFieldDecorator } = this.props.form;
    const keys = Object.keys(record)

    return (
      <div key={index}>
        {keys.map((option) => {
          return (
            <Form.Item label={option} key={option}>
              {getFieldDecorator(option, {
                initialValue: record[option],
                rules: [{
                  required: true,
                  message: Lang.get('assesments.error_message')
                }],
              })(
                <Input
                  placeholder="locale"
                  name={option}
                  onChange={this.handleChange}
                />
              )}
            </Form.Item>
          )
        })}
      </div>
    )
  }

  public render() {
    const { options } = this.props;

    return (
      <div>
        {options && options.map(this.renderOptions)}
      </div>
    )
  }
}

const WrappeedUpsertForm: any = Form.create({ name: 'upsertOptions' })(FormUpsertOptions)

export default WrappeedUpsertForm