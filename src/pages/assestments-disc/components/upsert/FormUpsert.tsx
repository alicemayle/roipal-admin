import React, { Component } from 'react';
import { Form, Input, Row, Col, Card, Button, Spin, BackTop } from 'antd';
import Autobind from 'class-autobind';
import FormUpsertOptions from './FormUpsertOptions';
import FormUpsertInterface from '../../models/FormUpsertInterface';
import Lang from 'src/support/Lang';

interface HandleChange {
  target: HTMLInputElement;
}

class FormUpsert extends Component<FormUpsertInterface> {
  constructor(props: FormUpsertInterface) {
    super(props);
    Autobind(this);
  }

  private handleChange(data: {}) {
    this.props.onChange(data);
  }

  private handleChangeQuestion(e: HandleChange) {
    const value = e.target.value;
    const name = e.target.name;
    this.props.onChangeQuestion(name, value)

  }

  private getType() {
    const { data } = this.props;
    if (data.name === 'DISC') {
      return true;
    }
    else {
      return false;
    }
  }

  private renderItems(record: { options: {}, question: string, number: string }, index: number) {
    const { getFieldDecorator } = this.props.form;
    const options = record && record.options ? [record.options] : [];

    return (
      <div key={index}>
        <Col md={12} lg={12} >
          <Card
            style={{ marginBottom: 16 }}
            title={Lang.get('assesments.question') + ' ' + record.number}>
            <Form.Item>
              {getFieldDecorator('question' + record.number, {
                initialValue: record.question,
                rules: [{
                  required: true,
                  message: 'Must enter a question'
                }],
              })(
                <Input
                  placeholder="locale"
                  disabled={this.getType() ? true : false}
                  name={record.number}
                  onChange={this.handleChangeQuestion}
                />
              )}
            </Form.Item>
            {options &&
              <FormUpsertOptions
                onChange={this.handleChange}
                options={options}
                question={record} />
            }

          </Card>
        </Col>
      </div>

    );
  }

  private handleSubmit() {
    this.props.onSubmit();
  }

  public render() {
    const { data } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: { span: this.getType() ? 3 : 2 }
      },
      wrapperCol: {
        xs: { span: this.getType() ? 21 : 22 }
      },
    };
    return (
      <div>
        {
          data.questions ?
            <Form {...formItemLayout} layout={this.getType() ? 'inline' : 'horizontal'}>
              <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
                <h1>{}</h1>
                <Button type="primary"
                  onClick={this.handleSubmit}
                  className="login-form-button"
                  style={{ marginBottom: 10 }}
                >
                  {Lang.get('generic.button.save')}
                </Button>
              </div>
              <Row gutter={16}>
                {data && data.questions && data.questions.map(this.renderItems)}
              </Row>
              <Row>
                <Button type="primary"
                  onClick={this.handleSubmit}
                  className="login-form-button"
                >
                  {Lang.get('generic.button.save')}
                </Button>
              </Row>
              <Col
                className="paginateAlingRigth"
                xs={{ span: 5 }}
                lg={{ span: 1 }}>
                <BackTop>
                  <div className="ant-back-top-inner">
                    <Button
                      icon="up"
                    />
                  </div>
                </BackTop>
              </Col>
            </Form> :
            <div style={{ position: 'absolute', left: '50%', top: '50%' }}>
              <Spin size="large" />
            </div>
        }

      </div>
    )
  }
}

const WrappeedUpsertForm: any = Form.create({ name: 'upsert' })(FormUpsert)

export default WrappeedUpsertForm;