import { Container } from 'unstated';
import Assestments from '../services/assestments';
import AssestmentsUpsertState from '../models/AssestmentsUpsertState';
import Lang from 'src/support/Lang';

const initialState = {
    data: [],
    httpBusy: false,
    message: '',
    error: null
}

class AssestmentsUpsertContainer extends Container<AssestmentsUpsertState> {
    public state: AssestmentsUpsertState = {
        ...initialState
    }

    public name: string = 'Assestments Upsert';

    public async upsert(uuid: string, data: {}) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'Assestments Upsert Starts'
            })

            const response = await Assestments.upsert(uuid, data);
            this.setState({
                httpBusy: false,
                ...response,
                message: Lang.get('assesments.success_update'),
                __action: 'Assestments Upsert Success'
            })
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                message: error.message,
                __action: 'Assestments Upsert Error'
            })
        }
    }

    public reset() {
        this.setState({
            ...initialState,
            __action: 'Assestments Upsert Reset'
        })
    }

    public clearError() {
        this.setState({
            ...this.state,
            error: null
        })
    }
}

export default AssestmentsUpsertContainer;