import { Container } from 'unstated';
import Assesments from '../services/assestments';
import AssestmentsListState from '../models/AssestmentsListState';
import moment from 'moment';

class CompaniesListContainer extends Container<AssestmentsListState> {
  public state: AssestmentsListState = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
    meta: null
  }

  public name: string = 'Assesments List';

  public async fetch(params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Assesments list starts'
      });

      const response = await Assesments.paginate(params);
      const newResponse = response.data.map((
        data: {
          uuid: string,
          key: string,
          created_at: any
        }) => {
        data = { ...data }
        data.key = data.uuid;
        data.created_at = moment(new Date(data.created_at)).format('DD-MM-YYYY')
        return data;
      })

      this.setState({
        httpBusy: false,
        data: newResponse,
        meta: response.meta,
        __action: 'Assesments list success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Assesments list error'
      });
    }
  }

  public reset() {
    this.setState({
      httpBusy: false,
      data: null,
      error: null,
      message: '',
      __action: 'Assesments list reset'
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Assesments list clear error'
    });
  }
}

export default CompaniesListContainer;