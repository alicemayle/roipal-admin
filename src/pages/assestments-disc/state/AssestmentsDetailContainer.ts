import { Container } from 'unstated';
import Assestments from '../services/assestments';
import AssestmentsListState from '../models/AssestmentsListState';

class AssestmentsDetailContainer extends Container<AssestmentsListState> {
    public state: AssestmentsListState = {
        httpBusy: false,
        data: null,
        error: null,
        message: ''
    }

    public name: string = 'Companies List';

    public async getAssestment(name:string, params: any) {

        try {
            this.setState({
                httpBusy: true,
                __action: 'Companies list starts'
            });

            const response = await Assestments.getDetail(name, params);

            this.setState({
                httpBusy: false,
                data: response.data,
                __action: 'Companies list success'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                message: error.message,
                __action: 'Companies list error'
            });
        }
    }

    public reset() {
        this.setState({
            httpBusy: false,
            data: null,
            error: null,
            message: '',
            __action: 'Companies list reset'
        });
    }

    public clearError() {
        this.setState({
            ...this.state,
            error: null,
            message: '',
            __action: 'Companies list clear error'
        });
    }
}

export default AssestmentsDetailContainer;