import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';
import AssestmentsDetailContainer from './state/AssestmentsDetailContainer';
import AssestmentsUpsertContainer from './state/AssestmentsUpsertContainer';
import FormUpsert from './components/upsert/FormUpsert';
import Notifications from '../../components/notifications/notifications'
import { Gate } from '@xaamin/guardian';
import { message, Divider } from 'antd';
import Lang from 'src/support/Lang';

class Upsert extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: {}
    }
    Autobind(this);

  }

  public componentWillUnmount() {
    this.setState({
      data: {}
    })
  }

  public async componentWillMount() {
    const data = this.props.history.location.state;
    if (data) {
      const { assestments } = this.props.containers;
      const meta = {
        includes: 'questions',
        lang: data.data.lang
      }
      await assestments.getAssestment(data.data.name, meta);
      this.setState({
        data: this.props.assestments.data
      })
    }
    else {
      this.props.history.push('/assestments-disc')
    }
  }

  private handleChange(_question: any) {
    const { data } = this.state;
    let _data = {};
    _data = data.questions.map((question: any) => {
      question = { ...question }
      if (question.number === _question.number) {
        question.options = _question.options
      }
      return question;
    })

    const _nuevo = {
      ...data,
      questions: _data
    }

    this.setState({
      data: _nuevo
    })
  }

  private handleChangeQuestion(key: any, _question: string) {
    key = parseInt(key, 10)
    const { data } = this.state;
    let _data = {};
    _data = data.questions.map((question: any) => {
      question = { ...question }

      if (question.number === key) {
        question.question = _question
      }
      return question;
    })

    const _nuevo = {
      ...data,
      questions: _data
    }

    this.setState({
      data: _nuevo
    })
  }

  private handleSubmit() {
    if (Gate.allows('assessment::edit')) {
      const { data } = this.state;
      const _data = { ...data };
      const notification = new Notifications;

      let msj: {};

      this.props.containers.upsert.upsert(data.uuid, _data);
      if (this.props.upsert.error) {
        msj = {
          type: 'error',
          message: this.props.upsert.error.message || this.props.upsert.message,
        }
      }
      else {
        msj = {
          type: 'success',
          message: this.props.upsert.message,
        }
      }
      notification.openNotification(msj)
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  public render() {
    const { data } = this.state;

    return (
      <div>
        <h1>{Lang.get('assesments.edit_assessment')}</h1>
        <Divider />
        {data &&
          <FormUpsert
            onSubmit={this.handleSubmit}
            onChangeQuestion={this.handleChangeQuestion}
            onChange={this.handleChange}
            data={data} />
        }
      </div>
    )
  }
}

const config = {
  assestments: AssestmentsDetailContainer,
  upsert: AssestmentsUpsertContainer
}

const mapStateToProps = (containers: any) => {
  return {
    assestments: containers.assestments.state,
    upsert: containers.upsert.state
  }
}




export default connect(config, mapStateToProps)(Upsert);
