import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class Assestments {

    // TODO: investigar la forma correcta para el response
    public paginate(params: any): any {
        return Http.get(`api/assestments/`, { params })
    }

    public getDetail(name:string , params: any): Promise<AxiosResponse> {
        return Http.get(`api/assestments/${name}`, { params })
    }

    public upsert(uuid:string, data:{}) {
        return Http.put(`/api/assestments/${uuid}?includes=questions`, data)
    }
}

export default new Assestments;