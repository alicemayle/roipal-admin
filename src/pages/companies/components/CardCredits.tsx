import React, { Component } from 'react';
import { Card, Col, Row, Radio, InputNumber, Button, message, Icon } from 'antd';
import Autobind from 'class-autobind';
import CardMisionsProps from '../models/CardMissionProps';
import { connect } from 'unstated-enhancers';
import Text from 'antd/lib/typography/Text';
import AddCreditsContainer from '../state/AddCreditsContainer';
import Lang from '../../../support/Lang';
import { Gate } from '@xaamin/guardian';

interface ListState {
  credits: any
  option: any
  check: any
}

const style = {
  display: 'block',
  height: '30px',
  ineHeight: '30px',
}

class CardCredits extends Component<any, ListState> {
  constructor(props: CardMisionsProps) {
    super(props);
    Autobind(this);
  }

  public state: any = {
    credits: 0,
    option: true,
    check: 'NO'
  }

  private async save() {
    if (Gate.allows('company::credit-edit')) {
      const { data } = this.props;
      const uuidCompany = data.key
      const sumCredits = data.credits + this.state.credits

      const sendCredits = {
        available_credit: sumCredits
      }

      if (data) {
        const { companies } = this.props.containers
        await companies.fetch(uuidCompany, sendCredits)
      }

      const { error } = this.props.containers.companies.state

      if (error) {
        message.error(Lang.get('company.error_credits'));
      } else {
        this.props.onClose()
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }

  }

  private setValue(value: any) {
    this.setState({
      credits: value
    })
  }

  private setOption(e: any) {
    const value = e.target.value

    this.setState({
      option: value === 'YES' ? false : true,
      check: value === 'YES' ? 'YES' : 'NO'
    })
  }

  public shouldComponentUpdate(nextProps: any, nextState: any) {
    return (nextProps.data !== this.props.data ||
      nextState.credits !== this.state.credits ||
      nextState.option !== this.state.option)
  }

  public componentDidUpdate(prevProps: any) {
    if (prevProps.data !== this.props.data){
      this.setState({
        credits: 0,
        option: true,
        check: 'NO'
      })
    }
  }

  public render() {
    const { data } = this.props;

    return (
      <Card
        type="inner"
        title={Lang.get('company.credits')}
        bordered={false}>
        <Row gutter={8}>
          <Col
            xs={{ span: 15 }}
            md={{ span: 15 }}
            sm={{ span: 15 }}
            lg={{ span: 9 }}>
            <div style={{ height: '15vh', paddingTop: 15 }}>
              <b>{Lang.get('company.available_credits')}</b>
              <br />
              <Icon type={'dollar'}
                style={{ fontSize: 20, textAlign: 'center', color: '#ffbb2f' }} />
              <Text style={{ fontSize: 25, textAlign: 'center', marginLeft: 10 }}>
                {data.credits}
              </Text>
              <br />
            </div>
          </Col>
          {
            Gate.allows('company::credit-edit')
            && data.payments_profiles.length > 0 &&
            <Col
              xs={{ span: 12 }}
              md={{ span: 12 }}
              sm={{ span: 12 }}
              lg={{ span: 6 }}>
              <div style={{ paddingTop: 15 }}>
                <p>{Lang.get('company.add_credits')}</p>
                <Radio.Group
                  value={this.state.check}
                  onChange={this.setOption}
                >
                  <Radio
                    style={{ ...style }}
                    value={'YES'}>
                    {Lang.get('company.yes')}
                  </Radio>
                  <Radio
                    style={{ ...style }}
                    value={'NO'}
                  >
                    {Lang.get('company.no')}
                  </Radio>
                </Radio.Group>
              </div>
            </Col>
          }
          {
            Gate.allows('company::credit-edit')
            && data.payments_profiles.length > 0 &&
            <Col
              xs={{ span: 12 }}
              md={{ span: 12 }}
              sm={{ span: 12 }}
              lg={{ span: 6 }}>
              <div style={{ marginLeft: 20, paddingTop: 15 }}>
                <InputNumber
                  min={0}
                  max={50000}
                  value={this.state.credits}
                  onChange={this.setValue}
                  disabled={this.state.option}
                />
                <Button
                  style={{
                    marginTop: 10,
                    marginLeft: 3,
                    backgroundColor: this.state.option === true ? 'lightgrey' : '#52bebb'
                  }}
                  onClick={this.save}
                  disabled={this.state.option}
                >
                  <Text style={{ color: 'white' }}>{Lang.get('company.save')}</Text>
                </Button>
              </div>
            </Col>
          }
        </Row>
      </Card>
    );
  }
}

const config = {
  companies: AddCreditsContainer
}

const mapStateToProps = (containers: any) => {
  return {
    companies: containers.companies.state
  };
}

export default connect(config, mapStateToProps)(CardCredits);

