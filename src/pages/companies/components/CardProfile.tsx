import React, { Component } from 'react';
import { Card, Typography, Row, Col } from 'antd';
import Autobind from 'class-autobind';
import Lang from '../../../support/Lang';

const { Text } = Typography;

class CardProfile extends Component<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public render() {
    const { data } = this.props;

    return (
      <Card
        type="inner"
        title={Lang.get('company.profile')}
        bordered={false}>
        <Row gutter={3}>
          {data.profile.photo_bio_url &&
            <Col span={6}>
              <img src={data.profile.photo_bio_url} height={'90%'} width={'90%'} />
            </Col>
          }
          <Col span={25}>
            <Row>
              <Text><b>{Lang.get('company.business_name')}:</b> &nbsp;</Text>
              <Text>{data.business_name}</Text>
            </Row>
            <Row>
              <Text><b>{Lang.get('company.name')}:</b> &nbsp;</Text>
              <Text>{data.name}</Text>
            </Row>
            <Row>
              <Text><b>{Lang.get('company.website')}:</b> &nbsp;</Text>
              <Text>{data.profile.website}</Text>
            </Row>
            <Row>
              <Text><b>{Lang.get('company.contact')}:</b> &nbsp;</Text>
              <Text>{data.contact.contact_name}</Text>
            </Row>
            <Row>
              <Text><b>{Lang.get('company.phone')}:</b> &nbsp;</Text>
              <Text>{data.contact.contact_phone}</Text>
            </Row>
            <Row>
              <Text><b>{Lang.get('company.description')}:</b> &nbsp;</Text>
              <Text>{data.profile.bio}</Text>
            </Row>
          </Col>
        </Row>
      </Card>
    );
  }
}

export default CardProfile;