import React, { Component } from 'react';
import Autobind from 'class-autobind';
import CardMap from './CardMap';
import { Col } from 'antd';
import CardMisions from './CardMissions';
import CardProfile from './CardProfile';
import CardCredits from './CardCredits';
class ListTableRowExpanded extends Component<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public render() {
    const { data, missions, onClose } = this.props;
    let position = {
      lat: 0,
      lng: 0
    };

    if (data && data.profile && data.profile.position) {
      position = {
        lat: data.profile.position.latitude,
        lng: data.profile.position.longitude
      }
    }

    return (
      <div>
        <Col>
          <CardProfile
            data={data} />
        </Col>
        <Col>
          <CardMisions
            missions={missions} />
        </Col>
        <Col>
          <CardCredits
            data={data} onClose={onClose} />
        </Col>
        <Col>
          {data &&
            <CardMap
              position={position}
              company={data} />
          }
        </Col>
      </div>
    );
  }
}
export default (ListTableRowExpanded);