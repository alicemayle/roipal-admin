import React, { PureComponent } from 'react';
import { Row, Col, Typography } from 'antd';
import Lang from 'src/support/Lang';

const  { Text } = Typography;

interface CardMapDetailProps {
    company: {
        name: string,
        business_name: string,
        profile: {
            address: string,
            website: string
        }
    }
}

class CardMapDetail extends PureComponent<CardMapDetailProps> {
    constructor(props: CardMapDetailProps) {
        super(props);
    }

    public render() {
        const { company } = this.props;

        return (
            <div>
                <Row>
                    <Col span={24}>
                        <Text><b>{Lang.get('company.location.address')}:</b> &nbsp;</Text>
                        <Text>{company.profile.address}</Text>
                    </Col>
                </Row>
                <br/>
                {/* <Row>
                    <Col span={24}>
                        <Text><b>{Lang.get('company.website')}:</b> &nbsp;</Text>
                        <Text>{company.profile.website}</Text>
                    </Col>
                </Row> */}
            </div>
        )
    }
}

export default CardMapDetail;