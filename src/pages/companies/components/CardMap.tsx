import React, { Component } from 'react';
import Autobind from 'class-autobind';
import GoogleMap from 'google-map-react';
import { Card, Row, Col } from 'antd';
import Mark from '../../../components/map/Marker';
import CardMapProps from '../models/cardMap/CardMapProps';
import CardMapState from '../models/cardMap/CardMapState';
import CardMapDetail from './cardMap/CardMapDetail';
import Lang from 'src/support/Lang';

class CardMap extends Component<CardMapProps, CardMapState> {
    constructor(props:any) {
        super(props);
        Autobind(this);
        this.state = {
            center: {
                lat: 19.3665903,
                lng: -99.1832562
            }
        }
    }

    public render() {
        const { company, position } = this.props;
        const markers = [ company ];

        const Markers = markers &&
        markers.map((marker:any) => {
            const companyInfo = {
                name: marker.business_name,
                address: marker.profile.address
            }
            return (
                <Mark
                key={marker.uuid}
                lat={position.lng}
                lng={position.lat}
                marker={companyInfo}
                />
            )
        });
        return (
            <Card
            type="inner"
            title={Lang.get('company.location.title')}
            bordered={false}>
                <Row gutter={14}>
                    <Col span={12}>
                        <div style={{ height: '18vh'}}>
                            <GoogleMap
                            bootstrapURLKeys={{ key: 'AIzaSyCzTCLGCeoIPtGng_7IhV4aZRgL-dYyPaw' }}
                            zoom={14}
                            // onGoogleApiLoaded={this.renderMarker}
                            center={ [position.lng,position.lat]} >
                            {Markers
                            }
                            </GoogleMap>
                        </div>
                    </Col>
                    <Col span={12}>
                        <CardMapDetail
                        company={company} />
                    </Col>
                </Row>
            </Card>

        )
    }
}

export default CardMap