import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import { Modal, Input, Form, Button, message } from 'antd';
import { connect } from 'unstated-enhancers';
import ChangeRfcContainer from '../../companies/state/ChangeRfcContainer'
import Text from 'antd/lib/typography/Text';
import Lang from 'src/support/Lang';
import { Gate } from '@xaamin/guardian';

interface ListState {
  rfc: any
}

class ModalEditRfc extends PureComponent<any, ListState> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state = {
    rfc: ''
  }

  private changeVisible() {
    const { onSetVisible } = this.props
    if (onSetVisible) {
      onSetVisible();
    }
  }

  private async handleSubmit() {
    if (Gate.allows('company::profile-edit')) {
      const companySelected = this.props.data
      const { companies } = this.props.containers

      if (this.state.rfc) {
        const rfc = this.state.rfc
        const convertRfc = rfc.toUpperCase()

        const infoCompany = {
          rfc: convertRfc
        }

        const newInfoRfc = {
          company: infoCompany
        }

        await companies.fetch(companySelected.uuid, newInfoRfc)
      }

      const { error } = this.props.containers.companies.state

      if (!error) {
        this.changeVisible()
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }

  }

  private changeText(e: any) {
    const value = e.target.value;

    this.setState({
      rfc: value
    })
  }

  public render() {
    const { visible, data } = this.props
    const { httpBusy } = this.props.containers.companies.state
    const motive = this.props.containers.companies.state.message

    return (
      <div>
        <Modal
          title={data.business_name}
          visible={visible}
          onCancel={this.changeVisible}
          centered={true}
          destroyOnClose={true}
          footer={[
            <Button
              key="cancel"
              onClick={this.changeVisible}>
              {Lang.get('company.cancel')}
            </Button>,
            <Button
              key="submit"
              type="primary"
              onClick={this.handleSubmit}
              loading={httpBusy}
            >
              {Lang.get('company.save')}
            </Button>
          ]}
        >
          <Form>
            <Form.Item>
              <Text style={{ fontWeight: 'bold' }}>
                {Lang.get('company.registered')} {data.rfc}
              </Text>
            </Form.Item>
            <Form.Item>
              <Input
                onChange={this.changeText}
                placeholder={Lang.get('company.new_rfc')} />
              {
                motive &&
                <Text style={{ color: 'red' }}>
                  {Lang.get('company.invalid_rfc')}
                </Text>
              }
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

const config = {
  companies: ChangeRfcContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    companies: containers.companies.state,
  };
}

export default connect(config, mapStateToProps)(ModalEditRfc);
