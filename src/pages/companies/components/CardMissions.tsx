import React, { Component } from 'react';
import { Card } from 'antd';
import ListMissions from './listMissions/ListMissions';
import Autobind from 'class-autobind';
import CardMisionsProps from '../models/CardMissionProps';
import Lang from 'src/support/Lang';

class CardMisions extends Component<CardMisionsProps> {
  constructor(props: CardMisionsProps) {
    super(props);
    Autobind(this);
  }

  public shouldComponentUpdate(nextProps: any, nextState: any) {
    return (nextProps.missions !== this.props.missions)
  }

  public render() {
    const { missions } = this.props;

    return (
      <Card
        type="inner"
        title={Lang.get('company.mission.title')}
        bordered={false}>
        <div style={{ height: '18vh' }}>
          {missions &&
            <ListMissions
              missions={missions} />
          }
        </div>
      </Card>
    );
  }
}

export default CardMisions;