import React, { Component } from 'react';
import Autobind from 'class-autobind';
import './styles.css';
import ListMissionsProps from '../../models/listMissions/ListMissionsProps';
import { Row, Col } from 'antd';
import ListIcons from '../../../../components/ListIcons';
import Lang from 'src/support/Lang';

interface ListState {
  created: number
  started: number
  completed: number
  canceled: number
  Missions: number
}

class ListMissions extends Component<ListMissionsProps, ListState> {
  constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public state: ListState = {
    created: 0,
    started: 0,
    completed: 0,
    canceled: 0,
    Missions: 0
  }

  private async countMissions(missions: any) {
    let createdB = 0;
    let startedB = 0;
    let completedB = 0;
    let canceledB = 0;
    let MissionsB = 0;

    missions.map((mission: { status: number }) => {
      MissionsB++;

      if (mission.status === 0) {
        createdB++;
      } else if (mission.status === 1) {
        startedB++;
      } else if (mission.status === 10) {
        completedB++;
      } else if (mission.status === -1) {
        canceledB++;
      }
    })

    this.setState({
      created: createdB,
      started: startedB,
      completed: completedB,
      canceled: canceledB,
      Missions: MissionsB
    })
  }

  public componentDidMount() {
    this.countMissions(this.props.missions)
  }

  public componentWillReceiveProps(nextProps: any) {
    if (this.props.missions !== nextProps.missions) {
      this.countMissions(nextProps.missions)
    }
  }

  public render() {
    const { created, started, completed, canceled, Missions } = this.state
    return (
      <div >
        <Row>
          <Col xs={{ span: 23, offset: 1 }} lg={{ span: 23, offset: 1 }}>
            <label>
              <b>{`${Lang.get('company.mission.total')}: `}</b>
              {Missions}
            </label>
          </Col>
        </Row>
        <br />
        <Row gutter={8}>
          <Col
            xs={{ span: 12 }}
            md={{ span: 12 }}
            sm={{ span: 12 }}
            lg={{ span: 6 }}>
            <ListIcons
              type="vertical"
              colorIcon={'#ffbb2f'}
              label={`${Lang.get('company.mission.created')}: ${created}`}
              icon={'pause-circle'} />
          </Col>
          <Col
            xs={{ span: 12 }}
            md={{ span: 12 }}
            sm={{ span: 12 }}
            lg={{ span: 6 }}>
            <ListIcons
              type="vertical"
              colorIcon={'#66CDAA'}
              label={`${Lang.get('company.mission.started')}: ${started}`}
              icon={'clock-circle'}
            />
          </Col>
          <Col
            xs={{ span: 12 }}
            md={{ span: 12 }}
            sm={{ span: 12 }}
            lg={{ span: 6 }}>
            <ListIcons
              type="vertical"
              colorIcon={'#87CEEB'}
              label={`${Lang.get('company.mission.completed')}: ${completed}`}
              icon={'check-circle'}
            />
          </Col>
          <Col
            xs={{ span: 12 }}
            md={{ span: 12 }}
            sm={{ span: 12 }}
            lg={{ span: 6 }}>
            <ListIcons
              type="vertical"
              colorIcon={'#CD5C5C'}
              label={`${Lang.get('company.mission.canceled')}: ${canceled}`}
              icon={'close-circle'}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export default ListMissions;