import React, { PureComponent } from 'react';
import { Table, Icon, Divider } from 'antd';
import FilterSearch from '../../../components/search/FilterSearch';
import FilterDropdDownInterface from '../../../components/search/FilterDropDownInterface';
import Tag from '../../../components/Tags';
import ListTableState from '../models/ListTableState';
import ListTableProps from '../models/ListTableProps';
import Autobind from 'class-autobind';
import Lang from 'src/support/Lang';
import DetailsDrawer from './DetailsDrawer';
import { connect } from 'unstated-enhancers';
import MissionCompanieContainer from '../state/MissionsCompanieContainer';
import ModalEditRfc from './ModalEditRfc';
import IconButton from 'src/components/buttons/IconButton';
// import { Gate } from '@xaamin/guardian';
import '../../../global.scss'
import Text from 'antd/lib/typography/Text';

const { Column } = Table;

class ListTable extends PureComponent<ListTableProps, ListTableState> {
  public searchInput: any = null;
  public searchField: string = '';

  constructor(props: ListTableProps) {
    super(props);
    this.state = {
      searchText: '',
      fields: [],
      showDetails: false,
      itemDetails: '',
      visible: false,
      companySelected: null
    }
    Autobind(this)
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterDropdDownInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  });

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private renderTags
    (field: { name: string, status: boolean }, index: number) {

    return (
      <Tag
        index={index}
        key={index}
        field={field}
      />
    );
  }

  private renderActions(item: any) {
    return (
      <IconButton
        item={item}
        onFuction={this.handleOpenDetails}
        size={18}
        colorIcon={'#52bebb'}
        name={'info-circle'}
      />
    );
  }

  private setVisible() {
    this.setState({
      visible: !this.state.visible
    })
  }

  private handleShowModal(company: any) {
    const { data = {} } = this.props;

    const uuidCompany = company.key

    if (uuidCompany) {
      const find = data.find((item: any) => item.uuid === uuidCompany)

      this.setVisible()

      this.setState({
        companySelected: find
      })
    }
  }

  private renderRfc(item: any) {
    return (
      <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
        <Text>{item.rfc}</Text>
        {
          // Gate.allows('company::profile-edit') &&
          <IconButton
            item={item}
            onFuction={this.handleShowModal}
            size={18}
            colorIcon={'#52bebb'}
            name={'edit'}
          />
        }
      </div>
    )
  }

  private async handleOpenDetails(item: any) {
    const { missions } = this.props.containers;

    const meta = {
      includes: 'locations,company,executive',
      limit: 50
    }
    await missions.list(item.key, meta);

    this.setState({
      showDetails: true,
      itemDetails: item,
    })
  }

  private closeDrawer() {
    this.setState({
      showDetails: false,
    });
  }

  public render() {
    const { data = {}, httpBusy } = this.props;
    const { fields } = this.state;

    return (
      <div>
        {fields && fields.map(this.renderTags)}
        {fields && <Divider />}
        <Table
          dataSource={data}
          loading={httpBusy}
          scroll={{ x: 500 }}
          pagination={false}
        >
          <Column
            title={Lang.get('company.date')}
            dataIndex={'created_at'}
            key={'created_at'}
            align={'center'}
          />
          <Column
            title={Lang.get('company.business_name')}
            dataIndex={'business_name'}
            key={'business_name'}
            {...this.getColumnSearchProps('business_name')}
          />
          <Column
            title={Lang.get('company.name')}
            dataIndex={'name'}
            key={'name'}
            {...this.getColumnSearchProps('name')}
          />
          <Column
            title={Lang.get('company.vertical')}
            dataIndex="profile.vertical"
            key="profile.vertical"
          />
          <Column
            title={'RFC'}
            render={this.renderRfc}
            width={'11%'}
            key="rfc"
            align={'center'}
            {...this.getColumnSearchProps('rfc')}
          />
          <Column
            title={Lang.get('company.available_credits')}
            dataIndex="credits"
            key="credits"
            align={'right'}
          />
          <Column
            title={Lang.get('generic.button.details')}
            dataIndex=""
            key="0"
            render={this.renderActions}
            align={'center'}
          />
        </Table>
        <DetailsDrawer
          visible={this.state.showDetails}
          onClose={this.closeDrawer}
          item={this.state.itemDetails}
        />
        {
          this.state.companySelected &&
          <ModalEditRfc
            data={this.state.companySelected}
            visible={this.state.visible}
            onSetVisible={this.setVisible}
          />
        }
      </div>
    );
  }
}

const config = {
  missions: MissionCompanieContainer
}

const mapStateToProps = (containers: any) => {
  return {
    missions: containers.missions.state
  }
}

export default connect(config, mapStateToProps)(ListTable);
