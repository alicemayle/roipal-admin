import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Drawer } from 'antd';
import Lang from '../../../support/Lang';
import ListTableRowExpanded from './ListTableRowExpanded'
import { connect } from 'unstated-enhancers';
import MissionCompanieContainer from '../state/MissionsCompanieContainer';

const pStyle = {
  fontSize: 18,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16,
};

class DetailsDrawer extends Component<any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public shouldComponentUpdate(nextProps: any) {
    return (
      nextProps.item !== this.props.item ||
      nextProps.visible !== this.props.visible
    )
  }

  public render() {
    const { visible, onClose, item } = this.props;
    const { data } = this.props.containers.missions.state;

    return (
      <Drawer
        placement="right"
        width={'55%'}
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <p style={pStyle}>{Lang.get('company.details')}</p>
        <ListTableRowExpanded data={item} missions={data} onClose={onClose}/>
      </Drawer>
    )
  }
}

const config = {
  missions: MissionCompanieContainer
}

const mapStateToProps = (containers: any) => {
  return {
    missions: containers.missions.state
  }
}

export default connect(config, mapStateToProps)(DetailsDrawer);