import React, { Component } from 'react'
import { Container } from 'unstated'
import { connect } from 'unstated-enhancers'

import CounterContainer from './state/CounterContainer'
import MessageContainer from './state/MessageContainer'

interface ListProps {
  containers: any,
  counter: Container<any>,
  messages: Container<any>,
  count: number,
  message: string
}

class List extends Component<ListProps> {

  public constructor(props: any) {
    super(props);

    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
    this.update = this.update.bind(this);
    this.random = this.random.bind(this);
  }

  private increment() {
    const { counter } = this.props.containers;

    counter.increment();
  }

  private decrement() {
    const { counter } = this.props.containers;

    counter.decrement();
  }

  private update() {
    const { messages } = this.props.containers;

    messages.change('Updated')
  }

  private random() {
    const { messages } = this.props.containers;

    messages.random()
  }

  public render() {
    // Get access to named containers
    const { counter, messages } = this.props.containers;

    return (
      <div>
        <header>
          <h1>App</h1>
        </header>
        <section>
          <div>
            <h4>Counter</h4>
            <button onClick={ this.decrement }>-</button>
            <button onClick={ this.increment }>+</button>
            <br />
            <br />
            <span>From counter state { counter.state.count }</span>
            <br />
            <span>From mapped state to props { this.props.count }</span>
            <br />
            <br />
          </div>
          <div>
            <h4>Message</h4>
            <button onClick={ this.update  }>Update</button>
            <button onClick={ this.random  }>Random</button>
            <br />
            <br />
            <span>From message state { messages.state.message }</span>
            <br />
            <span>From mapped state to props { this.props.message }</span>
            <br />
            <br />
          </div>
        </section>
      </div>
    );
  }
}

const config = {
  counter: CounterContainer,
  messages: MessageContainer
};

const mapStateToProps = (containers: any) => {
  return {
    count: containers.counter.state.count,
    message: containers.messages.state.message,
  }
}


export default connect(config, mapStateToProps)(List);