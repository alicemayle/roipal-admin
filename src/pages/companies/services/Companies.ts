import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class Companies {

  // TODO: investigar la forma correcta para el response
  public paginate( params: any): any {
    return Http.get(`api/companies`, { params })
  }

  public getDetail(uuid: string): Promise<AxiosResponse> {
    return Http.get(`${uuid}`);
  }

  public getMissions(uuid: string, params: any): any {
    return Http.get(`api/companies/${uuid}/missions`, { params });
  }

  public upsert(uuid: any, data: any) {
    return Http.put(`/api/companies/${uuid}`, data);
  }

}

export default new Companies;