interface ListTableState {
    searchText: string,
    fields: any,
    itemDetails: any,
    showDetails: boolean,
    visible: any,
    companySelected: any
  }

  export default ListTableState;