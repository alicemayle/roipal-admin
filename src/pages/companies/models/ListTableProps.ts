interface ListTableProps {
  data: any,
  httpBusy: boolean,
  meta?: any,
  limit: number,
  containers: any,
  onFetch: (limit: number, page: number) => void,
  onChangeLimit: (limit: number, page: number) => void
}

export default ListTableProps;