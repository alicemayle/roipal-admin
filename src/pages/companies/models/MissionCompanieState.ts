interface MissionsCompanieState {
    data: [] | null,
    error: [] | null,
    httpBusy: boolean,
    message: string,
    __action?: string
}

export default MissionsCompanieState;