import { ListState } from '../../../roipal/models';

interface CompaniesListState extends ListState {
  __action?: string,
  meta: any
}

export default CompaniesListState;