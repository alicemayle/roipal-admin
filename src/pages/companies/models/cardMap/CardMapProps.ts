interface CardMapProps {
    company: any,
    position: {
        lat: number,
        lng: number
    }
}

export default CardMapProps;