interface CardMapState {
    uuid?: string | '',
    time?: number | null,
    center: {
        lat: number,
        lng: number
    }
}

export default CardMapState;