import { ListState } from '../../../roipal/models';

interface CompaniesDetailState extends ListState {
  __action?: string
}

export default CompaniesDetailState;