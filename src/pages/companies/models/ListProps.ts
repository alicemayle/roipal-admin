interface ListProps {
    containers: any,
    companies: any,
    nextProps: {
      companies: {
        data:{}
      },
      httpBusy: boolean
    },
    history:{
      push:(url:string) =>void
    }
}

export default ListProps;