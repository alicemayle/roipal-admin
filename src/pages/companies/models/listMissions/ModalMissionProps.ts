interface ModalMissionProps {
    onClose:()=>void,
    open: boolean,
    mission: {
        name:string,
        status: number,
        time: number,
        time_unit: string,
        profile: string,
        type:string,
        executives_requested: number,
        invitation_sent: number,
        subtotal: number,
        total: number,
        description: string
    }
}

export default ModalMissionProps;