import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import CompaniesListContainers from './state/CompaniesListContainer';
import ListTable from './components/ListTable';
import ListProps from './models/ListProps';
import Autobind from 'class-autobind';
import Lang from 'src/support/Lang';
import Pagination from '../../components/pagination/Pagination';
import './companies.scss'
import Event from 'src/support/Event';
import Notifications from 'src/components/notifications/notifications';

interface ListState {
  limit: number,
  page: number
}
class List extends Component<ListProps, ListState> {

  public constructor(props: ListProps) {
    super(props);
    Autobind(this);
    Event.listen('company::edit', this.listeForUpsert)
  }

  public state: ListState = {
    limit: 50,
    page: 1
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.handleOnComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.handleOnComplete('error');
    }

    notification.openNotification(msj)
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.handleFetch();
    }
  }

  public handleFetch(limit?: number, page?: number) {
    const { companies } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      includes: 'profile,locations,payments_profiles,contact',
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    companies.fetch(meta);
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  public componentWillUnmount() {
    Event.remove('company::edit', this.listeForUpsert);
  }

  public componentDidMount() {
    this.handleFetch()
  }

  public render() {
    const { data, httpBusy, meta } = this.props.companies;

    return (
      <div>

        <h1>
          {Lang.get('company.title')}
        </h1>
        <ListTable
          data={data}
          httpBusy={httpBusy}
        />
        {meta &&
          <Pagination
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
            limit={this.state.limit}
          />
        }
      </div>
    )
  }
}

const config = {
  companies: CompaniesListContainers
}

const mapStateToProps = (containers: any) => {
  return {
    companies: containers.companies.state
  };
}

export default connect(config, mapStateToProps)(List);