import { Container } from 'unstated';

interface MessageState {
  message: string,
  __action?: string
};

class MessageContainer extends Container<MessageState> {
  public state: MessageState = {
    message: 'Not defined'
  };

  public name: string = 'Messages';

  public change(message: string) {
    this.setState({
      message,
      __action: 'MESSAGE_CHANGE'
    });
  }

  public random() {
    this.setState({
      message: 'Random ' + Math.random(),
      __action: 'MESSAGE_RANDOM'
    });
  }
}

export default MessageContainer;