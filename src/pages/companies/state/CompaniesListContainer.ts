import { Container } from 'unstated';
import Companies from '../services/Companies';
import CompaniesListState from '../models/CompaniesListState';
import moment from 'moment';

class CompaniesListContainer extends Container<CompaniesListState> {
  public state: CompaniesListState = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
    meta: null
  }

  public name: string = 'Companies List';

  public async fetch(params: any) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Companies list starts'
      });

      const response = await Companies.paginate(params);

      const newData = response.data.map((find: any) => {

        const newForm = find.payments_profiles.find((item: any) =>
          item.available_credit !== null)

        if (newForm) {
          find = { ...find }
          find.key = find.uuid;
          find.credits = newForm.available_credit
          find.created_at =
            moment(new Date(find.created_at)).format('DD-MM-YYYY')
          return find
        } else {
          find = { ...find }
          find.key = find.uuid;
          find.credits = 0
          find.created_at =
            moment(new Date(find.created_at)).format('DD-MM-YYYY')
          return find
        }
      })

      this.setState({
        httpBusy: false,
        data: newData,
        meta: response.meta,
        __action: 'Companies list success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Companies list error'
      });
    }
  }

  public reset() {
    this.setState({
      httpBusy: false,
      data: null,
      error: null,
      message: '',
      __action: 'Companies list reset'
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Companies list clear error'
    });
  }
}

export default CompaniesListContainer;