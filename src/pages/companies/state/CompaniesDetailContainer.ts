import { Container } from 'unstated';
import Companies from '../services/Companies';
import CompaniesDetailState from '../models/CompaniesDetailState';

const initialState = {
    httpBusy: false,
    data: null,
    error: null,
    message: null
}

class CompaniesDetailContainer extends Container<CompaniesDetailState> {
    public state = {
        ...initialState
    }

    public async getDetailCompanie(uuid:string) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'COMPANIE DETAIL STARTS'
            });
            
            const response = await Companies.getDetail(uuid);

            this.setState({
                httpBusy: false,
                data: response.data,
                __action: 'COMPANIE DETAIL SUCCESS'
            });

        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                message: error.message,
                __action: 'COMPANIE DETAIL ERROR'
            });
        }
    }

    public clearError() {
        this.setState({
            ...this.state,
            error: null,
            message: null,
            __action: 'COMPANIE DETAIL CLEAR ERROR'
        });
    }

    public reset() {
        this.setState({
            ...initialState,
            __action: 'COMPANIE DETAIL RESET'
        })
    }
}

export default CompaniesDetailContainer;