import { Container } from 'unstated';

interface CounterState {
  count: number,
  __action?:string
  
}

class CounterContainer extends Container<CounterState> {
  public state: CounterState = {
    count: 0
  };

  public name: string = 'Counter';

  public increment() {
    this.setState({
      count: this.state.count + 1,
      __action: 'INCREMENT'
    });
  }

  public decrement() {
    this.setState({
      count: this.state.count - 1,
      __action: 'DECREMENT'
    });
  }
}

export default CounterContainer;