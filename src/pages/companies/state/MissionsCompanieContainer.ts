import { Container } from 'unstated';
import Companies from '../services/Companies';
import MissionsCompanieState from '../models/MissionCompanieState';

const initialState = {
    data: null,
    error: null,
    httpBusy: false,
    message: ''
}

class MissionCompanieContainer extends Container<MissionsCompanieState> {
    public state = {
        ...initialState
    }

    public async list(companieId: string, meta: {}) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'LIST MISSIONS BY COMPANY STARTS'
            });

            const response = await Companies.getMissions(companieId, meta);

            this.setState({
                httpBusy: false,
                data: response.data,
                __action: 'LIST MISSIONS BY COMPANY SUCCESS'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'LIST MISSIONS BY COMPANY ERROR'
            });
        }
    }
}

export default MissionCompanieContainer;