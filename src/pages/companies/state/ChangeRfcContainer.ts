import { Container } from 'unstated';
import companies from '../services/Companies';
import Lang from 'src/support/Lang';
import Event from 'src/support/Event';

class ChangeRfcContainer extends Container<any> {
  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
    meta: null
  }

  public name: string = 'Change Rfc Container';

  public async fetch(uuid: any, data: any) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Change RFC starts'
      });

      const response = await companies.upsert(uuid, data);
      
      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Change RFC success'
      });

      result = {
        message: Lang.get('company.success_change_rfc'),
        action: 'Change RFC success'
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Change RFC error'
      });

      result = {
        error,
        action: 'Change RFC error'
      }
    }

    Event.notify('company::edit', result);
  }

  public reset() {
    this.setState({
      httpBusy: false,
      data: null,
      error: null,
      message: '',
      __action: 'Change RFC reset'
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Change RFC clear error'
    });
  }
}

export default ChangeRfcContainer;