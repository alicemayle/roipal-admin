import { Container } from 'unstated';
import Companies from '../services/Companies';
import Event from 'src/support/Event';
import Lang from 'src/support/Lang';

class AddCreditsContainer extends Container<any> {
  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
    meta: null
  }

  public name: string = 'Credits company';

  public async fetch(uuid: any, data: any) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Add credits starts'
      });

      const response = await Companies.upsert(uuid, data);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Add credits success'
      });

      result = {
        message: Lang.get('company.success_credits'),
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Add credits error'
      });

      result = {
        error,
      }
    }
    Event.notify('company::edit', result);
  }

  public reset() {
    this.setState({
      httpBusy: false,
      data: null,
      error: null,
      message: '',
      __action: 'Add credits reset'
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Add credits clear error'
    });
  }
}

export default AddCreditsContainer;