import React, { Component } from 'react';
import Autobind from 'class-autobind';
import Lang from '../../../../support/Lang';

import ActionButton from 'src/components/buttons/ActionButton';
interface ActionButtonProps {
  field: string,
  current: number,
  pages: number,
  onHandleClick: (typeButton: string) => void
}

class ActionButtons extends Component <ActionButtonProps> {
  constructor(props: ActionButtonProps) {
    super(props)
    Autobind(this);
  }

  private handleClick(typeButton: string) {
    const { onHandleClick } = this.props;

    if ( onHandleClick ) {
      onHandleClick(typeButton)
    }
  }

  public render() {
    const { current, pages } = this.props;

    return (
      <div className="steps-action">
        {
          current > 0 && <ActionButton 
            field={'previous'} 
            label={Lang.get('generic.button.previous')} 
            onHandleClick={this.handleClick} 
          />
        }

        {
          current < (pages -1) && <ActionButton 
            field={'next'} 
            label={Lang.get('generic.button.next')} 
            onHandleClick={this.handleClick} 
          />
        }

        {
          (pages -1) === current && <ActionButton 
            field={'done'} 
            label={Lang.get('generic.button.done')} 
            onHandleClick={this.handleClick} 
          />
        }
      </div>
    )
  }

}

export default ActionButtons;