import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Form, Input, Select } from 'antd';
// import User from '../../../../auth/User';
import HandleChange from '../../models/Upsert/HandleChange'
import lang from '../../../../support/Lang';
import ActionButtons from './ActionButtons';
import Auth from '../../../../auth/User';

const { Option } = Select;
// import UploadVideo from '../../services/UploadVideo'

// interface state {
//   fileList ?: [{}] |
// }

// interface info {
//   target?: HTMLInputElement,
//   file: {
//     lastModified: number,
//     lastModifiedDate: Date,
//     name: string,
//     originFileObj: {},
//     percent: number,
//     response: { video: string },
//     size: number,
//     status: string,
//     type: string,
//     uid: string,
//   },

//   fileList: [
//     {
//       lastModified: number,
//       lastModifiedDate: Date,
//       name: string,
//       originFileObj: {},
//       percent: number,
//       response: { video: string },
//       size: number,
//       status: string,
//       type: string,
//       uid: string,
//     }
//   ],
//   handleChangeVideo?:(info:{})=>void

// }

class Description extends PureComponent<any, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
      fileList: [],
      data: {
      }
    }
  }

  public normFile(e: { fileList: {} }) {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  private handleChange(e: HandleChange) {
    const value = e.target.value;
    const name = e.target.name;

    this.setState({
      data: {
        ...this.state.data,
        [name]: value
      }
    })

    this.handleChangeData()
  }

  private handleChangeData() {
    const { onHandleChangeData } = this.props;

    if (onHandleChangeData) {
      onHandleChangeData(this.state.data)
    }
  }

  // private handleChangeVideo(info: any) {
  //   let fileList = [...info.fileList];

  //   if (info.file.status !== undefined) {
  //     fileList = fileList.slice(-1);

  //     fileList = fileList.map(file => {
  //       if (file.response) {
  //         // file.url = file.response.video;
  //         // this.props.onChange('video', file.response)

  //         this.setState({
  //           data: {
  //             ...this.state.data,
  //             video: file.response
  //           }
  //         })
  //         this.handleChangeData()
  //       }
  //       return file;
  //     });
  //     this.setState({ fileList });
  //   }

  // }

  // private beforeUpload(file: { type: string, size: number }) {
  //   const isVIDEO = file.type === 'video/mp4';

  //   if (!isVIDEO) {
  //     message.error('You can only upload mp4 file!');
  //   }
  //   const isLt2M = file.size / 1024 / 1024 < 6;
  //   if (!isLt2M) {
  //     message.error('Image must smaller than 2MB!');
  //   }

  //   return isVIDEO && isLt2M;
  // }

  private check(typeButton: string) {
    this.props.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        console.info('success', values);
        this.props.onHandleCompleteData(this.props, this.state.data)
        this.props.onHandleClick(typeButton)
      }
    });
  };

  private onChangeCompany(value: string, option: any) {
    this.props.onHandleChangeCompany(value, option.props.children)
  }

  private renderOptions(data: any) {
    const value = data.business_name

    return (
      <Option key={data.key} value={data.uuid}>
        {value}
      </Option>
    );
  }

  public componentDidMount() {
    if(Auth.getData().company) {
      const uuid = Auth.getData().company.uuid;

      this.props.onHandleChangeCompany(uuid)
    }
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { selectedValues, companies } = this.props;
    // const token = User.getAccessToken();

    const newPropsActionButtons = {
      field: this.props.field,
      current: this.props.current,
      pages: this.props.pages,
      onHandleClick: this.check
    }

    let company = null

    if (Auth.getData().company) {
      company = true
    } else {
      company = false
    }

    return (
      <div>
        <Form.Item label={lang.get('buses.mission.add.description.select_company.label')}>
          {getFieldDecorator('company', {
            rules: [
              {
                required: true,
                message: lang.get('buses.mission.add.description.select_company.message'),
              },
            ],
            initialValue: company ? Auth.getData().company.uuid : undefined
          })(
            <Select
              showSearch={true}
              placeholder={lang.get('buses.mission.add.description.select_company.label')}
              optionFilterProp="children"
              onChange={this.onChangeCompany}
              disabled={company ? true : false}
            >
              {
                !company && companies &&
                companies.map(this.renderOptions)
              }
              {
                company &&
                <Option key={Auth.getData().company.uuid} value={Auth.getData().company.uuid}>
                  {Auth.getData().company.business_name}
                </Option>
              }
            </Select>
          )}
        </Form.Item>


        <Form.Item label={lang.get('buses.mission.add.description.input_name.label')}>
          {getFieldDecorator('title', {
            initialValue: selectedValues.name,
            rules: [
              {
                required: true,
                type: 'string',
                message: lang.get('buses.mission.add.description.input_name.message'),
              },
            ],
          })(
            <Input
              placeholder={lang.get('buses.mission.add.description.input_name.label')}
              name={'name'}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>

        <Form.Item
          label={lang.get('buses.mission.add.description.input_descrp.label')}>
          {getFieldDecorator('description', {
            initialValue: selectedValues.description,
            rules: [
              {
                required: true,
                type: 'string',
                min: 50,
                message: lang.get('buses.mission.add.description.input_descrp.message'),
              },
            ],
          })(
            <Input
              placeholder={lang.get('buses.mission.add.description.input_descrp.label')}
              name={'description'}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>


{/******Se comenta el codigo por la adaptacion del componente para subir un video
 * descomentar cuando se realize la adaptacion*******/}
        {/* <Form.Item label={lang.get('buses.mission.add.description.video.label')}>
          <div className="dropbox">
            {getFieldDecorator('dragger', {
              getValueFromEvent: this.normFile,
            })(
              <Upload.Dragger
                name="video"
                action="https://api-roipal.nulldata.com/api/files"
                headers={{ 'authorization': `Bearer ${token}` }}
                multiple={false}
                beforeUpload={this.beforeUpload}
                fileList={this.state.fileList}
                onChange={this.handleChangeVideo}
              >
                <p className="ant-upload-drag-icon">
                  <Icon type="video-camera" />
                </p>
                <p className="ant-upload-text">
                  {lang.get('buses.mission.add.descrip.video.msg')}
                </p>
              </Upload.Dragger>,
            )}
          </div>
        </Form.Item> */}

        <ActionButtons {...newPropsActionButtons} />
      </div >

    );
  }
}

const WrappedDemo: any = Form.create({ name: 'form_description' })(Description);
export default WrappedDemo;