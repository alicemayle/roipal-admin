import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { List, Row, Col, Divider } from 'antd';
import ActionButtons from './ActionButtons';
import Lang from '../../../../support/Lang';
import CardDetailsMission from './CardDetailsMission';
import NumberFormat from '../../../../support/NumberFormat';

interface DetailsMissionProps {
  selectedValues: any,
  onHandleChangeData: (data: any) => void,
  field: string,
  current: number,
  pages: number,
  onHandleClick: (typeButton: string) => void
  onHandleCompleteData: (field: string, data?: any) => void
}

class DetailsMission extends Component<DetailsMissionProps, any> {
  constructor(props: any) {
    super(props)
    this.state = {}

    Autobind(this);
  }

  private renderItem(item: any) {
    const { currency, country } = this.props.selectedValues;
    const priceLocationFormat = NumberFormat.formatByCountry(item.location_total, country.value)

    return (
      <List.Item>
        <Row gutter={16}>
          <Col>
            <p style={{ fontWeight: 'bold' }}>{item.name}</p>
          </Col>
        </Row>
        <Row>
          <Col span={10}>
            {item.address}
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={10}>
            <p>{Lang.get('buses.mission.add.details.number_executives')}:</p>
          </Col>
          <Col span={5}>
            <p>{item.executives_requested}</p>
          </Col>
        </Row>
        <Row>
          <Col span={10}>
            <p>{Lang.get('buses.mission.add.details.price_location')}:</p>
          </Col>
          <Col span={10}>
            <p>{currency.symbol} {priceLocationFormat} {currency.currency}</p>
          </Col>
        </Row>
        <br />
        <Divider />
      </List.Item>
    )
  }

  private check(typeButton: string) {
    if (typeButton === 'next') {
      this.props.onHandleCompleteData(this.props.field)
      this.props.onHandleClick(typeButton)
    } else {
      this.props.onHandleClick(typeButton)
    }
  }

  private async numberFormat() {
    const { country, price, total, commissions, tax } = this.props.selectedValues;

    const priceFormat = NumberFormat.formatByCountry(price, country.value)
    const commissionsFormat = NumberFormat.formatByCountry(commissions, country.value)
    const taxFormat = NumberFormat.formatByCountry(tax, country.value)
    const totalFormat = NumberFormat.formatByCountry(total, country.value)

    this.setState({
      total: totalFormat,
      tax: taxFormat,
      price: priceFormat,
      commissions: commissionsFormat
    })
  }

  public componentDidMount() {
    this.numberFormat()
  }

  public render() {
    const {
      name,
      description,
      date,
      type,
      time,
      time_unit,
      currency,
      locations,
      commercialNeed
    } = this.props.selectedValues;

    const newPropsActionButtons = {
      field: this.props.field,
      current: this.props.current,
      pages: this.props.pages,
      onHandleClick: this.check
    }

    const {
      price,
      commissions,
      tax,
      total,
    } = this.state

    return (
      <div style={{ background: '#ECECEC', padding: '30px' }}>
        <Row gutter={16}>
          <Col span={12}>
            <CardDetailsMission
              icon="form"
              title={Lang.get('buses.mission.add.details.name')}
              text={name}
            />
          </Col>
          <Col span={12}>
            <CardDetailsMission
              icon="calendar"
              title={Lang.get('buses.mission.add.details.date')}
              text={date}
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            <CardDetailsMission
              icon="file-text"
              title={Lang.get('buses.mission.add.details.description')}
              text={description}
            />
          </Col>
        </Row>
        <br />
        <Row gutter={16}>
          <Col span={8}>
            <CardDetailsMission
              icon="project"
              title={Lang.get('buses.mission.add.details.commercial_need')}
              text={commercialNeed}
            />
          </Col>
          <Col span={8}>
            <CardDetailsMission
              icon="tag"
              title={Lang.get('buses.mission.add.details.type')}
              text={type}
            />
          </Col>
          <Col span={8}>
            <CardDetailsMission
              icon="hourglass"
              title={Lang.get('buses.mission.add.details.time')}
              text={time + ' ' + time_unit}
            />
          </Col>
        </Row>
        <br />
        <Row gutter={16}>
          <Col>
            <CardDetailsMission
              icon="environment"
              title={Lang.get('buses.mission.add.details.locations')}
            >
              <List
                grid={{ column: 2 }}
                dataSource={locations}
                renderItem={this.renderItem}
              />
            </CardDetailsMission>
          </Col>
        </Row>
        <br />
        <Row style={{ marginBottom: 16 }}>
          <Col>
            <CardDetailsMission
              icon="dollar"
              title={Lang.get('buses.mission.add.details.cost')}
            >
              <Row gutter={8}>
                <Col
                  xs={{ span: 12 }}
                  md={{ span: 12 }}
                  sm={{ span: 12 }}
                  lg={{ span: 6 }}>
                  <p>
                    {Lang.get('buses.mission.add.details.subtotal')}
                    : {currency.symbol} {price} {currency.currency}
                  </p>
                </Col>
                <Col
                  xs={{ span: 12 }}
                  md={{ span: 12 }}
                  sm={{ span: 12 }}
                  lg={{ span: 6 }}>
                  <p>
                    {Lang.get('buses.mission.add.details.commissions')}
                    : {currency.symbol} {commissions} {currency.currency}
                  </p>
                </Col>
                <Col
                  xs={{ span: 12 }}
                  md={{ span: 12 }}
                  sm={{ span: 12 }}
                  lg={{ span: 6 }}>

                  <p>
                    {Lang.get('buses.mission.add.details.tax')}
                    : {currency.symbol} {tax} {currency.currency}
                  </p>
                </Col>
                <Col
                  xs={{ span: 12 }}
                  md={{ span: 12 }}
                  sm={{ span: 12 }}
                  lg={{ span: 6 }}>
                  <p style={{ fontWeight: 'bold' }}>
                    {Lang.get('buses.mission.add.details.total')}
                    : {currency.symbol} {total} {currency.currency}
                  </p>
                </Col>
              </Row>
            </CardDetailsMission>
          </Col>
        </Row>
        <ActionButtons {...newPropsActionButtons} />
      </div>

    )
  }

}

export default DetailsMission;