import { ItemState } from '../../../../roipal/models'
import { number } from 'prop-types';

interface OptionsProps {
  uuid: string,
  description: string,
  type_activities: []
}

interface DataActivitiesProps {
  uuid: string,
  type_sale_activities: []
}

interface BusinessProps {
  onChange: (label: string, option: {}) => void,
  listActivities: {
    fetch: (params: {}) => void,
    fetchCustomActivities: () => void
  },
  commercial: array[{}],
  activities: array[{}],
  data: {},
  selectedValues: {}
}

interface ActivitiesContainer {
  activities: {
    state: {},
  }
}

interface TypeSaleProp {
  custom_uuid?: string,
  type_sale_uuid: string,
  commercial_need_uuid: string,
  type_sale_activities?: number[]
}

interface BusinessState {
  custom: boolean,
  selected?: string,
  type_sale?: any
}

interface BusinessActivitiesProps {
  data: OptionsProps,
  typeCustom: boolean,
  selected: string,
  key: string,
  onChange: (data: any) => void
}
interface BusinessActivitiesState {
  checkedList: array[{}]
}

interface BusinessListProps {
  custom?: boolean,
  type?: 'checkbox',
  dataSource: [],
  checkedValues?: [],
  data?: string,
}

interface BusinessItemProps {
  custom?: boolean,
  type?: 'checkbox',
  data: string,
  key: number,
  checkedValues?: any
}

interface dataMission {
  name?: string,
  description?: string,
  type?: string,
  commercial_need?: string,
  profile?: string,
  time?: number,
  time_unit?: string,
  fare_by_time?: number,
  subtotal?: number,
  date?: string,
  tax?: number,
  total?: number,
  currency?: {
    currency?: string,
    symbol?: string,
    name?: string
  },
  suggested?: {
    profile?: string,
    time?: number,
    time_unit?: string,
    min_time?: number,
    max_time?: number,
    fare_by_time?: number,
    estimations?: [
      {
        min?: number,
        max?: number,
        fare?: number
      }
    ]
  },
  locations?: [
    {
      uuid?: string,
      executives_requested?: number
    }
  ],
  type_sale?:
  {
    type_sale_activities?: [],
    uuid?: string
  },
  country?: string,
  thumbnail_video?: string
}

interface AddressSelected {
  latLng: {lat: number, lng: number}
  address: string
}

interface AddressSelectionComponentProps {
onChangeAddress(): () => AddressSelected
}

export {
  OptionsProps,
  DataActivitiesProps,
  BusinessProps,
  ActivitiesContainer,
  BusinessState,
  BusinessActivitiesProps,
  BusinessActivitiesState,
  BusinessListProps,
  BusinessItemProps,
  dataMission,
  AddressSelected,
  AddressSelectionComponentProps
};