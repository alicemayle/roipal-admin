import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { InputNumber, Form } from 'antd';
import lang from '../../../../support/Lang';
interface InputNumberItemProps {
  item: any,
  onHandleChangeNumber?: (item: any, value: number) => void
}

class InputNumberItem extends PureComponent<any, any> {
  constructor(props: InputNumberItemProps) {
    super(props);
    Autobind(this);
  }

  private handleChangeNumber(value: number) {
    const { item, onHandleChangeNumber } = this.props;

    if ( onHandleChangeNumber ) {
      onHandleChangeNumber(item, value);
    }
  }

  public componentWillReceiveProps(nextProps: any) {
    if(nextProps.check !== this.props.check && nextProps.check === true) {
      this.props.form.validateFields(async (err: any, values: any) => {
        if (!err) {
          this.props.onHadleValidate(true)
        } else {
          this.props.onHadleValidate(false)
        }
      })
    }
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { edit, item } = this.props;
    const value = edit ? item.executives_requested : undefined

    return (
      <Form.Item label={lang.get('buses.mission.add.locations.num_executives')}>
          {getFieldDecorator('executives_requested', {
            initialValue: value,
            rules: [
              {
                required: true,
                message: lang.get('generic.legends.required'),
              },
            ],
          })(
      <InputNumber
        min={edit && item.totalExecutives > 0 ? item.totalExecutives : 1}
        max={1000}
        onChange={this.handleChangeNumber}
      />
      )}
        </Form.Item>
    )
  }
}
const FormInputNumber: any = Form.create({ name: 'form_estimate' })(InputNumberItem);

export default FormInputNumber;