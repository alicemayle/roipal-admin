import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { connect, Logger } from 'unstated-enhancers';
import { Button, Row, Col, Alert } from 'antd';
import LocationsContainer from '../../state/LocationContainer';
import LocationCostContainer from '../../state/LocationsCostContainer';
import CreateLocationsForm from './CreateLocationsForm';
import Map from './Map';
import { AddressSelected } from './Upsert';
import LocationList from './LocationList';
import UUID from 'uuid-js';
import LocationListSlider from './LocationListSlider';
import ActionButtons from './ActionButtons';
import lang from '../../../../support/Lang';
interface LocationProps {
  containers: any,
  company: any,
  selectedValues: any,
  handleChangeAddress: (data: AddressSelected) => void,
  handleChange: (field: string, value: any) => void,
  handleSelectLocation: (item: any) => void,
  onHandleChangeData: (data: any) => void,
  field: string,
  current: number,
  pages: number,
  onHandleClick: (typeButton: string) => void
  onHandleCompleteData: (field: string, data: any) => void
}
class Locations extends PureComponent<LocationProps, any> {

  constructor(props: LocationProps) {
    super(props);
    Autobind(this);
    this.state = {
      showPrincialComponent: true,
      selectLocationList: [],
      total: 0,
      subtotal: 0,
      price: 0,
      check: false,
      validate: false,
      emptyList: false
    }
  }

  public async componentWillMount() {
    this.setState({
      defaultCenter: [19.3665953, -99.1832562]
    })
    await this.getLocations();

    const { locations } = this.props.containers;
    const { data } = locations.state;

    this.setState({
      locationsList: [...data]
    })
  }

  private async getLocations() {
    const { locations } = this.props.containers;

    const params = {
      search: '',
      limit: 25,
      page: 1
    };

    const company = this.props.company;

    await locations.fetch(company, params);

    const { error } = locations;

    if (error) {
      // TODO: tratar el error
    }
  }

  private handleChange(field: string, value: any) {
    this.setState({
      [field]: value
    })
  }

  private async handleSaveLocation() {
    const { addressSelected, addressName, selectLocationList = [], locationsList } = this.state;
    const { type_sale, time_unit_cod, company } = this.props.selectedValues;
    const costContainer = this.props.containers.locationsCost;

    const uuidNew = UUID.create().toString()

    const loc = [{
      latitude: addressSelected.latLng.lat,
      longitude: addressSelected.latLng.lng,
      uuid: uuidNew
    }]

    const data = {
      company_uuid: company,
      commercial_need_uuid: type_sale.commercial_need_uuid,
      type_sale_uuid: type_sale.type_sale_uuid,
      time: this.props.selectedValues.time,
      time_unit: time_unit_cod,
      locations: loc
    }

    await costContainer.getCostLocations(data)

    const costLocation = costContainer.state.data.locations;

    let executiveCost = 0
    let executivePaymentCost = 0

    const getCost = costLocation.map((item: any) => {
      executiveCost = item.executive_cost
      executivePaymentCost = item.executive_payment_cost
    })

    Logger.dispatch('getCost', getCost)

    const exesCost = (executiveCost).toFixed(2)
    const paymentCost = (executivePaymentCost).toFixed(2)

    const newLocation = {
      uuid: uuidNew,
      address: addressSelected.address,
      latitude: addressSelected.latLng.lat,
      longitude: addressSelected.latLng.lng,
      name: addressName,
      selected: true,
      executive_cost_min: parseFloat(exesCost),
      executive_cost: parseFloat(exesCost),
      executive_payment_cost: parseFloat(paymentCost),
      executives_requested: 0
    }

    const array = [...selectLocationList, newLocation]

    this.setState({
      selectLocationList: array,
      locationsList: [newLocation, ...locationsList]
    })

    this.handleChangeData();

    this.handleShowAndHiddeComponents();
  }

  private handleShowAndHiddeComponents() {
    const { showPrincialComponent } = this.state;

    this.setState({
      showPrincialComponent: !showPrincialComponent
    })
  }

  private handleChangeAddress(data: AddressSelected) {
    this.setState({
      addressSelected: data
    })
  }

  private async handleSelectLocation(item: any) {
    if (item.selected) {
      const { selectLocationList = {}, locationsList } = this.state;

      const updateLoctionList = locationsList.map((location: any) => {
        let obj = location;

        if (item.uuid === location.uuid) {
          obj = {
            address: item.address,
            created_at: item.created_at,
            latitude: item.latitude,
            longitude: item.longitude,
            name: item.name,
            updated_at: item.updated_at,
            uuid: item.uuid
          }
        }
        return obj
      })

      const filter = selectLocationList.filter((fil: any) => fil.uuid !== item.uuid)

      this.setState({
        selectLocationList: filter,
        locationsList: updateLoctionList,
      })

      this.handleChangeData()
    } else {
      const { type_sale, time_unit_cod, company } = this.props.selectedValues;

      const costContainer = this.props.containers.locationsCost;

      const loc = [{
        latitude: item.latitude,
        longitude: item.longitude,
        uuid: item.uuid
      }]

      const data = {
        company_uuid: company,
        commercial_need_uuid: type_sale.commercial_need_uuid,
        type_sale_uuid: type_sale.type_sale_uuid,
        time: this.props.selectedValues.time,
        time_unit: time_unit_cod,
        locations: loc
      }

      await costContainer.getCostLocations(data)

      const { locationsList, selectLocationList } = this.state;

      const costLocation = costContainer.state.data.locations;

      let executiveCost = 0
      let executivePaymentCost = 0

      costLocation.map((find: any) => {
        executiveCost = find.executive_cost
        executivePaymentCost = find.executive_payment_cost
      })

      const updateLoctionList = locationsList.map((location: any) => {
        let obj = location;
        const selected = location.selected === undefined ? true : !location.selected;

        const exeCost = (executiveCost).toFixed(2)
        const paymentCost = (executivePaymentCost).toFixed(2)

        if (item.uuid === location.uuid) {
          obj = {
            ...location,
            selected,
            executive_cost_min: parseFloat(exeCost),
            executive_cost: parseFloat(exeCost),
            executive_payment_cost: parseFloat(paymentCost),
            executives_requested: 0
          }
        }
        return obj
      })

      const newLoc = updateLoctionList.find((element: any) =>
        element.uuid === item.uuid
      );

      this.setState({
        selectLocationList: [...selectLocationList, newLoc],
        locationsList: updateLoctionList,
        emptyList: false
      })

      this.handleChangeData()
    }
  }

  private setTotal(value: any) {
    const costContainer = this.props.containers.locationsCost.state.data;
    const { iva } = this.props.selectedValues;

    if (value) {

      const comision = (value * costContainer.sum_percentage / 100) +
        costContainer.sum_landline

      const addSubtotal = (value).toFixed(2)
      const addCommissions = (comision).toFixed(2)
      const addTotal = (parseFloat(addCommissions) + parseFloat(addSubtotal)).toFixed(2)
      const addTax = (parseFloat(addTotal) * iva).toFixed(2)
      const Total = (parseFloat(addTax) + parseFloat(addTotal)).toFixed(2)

      this.setState({
        price: parseFloat(addSubtotal),
        subtotal: parseFloat(addTotal),
        commissions: parseFloat(addCommissions),
        tax: parseFloat(addTax),
        total: parseFloat(Total)

      }, this.handleChangeData)

      this.handleChangeData()
    }
  }

  private handleState(field: string, value: any) {
    this.setState({
      ...this.state,
      [field]: value
    }, this.handleChangeData)

    this.handleChangeData()
  }

  private handleChangeData() {
    const { onHandleChangeData } = this.props;

    if (onHandleChangeData) {
      onHandleChangeData(this.state)
    }
  }

  private async handleChangeNumber(item: any, value: number) {
    const { selectLocationList = [] } = this.state;
    const index = selectLocationList.indexOf(item);
    const locationCost = (item.executive_cost * value).toFixed(2)

    if (index > -1) {
      const updatedLocation = {
        ...item,
        executives_requested: value,
        location_total: parseFloat(locationCost)
      }

      const sub = (this.state.price).toFixed(2)
      const subCost = ((value - item.executives_requested)
        * item.executive_cost).toFixed(2)

      const totalMission = (parseFloat(sub) + parseFloat(subCost)).toFixed(2)
      const copySelectLocationList = [...selectLocationList];

      copySelectLocationList.splice(index, 1, updatedLocation);

      await this.handleState('selectLocationList', copySelectLocationList)
      this.setTotal(parseFloat(totalMission))
    }
  }

  private async handleChangeNumberSlider(item: any, value: number) {
    const { selectLocationList = [] } = this.state;
    const index = selectLocationList.indexOf(item);
    const locationCost = (item.executives_requested * value).toFixed(2)

    const subtotal = (this.state.price).toFixed(2)

    if (index > -1) {
      const updatedLocation = {
        ...item,
        executive_cost: value,
        location_total: parseFloat(locationCost)
      }

      const totalMission = (parseFloat(subtotal) - item.location_total
        + parseFloat(locationCost)).toFixed(2)

      const copySelectLocationList = [...selectLocationList];

      copySelectLocationList.splice(index, 1, updatedLocation);

      await this.handleState('selectLocationList', copySelectLocationList)
      this.setTotal(parseFloat(totalMission))
    }
  }

  private check(typeButton: string) {
    if (typeButton === 'next') {
      if(this.state.selectLocationList.length < 1) {
        this.setState({
          emptyList: true
        })
        return
      }

      this.setState({
        check: true
      })

      setTimeout(() => {
        const { validate } = this.state;

        if(validate) {
          this.props.onHandleCompleteData(this.props.field, this.state)
          this.props.onHandleClick(typeButton)
        }
      }, 500)

    } else {
      this.props.onHandleClick(typeButton)
    }
  }

  private handleValidateNumExecutive(_validate: boolean) {
    this.setState({
      validate: _validate,
      check: false
    })
  }

  public render() {
    const {
      addressSelected,
      defaultCenter,
      showPrincialComponent,
      selectLocationList = [],
      locationsList = [],
      check,
      emptyList,
      price
    } = this.state;

    const {
      currency
    } = this.props.selectedValues;

    const newPropsActionButtons = {
      field: this.props.field,
      current: this.props.current,
      pages: this.props.pages,
      onHandleClick: this.check
    }

    return (
      <div>
        {showPrincialComponent &&
          <div>
            <Row gutter={55}
            type="flex"
            align="middle"
            style={{
              justifyContent: 'space-between',
              backgroundColor: '#F3F3F3',
              padding: 20,
              marginBottom: 16
            }}>
              <Col span={10}>
                <Button type="primary"
                  icon="plus-square"
                  onClick={this.handleShowAndHiddeComponents}
                >
                  {lang.get('generic.button.add')}
                </Button>
              </Col>
              <Col span={8}>
                <div>
                {lang.get('buses.mission.add.details.subtotal')}
                : {currency.symbol} {price} {currency.currency}
                </div>
              </Col>
            </Row>
            {
              emptyList &&
              <Alert
                message={lang.get('buses.mission.add.locations.empty_list')}
                type="error"
                showIcon={true}
              />
            }
            <Row gutter={55}>
              <Col span={9}>
                <LocationList
                  data={locationsList}
                  custom={false}
                  onHandleSelectLocation={this.handleSelectLocation}
                />
              </Col>
              <Col span={15}>
                <LocationListSlider
                  data={selectLocationList}
                  total={this.state.total}
                  onHandleChangeNumber={this.handleChangeNumber}
                  onhandleChangeNumberSlider={this.handleChangeNumberSlider}
                  onCheck={check}
                  onHadleValidate={this.handleValidateNumExecutive}
                />
              </Col>
            </Row>
          </div>
        }
        <br />
        {!showPrincialComponent &&
          <div>
            <Row gutter={20}>
              <Col span={7} style={{ backgroundColor: '#F3F3F3' }}>
                <CreateLocationsForm
                  onHandleCreateLocation={this.handleSaveLocation}
                  onChangeAddress={this.handleChangeAddress}
                  onHandleNameAddress={this.handleChange}
                  onHandleClose={this.handleShowAndHiddeComponents}
                />
              </Col>
              <Col span={17} >
                <Map
                  defaultCenter={defaultCenter}
                  addressSelected={addressSelected}
                  onChangeAddress={this.handleChangeAddress}
                />
              </Col>
            </Row>
          </div>
        }

        <ActionButtons {...newPropsActionButtons} />
      </div>
    )
  }
}

const container = {
  locations: LocationsContainer,
  locationsCost: LocationCostContainer
}

const mapStateToProps = (containers: any) => {
  return {
    locations: containers.locations.state,
    locationsCost: containers.locationsCost.state
  }
}

export default connect(container, mapStateToProps)(Locations);