import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Button, Form } from 'antd';
import SelectAddress from 'src/components/selectAddress/SelectAddress';
import { AddressSelected } from './Upsert';
import InputItem from 'src/components/inputItem/InputItem';
import Lang from 'src/support/Lang';

interface ItemPropsI {
  onHandleCreateLocation: () => void,
  onChangeAddress: (data: AddressSelected) => void,
  onHandleNameAddress: (field: string, value: any) => void
  onHandleClose: () => void
}

class CreateLocationsForm extends PureComponent<ItemPropsI, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }

  private handleChange(field: string, value: any) {
    const { onHandleNameAddress } = this.props;

    if (onHandleNameAddress) {
      onHandleNameAddress(field, value);
    }
  }

  private handleCreateLocation() {
    const { onHandleCreateLocation } = this.props;

    if (onHandleCreateLocation) {
      onHandleCreateLocation()
    }
  }

  public render() {
    const { onHandleClose } = this.props;

    return (
      <div>
        <Form.Item label={Lang.get('map.name_location')}>
          <InputItem
            field="addressName"
            placeholder={Lang.get('map.name_location')}
            onChange={this.handleChange} />
        </Form.Item>
        <Form.Item label={Lang.get('map.location')}>
          <SelectAddress onChangeAddress={this.props.onChangeAddress} />
        </Form.Item>
        <Form.Item>
          <Button
          onClick={this.handleCreateLocation}
          style={{ marginRight: 5}}
          type="primary">
            {Lang.get('generic.button.send')}
            </Button>
          <Button
           onClick={onHandleClose}>
            {Lang.get('generic.button.cancel')}
            </Button>
        </Form.Item>
      </div>
    )
  }
}

export default CreateLocationsForm