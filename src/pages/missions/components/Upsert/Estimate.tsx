import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Form, DatePicker, Input, Select, InputNumber } from 'antd';
import Lang from '../../../../support/Lang';
import { Logger } from 'unstated-enhancers';
import { connect } from 'unstated-enhancers';
import EstimationsContainer from '../../state/EstimationsContainer';
import CountryContainer from '../../state/CountryContainer';
import CurrencyContainer from '../../state/CurrencyContainer';
import ActionButtons from './ActionButtons';
import moment from 'moment';
import '../../../../styles/datepicker.scss'
import 'moment/locale/fr';
import 'moment/locale/es'
import locFR from 'antd/es/date-picker/locale/fr_FR'
import locEN from 'antd/es/date-picker/locale/en_US'
import locES from 'antd/es/date-picker/locale/es_ES'

const { Option } = Select;

class Estimate extends PureComponent<any, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
      country: {},
      currency: {}
    }
  }

  private onChangeCalendar(date: any, dateString: any) {
    this.setState({
      date: dateString
    })
    this.handleChangeData()
  }

  private handleState(field: string, value: any) {
    this.setState({
      ...this.state,
      [field]: value
    }, this.handleChangeData)

    this.handleChangeData()
  }

  private handleCurrencySelected(field: string, value: any) {
    const obj = this.state.currencies.find((item: any) => item.value === value);

    if (obj) {
      const result = {
        symbol: obj.symbol,
        currency: obj.currency,
        name: obj.name
      }

      this.handleState(field, result)
    }
  }

  private async getCountries() {
    const countryContainer = this.props.containers.country;

    await countryContainer.fetch();

    const countrie = countryContainer.state.data

    const { error } = countryContainer.state;

    if (error !== null) {
      Logger.dispatch('ERROR al obtener en pais')
    } else {
      this.onSelectedCountrie(countrie)
    }
  }

  private async onSelectedPerfile() {
    const option = 'profile'

    const estimationsContainer = this.props.containers.estimations;
    const { type_sale } = this.props.selectedValues;

    await estimationsContainer.fetch(type_sale.commercial_need_uuid);

    const profile = estimationsContainer.state.data.profile[0].cod

    this.setState({
      profile_name: estimationsContainer.state.data.profile[0].name,
      time_unit: estimationsContainer.state.data.time_unit,
      time_unit_cod: estimationsContainer.state.data.time_unit_cod,
      updated_at: estimationsContainer.state.data.updated_at,
      iva: estimationsContainer.state.data.iva,
      tax: 0
    })

    this.handleState(option, profile)
  }

  private async onSelectedCountrie(value: any) {
    let result;
    let mex;

    result = value.map((item: any) => {
      const data = {
        id: value.indexOf(item),
        label: item.name,
        value: item.cod,
        ...item
      }
      if (item.cod === 'MX') {
        mex = data
      }
      return data
    });

    await this.setState({
      countries: result,
      country: mex
    })
    this.onSelectedPerfile()
  }

  private async onSelectedCurrency(value: any) {
    let result;
    let mex;

    result = value.map((item: any) => {
      const data = {
        id: value.indexOf(item),
        label: item.currency.concat(' - ').concat(item.name),
        value: item.currency,
        ...item
      }
      if (item.currency === 'MXN') {
        mex = data
      }
      return data
    });

    await this.setState({
      currencies: result,
      currency: mex
    })
    this.getCountries()
  }

  private async getCurrency() {
    const currencyContainer = this.props.containers.currency;

    await currencyContainer.fetch();

    const currency = currencyContainer.state.data

    const { error } = currencyContainer.state;

    if (error !== null) {
      Logger.dispatch('ERROR al obtener la moneda')
    } else {
      this.onSelectedCurrency(currency)
    }
  }

  private handleChangeData() {
    const { onHandleChangeData } = this.props;

    if (onHandleChangeData) {
      onHandleChangeData(this.state)
    }
  }

  private renderOptions(data: any) {
    return (
      <Option key={data.id} value={data.value}>
        {data.label}
      </Option>
    );
  }

  private check(typeButton: string) {
    if (typeButton === 'next') {
      this.props.form.validateFields(async (err: any, values: any) => {
        if (!err) {
          this.props.onHandleCompleteData(this.props.field, this.state)
          this.props.onHandleClick(typeButton)
        }
      })
    } else {
      this.props.onHandleClick(typeButton)
    }
  }

  private getDate() {
    const date = new Date();
    const dateFormat = 'YYYY/MM/DD';

    const tomorrow = new Date(date.getTime() + 24 * 60 * 60 * 1000);
    const tomorrowDate = new Date(tomorrow)
    const formatDate = moment(tomorrowDate).format(dateFormat)
    this.setState({
      date: formatDate
    })

    this.handleChangeData()
  }

  private disabledDate(current: any) {
    return current && current < moment().endOf('day');
  }

  private handleChangeNumber(value: number) {
    const { data } = this.props.containers.estimations.state;

    if (value >= data.min_time) {
      this.setState({
        ...this.state,
        time: value
      })

      this.handleChangeData()
      this.handleState('time', value)
    }
  }

  public async componentDidMount() {
    this.getCurrency();
    this.getDate()
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const data = this.props.containers.estimations.state.data || {};

    const newPropsActionButtons = {
      field: this.props.field,
      current: this.props.current,
      pages: this.props.pages,
      onHandleClick: this.check
    }

    moment.locale(Lang.getLocale())
    const locale = Lang.getLocale() === 'fr' ? locFR : Lang.getLocale() === 'es' ? locES : locEN

    return (
      <div>
        <Form.Item label={Lang.get('buses.mission.country')}>
          {getFieldDecorator('country', {
            initialValue: this.state.country.value,
            rules: [
              {
                required: true,
                message: Lang.get('buses.mission.add.estimate.select_country.message'),
              },
            ],
          })(
            <Select
              showSearch={true}
              placeholder={Lang.get('buses.mission.selectCountry')}
              onChange={this.handleCurrencySelected}
              disabled={true}>
              {
                this.state.countries &&
                this.state.countries.map(this.renderOptions)
              }
            </Select>
          )}
        </Form.Item>

        <Form.Item label={Lang.get('buses.mission.currency')}>
          {getFieldDecorator('currency', {
            initialValue: this.state.currency.value,
            rules: [
              {
                required: true,
                message: Lang.get('buses.mission.add.estimate.select_currency.message'),
              },
            ],
          })(
            <Select
              showSearch={true}
              placeholder={Lang.get('buses.mission.selectCurrency')}
              onChange={this.handleState}
              disabled={true}>
              {
                this.state.currencies &&
                this.state.currencies.map(this.renderOptions)
              }
            </Select>
          )}
        </Form.Item>

        <Form.Item label="Perfil">
          <Input
            value={this.state.profile_name}
            name={'profile'}
            disabled={true}
          />
        </Form.Item>

        <Form.Item label={Lang.get('buses.mission.date')}>
          {getFieldDecorator('date', {
            initialValue: moment(this.state.date),
            rules: [
              {
                required: true,
                message: Lang.get('buses.mission.add.estimate.select_date.message'),
              },
            ],
          })(
            <DatePicker
              disabledDate={this.disabledDate}
              placeholder={Lang.get('buses.mission.select_date')}
              onChange={this.onChangeCalendar}
              allowClear={true}
              locale={locale}
            />

          )}
        </Form.Item>

        <Form.Item label={Lang.get('buses.mission.add.estimate.select_time.label')}>
          {getFieldDecorator('time', {
            rules: [
              {
                required: true,
                message: Lang.get('buses.mission.add.estimate.select_time.message'),
              },
            ],
          })(
            <InputNumber
              name={'time'}
              min={data.min_time}
              style={{ width: 170 }}
              placeholder={Lang.get('buses.mission.add.estimate.select_time.placeholder')}
              onChange={this.handleChangeNumber}
            />
          )}
          <span className="ant-form-text"> {data.time_unit}</span>
        </Form.Item>

        <Form.Item style={{ backgroundColor: '#F3F3F3', padding: 15 }}>
          {
            `${Lang.get('buses.mission.add.estimate.time_min.message_1')}
            ${this.props.selectedValues.type}
            ${Lang.get('buses.mission.add.estimate.time_min.message_2')}
            ${data.min_time} ${data.time_unit}
            ${Lang.get('buses.mission.add.estimate.time_min.message_3')}`
          }
        </Form.Item>
        <ActionButtons {...newPropsActionButtons} />
      </div>
    );
  }
}

const container = {
  estimations: EstimationsContainer,
  country: CountryContainer,
  currency: CurrencyContainer
}

const mapStateToProps = (containers: any) => {
  return {
    estimations: containers.estimations.state,
    country: containers.country.state,
    currency: containers.currency.state
  }
}

const FormEstimate: any = Form.create({ name: 'form_estimate' })(Estimate);
export default connect(container, mapStateToProps)(FormEstimate);