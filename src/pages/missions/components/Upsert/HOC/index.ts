import SectionWithButtons from '../SectionWithButtons';
import Location from '../Locations';
import Estimate from '../Estimate';
import Description from '../Description';
import Business from '../Business';
import DetailsMission from '../DetailsMission';
import PaymentMethod from '../PaymentMethod';

const LocationSection = SectionWithButtons(Location);
const DescriptionSection = SectionWithButtons(Description);
const EstimateSection = SectionWithButtons(Estimate);
const BusinessSection = SectionWithButtons(Business);
const DetailsMissionSection = SectionWithButtons(DetailsMission);
const PaymentMethodSection = SectionWithButtons(PaymentMethod);

export {
  LocationSection,
  DescriptionSection,
  BusinessSection,
  EstimateSection,
  DetailsMissionSection,
  PaymentMethodSection
}