import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Card, Row, Col, Checkbox, Divider } from 'antd';
import {
  BusinessActivitiesProps, BusinessActivitiesState
} from './Upsert'
import BusinessList from './BusinessList'

const { Meta } = Card;
import lang from '../../../../support/Lang'

class BussinesActivities extends PureComponent<BusinessActivitiesProps, BusinessActivitiesState> {
  constructor(props: BusinessActivitiesProps) {
    super(props);
    Autobind(this);
    this.state = {
      checkedList: []
    }
  }

  private onChangeCheckbox(checkedValues: []) {
    const { data } = this.props;
    const { onChange } = this.props;
    const checkboxList:[] = [];

    checkedValues.map(item => {
      const obj = data.type_activities.find((element) => Object.keys(element)[0] === item);

      if (obj) {
        checkboxList.push(obj)
      }
    })

    this.setState({ checkedList: checkboxList })

    onChange(checkedValues)
  }

  public render() {
    const { data, typeCustom } = this.props
    const { checkedList } = this.state;

    return (
      <Card
        key={data.uuid}
        style={{ width: '100%', borderRadius: 10, marginBottom: 16 }}>
        <Row>
          <Col span={12}>
            <Meta
              title={lang.get('buses.mission.add.description.input_descrp.label')}
              description={data.description}
            />
            <Divider />
            {
              typeCustom &&
              <BusinessList
                dataSource={checkedList}
                custom={typeCustom}
                type="checkbox"
                checkedValues={checkedList}
              />
            }
          </Col>
          <Col span={11} offset={1}>
            <Checkbox.Group style={{ width: '100%' }} onChange={this.onChangeCheckbox}>
              <BusinessList
                dataSource={data.type_activities}
                custom={typeCustom}
                checkedValues={checkedList}
              />
            </Checkbox.Group>
          </Col>
        </Row>
      </Card>
    )
  }
}

export default BussinesActivities;