import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { BusinessItemProps } from './Upsert'
import { List, Checkbox, Typography, Icon } from 'antd';

class BussinesList extends PureComponent<BusinessItemProps, any> {
  constructor(props: BusinessItemProps) {
    super(props);
    Autobind(this);
  }

  private isDisabled(data: string) {
    const { checkedValues } = this.props

    return (
      checkedValues &&
      checkedValues.length > 4 && checkedValues.indexOf(data) === -1
    );
  };

  public render() {
    const { data, key, custom, type } = this.props;
    const values = Object.keys(data).map(index => data[index]);
    const id = Object.keys(data)

    const text = custom ? values[0] : data.toString();
    const description = text.split(':');

    return (
      <List.Item key={key}>
        {
          custom ?
            type === 'checkbox' ?
              <div>
                <Icon type="check-square" theme="twoTone" twoToneColor="#52c41a" />
                <Typography.Text strong={true}> {description[0] || ''}:</Typography.Text>
              </div>
              :
              <Checkbox value={id[0] || ''}
                disabled={this.isDisabled(data)}
              >
                <Typography.Text strong={true}>{description[0] || ''}:</Typography.Text>
              </Checkbox>
            :
            <div>
              <Icon type="file-text" style={{marginRight: 10, color: '#52bebb' }}/>
              <Typography.Text strong={true}>{description[0] || ''}:</Typography.Text>
            </div>
        } {description[1] || ''}
      </List.Item>
    )
  }
}

export default BussinesList;