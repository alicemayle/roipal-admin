import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Card, Icon, Typography } from 'antd';

class CardDetailsMission extends PureComponent<any, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {}
  }

  public render() {
    const { text = '', icon, title } = this.props;

    return (
      <Card
        title={
          <div>
            <Icon type={icon} style={{ color: '#52bebb', marginRight: 10 }} />
            <Typography.Text>
              {title}
            </Typography.Text>
          </div>
        }
        bordered={false}
      >
        {text}
        {this.props.children}
      </Card>
    );
  }
}

export default CardDetailsMission;