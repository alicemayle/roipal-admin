import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { BusinessListProps } from './Upsert'
import BusinessItem from './BusinessItem';
import { List } from 'antd';

class BussinesList extends PureComponent<BusinessListProps, any> {
  constructor(props: BusinessListProps) {
    super(props);
    Autobind(this);
  }

  private renderListActivities(data: string, key: any) {
    const { custom, type, checkedValues } = this.props;

    return (
      <BusinessItem
        data={data}
        custom={custom}
        key={key}
        type={type}
        checkedValues={checkedValues}
      />
    )
  }

  public render() {
    const { dataSource } = this.props
    return (
      <List
        dataSource={dataSource}
        renderItem={this.renderListActivities}
      />
    )
  }
}

export default BussinesList;