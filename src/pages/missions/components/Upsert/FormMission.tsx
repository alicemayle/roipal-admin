import React, { PureComponent } from 'react';
import FormProps from '../../models/Upsert/FormProps'
import Autobind from 'class-autobind';
import { Form } from 'antd';
import Step from '../../components/Upsert/Step'
class FormMission extends PureComponent<FormProps, any> {
  constructor(props: FormProps) {
    super(props);
    Autobind(this);
    this.state = {}
  }

  public render() {
    const { companies, commercial, history } = this.props;

    return (
      <Form layout="vertical">
        <Step
          commercial={commercial}
          companies={companies}
          history={history}
        />
      </Form>
    );
  }
}

export default FormMission;