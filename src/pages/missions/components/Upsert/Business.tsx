import React, { PureComponent } from 'react';
import { connect } from 'unstated-enhancers';
import Autobind from 'class-autobind';
import ListActivitiesContainer from '../../state/ListActivitiesContainer'
import CustomActivitiesContainer from '../../state/CustomActivitiesContainer'
import BusinessActivities from './BusinessActivities'
import { OptionsProps, DataActivitiesProps } from './Upsert'
import { Form, Checkbox, Select } from 'antd';
import lang from '../../../../support/Lang'
import ActionButtons from './ActionButtons';
const { Option } = Select;

class Business extends PureComponent<any, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
      custom: false,
    }
  }

  private async setCommercial(UUID: string, option: any) {
    const { activities } = this.props.containers;
    const params = { UUID }

    const custom = option.props.children === 'Custom' ? true : false;

    await this.setState({
      custom,
      type_sale: {
        ...this.state.type_sale,
        commercial_need_uuid: UUID,
      },
      commercialNeed: option.props.children
    })

    this.handleChangeData()

    activities.fetch(params)
  }

  private async setType(UUID: string, option: any) {
    await this.setState({
      selected: UUID,
      type_sale: {
        ...this.state.type_sale,
        type_sale_uuid: UUID,
      },
      type: option.props.children
    })

    this.handleChangeData()
  }

  private async setActivities(data: DataActivitiesProps) {
    await this.setState({
      type_sale: {
        ...this.state.type_sale,
        type_sale_activities: data
      }
    })

    this.handleChangeData()
  }

  private renderActivities(option: OptionsProps) {
    const { data } = this.props.containers.customActivities.state;
    const { selected, custom } = this.state;

    const dataSource = custom ? data : option;

    return (
      selected === option.uuid &&
      <BusinessActivities
        key={dataSource.uuid}
        data={dataSource}
        typeCustom={custom}
        selected={selected}
        onChange={this.setActivities}
      />
    );
  }

  private async handleCustomTypeSales() {
    const { customActivities } = this.props.containers;
    const { type_sale } = this.state;

    const isCustom = !this.state.custom;

    await customActivities.fetch();

    const { data } = customActivities.state;

    let typeSale;

    if (isCustom) {
      typeSale = {
        ...type_sale,
        custom_uuid: data.uuid,
      }
    } else {

      typeSale = {
        ...type_sale,
      }
      delete typeSale.custom_uuid
      delete typeSale.type_sale_activities
    }

    await this.setState({
      custom: isCustom,
      type_sale:
      {
        ...typeSale,
      }
    })

    this.handleChangeData()
  }


  private handleChangeData() {
    const { onHandleChangeData } = this.props;

    if (onHandleChangeData) {
      onHandleChangeData(this.state)
    }
  }

  private renderOptions(data: any) {
    const value = data.commercial_need ||
      data.type;

    return (
      <Option key={data.key} value={data.uuid}>
        {value}
      </Option>
    );
  }

  private check(typeButton: string) {
    if (typeButton === 'next') {
      this.props.form.validateFields(async (err: any, values: any) => {
        if (!err) {
          this.props.onHandleCompleteData(this.props.field, this.state)
          this.props.onHandleClick(typeButton)
        }
      })
    } else {
      this.props.onHandleClick(typeButton)
    }
  };

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { commercial, activities } = this.props;
    const { data } = activities || '';

    const newPropsActionButtons = {
      field: this.props.field,
      current: this.props.current,
      pages: this.props.pages,
      onHandleClick: this.check
    }

    return (
      <div>
        <Form.Item label={lang.get('buses.mission.add.activities.select_commercial.label')}>
          {getFieldDecorator('commercial_need_uuid', {
            rules: [
              {
                required: true,
                message: lang.get('buses.mission.add.activities.select_commercial.message'),
              },
            ],
          })(
            <Select
              showSearch={true}
              placeholder={lang.get('buses.mission.add.activities.select_commercial.label')}
              optionFilterProp="children"
              onChange={this.setCommercial}
            >
              {
                commercial.comercialNeeds &&
                commercial.comercialNeeds.map(this.renderOptions)
              }
            </Select>
          )}
        </Form.Item>

        <Form.Item label={lang.get('buses.mission.add.activities.select_type.label')}>
          {getFieldDecorator('type', {
            rules: [
              {
                required: true,
                message: lang.get('buses.mission.add.activities.select_type.message'),
              },
            ],
          })(
            <Select
              showSearch={true}
              placeholder={lang.get('buses.mission.add.activities.select_type.label')}
              optionFilterProp="children"
              onChange={this.setType}
            >
              {
                data &&
                data.map(this.renderOptions)
              }
            </Select>
          )}
        </Form.Item>

        <Checkbox
          checked={this.state.custom}
          onChange={this.handleCustomTypeSales}
          style={{marginBottom: 16}}
        >
          {lang.get('buses.mission.add.activities.custom')}
        </Checkbox>
        {
          data &&
          data.map(this.renderActivities)
        }

        <ActionButtons {...newPropsActionButtons} />
      </div>
    );
  }
}

const container = {
  activities: ListActivitiesContainer,
  customActivities: CustomActivitiesContainer
}

const mapStateToProps = (containers: any) => {
  return {
    activities: containers.activities.state,
    listActivities: containers.activities.state,
    customActivities: containers.customActivities.state
  }
}
const FormBusiness: any = Form.create({ name: 'validate_business' })(Business);

export default connect(container, mapStateToProps)(FormBusiness);