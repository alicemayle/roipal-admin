import React, { PureComponent } from 'react';
import StepButtonProps from '../../models/Upsert/StepButtonProps'
import Autobind from 'class-autobind';
import { Button, message } from 'antd';

class StepButton extends PureComponent<StepButtonProps, any> {
  constructor(props: StepButtonProps) {
    super(props);
    Autobind(this);
  }

  private next() {
    this.props.onChange(1)
  }

  private prev() {
    this.props.onChange(-1)
  }

  public render() {
    const { label, type } = this.props;
    return (
      <Button type="primary"
       onClick={
        type === 0 ?
          () => this.next() :
          type === 1 ?
          () => this.prev() :
          () => message.success('Processing complete!')}>
        {label}
      </Button>
    );
  }
}

export default StepButton;