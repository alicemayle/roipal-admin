import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { AddressSelected } from './Upsert';
import GoogleMap from 'google-map-react';
import { Col, Row, Card } from 'antd';
import Marker from 'src/components/map/Marker';

interface MapProps {
  addressSelected?: AddressSelected,
  defaultCenter?: [number, number],
  onChangeAddress: (data: AddressSelected) => void
}

/* interface StatePropsI {} */

class Map extends PureComponent<MapProps, any> {
  constructor(props: MapProps) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }



  public render() {
    const { addressSelected, defaultCenter } = this.props;       
    const center = !!addressSelected 
    ? [addressSelected.latLng.lat, addressSelected.latLng.lng] 
    : defaultCenter;

    return (
      <Card>
        <Row>
          <Col span={24}>
            <div style={{ height: '22vh' }}>
              <GoogleMap
                bootstrapURLKeys={{ key: 'AIzaSyCzTCLGCeoIPtGng_7IhV4aZRgL-dYyPaw' }}
                zoom={14}
                // onGoogleApiLoaded={this.renderMarker}
                center={center}>
                {addressSelected && <Marker
                  lat={addressSelected.latLng.lat}
                  lng={addressSelected.latLng.lng}
                  marker={{name:'title', address:'address'}}
                />}
                </GoogleMap>
            </div>
          </Col>
        </Row>
      </Card>
    )
  }
}

export default Map