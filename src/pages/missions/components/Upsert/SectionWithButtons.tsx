import React from 'react';
import ActionButtons from './ActionButtons';

let data: any = null

const SectionWithButtons = (component: any) => (props: any) => {
  const Decorated = component;

  const onChangeSectionRequest = props.onHandleClick;
  const onChangeValue = props.onHandleChangeData;

  const newPropsActionButtons = {
    ...props,
    onHandleClick: (typeButton: string) =>{
      props.onHandleCompleteData(props.field, data)

      onChangeSectionRequest(typeButton)
    }
  }

  const newPropsDecorated = {
    ...props,
    onHandleChangeData: (value: any) => {
        data = value;

        onChangeValue();
    }
  }

  return (
    <div>
      <Decorated {...newPropsDecorated} />
      <ActionButtons {...newPropsActionButtons} />
    </div>
  );
}

export default SectionWithButtons;