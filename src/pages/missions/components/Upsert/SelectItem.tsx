import React, { PureComponent } from 'react';
import SearchProps from '../../models/Upsert/SearchProps'
import Autobind from 'class-autobind';
import { Form, Select } from 'antd';

const { Option } = Select;

class SelectItem extends PureComponent<SearchProps, any> {
  constructor(props: SearchProps) {
    super(props);
    Autobind(this);

  }

  private onChange(value: string, option: any) {
    this.props.onSearch(value, option.props.children)
  }


  private renderOptions(data: any) {
    const value = data.business_name ||
                            data.commercial_need ||
                            data.type;

    return (
      <Option key={data.key} value={data.uuid}>
        {value}
      </Option>
    );
  }

  public render() {
    const { data, label } = this.props;
    return (
      <Form.Item label={label}>
        <Select
          showSearch={true}
          placeholder="Select a company"
          optionFilterProp="children"
          onChange={this.onChange}
          defaultValue={''}
        >
          {
            data &&
            data.map(this.renderOptions)
          }
        </Select>
      </Form.Item>
    );
  }
}

export default SelectItem;