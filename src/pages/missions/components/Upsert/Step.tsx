import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Steps, message } from 'antd';
import { connect } from 'unstated-enhancers';
import MissionContainer from '../../containers/MissionContainer'
import Description from './Description';
import Activities from './Business';
import Estimate from './Estimate';
import Location from './Locations';
import DetailsMission from './DetailsMission';
import PaymentMethod from './PaymentMethod';
import lang from '../../../../support/Lang'
import { Gate } from '@xaamin/guardian';
import '../../../../styles/steps.scss'

const { Step } = Steps;

class StepMission extends PureComponent<any, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
      current: 0,
      data: {
      }
    };
  }

  private setCompany(UUID: string) {
    this.setState({
      company: UUID
    })
  }

  private handleClick(typeButton: string) {
    const { current } = this.state

    const index = typeButton === 'next'
      ? current + 1
      : typeButton === 'previous'
        ? current - 1
        : current

    this.setState({
      current: index
    })

    if (typeButton === 'done') {
      setTimeout(() => {
        this.createMission()
      }, 1000)
    }
  }

  private handleChangeData(data: any) {
    return data
  }

  private handleCompleteData(field: string, data?: any) {
    const newState = field === 'location'
      ? {
        ...this.state,
        data: {
          ...this.state.data,
          locations: data.selectLocationList,
          subtotal: data.subtotal,
          total: data.total,
          tax: data.tax,
          price: data.price,
          commissions: data.commissions
        },
      }
      : {
        ...this.state,
        data: {
          company: this.state.company,
          ...this.state.data,
          ...data,
        },
      }

    this.setState({
      ...newState
    })
  }

  private async createMission() {
    if (Gate.allows('mission::create')) {
      const { mission } = this.props.containers;
      const {company, data } = this.state;

      delete data.countries
      delete data.currencies
      delete data.custom
      delete data.defaultCenter
      delete data.locationsList
      delete data.selectLocationList
      delete data.selected
      delete data.showPrincialComponent
      delete data.type
      delete data.commercialNeed
      delete data.profile_name
      delete data.value
      delete data.showCard
      delete data.showCredits
      delete data.iva

      const country = data.country

      data.country = country.value

      await mission.createMission(company, data)

      const { error } = mission.state;

      if (error) {
        message.error(error.message);
      } else {
        message.success(lang.get('buses.mission.add.success'));
        this.props.history.push({
          pathname: '/missions',
        })
      }
    } else {
      message.error(lang.get('generic.policies.denied'));
    }
  }

  public render() {
    const { current, data } = this.state;
    const { commercial, companies } = this.props;
    const steps = [
      {
        title: lang.get('buses.mission.add.description.input_descrp.label'),
        id: 0,
      },
      {
        title: lang.get('buses.mission.add.activities.title'),
        id: 1,
      },
      {
        title: lang.get('buses.mission.add.estimate.title'),
        id: 2,
      },
      {
        title: lang.get('buses.mission.add.locations.title'),
        id: 3,
      },
      {
        title: lang.get('buses.mission.add.details.title'),
        id: 4,
      },
      {
        title: lang.get('buses.mission.add.payment.title'),
        id: 5,
      },
    ]

    return (
      <div>
        <Steps progressDot={true} current={current}>
          {steps.map(item => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>

        <div className="steps-content" style={{ padding: '6%' }}>
          {
            current === 0 &&
            <Description
              current={this.state.current}
              pages={steps.length}
              field={'description'}
              selectedValues={data}
              onHandleClick={this.handleClick}
              onHandleChangeData={this.handleChangeData}
              onHandleCompleteData={this.handleCompleteData}
              companies={companies}
              onHandleChangeCompany={this.setCompany}
            />
          }
          {
            current === 1 &&
            <Activities
              current={this.state.current}
              pages={steps.length}
              field={'activities'}
              commercial={commercial}
              selectedValues={data}
              onHandleClick={this.handleClick}
              onHandleChangeData={this.handleChangeData}
              onHandleCompleteData={this.handleCompleteData}
            />
          }
          {
            current === 2 &&
            <Estimate
              current={this.state.current}
              pages={steps.length}
              field={'estimate'}
              commercial={commercial}
              selectedValues={data}
              onHandleClick={this.handleClick}
              onHandleChangeData={this.handleChangeData}
              onHandleCompleteData={this.handleCompleteData}
            />

          }
          {
            current === 3 &&
            <Location
              current={this.state.current}
              pages={steps.length}
              field={'location'}
              selectedValues={data}
              company={this.state.company}
              onHandleClick={this.handleClick}
              onHandleChangeData={this.handleChangeData}
              onHandleCompleteData={this.handleCompleteData}
            />
          }
          {
            current === 4 &&
            <DetailsMission
              current={this.state.current}
              pages={steps.length}
              field={'detailsMisssion'}
              selectedValues={data}
              onHandleClick={this.handleClick}
              onHandleChangeData={this.handleChangeData}
              onHandleCompleteData={this.handleCompleteData}
            />
          }
          {
            current === 5 &&
            <PaymentMethod
              current={this.state.current}
              pages={steps.length}
              field={'paymentMethod'}
              selectedValues={data}
              company={this.state.company}
              onHandleClick={this.handleClick}
              onHandleChangeData={this.handleChangeData}
              onHandleCompleteData={this.handleCompleteData}
            />
          }
        </div>
      </div>

    );
  }
}
const container = {
  mission: MissionContainer
}

const mapStateToProps = (containers: any) => {
  return {
    mission: containers.mission.state
  }
}

export default connect(container, mapStateToProps)(StepMission);