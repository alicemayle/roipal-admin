import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { Radio, Row, Col, Card, Icon } from 'antd';
import { connect, Logger } from 'unstated-enhancers';
import DataPaymentContainer from '../../state/DataPaymentContainer';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import ActionButtons from './ActionButtons';
import Lang from '../../../../support/Lang';
import NumberFormat from 'src/support/NumberFormat';
import cash from '../../../../images/coin.svg'
import debitCard from '../../../../images/debit-card.svg'

class PaymentMethod extends Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      payment_method: 'card',
      showCard: false,
      showCredits: false,
      type_payment: 1,
    }

    Autobind(this);
  }

  private onChange(e: any) {
    this.setState({
      payment_method: e.target.value,
      showCard: e.target.value === 'card' ? true : false,
      showCredits: e.target.value === 'credit' ? true : false,
    }, () => this.selectCredits());
  }

  private renderItem(item: any) {
    const image = item.value === 'card' ? debitCard : cash
    return (
      <div key={item.value}>
        <Card size="small" style={{ width: '60%' }} bordered={false}
          cover={<img
            src={image}
          />}
        >
          <Radio
            value={item.value}
            disabled={item.disabled}>

            {item.label}
          </Radio>
        </Card>
        <br />
        <br />
      </div>
    )
  }

  private selectCredits() {
    const payment = this.props.containers.payment.state.data;
    const { total, iva } = this.props.selectedValues;
    const { showCredits } = this.state;
    let dataPayment = {}

    if (showCredits) {
      if (payment.available_credit >= total) {
        dataPayment = {
          type_payment: 3,
          payment_by_credit: total
        }
      } else {
        const amount = total - payment.available_credit
        const _subtotal = parseFloat((amount / (1 + iva)).toFixed(2))
        const _tax = parseFloat((_subtotal * iva).toFixed(2))
        const _total = parseFloat((_subtotal + _tax).toFixed(2))

        dataPayment = {
          type_payment: 2,
          payment_mixed: {
            subtotal: _subtotal,
            tax: _tax,
            total: _total,
            credit_used: payment.available_credit
          }
        }
      }
    } else {
      dataPayment = {
        type_payment: 1,
      }
    }

    this.setState({
      ...this.state,
      ...dataPayment
    }, () => Logger.dispatch('state change payment', this.state))
  }

  private async getDataPayment() {
    const payment = this.props.containers.payment;

    const company = this.props.company;

    await payment.fetch(company);

    const error = payment.state.error;

    if (error) {
      Logger.dispatch('Error al pedir la informacion de la tarjeta')
    }
  }

  private async getToken() {
    const payment = this.props.containers.payment;

    await payment.getToken();

    const error = payment.state.error;

    if (!error) {
      this.setState({
        showCard: true,
        payment_token: this.props.containers.payment.state.token_payment.token
      })
    }
  }

  private check(typeButton: string) {
    const data = { ... this.state }
    const { type_payment } = this.state

    if (type_payment === 1) {
      delete data.payment_mixed
      delete data.payment_by_credit
    }

    Logger.dispatch('data payment', data)

    this.props.onHandleCompleteData(this.props.field, data)
    this.props.onHandleClick(typeButton)
  }

  public async componentDidMount() {
    await this.getDataPayment()
    await this.getToken()
  }

  public render() {
    const { payment_method, showCard, showCredits, type_payment, payment_mixed } = this.state;
    const payment = this.props.containers.payment.state.data;
    const { currency, total } = this.props.selectedValues;
    let creditsFormat = null;
    let cardNumber = '';
    let amountFormat = null
    const totalFormat = NumberFormat.formatByCountry(total)

    if (payment) {
      creditsFormat = NumberFormat.formatByCountry(payment.available_credit)
      cardNumber = '············'.concat(payment.card_number)
    }

    if (payment_mixed) {
      amountFormat = NumberFormat.formatByCountry(payment_mixed.total)
    }

    const newPropsActionButtons = {
      field: this.props.field,
      current: this.props.current,
      pages: this.props.pages,
      onHandleClick: this.check
    }

    const payments = [
      {
        label: Lang.get('payments.payment.card'),
        value: 'card',
        disabled: false
      },
      {
        label: Lang.get('payments.payment.credits'),
        value: 'credit',
        disabled: (payment && payment.available_credit > 0) ? false : true
      }
    ]

    return (
      <div>
        <Row gutter={50}>
          <Col span={11}>
            <Radio.Group onChange={this.onChange} value={payment_method}>
              {
                payments.map(this.renderItem)
              }
            </Radio.Group>
          </Col>
          <Col span={11}>
            <Card
              bodyStyle={{ backgroundColor: '#F3F3F3', textAlign: 'center' }}
              bordered={false}
            >
              <h1>
                {
                  `${Lang.get('buses.mission.add.details.cost')}:
                  ${currency.symbol} ${totalFormat} ${currency.currency}`
                }
              </h1>
              <br />
              {
                payment && (showCard || type_payment < 3) &&
                <Cards
                  number={cardNumber}
                  name={payment.owner}
                  expiry={payment.exp}
                  cvc={payment.ccv}
                  focused={payment.name}
                  preview={true}
                />
              }
              <br />
              {
                showCredits &&
                <div>
                  <div className="list-icon-vertical">
                    <Icon type="dollar" style={{ color: '#52bebb', fontSize: 60, margin: 10 }} />
                    {
                      `${Lang.get('payments.payment.available_credits')}:
                        ${currency.symbol} ${creditsFormat} ${currency.currency}`
                    }
                  </div>
                  <br />
                  {
                    type_payment === 3 &&
                    Lang.get('payments.payment.sufficient_credits')
                  }
                  {
                    type_payment === 2 &&
                    <div>
                      <br />
                      {
                        Lang.get('payments.payment.remaining') + ': '
                        + currency.symbol + ' ' + amountFormat + ' ' + currency.currency
                      }
                    </div>
                  }
                </div>
              }
            </Card>
          </Col>
        </Row>
        <ActionButtons {...newPropsActionButtons} />
      </div>
    )
  }

}

const container = {
  payment: DataPaymentContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    payment: containers.payment.state
  }
}

export default connect(container, mapStateToProps)(PaymentMethod);