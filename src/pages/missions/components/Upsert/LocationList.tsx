import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { List, Checkbox, Divider } from 'antd';
import Lang from 'src/support/Lang';
import InfiniteScroll from 'react-infinite-scroller';
import '../../style.scss'

interface LocationListProps {
  data: [],
  custom: boolean,
  onHandleSelectLocation?: (value: any) => void,
  onHandleChangeNumber?: (item: any, value: number) => void
}

class LocationList extends Component<LocationListProps, any> {
  constructor(props: LocationListProps) {
    super(props);
    Autobind(this);
  }

  public state = {
    loading: false,
    hasMore: true,
    ready: false
  }

  private handleChange(e: any) {
    const { onHandleSelectLocation } = this.props;
    const item = e.target.value;

    if (onHandleSelectLocation) {
      onHandleSelectLocation(item);
    }
  }

  private renderItemList(item: any) {
    const isChecked = item.selected ? true : false;
    const isDisabled = item.disabled ? true : false;

    return (
      <List.Item>
        <Checkbox
          checked={isChecked}
          value={item}
          onChange={this.handleChange}
          disabled={isDisabled}
        >
          {''}
        </Checkbox>
        <List.Item.Meta
          title={item.name}
          description={item.address}
        />
      </List.Item>
    )
  }

  private setData(){
    this.setState({
      ready: true
    })
  }

  public render() {
    const { data } = this.props

    return (
      <div>
      <div>{Lang.get('buses.mission.edit.locations_list')}
      <Divider className="divider" />
      </div>
      <div className="demo-infinite-container">
        <InfiniteScroll
          initialLoad={false}
          pageStart={0}
          loadMore={this.setData}
          hasMore={!this.state.loading && this.state.hasMore}
          useWindow={false}
        >
          <List
            itemLayout="horizontal"
            dataSource={data}
            renderItem={this.renderItemList}
          />
        </InfiniteScroll>
      </div>
      </div>
    )
  }
}

export default LocationList;