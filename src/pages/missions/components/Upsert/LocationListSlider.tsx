import React, { Component } from 'react';
import Autobind from 'class-autobind';
import { List, Divider } from 'antd';
import InputNumberItem from './InputNumberItem';
import LocationItemSlider from './LocationItemSlider';
import Lang from 'src/support/Lang';
import InfiniteScroll from 'react-infinite-scroller';
import '../../style.scss'

interface LocationListProps {
  data: any,
  total?: any,
  onHandleSelectLocation?: (value: any) => void,
  onHandleChangeNumber?: (item: any, value: number) => void
  onhandleChangeNumberSlider?: (item: any, value: any) => void,
  onCheck?: boolean,
  onHadleValidate?: (validate: boolean) => void,
  edit?: boolean
}

class LocationListSlider extends Component<LocationListProps, any> {
  constructor(props: LocationListProps) {
    super(props);
    Autobind(this);
  }

  public state = {
    loading: false,
    hasMore: true,
    ready: false
  }

  private handleChange(item: any, value: number) {
    const { onhandleChangeNumberSlider } = this.props;

    if (onhandleChangeNumberSlider) {
      onhandleChangeNumberSlider(item, value);
    }
  }

  private renderItemList(item: any) {
    const { onHandleChangeNumber, onCheck, onHadleValidate, edit = false } = this.props;

    return (
      <List.Item
        key={item.uuid}
        extra={
          <div>
            <InputNumberItem
              item={item}
              onHandleChangeNumber={onHandleChangeNumber}
              check={onCheck}
              onHadleValidate={onHadleValidate}
              edit={edit}
            />
          </div>
        }
      >
        <List.Item.Meta
          title={item.name}
          description={item.address}
        />
        {
          !edit &&
          <div>
            <LocationItemSlider item={item} onhandleChangeNumberSlider={this.handleChange} />
            <br />
            <br />
          </div>
        }
        {
          item.location_total > 0 &&
          <p>
            {Lang.get('buses.mission.edit.price_locations')}
            {'$ '}{edit ? item.location_subtotal : item.location_total}{' MXN'}
          </p>
        }
      </List.Item>
    )
  }

  private setData() {
    this.setState({
      ready: true
    })
  }

  public render() {
    const { data } = this.props

    return (
      <div>
        <div>{Lang.get('buses.mission.edit.location_selected')}
          <Divider className="divider" />
        </div>
        <div className="demo-infinite-container">
          <InfiniteScroll
            initialLoad={false}
            pageStart={0}
            loadMore={this.setData}
            hasMore={!this.state.loading && this.state.hasMore}
            useWindow={false}
          >
            <List
              itemLayout="vertical"
              dataSource={data}
              renderItem={this.renderItemList}
            />
          </InfiniteScroll>
        </div>
      </div>
    )
  }
}

export default LocationListSlider;