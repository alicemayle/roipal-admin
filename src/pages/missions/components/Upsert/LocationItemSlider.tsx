import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Slider } from 'antd';
import '../../../../styles/slider.scss'

interface LocationItemSliderProps {
  item: any,
  onhandleChangeNumberSlider?: (item:any, value:any) => void
}

class LocationItemSlider extends PureComponent<LocationItemSliderProps, any> {
  constructor(props: LocationItemSliderProps) {
    super(props);
    Autobind(this);
  }

  private handleChange(value: number) {
    const { onhandleChangeNumberSlider, item } = this.props;

    if (onhandleChangeNumberSlider) {
      onhandleChangeNumberSlider(item, value);
    }
  }

  public render() {
    const { item } = this.props;
    const convert = (item.executive_cost_min*3).toFixed(2)
    const ranges = (item.executive_cost_min * 0.25).toFixed(2)

    const marks = {
      [item.executive_cost_min]: {
        label: <p>$ {item.executive_cost_min}</p>,
      },
      [parseFloat(convert)]: {
        label: <p>$ {parseFloat(convert)}</p>,
      },
    }

    return (
      <Slider
      min={item.executive_cost_min}
      max={parseFloat(convert)}
      step={parseFloat(ranges)}
      marks={marks}
      tooltipVisible={true}
      value={item.executive_cost}
      onChange={this.handleChange}
       />
    )
  }
}

export default LocationItemSlider;