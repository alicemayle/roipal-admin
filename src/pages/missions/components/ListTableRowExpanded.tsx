import React, { PureComponent } from 'react';
import ListTableRowExpandedProps from '../models/ListTableRowExpandedProps';
import { Row, Col, Card, Button, Icon } from 'antd';
import ListIcons from '../../../components/ListIcons';
import Lang from 'src/support/Lang';
import TableCharges from './ListTableCharges';
import ModalEditLocations from './EditLocations/ModalEditLocations'
import autobind from 'class-autobind';
import { Gate } from '@xaamin/guardian';
import moment from 'moment';
import Text from 'antd/lib/typography/Text';
import ReadMore from 'src/components/readMore/ReadMore';

class ListTableRowExpanded extends PureComponent<ListTableRowExpandedProps, any> {
  constructor(props: ListTableRowExpandedProps) {
    super(props);
    autobind(this)
    this.state = {
      openEditLocations: false
    }
  }

  private renderLocations(location: any, index: number) {
    return (
      <div key={index}>
        <Row>
          <Col span={1}>
            <Icon type="environment"
              style={{ color: '#cc0000', fontSize: 17, marginTop: 3 }} />
          </Col>
          <Col span={20}>
            <Text><b>{location.name}</b></Text>
            <br />
            <Text>{location.address}</Text>
          </Col>
        </Row>
        <br />
      </div>
    );
  }

  private renderStatus(status: number) {
    let icon = '';
    let label = '';
    let colorStatus = '';

    if (status === 0) {
      icon = 'pause-circle';
      colorStatus = '#ffbb2f'
      label = Lang.get('buses.mission.status_created');
    } else if (status === 1) {
      icon = 'clock-circle';
      colorStatus = '#66CDAA'
      label = Lang.get('buses.mission.status_started');
    } else if (status === 10) {
      icon = 'check-circle';
      colorStatus = '#87CEEB'
      label = Lang.get('buses.mission.status_completed')
    } else if (status === -1) {
      icon = 'close-circle';
      colorStatus = '#CD5C5C'
      label = Lang.get('buses.mission.status_canceled');
    }
    return (
      <ListIcons
        type="vertical"
        colorIcon={colorStatus}
        label={label}
        icon={icon} />
    )
  }

  private openEditLocations() {
    const { openEditLocations } = this.state;

    this.setState({
      openEditLocations: !openEditLocations
    })
  }

  public render() {
    const { data = {}, onClose } = this.props;
    const requested = Lang.get('buses.mission.requested')
    const acepted = Lang.get('buses.mission.accepted')

    return (
      <div>
        <Row>
          <Col>
            <Card title={Lang.get('buses.mission.description')}
              type="inner" bordered={false}>
              <ReadMore description={data.description}  />
              <p>{Lang.get('buses.mission.commercial_need')}: {data.commercial_name}</p>
              <p>{Lang.get('buses.mission.type_sale')}: {data.type}</p>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card title={Lang.get('buses.mission.mision')}
              type="inner" bordered={false} >
              <Row justify="center" type="flex" style={{ alignItems: 'center' }} >
                <Col span={8}>
                  {this.renderStatus(data.status)}
                </Col>
                <Col span={8}>
                  <ListIcons
                    type="vertical"
                    colorIcon={'orange'}
                    label={`${data.time} ${data.time_unit}`}
                    icon="hourglass" />
                </Col>
                <Col span={8}>
                  <ListIcons
                    type="vertical"
                    colorIcon="green"
                    label=
                    {`Total: ${data.currency.symbol} ${data.total} ${data.currency.currency}`}
                    icon="dollar" />
                </Col>
              </Row>
              <br />
              <Row type="flex" style={{ alignItems: 'center' }} justify="center" >
                <Col span={8}>
                  <ListIcons
                    type="vertical"
                    colorIcon={'#016eb8'}
                    label={`${'Inicio'}: ${moment(new Date(data.started_at)).format('DD-MM-YYYY')}`}
                    icon="calendar" />
                </Col>
                <Col span={8}>
                  <ListIcons
                    type="vertical"
                    colorIcon={'#002699'}
                    label={`${requested}: ${data.executives_requested}`}
                    icon="team" />
                </Col>
                <Col span={8}>
                  <ListIcons
                    type="vertical"
                    colorIcon={'#3babd4'}
                    label={`${acepted}: ${data.accepted}`}
                    icon="usergroup-add" />
                </Col>
              </Row>
              <br />
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card title={Lang.get('buses.mission.location')}
              type="inner" bordered={false}
              extra={
                Gate.allows('mission::edit')
                && (data.status === 0 || data.status === 1)
                && data.status_payment === 1 &&
                <Button type="primary"
                  icon="edit" onClick={this.openEditLocations}>
                  {Lang.get('generic.button.edit')}
                </Button>
              }>
              {data && data.locations &&
                data.locations.map(this.renderLocations)
              }
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            {
              data && data.charges.length > 0 &&
              <Card title={Lang.get('buses.mission.charges.title')} bordered={false} >
                <TableCharges charges={data.charges} />
              </Card>
            }
          </Col>
        </Row>
        <ModalEditLocations
          visible={this.state.openEditLocations}
          onClose={this.openEditLocations}
          data={data.locations}
          mission={data}
          onCloseDrawer={onClose}
          currency={data.currency}
        />
      </div>
    );
  }
}

export default ListTableRowExpanded;