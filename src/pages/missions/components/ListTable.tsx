import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import ListTableProps from '../models/ListTableProps';
import ListTableState from '../models/ListTableState';
import FilterDropdDownInterface from '../../../components/search/FilterDropDownInterface';
import FilterSearch from '../../../components/search/FilterSearch';
import autobind from 'class-autobind';
import ListTableDrawer from './ListTableDrawer';
import Lang from 'src/support/Lang';
import { status } from '../../../support/MissionConstants'
import IconButton from 'src/components/buttons/IconButton';
import ListExecutives from './DetailsExecutive/ListExecutives'
import { Gate } from '@xaamin/guardian';
import ActionButtons from './ChangeStatus/ActionButtons';
import moment from 'moment';

const { Column } = Table;

class ListTable extends Component<ListTableProps, ListTableState> {
  public searchInput: any = null;
  public searchField: string = '';
  constructor(props: ListTableProps) {
    super(props);
    autobind(this)
    this.state = {
      searchText: '',
      fields: [],
      visibleExecutives: false,
      listExecutives: []
    }
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterDropdDownInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  })

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private renderDetails(text: any) {
    const { onShow } = this.props;

    return (
      <IconButton
        item={text}
        onFuction={onShow}
        size={18}
        colorIcon={'#52bebb'}
        name={'info-circle'}
      />
    );
  }

  private renderStatus(text: any) {
    const colorText = text.status === status.MISSION_HOLD ? '#ffbb2f' :
      text.status === status.MISSION_IN_PROGRESS ? '#66CDAA' :
        text.status === status.MISSION_FINISHED ?
          '#87CEEB' : '#CD5C5C';

    const statusIcon = text.status === status.MISSION_HOLD ? 'pause-circle' :
      text.status === status.MISSION_IN_PROGRESS ? 'clock-circle' :
        text.status === status.MISSION_FINISHED ?
          'check-circle' : 'close-circle';

    return (
      <div>
        <Icon type={statusIcon} style={{ color: colorText, fontSize: 18 }} />
      </div>
    );
  }

  private renderAccepted(record: any) {
    return (
      <IconButton
        item={record}
        onFuction={this.showExecutivesDetails}
        size={18}
        colorIcon={'#002699'}
        name={'solution'}
        disable={record.executives.length === 0 ? true : false}
      />
    )
  }

  private showExecutivesDetails(executive: any) {
    if (executive.executives) {
      const newData = executive.executives.map((item: any) => {
        const format = {
          ...item,
          key: item.uuid,
          mission_uuid: executive.key,
          company_uuid: executive.company.uuid,
          created_at: moment(new Date(item.created_at)).format('DD-MM-YYYY')
        }
        return format
      })

      this.setState({
        listExecutives: newData,
        type_sales: executive.type_sales.uuid
      })
    }
    this.showDrawer()
  }

  private showDrawer() {
    this.setState({
      visibleExecutives: !this.state.visibleExecutives
    });
  };

  private renderActions(mission: any) {
    return (
      <ActionButtons mission={mission} />
    );
  }

  public shouldComponentUpdate(
    nextProps: {
      data: {},
      httpBusy: boolean,
      visible: any,
      onSelect: any,
      visibleExecutives: boolean,
      listExecutives: any
    },
    nextState: {
      searchText: string,
      fields: {},
      visibleExecutives: boolean,
      listExecutives: []
    }) {
    return (nextProps.data !== this.props.data
      || nextProps.httpBusy !== this.props.httpBusy
      || nextState.searchText !== this.state.searchText
      || nextState.fields !== this.state.fields
      || nextProps.visible !== this.props.visible
      || nextProps.onSelect !== this.props.onSelect
      || nextState.visibleExecutives !== this.state.visibleExecutives
      || nextState.listExecutives !== this.state.listExecutives)
  }


  public render() {
    const { data = {}, httpBusy, visible, onClose, onSelect } = this.props;
    const { visibleExecutives, listExecutives, type_sales } = this.state;

    return (
      <div>
        <Table
          dataSource={data}
          loading={httpBusy}
          scroll={{ x: 500 }}
          pagination={false}
          className="rwd-table"
        >
          <Column
            title={Lang.get('buses.mission.create_date')}
            dataIndex="created_at"
            key="created_at"
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.name')}
            dataIndex="name"
            key="name"
            {...this.getColumnSearchProps('name')}
          />
          <Column
            title={Lang.get('buses.mission.type_sale')}
            dataIndex="type"
            key="type"
            {...this.getColumnSearchProps('type')}
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.status')}
            key="statusText"
            render={this.renderStatus}
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.requested')}
            dataIndex="executives_requested"
            key="executives_requested"
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.list_executives')}
            render={this.renderAccepted}
            key="accepted"
            align={'center'}
          />
          <Column
            title={Lang.get('buses.button.details')}
            render={this.renderDetails}
            align={'center'}
          />
          {
            Gate.allows('mission::edit') &&
            <Column
              title={Lang.get('buses.button.actions')}
              render={this.renderActions}
              align={'center'}
            />
          }
        </Table>
        <ListTableDrawer visible={visible} onClose={onClose} onSelect={onSelect} />
        {
          listExecutives &&
          <ListExecutives
            visible={visibleExecutives}
            executives={listExecutives}
            onClose={this.showDrawer}
            type={type_sales}
          />
        }
      </div>
    );
  }
}

export default ListTable;

