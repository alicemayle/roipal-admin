import React, { PureComponent } from 'react';
import { Button, Modal } from 'antd';
import { connect } from 'unstated-enhancers';
import ChangeMissionStatusContainer from '../../state/ChangeMissionStatusContainer';
import autobind from 'class-autobind';
import Lang from 'src/support/Lang';

const { confirm } = Modal
class ActionButtons extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state = {
  }

  private openModalConfirm(e: any) {
    const status = Number(e.target.value)

    const { changeStatusMission } = this

    const selectText = status === 1 ? Lang.get('buses.mission.start_question')
      : status === 10 ? Lang.get('buses.mission.finish_question')
      : status === -1 ? Lang.get('buses.mission.cancel_mission') : undefined

      confirm({
        title: selectText,
        okText: Lang.get('generic.button.accept'),
        cancelText: Lang.get('generic.button.cancel'),
        onOk() {
          changeStatusMission(status)
        },
        centered: true,
        maskClosable: true
      })
  }

  private async changeStatusMission(stat: number) {
    const { missionStatus } = this.props.containers;
    const { mission } = this.props;
    const data = {
      status: stat
    }

    await missionStatus.changeStatus(mission.company.uuid, mission.uuid, data)
  }

  public render() {
    const { mission = {} } = this.props;
    const { status, accepted, status_payment, executives = [] } = mission;

    const missionsStarted = executives.filter((exe: any) => exe.status === 1)
    const missionInprocess = missionsStarted.length > 0 ? true : false

    return (
      <div>
        {
          accepted > 0 && status === 0 && status_payment === 1 &&
          <Button
            name={'startMission'}
            value={1}
            size="small"
            block={true}
            style={{ borderColor: '#66CDAA', color: '#66CDAA', marginBottom: 5 }}
            onClick={this.openModalConfirm}
          >
            {Lang.get('buses.button.start')}
          </Button>
        }
        {
          status === 1 && !missionInprocess &&
          <Button
            name={'endMission'}
            value={10}
            size="small"
            block={true}
            style={{ borderColor: '#87CEEB', color: '#87CEEB', marginBottom: 5 }}
            onClick={this.openModalConfirm}
          >
            {Lang.get('buses.button.finish')}
          </Button>
        }
        {
          (status === 0 || status === 1) && !missionInprocess &&
          <Button
            name={'cancelMission'}
            value={-1}
            size="small"
            block={true}
            style={{ borderColor: '#CD5C5C', color: '#CD5C5C' }}
            onClick={this.openModalConfirm}
          >
            {Lang.get('buses.button.cancel')}
          </Button>
        }
      </div>
    );
  }
}

const container = {
  missionStatus: ChangeMissionStatusContainer
}

const mapStateToProps = (containers: any) => {
  return {
    missionStatus: containers.missionStatus.state
  }
}

export default connect(container, mapStateToProps)(ActionButtons);