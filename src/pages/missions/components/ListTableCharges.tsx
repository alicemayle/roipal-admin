import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import ListTableState from '../models/ListTableState';
import { Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';
import Lang from '../../../support/Lang';
import { statusCharge } from '../../../support/StatusChargePayment';

const { Column } = Table;

class ListTableCharges extends Component<any, ListTableState> {
  public searchInput: any = null;
  public searchField: string = '';
  constructor(props: any) {
    super(props);
    autobind(this)
    this.state = {
      searchText: '',
      fields: []
    }
  }

  private renderstatus(item: any) {
    const colorIcon = item === statusCharge.CHARGE_EXPIRATION ? 'gray'
      : item === statusCharge.CHARGE_REJECTED ? '#CD5C5C'
      : item === statusCharge.CHARGE_APPROVED ? '#009933'
      : '#ffbb2f'

    const statusIcon = item === statusCharge.CHARGE_EXPIRATION
      || item === statusCharge.CHARGE_REJECTED ? 'close-circle'
      : item === statusCharge.CHARGE_APPROVED ? 'check-circle'
      : 'clock-circle'

    return (
      <div>
        <Icon type={statusIcon} style={{ color: colorIcon, fontSize: 18 }} />
      </div>
    );
  }

  private renderAmount(item: any) {
    return (
      <p>$ {item.transaction_amount} {item.currency}</p>
    )
  }

  public render() {
    Logger.count(this);
    const { charges = {} } = this.props;

    return (
      <div>
        <Table
          dataSource={charges}
          scroll={{ x: 500 }}
          pagination={false}
        >
          <Column
            title={Lang.get('buses.mission.charges.create_date')}
            dataIndex="created_at"
            key="created_at"
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.charges.executives')}
            dataIndex="executives_requested"
            key="executives_requested"
            align={'center'}
          />
         <Column
            title={Lang.get('buses.mission.charges.status')}
            dataIndex="status"
            key="status"
            render={this.renderstatus}
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.charges.amount')}
            align={'center'}
            render={this.renderAmount}
          />
        </Table>
      </div>
    );
  }
}

export default ListTableCharges;

