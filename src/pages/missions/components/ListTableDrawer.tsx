import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Drawer, Divider } from 'antd';
import ListTableRowExpanded from './ListTableRowExpanded';

const pStyle = {
  fontSize: 18,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16,
};

class ListTableDrawer extends Component<any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public render() {

    const { visible, onClose, onSelect } = this.props

    return (
      <Drawer
        placement="right"
        width={'55%'}
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <p style={{ ...pStyle, marginBottom: 24 }}>{onSelect.name}</p>
        <Divider />
        <ListTableRowExpanded data={onSelect} onClose={onClose}/>
      </Drawer>
    );
  }
}

export default ListTableDrawer;

