import React, { Component } from 'react';
import { Modal, Button, Row, Col, Divider, message } from 'antd';
import autobind from 'class-autobind';
import Lang from '../../../../support/Lang';
import LocationList from '../Upsert/LocationList';
import LocationListSlider from '../Upsert/LocationListSlider';
import { connect } from 'unstated-enhancers';
import EditMissionContainer from '../../state/EditLocationsContainer';
import { Gate } from '@xaamin/guardian';

class ModalEditLocations extends Component<any, any> {
  public form: any = null;
  constructor(props: any) {
    super(props);
    autobind(this);
    this.state = {
      selectLocationList: [],
      locationsList: [],
      location_total: 0,
      subtotal: 0,
      commissions: 0,
      tax: 0,
      check: false,
      validate: false,
    }
  }

  private handleClose() {
    this.props.onClose();
  }

  private async selectLocation(loc: any) {
    const { editLocations } = this.props.containers;
    const { locationsList, selectLocationList } = this.state;
    const { mission } = this.props

    const _selected = selectLocationList
    const _list = locationsList
    const index = locationsList.findIndex((item: any) => item.uuid === loc.uuid)
    const found = _selected.findIndex((item: any) => item.uuid === loc.uuid)

    const _loc = {
      ...loc
    }

    if (!loc.executive_cost) {
      const locs = []
      locs.push(loc)

      const _cost = {
        company_uuid: mission.company.uuid,
        commercial_need_uuid: mission.commercial_need.uuid,
        type_sale_uuid: mission.type_sales.uuid,
        time: mission.time,
        time_unit: mission.time_unit_cod,
        locations: locs
      }

      await editLocations.getCostLocation(_cost)

      const { locationsCost } = editLocations.state;
      let executiveCost = 0
      let executivePaymentCost = 0

      locationsCost.locations.map((element: any) => {
        executiveCost = element.executive_cost
        executivePaymentCost = element.executive_payment_cost
      })

      _loc.selected = loc.selected ? !loc.selected : true,
        _loc.executive_cost = executiveCost,
        _loc.executive_payment_cost = executivePaymentCost
      _loc.totalExecutives = 0
      _loc.location_total = 0
      _loc.location_subtotal = 0
      _loc.executives_new = 0
    }

    if (index > -1) {
      _loc.selected = loc.selected ? !loc.selected : true,
        _list.splice(index, 1, _loc)
    }

    if (found > -1) {
      _selected.splice(found, 1)
    } else {
      _selected.push(_loc)
    }

    this.setState({
      locationsList: _list,
      selectLocationList: _selected
    })
  }

  private async getLocations(miss?: any) {
    const { editLocations } = this.props.containers;
    const { mission } = this.props;
    const _missionUuid = miss ? miss.uuid : mission.uuid;
    const _company = mission.company.uuid

    const params = {
      search: '',
      limit: 50,
      page: 1
    };

    await editLocations.getLocationNoMission(_missionUuid, _company, params);
  }

  private changeExecutivesRequest(item: any, value: number) {
    const { iva, sum_landline, sum_percentage } = this.props.editLocations
    const { selectLocationList, subtotal } = this.state
    const _selected = selectLocationList

    const found = _selected.findIndex((loc: any) => loc.uuid === item.uuid)
    const executivesNew = value - item.totalExecutives
    const locationCost = (item.executive_cost * value).toFixed(2)
    const locationSubCost = (item.executive_cost * executivesNew).toFixed(2)

    const _subtotalNew = subtotal > 0
      ? (subtotal - item.location_subtotal
        + parseFloat(locationSubCost)).toFixed(2)
      : parseFloat(locationSubCost).toFixed(2)

    const _commissions = (sum_landline
      + (parseFloat(_subtotalNew) * sum_percentage / 100))
      .toFixed(2)

    const _tax = ((parseFloat(_subtotalNew)
      + parseFloat(_commissions)) * iva)
      .toFixed(2)

    const _total = (parseFloat(_subtotalNew)
      + parseFloat(_commissions)
      + parseFloat(_tax))
      .toFixed(2)

    if (found > -1) {
      const _loc = {
        ...item,
        executives_requested: value,
        executives_new: executivesNew,
        location_total: parseFloat(locationCost),
        location_subtotal: parseFloat(locationSubCost)
      }
      _selected.splice(found, 1, _loc)
    }

    this.setState({
      selectLocationList: _selected,
      subtotal: Number(_subtotalNew),
      commissions: parseFloat(_subtotalNew) > 0 ? Number(_commissions) : 0,
      tax: parseFloat(_subtotalNew) > 0 ? Number(_tax) : 0,
      location_total: parseFloat(_subtotalNew) > 0 ? Number(_total) : 0
    })
  }

  private async getCommissions() {
    const { editLocations } = this.props.containers;
    const { mission } = this.props;

    const data = {
      type_sale_uuid: mission.type_sales.uuid
    }

    await editLocations.getCommissions(mission.company.uuid, data)

    editLocations.getPaymentToken()
  }

  private async handleSubmit() {
    if (Gate.allows('mission::edit')) {
      await this.setState({
        check: true
      })

      const { locationsList, selectLocationList, tax, location_total, validate } = this.state
      const { editLocations } = this.props.containers
      const { mission } = this.props
      const selected = locationsList.filter((item: any) => item.selected === true)

      if (selected.length > 0 && validate) {
        const data = {
          locations: selected
        }

        await editLocations.addLocationsMission(mission.uuid, mission.company.uuid, data)
      }

      const { error, newLocations, payment_token } = editLocations.state

      if (!error && validate) {
        const filtetLocs = selectLocationList.filter((item: any) =>
          item.executives_requested !== item.totalExecutives)

        const updateLocs = filtetLocs.map((location: any) => {
          let item = {}

          item = {
            executives_requested: location.executives_requested,
            executives_new: location.executives_new,
            location_subtotal: location.location_subtotal,
            location_total: location.location_total
          }

          if (location.totalExecutives === 0 && location.selected === true && newLocations) {
            const found = newLocations.find((ele: any) =>
              ele.latitude === location.latitude && ele.longitude === location.longitude
            )

            item = {
              ...item,
              uuid: found.uuid
            }
          } else {
            item = {
              ...item,
              uuid: location.uuid,
            }
          }
          return item
        })

        const _total = location_total
        const _iva = tax
        const _token = payment_token
        const _data = {
          payment_token: _token,
          location_total: _total,
          subtotal: location_total - tax,
          tax: _iva,
          locations: updateLocs
        }

        await editLocations.editLocations(mission.uuid, _data);

        const err = editLocations.state.error

        if (!err) {
          this.handleClose()
          this.props.onCloseDrawer()
        }
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  private handleValidateNumExecutive(_validate: boolean) {
    this.setState({
      validate: _validate,
      check: false
    })
  }

  private updateInitialLocations(locs?: any) {
    const { editLocations } = this.props.containers;
    const locations = editLocations.state.data;
    const { data } = this.props;

    const _locationsMission = locs ? locs : data

    _locationsMission.map((item: any) => {
      item.disabled = true
      item.totalExecutives = item.executives_requested
      item.location_subtotal = 0
    })

    this.setState({
      locationsList: [..._locationsMission, ...locations],
      selectLocationList: [..._locationsMission],
      location_total: 0,
      subtotal: 0,
      commissions: 0,
      tax: 0,
    })
  }

  public async componentWillMount() {
    await this.getLocations();
    this.updateInitialLocations()
  }

  public async componentWillReceiveProps(nextProps: any) {
    if (this.props.mission !== nextProps.mission) {
      await this.getLocations(nextProps.mission)
      this.updateInitialLocations(nextProps.data)
    }
  }

  public componentDidMount() {
    this.getCommissions()
  }

  public render() {
    const { visible, currency } = this.props;
    const { locationsList,
      selectLocationList,
      subtotal,
      commissions,
      tax,
      location_total,
      check
    } = this.state;

    const { busy } = this.props.editLocations

    return (
      <Modal
        title={Lang.get('buses.mission.edit.title')}
        style={{ top: 20 }}
        visible={visible}
        centered={true}
        onCancel={this.handleClose}
        destroyOnClose={true}
        width={'70%'}
        footer={[
          <Button
            key="cancel"
            onClick={this.handleClose}>
            {Lang.get('buses.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={busy}
            onClick={this.handleSubmit}>
            {Lang.get('buses.button.accept')}
          </Button>
        ]}
      >
        <div>
          <Row gutter={55}>
            <Col span={6}>
              <p>{Lang.get('buses.mission.edit.subtotal')}:</p>
              <p>{currency.symbol} {subtotal} {currency.currency}</p>
            </Col>
            <Col span={6}>
              <p>{Lang.get('buses.mission.edit.commissions')}:</p>
              <p>{currency.symbol} {commissions} {currency.currency}</p>
            </Col>
            <Col span={6}>
              <p>{Lang.get('buses.mission.edit.tax')}:</p>
              <p>{currency.symbol} {tax} {currency.currency}</p>
            </Col>
            <Col span={6}>
              <p style={{ fontWeight: 'bold' }}>{Lang.get('buses.mission.edit.total')}:</p>
              <p style={{ fontWeight: 'bold' }}>
                {currency.symbol} {location_total} {currency.currency}
              </p>
            </Col>
          </Row>

          <Divider />

          <Row gutter={55}>
            <Col span={9}>
              <LocationList
                data={locationsList}
                custom={false}
                onHandleSelectLocation={this.selectLocation}
              />
            </Col>
            <Col span={15}>
              <LocationListSlider
                data={selectLocationList}
                edit={true}
                onCheck={check}
                onHandleChangeNumber={this.changeExecutivesRequest}
                onHadleValidate={this.handleValidateNumExecutive}
              />
            </Col>
          </Row>
        </div>
      </Modal>
    );
  }
}

const container = {
  editLocations: EditMissionContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    editLocations: containers.editLocations.state,
  }
}

export default connect(container, mapStateToProps)(ModalEditLocations);