import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import { Drawer, Divider, Collapse } from 'antd';
import { connect } from 'unstated-enhancers';
import ActivitiesExecutiveContainer from '../../state/ActivitiesExecutiveContainer';
import Notifications from 'src/components/notifications/notifications';
import ListActivitiesHistory from './ListActivitiesHistory';
import Lang from 'src/support/Lang';

const { Panel } = Collapse;

const pStyle = {
  fontSize: 16,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16
};

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden',
};

class ListActivities extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
  }

  private renderActivity(record: any) {
    return (
      <Panel
        header={record.title}
        key={record.key}
        style={customPanelStyle}
      >
        <ListActivitiesHistory data={record.history} />
      </Panel>
    )
  }

  private async getActivitiesHistory(value: any) {
    const { activities } = this.props.containers
    const { executive } = this.props
    const activity = value[value.length - 1]

    const params = {
      limit: 50
    }

    if (executive && value.length > 0) {
      await activities.getActivitiesHistory(
        executive.uuid,
        executive.mission_uuid,
        activity,
        params
      );

      const { error } = this.props.containers.activities.state

      if (error) {
        const notification = new Notifications;
        const msj = {
          type: 'error',
          message: Lang.get('buses.mission.error_activity')
        }
        notification.openNotification(msj)
      }
    }
  }

  public render() {
    const { close, visible, data } = this.props

    return (
      <div>
        <Drawer
          placement="right"
          closable={false}
          destroyOnClose={true}
          width={'55%'}
          onClose={close}
          visible={visible}
        >
          <p style={{ ...pStyle }}>{Lang.get('buses.mission.activities')}</p>
          <Divider />
          <Collapse
            bordered={false}
            onChange={this.getActivitiesHistory}
          >
            {
              data.map(this.renderActivity)
            }
          </Collapse>
        </Drawer>
      </div>
    );
  }
}

const container = {
  activities: ActivitiesExecutiveContainer
}

const mapStateToProps = (containers: any) => {
  return {
    activities: containers.activities.state
  }
}

export default connect(container, mapStateToProps)(ListActivities);