import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';
import ActivitiesExecutiveContainer from '../../state/ActivitiesExecutiveContainer';
import { List, Avatar } from 'antd';
import moment from 'moment';
import logo from '../../../../images/icon.png'
import Lang from 'src/support/Lang';

class ListActivitiesHistory extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    avatar: logo
  }

  private renderItem(item: any) {
    const date = moment(new Date(item.created_at)).format('DD-MM-YYYY')

    const statusColor = item.type === 'comment' ? '#87CEEB' : '#66CDAA'

    const stausType = item.type === 'finish' ? Lang.get('generic.legends.finish')
      : item.type === 'start' ? Lang.get('generic.legends.start')
        : Lang.get('generic.legends.comment')

    return (
      <List.Item key={item.key} style={{ borderRadius: 24 }}>
        <List.Item.Meta
          avatar={<Avatar src={item.images ? item.images : this.state.avatar} />}
          title={item.comment}
          description={item.address}
        />
        <div>
          <p style={{ paddingLeft: 5, color: statusColor }}>{stausType}</p>
          <p>{date}</p>
        </div>
      </List.Item>
    )
  }

  public render() {
    const { data } = this.props

    return (
      <div>
        {
          data &&
          <List
            dataSource={data}
            renderItem={this.renderItem}
          />
        }
      </div>
    );
  }
}

const container = {
  activities: ActivitiesExecutiveContainer
}

const mapStateToProps = (containers: any) => {
  return {
    activities: containers.activities.state
  }
}

export default connect(container, mapStateToProps)(ListActivitiesHistory);