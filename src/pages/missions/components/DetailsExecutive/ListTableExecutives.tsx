import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import { Table, Rate, message } from 'antd';
import Lang from 'src/support/Lang';
import { connect } from 'unstated-enhancers';
import RejectExecutiveContainer from '../../state/RejectExecutiveContainer';
import { status } from '../../../../support/MissionConstants'
import ActionsButton from './ActionsButton';
import Notifications from 'src/components/notifications/notifications';
import RateExecutiveContainer from '../../state/RateExecutiveContainer';
import AddScore from './AddScore';
import ActivitiesExecutiveContainer from '../../state/ActivitiesExecutiveContainer';
import ListActivities from './ListActivities';
import Event from 'src/support/Event';
import { Gate } from '@xaamin/guardian';
import CancelMissionContainer from '../../state/CancelMissionContainer';
import ModalConfirmation from 'src/components/modal/ModalConfirmation';

const { Column } = Table;

class ListTableExecutives extends PureComponent<any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    data: null,
    visibleRate: false,
    visibleDrawer: false,
    visibleConfirm: false,
    cancelExecutive: false,
    rejectExecutive: undefined
  }

  private renderActions(record: any) {
    const { reject, rate, activities, cancel } = this.props.containers
    return (
      <ActionsButton
        data={record}
        onRate={this.openModalRate}
        onFunction={this.openModalConfirm}
        onActivities={this.getActivities}
        processReject={reject.state.httpBusy}
        processRate={rate.state.httpBusy}
        processActivities={activities.state.httpBusy}
        processCancel={cancel.state.httpBusy}
      />
    )
  }

  private openModalRate(e: any) {
    const { data } = this.props
    const find = data.find((item: any) => item.uuid === e.target.id)

    if (find) {
      this.setState({
        selectExecutive: find,
        visibleRate: true
      })
    }
  }

  private openModalConfirm(id: string, name: string) {
    this.setState({
      [name]: id,
      visibleConfirm: true
    })
  }

  private closeModal() {
    this.setState({
      visibleRate: false,
      visibleConfirm: false,
    })
  }

  private async rejectExecutive() {
    if (Gate.allows('mission::executive-reject')) {
      const { reject } = this.props.containers
      const { data } = this.props
      const { rejectExecutive } = this.state
      const statusExecutive = { status: status.REJECT }
      const find = data.find((item: any) => item.uuid === rejectExecutive)

      if (find) {
        await reject.rejectExecutive(find.uuid, find.mission_uuid, statusExecutive)
        const { error } = this.props.containers.reject.state

        if (error) {
          const notification = new Notifications;
          const msj = {
            type: 'error',
            message: Lang.get('buses.mission.reject_executive')
          }
          notification.openNotification(msj)
        } else {
          const result = {
            message: Lang.get('buses.mission.success_reject_executive')
          }
          this.props.onClose()
          Event.notify('mission::edit', result);
        }
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  private async rateExecutive(score: any) {
    const { rate } = this.props.containers

    if (score) {
      let infoScore = {}
      infoScore = {
        rate: score.score,
        review: score.comment
      }

      if (score.selectBonus) {
        infoScore = {
          ...infoScore,
          bonus: {
            payment_token: rate.state.payment_token,
            subtotal: parseFloat(score.subtotal) + parseFloat(score.commission),
            tax: parseFloat(score.tax),
            total: parseFloat(score.total)
          }
        }
      }

      await rate.rateExecutive(score.uuid, score.mission_uuid, infoScore)
      const { error } = this.props.containers.rate.state

      if (error) {
        const notification = new Notifications;
        const msj = {
          type: 'error',
          message: Lang.get('buses.mission.error_score')
        }
        notification.openNotification(msj)
      } else {
        this.closeModal()
        const result = {
          message: Lang.get('buses.mission.success_score')
        }
        this.props.onClose()
        Event.notify('mission::edit', result);
      }
    }
  }

  private async cancelMissionExecutive() {
    const { data } = this.props
    const { cancel } = this.props.containers
    const { cancelExecutive } = this.state
    const find = data.find((item: any) => item.uuid === cancelExecutive)

    if (find) {
      const newData = {
        status: -1
      }
      await cancel.cancelMission(find.mission_uuid, newData, find.uuid);
    }
  }

  private async getActivities(e: any) {
    const { data } = this.props
    const { activities } = this.props.containers
    const params = {
      limit: 50,
    }

    const find = data.find((item: any) => item.uuid === e.target.id)

    if (find) {
      await activities.getActivities(find.uuid, find.mission_uuid, params);
      const { error } = this.props.containers.activities.state

      if (error) {
        const notification = new Notifications;
        const msj = {
          type: 'error',
          message: Lang.get('buses.mission.error_activity')
        }
        notification.openNotification(msj)
      }

      const activitiesData = this.props.containers.activities.state.data

      this.setState({
        activitiesExecutive: activitiesData,
        selectExecutive: find
      })
      this.showDrawer()
    }
  }

  private showDrawer() {
    this.setState({
      visibleDrawer: !this.state.visibleDrawer
    })
  }

  private renderScore(record: any) {
    return (
      <Rate 
        disabled={true}
        style={{ fontSize: 16 }}
        defaultValue={record.rate_mission}
      />
    )
  }

  public render() {
    const { data, type } = this.props
    const httpBusyRate = this.props.containers.rate.state.httpBusy
    const httpBusyActivities = this.props.containers.activities.state.httpBusy
    const { selectExecutive, activitiesExecutive, rejectExecutive,
      visibleConfirm, cancelExecutive } = this.state

    const selectFunction = rejectExecutive ? this.rejectExecutive
      : cancelExecutive ? this.cancelMissionExecutive : undefined

    const selectText = rejectExecutive ? Lang.get('buses.mission.reject_question')
      : cancelExecutive ? Lang.get('buses.mission.cancel_question') : undefined

    return (
      <div>
        <Table
          dataSource={data}
          pagination={false}
          className="executives-table"
        >
          <Column
            title={Lang.get('buses.mission.create_date')}
            dataIndex="created_at"
            key="created_at"
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.name')}
            dataIndex="name"
            key="name"
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.email')}
            dataIndex="email"
            key="email"
            align={'center'}
          />
          <Column
            title={Lang.get('buses.mission.score')}
            key="score"
            render={this.renderScore}
            align={'center'}
          />
          <Column
            title={Lang.get('buses.button.actions')}
            key="reject"
            render={this.renderActions}
            align={'center'}
          />
        </Table>
        {
          selectExecutive &&
          <AddScore
            visible={this.state.visibleRate}
            save={this.rateExecutive}
            cancel={this.closeModal}
            data={selectExecutive}
            loading={httpBusyRate}
            type={type}
          />
        }
        {
          activitiesExecutive &&
          <ListActivities
            close={this.showDrawer}
            visible={this.state.visibleDrawer}
            loading={httpBusyActivities}
            data={activitiesExecutive}
            executive={selectExecutive}
          />
        }
        <ModalConfirmation
          onFunction={selectFunction}
          onVisible={visibleConfirm}
          onClose={this.closeModal}
          onText={selectText}
        />
      </div>
    );
  }
}

const container = {
  reject: RejectExecutiveContainer,
  rate: RateExecutiveContainer,
  activities: ActivitiesExecutiveContainer,
  cancel: CancelMissionContainer
}

const mapStateToProps = (containers: any) => {
  return {
    reject: containers.reject.state,
    rate: containers.rate.state,
    activities: containers.activities.state,
    cancel: containers.cancel.state,
  }
}

export default connect(container, mapStateToProps)(ListTableExecutives);