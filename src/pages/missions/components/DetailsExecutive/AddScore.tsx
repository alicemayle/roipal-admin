import React, { PureComponent } from 'react';
import { Modal, Button, Rate, Input, Radio, Row, Col, message } from 'antd';
import autobind from 'class-autobind';
import Lang from '../../../../support/Lang';
import BonusSlider from './BonusSlider';
import RateExecutiveContainer from '../../state/RateExecutiveContainer'
import { connect } from 'unstated-enhancers';
import { Gate } from '@xaamin/guardian';

class AddScore extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
    this.state = {
      selectBonus: false
    }
  }

  public state: any = {
    score: 0,
    comment: ''
  }

  private setScore(value: any) {
    this.setState({
      score: value
    })
  }

  private setText(value: any) {
    this.setState({
      comment: value.target.value
    })
  }

  private handleSubmit() {
    if (Gate.allows('mission::executive-rate')) {
      const { save, data } = this.props

      if (save) {
        const score = {
          ...this.state,
          ...data
        }

        save(score)
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  private selectBonus(e: any) {
    this.setState({
      selectBonus: e.target.value,
    })
  }

  private handleChangeBonus(value: number) {
    const { iva, sum_landline, sum_percentage } = this.props.rate

    const _commission = ((value * sum_percentage / 100) + sum_landline).toFixed(2)
    const _tax = ((value + parseFloat(_commission)) * iva).toFixed(2)
    const _total = (parseFloat(_commission) + parseFloat(_tax) + value).toFixed(2)

    this.setState({
      subtotal: parseFloat(value.toFixed(2)),
      commission: parseFloat(_commission),
      tax: parseFloat(_tax),
      total: parseFloat(_total)
    })
  }

  private async getCommissions() {
    const { type } = this.props
    const { rate } = this.props.containers
    const company = this.props.data.company_uuid

    const data = {
      type_sale_uuid: type
    }

    await rate.getCommissions(data, company)

    const { error, type_sales } = rate.state

    if (!error) {
      this.setState({
        bonusMin: type_sales.bonus_minimum
      })
    }

    rate.getPaymentToken()
  }

  public componentDidMount() {
    this.getCommissions()
  }

  public render() {
    const { visible, cancel, data, loading } = this.props
    const { selectBonus, bonusMin = 0, subtotal, total,
      commission, tax, score, comment } = this.state

    const min = parseFloat(bonusMin.toFixed(2))
    const max = parseFloat((bonusMin * 5).toFixed(2))
    const step = parseFloat((bonusMin * 0.5).toFixed(2))

    return (
      <div>
        <Modal
          title={data.name}
          visible={visible}
          onCancel={cancel}
          destroyOnClose={true}
          centered={true}
          footer={[
            <Button
              key="cancel"
              onClick={cancel}>
              {Lang.get('buses.button.cancel')}
            </Button>,
            <Button
              key="submit"
              disabled={score && comment ? false : true}
              type="primary"
              onClick={this.handleSubmit}
              loading={loading}
            >
              {Lang.get('buses.button.save')}
            </Button>
          ]}
        >
          <div>
            <Rate
              style={{ fontSize: 30, marginLeft: '28%' }}
              onChange={this.setScore}
            />
            <br />
            <br />
            <Input
              onChange={this.setText}
              placeholder={Lang.get('buses.mission.comment')}
            />
            <br />
            <br />
            <p>{Lang.get('buses.score.info_bono')}</p>
            <Radio.Group
              name="radiogroup"
              defaultValue={false}
              onChange={this.selectBonus}
            >
              <Radio value={true}>{Lang.get('buses.score.yes')}</Radio>
              <Radio value={false}>{Lang.get('buses.score.no')}</Radio>
            </Radio.Group>
            <br />
            <br />
            {
              selectBonus &&
              <BonusSlider
                min={min}
                max={max}
                step={step}
                onChangeBonus={this.handleChangeBonus}
              />
            }
            <br />
            {
              subtotal &&
              <div>
                <Row type="flex" justify={'center'}>
                  <Col span={6}>{Lang.get('buses.mission.subtotal')}</Col>
                  <Col span={6}>$ {subtotal} MXN</Col>
                </Row>
                <Row type="flex" justify={'center'}>
                  <Col span={6}>{Lang.get('buses.score.commissions')}:</Col>
                  <Col span={6}>$ {commission} MXN</Col>
                </Row>
                <Row type="flex" justify={'center'}>
                  <Col span={6}>{Lang.get('buses.score.iva')}:</Col>
                  <Col span={6}>$ {tax} MXN</Col>
                </Row>
                <Row type="flex" justify={'center'}>
                  <Col span={6}>{Lang.get('buses.score.total')}:</Col>
                  <Col span={6}>$ {total} MXN</Col>
                </Row>
              </div>
            }
          </div>

        </Modal>
      </div>
    );
  }
}

const container = {
  rate: RateExecutiveContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    rate: containers.rate.state,
  }
}

export default connect(container, mapStateToProps)(AddScore);