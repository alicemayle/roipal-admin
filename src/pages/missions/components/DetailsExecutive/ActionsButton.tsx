import React, { PureComponent } from 'react';
import { Button } from 'antd';
import { status } from '../../../../support/MissionConstants'
import Lang from 'src/support/Lang';
import { Gate } from '@xaamin/guardian';
import autobind from 'class-autobind';

class ActionsButton extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  private sendData(e: any) {
    const id = e.target.id
    const name = e.target.name
    const { onFunction } = this.props

    if (onFunction) {
      onFunction(id, name)
    }
  }

  public render() {
    const { data, processReject, onRate, onActivities,
      processRate, processCancel } = this.props;

    const enableReject = data.status === status.REJECT ? false
      : data.status === status.COMPLETED ? false
        : data.status === status.CANCELED ? false
          : data.status === status.STARTED ? false : true

    const enableRate = data.status === status.COMPLETED ?
      data.rate_mission === 0 ? true : false : false

    const enableActivities = data.status === status.STARTED ? true
      : data.status === status.COMPLETED ? true : false

    const enableCancel = data.status === status.STARTED ? true
      : data.status === status.REJECT ? false
        : data.status === status.COMPLETED ? false
          : data.status === status.CANCELED ? false : false

    return (
      <div>
        {
          enableReject && Gate.allows('mission::executive-reject') &&
          <Button
            name={'rejectExecutive'}
            size="small"
            block={true}
            id={data.key}
            onClick={this.sendData}
            loading={processReject}
            style={{ borderColor: '#CD5C5C', color: '#CD5C5C' }}
          >
            {Lang.get('buses.mission.reject_button')}
          </Button>
        }
        {
          enableRate && data.rate_mission === 0 &&
          Gate.allows('mission::executive-rate') &&
          <Button
            size="small"
            block={true}
            id={data.key}
            onClick={onRate}
            loading={processRate}
            style={{ borderColor: '#87CEEB', color: '#87CEEB', marginBottom: 5 }}
          >
            {Lang.get('buses.mission.rate_executive')}
          </Button>
        }
        {
          enableActivities && enableRate &&
          <Button
            size="small"
            id={data.key}
            block={true}
            onClick={onActivities}
            style={{ borderColor: '#66CDAA', color: '#66CDAA', marginBottom: 5 }}
          >
            {Lang.get('buses.mission.see_activity')}
          </Button>
        }
        {
          enableCancel &&
          <Button
            name={'cancelExecutive'}
            size="small"
            id={data.key}
            onClick={this.sendData}
            block={true}
            loading={processCancel}
            style={{ borderColor: '#CD5C5C', color: '#CD5C5C', marginBottom: 5 }}
          >
            {Lang.get('buses.button.cancel')}
          </Button>
        }
      </div>
    );
  }
}

export default ActionsButton;