import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Slider } from 'antd';
import '../../../../styles/slider.scss'

class BonusSlider extends PureComponent<any, any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
  }

  private handleChange(value: number) {
    const { onChangeBonus } = this.props;

    if (onChangeBonus) {
      onChangeBonus(value);
    }
  }

  public render() {
    const { min, max, step } = this.props

    const marks = {
      [min]: {
        label: <p>${min}</p>,
      },
      [max]: {
        label: <p>${max}</p>,
      },
    }

    return (
      <Slider
        min={min}
        max={max}
        step={step}
        marks={marks}
        tooltipVisible={true}
        onChange={this.handleChange}
      />
    )
  }
}

export default BonusSlider;