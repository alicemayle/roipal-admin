import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import { Drawer, Divider } from 'antd';
import ListTableExecutives from './ListTableExecutives'
import Lang from 'src/support/Lang';

const pStyle = {
  fontSize: 18,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16,
};

class ListExecutives extends PureComponent<any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    data: null
  }

  public render() {
    const { visible, onClose, executives, type } = this.props

    return (
      <Drawer
        placement="right"
        width={'55%'}
        closable={false}
        destroyOnClose={true}
        onClose={onClose}
        visible={visible}
      >
        <p style={{ ...pStyle, marginBottom: 24 }}>
          {Lang.get('buses.mission.list_executives')}
        </p>
        <Divider />
        <ListTableExecutives
          data={executives}
          onClose={onClose}
          type={type}/>
      </Drawer>
    );
  }
}

export default ListExecutives;

