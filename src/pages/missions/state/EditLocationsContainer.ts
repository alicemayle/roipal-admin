import { Container } from 'unstated';
import EditMission from '../services/EditMissionService';
import Event from 'src/support/Event';
import Lang from 'src/support/Lang';

const initialState = {
  httpBusy: true,
  data: undefined,
  error: null,
  message: ''
}

class EditLocationsMissionContainer extends Container<any> {
  public state: any = {
    ...initialState
  }

  public async getLocationNoMission(mission: string, company: string, params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Location no mission List starts'
      });
      const response = await EditMission.getLocationNoMission(mission, company, params);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Location no mission List success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Location no mission List error'
      })
    }
  }

  public async getCostLocation(data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get cost locations starts'
      })

      const response = await EditMission.getCostLocations(data);

      if (response) {

        await this.setState({
          httpBusy: false,
          locationsCost: response.data,
          __action: 'Get cost locations success'
        })
      }
    } catch (error) {
      await this.setState({
        httpBusy: false,
        error,
        __action: 'Get cost locations error'
      })
    }
  }

  public async getCommissions(company: string, data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get commissions starts'
      })

      const response = await EditMission.getCommissions(company, data);

      if (response) {

        await this.setState({
          httpBusy: false,
          ...this.state,
          comissions: response.data.company_commission,
          iva: response.data.commercial_need_group.iva,
          sum_percentage: response.data.sum_percentage,
          sum_landline: response.data.sum_landline,
          __action: 'Get commissions success'
        })
      }
    } catch (error) {
      await this.setState({
        httpBusy: false,
        error,
        __action: 'Get commissions error'
      })
    }
  }

  public async addLocationsMission(mission: string, company: string, data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Add locations starts'
      })

      const response = await EditMission.addLocationsMission(mission, company, data);

      if (response) {

        await this.setState({
          httpBusy: false,
          newLocations: response.data,
          __action: 'Add locations success'
        })
      }
    } catch (error) {
      await this.setState({
        httpBusy: false,
        error,
        __action: 'Add locations error'
      })
    }
  }

  public async getPaymentToken() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get payment token starts'
      })

      const response = await EditMission.getPaymentToken();

      if (response) {

        await this.setState({
          httpBusy: false,
          payment_token: response.token,
          __action: 'Get payment token success'
        })
      }
    } catch (error) {
      await this.setState({
        httpBusy: false,
        error,
        __action: 'Get payment token error'
      })
    }
  }

  public async editLocations(mission: string, data: any) {
    let result;

    try {
      this.setState({
        busy: true,
        __action: 'Edit locations mission starts'
      })

      const response = await EditMission.editLocations(mission, data);
      result = {
        message: Lang.get('buses.mission.success_edit_locations')
      }

      if (response) {

        await this.setState({
          busy: false,
          editLocations: response.data,
          __action: 'Edit locations mission success'
        })
      }
    } catch (error) {
      await this.setState({
        busy: false,
        error,
        __action: 'Edit locations mission error'
      })

      result = {
        error,
      }
    }
    Event.notify('mission::edit', result);
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default EditLocationsMissionContainer;