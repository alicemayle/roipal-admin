import { Container } from 'unstated';
import CurrencyState from '../models/CountryState';
import Currency from '../services/Currency';
class CurrencyContainer extends Container<CurrencyState> {
  public state: CurrencyState = {
      httpBusy: false,
      data: null,
      error: null,
      message: '',
  }

  public name: string = 'Currency Container';

  public async fetch() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get coins starts'
    });

    const response = await Currency.list();

    this.setState({
      httpBusy: false,
      data: response.data,
      __action: 'Get coins success'
  });

    } catch(error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Get coins error'
    });
    }
  }

public clearError() {
    this.setState({
        ...this.state,
        message: '',
        error: null
    })
}
}


export default CurrencyContainer;