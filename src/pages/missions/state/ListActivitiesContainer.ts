import { Container } from 'unstated';
import CommercialNeed from '../services/CommercialNeed';
import MissionsListState from '../models/MissionsListState';

const initialState = {
    httpBusy: true,
    data: null,
    error: null,
    message: ''
}

class ListActivitiesContainer extends Container<MissionsListState> {
    public state: MissionsListState = {
        ...initialState
    }

    public async fetch(params:{UUID:string}) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'Activities list starts'
            });
            const response = await CommercialNeed.activities(params);

            response.data.map((data:{key:string,uuid:string}) => {
                data.key = data.uuid;
            })

            this.setState({
                httpBusy: false,
                data: response.data,
                __action: 'Activities list success'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'Activities list error'
            })
        }
    }

    public reset() {
        this.setState({
            ...initialState
        });
    }

    public clearError() {
        this.setState({
            ...this.state,
            message: '',
            error: null
        })
    }
}

export default ListActivitiesContainer;