import { Container } from 'unstated';
import MissionsService from '../services/MissionsService';
import moment from 'moment';

const initialState = {
  httpBusy: true,
  data: null,
  error: null,
  message: ''
}

class ChargesMissionContainer extends Container<any> {
  public state: any = {
    ...initialState
  }

  public async getChargesMission(uuid: string, params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Charges mission starts'
      });
      const response = await MissionsService.getChargesMission(uuid, params);

      response.data.map((data: any) => {
        data.key = data.uuid;
        data.created_at =
          moment(new Date(data.created_at)).format('DD-MM-YYYY')
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Get Charges mission success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Get Charges mission error'
      })
    }
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default ChargesMissionContainer;