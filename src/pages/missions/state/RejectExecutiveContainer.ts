import { Container } from 'unstated';
import Reject from '../services/RejectExecutiveService';

class RejectExecutiveContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
  }

  public async rejectExecutive(executive: any, mission: any, data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Reject executive starts'
      });

      const response = await Reject.rejectExecutive(executive, mission, data)

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Reject executive success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Reject executive error'
      })
    }
  }
}

export default RejectExecutiveContainer;