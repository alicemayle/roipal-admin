import { Container } from 'unstated';
import CommercialNeed from '../services/CommercialNeed';
import MissionsListState from '../models/MissionsListState';

const initialState = {
    httpBusy: true,
    data: null,
    error: null,
    message: ''
}

class CommercialNeedContainer extends Container<MissionsListState> {
    public state: MissionsListState = {
        ...initialState
    }

    public async fetch() {
        try {
            this.setState({
                httpBusy: true,
                __action: 'Comercial list starts'
            });

            const params = {
                limit: 5
            }
            const response = await CommercialNeed.list(params);

            const comercialNeeds = response.data;

            const custom = comercialNeeds.find((item: any) => item.commercial_need === 'Custom')
            

            const index = comercialNeeds.indexOf(custom)

            if (index !== -1) {
                comercialNeeds.splice(index,1);
            }

            comercialNeeds.map((data:{key:string,uuid:string}) => {
                data.key = data.uuid;
            })

            this.setState({
                httpBusy: false,
                data: {
                    comercialNeeds: [...comercialNeeds],
                },
                __action: 'Comercial list success'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'Comercial list error'
            })
        }
    }

    public reset() {
        this.setState({
            ...initialState
        });
    }

    public clearError() {
        this.setState({
            ...this.state,
            message: '',
            error: null
        })
    }
}

export default CommercialNeedContainer;