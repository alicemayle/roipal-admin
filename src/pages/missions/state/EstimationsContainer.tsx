import { Container } from 'unstated';
import EstimationsState from '../models/EstimationsState';
import Estimations from '../services/Estimations';

class EstimationsContainer extends Container<EstimationsState> {
  public state: EstimationsState = {
      httpBusy: false,
      data: null,
      error: null,
      message: ''
  }

  public name: string = 'Estimations Container';

  public async fetch(uuid: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Start fetch estimations'
    });

    const response = await Estimations.list(uuid);

    this.setState({
      httpBusy: false,
      data: response.data,
      __action: 'Success fetch estimations'
  });

    } catch(error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Error fetch estimations'
    });
    }
  }
}

export default EstimationsContainer;