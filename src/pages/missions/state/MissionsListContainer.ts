import { Container } from 'unstated';
import Missions from '../services/MissionsService';
import MissionsListState from '../models/MissionsListState';
import Lang from 'src/support/Lang';
import moment from 'moment';
import { status } from '../../../support/MissionConstants'

const initialState = {
  httpBusy: true,
  data: null,
  error: null,
  message: '',
  meta: null
}

class MissionsListContainer extends Container<MissionsListState> {
  public state: MissionsListState = {
    ...initialState
  }

  public async fetch(params: {}) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Missions list starts'
      });
      const response = await Missions.paginate(params);

      response.data.map((data: any) => {
        data.key = data.uuid;
        data.statusText = data.status === status.MISSION_FINISHED ?
          Lang.get('buses.mission.finished')
          : data.status === status.MISSION_HOLD ? Lang.get('buses.mission.onHold')
            : data.status === status.MISSION_IN_PROGRESS ? Lang.get('buses.mission.inProcess')
              : Lang.get('buses.mission.cancelled')
        data.created_at =
          moment(new Date(data.created_at)).format('DD-MM-YYYY')
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        meta: response.meta,
        __action: 'Missions list success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Mission list error'
      })
    }
  }

  public async getMissionsCompany(company: string, meta: {}) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'LIST MISSIONS BY COMPANY STARTS'
      });

      const response = await Missions.getMissionsCompany(company, meta);

      response.data.map((data: any) => {
        data.key = data.uuid;
        data.statusText = data.status === status.MISSION_FINISHED ?
          Lang.get('buses.mission.finished')
          : data.status === status.MISSION_HOLD ? Lang.get('buses.mission.onHold')
            : data.status === status.MISSION_IN_PROGRESS ? Lang.get('buses.mission.inProcess')
              : Lang.get('buses.mission.cancelled')
        data.created_at =
          moment(new Date(data.created_at)).format('DD-MM-YYYY')

        data.executives.map((item: any) => {
          item.created_at = moment(new Date(item.created_at)).format('DD-MM-YYYY')
        })
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        meta: response.meta,
        __action: 'LIST MISSIONS BY COMPANY SUCCESS'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'LIST MISSIONS BY COMPANY ERROR'
      });
    }
  }

  public setData(missions: any) {
    this.setState({
      ...this.state,
      data: missions,
      __action: 'Set Data mission list'
    })
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default MissionsListContainer;