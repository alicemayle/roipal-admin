import { Container } from 'unstated';
import cancel from '../services/CancelMissionService'
import Event from 'src/support/Event';
import Lang from 'src/support/Lang';

class CancelMissionContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
  }

  public async cancelMission(mission: any, data: any, user: any) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Cancel mission to executive starts'
      });

      const response = await cancel.cancelMission(mission, data, user)

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Cancel mission to executive success'
      });
      result = {
        message: Lang.get('buses.mission.success_cancel_executive')
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Cancel mission to executive error'
      })
      result = {
        error,
      }
    }
    Event.notify('mission::edit', result);
  }
}

export default CancelMissionContainer