import { Container } from 'unstated';
import DataPaymentState from '../models/DataPaymentState'
import DataPayment from '../services/DataPayment'

class DataPaymentContainer extends Container<DataPaymentState> {
  public state: DataPaymentState = {
      httpBusy: false,
      data: null,
      message: '',
      token_payment: null
  }

  public name: string = 'DataPayment Container';

  public async fetch(uuid: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Start fetch DataPayment'
    });

    const response = await DataPayment.list(uuid);

    this.setState({
      httpBusy: false,
      data: response.data.payments_profiles[0],
      __action: 'Success fetch DataPayment'
  });

    } catch(error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Error fetch DataPayment'
    });
    }
  }

  public async getToken(){
    try {
      this.setState({
        httpBusy: true,
        __action: 'Start token payment'
    });

    const response = await DataPayment.getToken();

    this.setState({
      httpBusy: false,
      token_payment: response,
      __action: 'Success token payment'
  });

    } catch(error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Error token payment'
    });
    }
  }
}

export default DataPaymentContainer;