import { Container } from 'unstated';
import Location from '../services/Location';

const initialState = {
  httpBusy: true,
  data: undefined,
  error: null,
  message: ''
}

class LocationContainer extends Container<any> {
  public state: any = {
    ...initialState
  }

  public async fetch(company: string, params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Location List starts'
      });
      const response = await Location.list(company, params);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Location List success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Location List error'
      })
    }
  }

  public setData(data: any) {
    this.setState({
      data
    })
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default LocationContainer;