import { Container } from 'unstated';
import activities from '../services/ActivitiesExecutiveService';

class ActivitiesExecutiveContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
    dataHistory: null
  }

  public async getActivities(executive: any, mission: any, params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'get activities executive starts'
      });

      const response = await activities.getActivities(executive, mission, params)

      response.data.map((item: any) => {
        item.key = item.uuid
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'get activities executive success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'get activities executive error'
      })
    }
  }

  public async getActivitiesHistory(executive: any, mission: any, activity: any, params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'get history activities executive starts'
      });

      const response = await activities.getActivitiesHistory(executive, mission, activity, params)

      const original = this.state.data

      original.map((item: any) => {
        if (item.uuid === activity) {
          item.history = response.data
        }
      })

      this.setState({
        httpBusy: false,
        data: original,
        __action: 'get history activities executive success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'get history activities executive error'
      })
    }
  }
}

export default ActivitiesExecutiveContainer;