import { Container } from 'unstated';
import CountryState from '../models/CountryState';
import Country from '../services/Country';
class CountryContainer extends Container<CountryState> {
  public state: CountryState = {
      httpBusy: false,
      data: null,
      error: null,
      message: ''
  }

  public name: string = 'Country Container';

  public async fetch() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Start fetch country'
    });

    const response = await Country.list();

    this.setState({
      httpBusy: false,
      data: response.data,
      __action: 'Success fetch country'
  });

    } catch(error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Error fetch country'
    });
    }
  }
}

export default CountryContainer;