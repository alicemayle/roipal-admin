import { Container } from 'unstated';
import MissionsService from '../services/MissionsService';
import Event from 'src/support/Event';
import Lang from 'src/support/Lang';

const initialState = {
  httpBusy: true,
  data: null,
  error: null,
  message: ''
}

class ChangeMissionStatusContainer extends Container<any> {
  public state: any = {
    ...initialState
  }

  public async changeStatus(company: string, mission: string, data: any) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Change status mission starts'
      });
      const response = await MissionsService.changeStatus(company, mission, data);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Change status mission success'
      });
      result = {
        message: Lang.get('buses.mission.success_change_status')
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Change status mission error'
      })
      result = {
        error,
      }
    }
    Event.notify('mission::edit', result);
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default ChangeMissionStatusContainer;