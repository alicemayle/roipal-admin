import { Container } from 'unstated';
import CommercialNeed from '../services/CommercialNeed';
import MissionsListState from '../models/MissionsListState';

const initialState = {
    httpBusy: true,
    data: null,
    error: null,
    message: ''
}

class CustomActivitiesContainer extends Container<MissionsListState> {
    public state: MissionsListState = {
        ...initialState
    }

    public async fetch() {
        try {
            this.setState({
                httpBusy: true,
                __action: 'Custom Activities list starts'
            });
            const response = await CommercialNeed.customActivities();

            response.data.map((data:{key:string,uuid:string}) => {
                data.key = data.uuid;
            })

            this.setState({
                httpBusy: false,
                data:  response.data[0].type_sales[0],
                __action: 'Custom Activities list success'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'Custom Activities list error'
            })
        }
    }

    public reset() {
        this.setState({
            ...initialState
        });
    }

    public clearError() {
        this.setState({
            ...this.state,
            message: '',
            error: null
        })
    }
}

export default CustomActivitiesContainer;