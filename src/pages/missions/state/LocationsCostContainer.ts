import { Container } from 'unstated';
import Location from '../services/Location';

const initialState = {
  httpBusy: true,
  data: undefined,
  error: null,
  message: ''
}

class LocationCostContainer extends Container<any> {
  public state: any = {
    ...initialState
  }

  public async getCostLocations(params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get cost locations starts'
      })

      const response = await Location.getCostLocations(params);

      if (response) {

        await this.setState({
          httpBusy: false,
            data: response.data,
          __action: 'Get cost locations success'
        })
      }
    } catch (error) {
      await this.setState({
        httpBusy: false,
        error,
        __action: 'Get cost locations error'
      })
    }
  }

  public setData(data: any) {
    this.setState({
      data
    })
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default LocationCostContainer;