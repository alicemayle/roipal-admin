import { Container } from 'unstated';
import score from '../services/ScoreService';

class RateExecutiveContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: ''
  }

  public async rateExecutive(executive: any, mission: any, infoScore: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Rate executive starts'
      });

      const response = await score.rateExecutive(executive, mission, infoScore)

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Rate executive success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Rate executive error'
      })
    }
  }

  public async getCommissions(data: any, company: string) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get commissions starts'
      });

      const response = await score.getCommissions(data, company)

      this.setState({
        httpBusy: false,
        comissions: response.data.company_commission,
        iva: response.data.commercial_need_group.iva,
        type_sales: response.data.type_sale,
        sum_landline: response.data.sum_landline,
        sum_percentage: response.data.sum_percentage,
        __action: 'Get commissions success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Get commissions error'
      })
    }
  }

  public async getPaymentToken() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get payment token starts'
      })

      const response = await score.getPaymentToken();

      if (response) {

        await this.setState({
          httpBusy: false,
          payment_token: response.token,
          __action: 'Get payment token success'
        })
      }
    } catch (error) {
      await this.setState({
        httpBusy: false,
        error,
        __action: 'Get payment token error'
      })
    }
  }
}

export default RateExecutiveContainer;