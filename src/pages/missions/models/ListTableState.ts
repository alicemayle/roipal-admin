interface ListTableSate {
    searchText: string,
    fields?:any,
    data?: null,
    editingKey?: '',
    visibleExecutives?: boolean,
    listExecutives?: any,
    type_sales?: string
}

export default ListTableSate;