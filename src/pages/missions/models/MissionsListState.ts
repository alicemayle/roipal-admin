import { ItemState } from '../../../roipal/models';

interface MissionsListState extends ItemState {
  __action?: string,
  meta?: any
}

export default MissionsListState;