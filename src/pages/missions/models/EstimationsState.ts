import { ListState } from '../../../roipal/models';

interface EstimationsState extends ListState {
  __action?: string
}

export default EstimationsState;