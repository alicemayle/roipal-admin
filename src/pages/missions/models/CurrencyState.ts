import { ListState } from '../../../roipal/models';

interface CurrencyState extends ListState {
  __action?: string,
  dataCurrency: []
}

export default CurrencyState;