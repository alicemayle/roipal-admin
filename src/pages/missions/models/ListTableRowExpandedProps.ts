interface ListTableRowExpandedProps {
    data: any,
    onClose: () => void
}

export default ListTableRowExpandedProps;