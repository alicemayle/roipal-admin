interface FormProps {
  companies?: [
    {
      key: string,
      uuid: string,
      business_name: string
    }
  ],
  commercial?: [
    {
      commercial_need:string
      key:string
      uuid:string
    }
  ],
  history?: any
  // onChangeCurrent: (current:number)=>void
}

export default FormProps;