interface FormProps {
  data?: [
    {
      key: string,
      uuid: string,
      business_name ?: string
      commercial_need ?: string
      type?: string
    }
  ],
  label: string,
  selectedValues ?: {}
  onSearch: (UUID: string, option ?:string) => void
}

export default FormProps;