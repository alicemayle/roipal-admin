interface FormProps {
  label: string,
  onChange: (current:number) => void
  type: number,
}

export default FormProps;