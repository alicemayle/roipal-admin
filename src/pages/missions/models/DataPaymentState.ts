import { ListState } from '../../../roipal/models';

interface DataPaymentState extends ListState {
  __action?: string,
  token_payment: any
}

export default DataPaymentState;