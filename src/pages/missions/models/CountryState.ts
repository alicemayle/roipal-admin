import { ListState } from '../../../roipal/models';

interface CountryState extends ListState {
  __action?: string
}

export default CountryState;