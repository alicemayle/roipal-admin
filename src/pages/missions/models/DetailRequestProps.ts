interface DetailRequestProps {
 data: {
   accepted: number,
   invitations_sent: number,
   rejected: number,
   executives_requested: number,
 }
}

export default DetailRequestProps;