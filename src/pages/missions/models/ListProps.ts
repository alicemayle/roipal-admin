interface ListProps {
    containers: any,
    missions: any,
    data: any,
    httpBusy: boolean,
    location: {
      search: {}
    },
    history: any
  }

  export default ListProps;