interface ListTableProps {
  data: any,
  httpBusy: boolean,
  record?: any,
  onClose?: any,
  onShow?: any,
  onSelect?: any,
  visible: any,
  onChangeExecutive?: any
}

export default ListTableProps;