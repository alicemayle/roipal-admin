import { Http } from '../../../shared';

class CancelMissionService {

  public cancelMission(mission:any, data:any, uuid:any): any {
    return Http.put(`api/executives/${uuid}/missions/${mission}/status`, data);
  }

}

export default new CancelMissionService;