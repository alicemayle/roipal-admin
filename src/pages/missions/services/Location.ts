import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class Location {

  public list(company: string, params: any): Promise<AxiosResponse> {
    return Http.get(`api/companies/${company}/locations`, { params });
  }

  public getCostLocations(data: any) {
    return Http.post(`/api/missions/suggestions-locations`, data);
  }

}

export default new Location;