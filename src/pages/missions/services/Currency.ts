
  import { Http } from '../../../shared';
  import { AxiosResponse } from 'axios';
  
  class Currency {
  
    public list(): Promise<AxiosResponse> {
        return Http.get(`/api/translations/catalogs?namespace=catalogs&group=currency`);
      }
  
  }
  
  export default new Currency;
