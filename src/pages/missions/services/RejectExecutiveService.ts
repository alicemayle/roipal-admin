import { Http } from '../../../shared';

class RejectExecutiveService {

  public rejectExecutive(executive: any, mission: any, data: any): any {
    return Http.put(`/api/executives/${executive}/missions/${mission}/status?includes=
    executives,executives.profile,executives.address`, data);
  }
}

export default new RejectExecutiveService;