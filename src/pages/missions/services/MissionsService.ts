import { Http } from '../../../shared';
class MissionsService {

  // TODO: investigar la forma correcta para el response
  public paginate( params: any): any {
    return Http.get(`api/missions`, { params })
  }

  public saveMission(uuid: string, data: any): any {
    return Http.post(
      `/api/companies/${uuid}/missions?includes=locations,company,executives,suggestion`, 
      data);
  }

  public getMissionsCompany(uuid: string, params: any): any {
    return Http.get(`api/companies/${uuid}/missions`, { params });
  }

  public changeStatus(company: string, mission: string, data: any) {
    return Http.put(`/api/companies/${company}/missions/${mission}/status`, data)
  }

  public getChargesMission(mission: string, params: any) {
    return Http.get(`api/missions/${mission}/company-charges`, {params})
  }
}

export default new MissionsService;