import { Http } from '../../../shared';

class EditMissionService {
  public getLocationNoMission(mission: string, company: string, params: any): any {
    return Http.get(`/api/companies/${company}/missions/${mission}/locations`, { params })
  }

  public getCostLocations(data: any) {
    return Http.post(`/api/missions/suggestions-locations`, data);
  }

  public getCommissions(company: string, data: any) {
    return Http.post(`/api/companies/${company}/commissions`, data)
  }

  public addLocationsMission(mission: string, company: string, data: any) {
    return Http.post(`/api/companies/${company}/missions/${mission}/locations`, data)
  }

  public getPaymentToken() : any{
    return Http.get(`/api/auth/token`)
  }

  public editLocations(mission: string, data: any) {
    return Http.post(`/api/missions/${mission}/locations?`, data)
  }
}

export default new EditMissionService;