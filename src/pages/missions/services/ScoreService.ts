import { Http } from '../../../shared';

class ScoreService {

  public rateExecutive(executive: string, mission: string, data: any): any {
    return Http.put(
      `/api/companies/missions/${mission}/executives/${executive}/rating?includes=executives`,
      data);
  }

  public getCommissions(data: any, company:string) {
    return Http.post(`/api/companies/${company}/commissions`, data)
  }

  public getPaymentToken() : any{
    return Http.get(`/api/auth/token`)
  }
}

export default new ScoreService;