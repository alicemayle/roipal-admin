import { Http } from '../../../shared';

class ActivitiesExecutiveService {

  public getActivities(executive: string, mission: string, params: any): any {
    return Http.get(`/api/executives/${executive}/missions/${mission}/activities`, { params });
  }

  public getActivitiesHistory(executive: string, mission: string, activity: any, params: any): any {
    return Http.get(
      `api/executives/${executive}/missions/${mission}/activities/${activity}/events`, { params });
  }
}

export default new ActivitiesExecutiveService;