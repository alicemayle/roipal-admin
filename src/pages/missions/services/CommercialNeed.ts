import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class CommercialNeed {

  public list(params: any): Promise<AxiosResponse> {
    return Http.get(`/api/commercial-needs`, {params})
  }

  public activities(data:{UUID:string}): Promise<AxiosResponse> {
    return Http.get(`/api/commercial-needs/${data.UUID}/type-sales/?includes=type_activities`)
  }

  public customActivities(): Promise<AxiosResponse> {
    return Http.get('/api/commercial-needs/?type=custom&includes=type_sales.type_activities');
  }

}

export default new CommercialNeed;