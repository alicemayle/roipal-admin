import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class Estimations {

  public list(uuid: any): Promise<AxiosResponse> {
    return Http.get(`/api/missions/suggestions/${uuid}`);
  }

}

export default new Estimations;