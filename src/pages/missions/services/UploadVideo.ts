import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class UploadVideo {

  public upload( data:any, config:any): Promise<AxiosResponse> {
    return Http.post(`/api/files`, data, config)
  }

}

export default new UploadVideo;