import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class DataPayment {

  public list(uuid: any): Promise<AxiosResponse> {
    return Http.get(`/api/companies/${uuid}?includes=payments_profiles`);
  }

  public getToken(): Promise<AxiosResponse> {
    return Http.get(`/api/auth/token`);
  }

}

export default new DataPayment;