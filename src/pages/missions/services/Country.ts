import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class Country {

  public list(): Promise<AxiosResponse> {
    return Http.get(`/api/translations/catalogs?namespace=catalogs&group=countries`)
  }

}

export default new Country;