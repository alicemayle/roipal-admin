import { Container } from 'unstated';
import MissionServices from '../services/MissionsService';

class MissionContainer extends Container<any> {
  public state: any = {
      httpBusy: false,
      data: null,
      error: null,
      message: '',
      meta: null
  }

  public name: string = 'Create Mission Container';

  public async createMission(uuid: string, data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Start fetch create mission'
    });

    const response = await MissionServices.saveMission(uuid, data);

    this.setState({
      httpBusy: false,
      data: response.data,
      meta: response.meta,
      __action: 'Success fetch create mission'
  });

    } catch(error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Error fetch create mission'
    });
    }
  }
}

export default MissionContainer;