import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import MissionsListContainer from './state/MissionsListContainer';
import ListTable from './components/ListTable';
import ListProps from './models/ListProps';
import Lang from '../../support/Lang';
import { Button, Divider } from 'antd';
import autobind from 'class-autobind';
import Pagination from 'src/components/pagination/Pagination';
import { Gate } from '@xaamin/guardian';
import Auth from '../../auth/User';
import Event from 'src/support/Event';
import Notifications from 'src/components/notifications/notifications';
import ChargesMissionContainer from './state/ChargesMissionContainer';

interface ListState {
  visible: any
  misionSelected: any,
  limit: number,
  page: number
}

class List extends Component<ListProps, ListState> {

  constructor(props: ListProps) {
    super(props);
    autobind(this);
    Event.listen('mission::edit', this.listeForUpsert);
  }

  public state: any = {
    visible: false,
    misionSelected: [],
    limit: 50,
    page: 1
  }

  public handleFetch(limit?: number, page?: number) {
    const roles = Auth.getData().roles || [];
    const scope = Auth.getData().scope;
    const found = roles.find((item: any) => item.name === 'company')
    const _limit = this.state.limit;
    const _page = this.state.page
    const { missions } = this.props.containers;

    const meta = {
      includes: 'executives,company,locations,activities,commercial_need,type_sales',
      field: 'status',
      limit: limit || _limit.toString(),
      page: page || _page
    }

    if (found || scope === 'company') {
      const uuid = Auth.getData().company.uuid
      missions.getMissionsCompany(uuid, meta)
    } else {
      missions.fetch(meta);
    }
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  private handleOpenUpsert() {
    this.props.history.push({
      pathname: '/mission/create',
    })
  }

  private showDrawer() {
    this.setState({
      visible: true,
    });
  };

  private closeDrawer() {
    this.setState({
      visible: false,
    });
  };

  private async showMisionDetails(mission: any) {
    const { data } = this.props.containers.missions.state;
    const { charges } = this.props.containers;

    const params = {
      limit: this.state.limit,
    }

    await charges.getChargesMission(mission.key, params);

    const _charges = charges.state.data

    const ItemSelected = await data.find((item: any) => item.key === mission.key);

    this.setState({
      misionSelected: {
        ...ItemSelected,
        charges: _charges
      }
    })

    this.showDrawer()
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.handleOnComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.handleOnComplete('error');
    }

    notification.openNotification(msj)
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.handleFetch();
    }
  }

  public componentWillUnmount() {
    Event.remove('mission::edit', this.listeForUpsert);
  }

  public componentDidMount() {
    this.handleFetch()
  }

  public render() {
    const { data, httpBusy, meta } = this.props.missions;

    return (
      <div>
        <h1>{Lang.get('buses.mission.title')}</h1>
        <Divider />
        <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
          <h1>{}</h1>
          {
            Gate.allows('mission::create') &&
            <Button type="primary"
              icon="plus-square"
              onClick={this.handleOpenUpsert}
            >
              {Lang.get('buses.button.add')}
            </Button>
          }
        </div>
        <br />
        <ListTable
          data={data}
          httpBusy={httpBusy}
          onShow={this.showMisionDetails}
          onClose={this.closeDrawer}
          onSelect={this.state.misionSelected}
          visible={this.state.visible}
        />
        {
          meta &&
          <Pagination
            limit={this.state.limit}
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
          />
        }
      </div>
    )
  }
}

const container = {
  missions: MissionsListContainer,
  charges: ChargesMissionContainer
}

const mapStateToProps = (containers: any) => {
  return {
    missions: containers.missions.state,
    charges: containers.charges.state
  }
}

export default connect(container, mapStateToProps)(List);