import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import CompaniesListContainers from '../../pages/companies/state/CompaniesListContainer';
import CommercialNeedContainer from './state/CommercialNeedContainer';
import FormMission from './components/Upsert/FormMission'
import ListProps from './models/ListProps';
import lang from '../../support/Lang';

class MissionsUpsert extends Component<ListProps> {

  constructor(props: ListProps) {
    super(props);
  }

  public componentDidMount() {
    const { companies, commercial } = this.props.containers;

    const meta = {
      includes: 'profile,payments_profiles',
      limit: 100
    }
    commercial.fetch();
    companies.fetch(meta);
  }

  public render() {
    const { data } = this.props.containers.companies.state;
    const commercial = this.props.containers.commercial.state.data;

    return (
      <div>
        <h1>
          {lang.get('buses.mission.add.new_mission')}
        </h1>
        <FormMission
        companies={data}
        commercial={commercial}
        history={this.props.history}
        />
      </div>
    )
  }
}

const container = {
  companies: CompaniesListContainers,
  commercial: CommercialNeedContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    companies: containers.companies.state,
    commercial: containers.commercial.state,
  }
}

export default connect(container, mapStateToProps)(MissionsUpsert);