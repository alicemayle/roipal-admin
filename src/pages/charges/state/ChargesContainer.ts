import { Container } from 'unstated';
import Charges from '../service/ChargesService';
import ChargesState from '../models/ListState';
import Lang from '../../../support/Lang';
import moment from 'moment';
import { statusCharges } from '../../../support/ChargesConstants'

class ChargesContainer extends Container<ChargesState> {

  public state: any = {
    httpBusy: false,
    data: null,
    meta: null,
    error: null,
    message: '',
    retry: null,
    retryBussy: false
  }

  public name: string = 'Charges Container';

  public async fetch(dateStart: any, dateEnd: any, params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Charges Starts'
      })

      const response = await Charges.list(dateStart, dateEnd, params);

      const newResponse: {} = response.data.map((item: any) => {

        const motive = item.concept === statusCharges.MISSION_NEW_MISSION ?
          Lang.get('charges.charge.new_mission') :
          item.concept === statusCharges.MISSION_EDIT ?
            Lang.get('charges.charge.edit_mission') :
            item.concept === statusCharges.MISSION_BONO ?
              Lang.get('charges.charge.bono') : 'Desconocido'

        const _status = item.company_invoice.length !== 0
          ? Lang.get('charges.charge.ringing')
          : item.company_invoice.length === 0 && item.invoice_failed.length === 0 ?
          Lang.get('charges.charge.not_invoice') : Lang.get('charges.charge.error')

        const data = {
          key: item.uuid,
          business: item.company.business_name,
          concept: motive,
          bill: item.company_invoice,
          billFailed: item.invoice_failed,
          total: '$ ' + item.transaction_amount + ' MXN',
          status: _status,
          subtotal: item.subtotal,
          tax_iva: item.tax_iva,
          mission_uuid: item.mission_uuid,
          payment_id: item.payment_id,
          transaction_amount: item.transaction_amount,
          created_at: moment(new Date(item.created_at)).format('DD-MM-YYYY')
        }
        return data
      })

      this.setState({
        httpBusy: false,
        data: newResponse,
        meta: response.meta,
        __action: 'Charges Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Charges Error'
      })
    }
  }

  public async retryBill(uuid: any, data: any) {
    try {
      this.setState({
        retryBussy: true,
        __action: 'Retry Bill Starts'
      })

      const response = await Charges.retryBill(uuid, data);

      this.setState({
        retryBussy: false,
        retry: response.data,
        __action: 'Retry Bill Success'
      })
    } catch (error) {
      this.setState({
        retryBussy: false,
        error,
        message: error.message,
        __action: 'Retry Bill Error'
      })
    }
  }

  public async setData(charge: any) {
    const dataOriginal = this.state.data
    const response = this.state.retry

    await dataOriginal.map((find: any) => {
      if (find.payment_id === charge) {
        find.bill = response.company_invoice;
        find.billFailed = response.invoice_failed;
      }
      return find;
    });

    this.setState({
      data: dataOriginal
    })
  }
}

export default ChargesContainer;