import { Http } from '../../../shared';

class Assestments {
  public list( dateStart:any, dateEnd:any, params: any): any  {
    return Http.get(`/api/company-charges/?includes=company,
    invoice_failed,company_invoice&from=${dateStart}&to=${dateEnd}`, { params })
}

  public retryBill(uuid: any, data: any) {
    return Http.post(
      `/api/missions/${uuid}/invoice/retry?includes=invoice_failed,company_invoice`,
      data)
  }
}

export default new Assestments;