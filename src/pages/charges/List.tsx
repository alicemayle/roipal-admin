import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Typography, Divider, Button, message, Row, Col } from 'antd';
import Chargescontainer from './state/ChargesContainer'
import { connect } from 'unstated-enhancers';
import ChargesTable from './components/ChargesTable';
import Pagination from 'src/components/pagination/Pagination';
import Lang from '../../support/Lang'
import ModalDate from './components/ModalDate';

const { Text } = Typography;

interface ListState {
  limit: number,
  page: number,
  containers?: any,
  dateStart?: any,
  dateEnd?: any,
  visible: any
}

class List extends Component<ListState> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: ListState = {
    limit: 50,
    dateStart: '',
    dateEnd: '',
    visible: false,
    page: 1
  }

  private clearFilter() {
    this.setState({
      dateStart: '',
      dateEnd: ''
    })

    setTimeout(() => {
      this.getCharges()
    }, 200)
  }


  private async setDate(inicial: any, finaly: any) {
    this.setState({
      dateStart: inicial,
      dateEnd: finaly
    })

    this.setVisible()

    setTimeout(() => {
      this.getCharges()
    }, 500)
  }

  private async getCharges(limit?: number, page?: number) {
    const { charges } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;
    const { dateStart, dateEnd } = this.state

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page
    }

    await charges.fetch(dateStart, dateEnd, meta)

    if (charges.state.error) {
      message.error(Lang.get('charges.charge.error_charges'));
    }
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.getCharges(_limit, _page)
  }

  public componentDidMount() {
    this.getCharges()
  }

  private setVisible() {
    const { visible } = this.state

    this.setState({
      visible: !visible
    })
  }

  public render() {

    const { data, httpBusy, meta, retryBussy } = this.props.containers.charges.state
    const { limit } = this.state
    const { dateStart, dateEnd } = this.state

    return (
      <div>
        <h1>{Lang.get('charges.charge.title')}</h1>
        <Divider />
        <Row gutter={16}>
          <Col className="gutter-row" span={16}>
            <h1>{}</h1>
          </Col>
          <Col className="gutter-row" span={8} style={{ textAlign: 'right' }}>
            <Button
              type="primary"
              onClick={this.setVisible}>
              <Text style={{ color: 'white' }}>{Lang.get('charges.charge.filter')}</Text>
            </Button>
            {
              dateStart && dateEnd &&
              <Button
                style={{ marginLeft: 5, borderColor: '#52bebb' }}
                onClick={this.clearFilter}>
                <Text style={{ color: '#52bebb' }}>{Lang.get('charges.charge.clear_filter')}</Text>
              </Button>
            }
          </Col>
        </Row>
        <br />
        <ChargesTable data={data} load={httpBusy} bussy={retryBussy} />
        {meta &&
          <Pagination
            limit={limit}
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
          />
        }
        <ModalDate
          visible={this.state.visible}
          onCancel={this.setVisible}
          onSumit={this.setDate}
        />
      </div>
    )
  }
}

const container = {
  charges: Chargescontainer,
}

const mapStateToProps = (containers: any) => {
  return {
    charges: containers.charges.state,
  }
}

export default connect(container, mapStateToProps)(List);

