import { ItemState } from '../../../roipal/models';

interface AssestmentState extends ItemState {
  httpBusy: boolean,
  data: any,
  error: any,
  meta: any,
  message: any,
  __action?: string,
  retry: any,
  retryBussy: boolean
}

export default AssestmentState;