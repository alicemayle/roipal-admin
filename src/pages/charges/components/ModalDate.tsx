import React, { Component } from 'react';
import autobind from 'class-autobind';
import Chargescontainer from '../state/ChargesContainer'
import { connect } from 'unstated-enhancers';
import { Modal, DatePicker, Button } from 'antd';
import Text from 'antd/lib/typography/Text';
import Lang from 'src/support/Lang';
import '../../../styles/datepicker.scss'

interface ListState {
  dateStart: any,
  dateEnd: any
}

class ModalDate extends Component<any, ListState> {
  constructor(props: any) {
    super(props);
    autobind(this)
    this.state = {
      dateStart: '',
      dateEnd: ''
    }
  }

  private onChangeStart(date: any, dateString: any) {
    this.setState({
      dateStart: dateString
    })
  }

  private onChangeEnd(date: any, dateString: any) {
    this.setState({
      dateEnd: dateString
    })
  }

  private onSetData() {
    const { onSumit } = this.props
    const { dateStart, dateEnd } = this.state

    if (onSumit) {
      onSumit(dateStart, dateEnd)
    }
  }

  public render() {
    const { visible, onCancel } = this.props
    const { httpBusy } = this.props.containers.charges.state

    return (
      <div>
        <Modal
          title={Lang.get('charges.charge.filter')}
          visible={visible}
          onCancel={onCancel}
          onOk={this.onSetData}
          centered={true}
          destroyOnClose={true}
          footer={[
            <Button
              key="cancel"
              onClick={onCancel}>
              {Lang.get('charges.button.cancel')}
            </Button>,
            <Button
              key="submit"
              type="primary"
              onClick={this.onSetData}
              loading={httpBusy}
            >
              {Lang.get('charges.button.confirm')}
            </Button>
          ]}
        >
          <Text>{Lang.get('charges.charge.date_filter')}</Text>
          <br />
          <br />
          <DatePicker
            onChange={this.onChangeStart}
            placeholder={Lang.get('charges.charge.start')}
            style={{ marginLeft: 25 }}
          />
          <Text style={{ marginLeft: 15, marginRight: 15 }}>-</Text>
          <DatePicker
            onChange={this.onChangeEnd}
            placeholder={Lang.get('charges.charge.end')}
          />
        </Modal>
      </div>
    )
  }
}

const container = {
  charges: Chargescontainer,
}

const mapStateToProps = (containers: any) => {
  return {
    charges: containers.charges.state,
  }
}

export default connect(container, mapStateToProps)(ModalDate);

