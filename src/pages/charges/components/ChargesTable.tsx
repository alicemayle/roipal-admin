import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Table, Tooltip, Button, Icon, message } from 'antd';
import { Typography } from 'antd';
import Lang from '../../../support/Lang';
import FilterDropdDownInterface from 'src/components/search/FilterDropDownInterface'
import FilterSearch from 'src/components/search/FilterSearch';
import { connect } from 'unstated-enhancers';
import ChargesContainer from '../state/ChargesContainer';
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';

const { Column } = Table;
const { Text } = Typography;

class ChargesTable extends Component<any> {
  public searchField: string = '';
  public searchInput: any = null;
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    searchText: '',
    fields: []
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterDropdDownInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  })

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private async retryBill(bill: any) {
    if(Gate.allows('company::invoice-retry')) {
      const { data = {} } = this.props;

      if (bill) {
        const find = data.find((item: any) => item.key === bill.key)

        if (find) {
          const mission_uuid = find.mission_uuid
          const newData = {
            subtotal: find.subtotal,
            tax_iva: find.tax_iva,
            transaction_amount: find.transaction_amount,
            payment_id: find.payment_id
          }

          const { charges } = this.props.containers

          await charges.retryBill(mission_uuid, newData)

          const { error } = this.props.containers.charges.state
          if (!error) {
            await charges.setData(find.payment_id)
          } else {
            message.error(Lang.get('charges.charge.error_bill'));
          }
        }
      } else {
        message.error(Lang.get('generic.policies.denied'));
      }
    }

  }

  private renderActions(item: any) {
    return (
      <div>
        {
          item.bill.length !== 0 &&
          <div>
            <Text style={{ color: 'green' }}>{Lang.get('charges.charge.ringing')}</Text>
          </div>
        }
        {
          item.billFailed.length !== 0 &&
          <div>
            <Tooltip title={item.billFailed.messege}>
              <Text style={{ color: 'red' }}>{Lang.get('charges.charge.error')}</Text>
            </Tooltip>
          </div>
        }
        {
          item.billFailed.length === 0 && item.bill.length === 0 &&
          <div>
            <Text style={{ color: 'red' }}>{Lang.get('charges.charge.not_invoice')}</Text>
          </div>
        }
      </div>
    );
  }

  private renderActionsButton(item: any) {
    return (
      <div>
        {
          item.bill.length !== 0 &&
          <div>
            <Button
              icon="download"
              size="default"
              type="link"
              href={item.bill.invoice_url}
            />
          </div>
        }
        {
          item.billFailed.length !== 0 &&
          <IconButton
            item={item}
            onFuction={this.retryBill}
            size={18}
            colorIcon={'red'}
            name={'reload'}
          />
        }
      </div>
    );
  }

  public render() {

    const { data, load, bussy } = this.props;

    return (
      <div>
        <Table
          dataSource={data}
          scroll={{ x: 800 }}
          loading={load || bussy}
          pagination={false}
        >
          <Column
            title={Lang.get('charges.charge.date')}
            dataIndex="created_at"
            key="created_at"
            width={150}
            align={'center'}
          />
          <Column
            title={Lang.get('charges.charge.company')}
            dataIndex="business"
            key="business"
            width={150}
            {...this.getColumnSearchProps('business')}
          />
          <Column
            title={Lang.get('charges.charge.description')}
            dataIndex="concept"
            key="concept"
            width={150}
            {...this.getColumnSearchProps('concept')}
          />
          <Column
            title={Lang.get('charges.charge.total')}
            dataIndex="total"
            key="total"
            width={150}
            align={'right'}
          />
          <Column
            title={Lang.get('charges.charge.bill')}
            key="factura"
            width={150}
            render={this.renderActions}
            {...this.getColumnSearchProps('status')}
            align={'center'}
          />
          {
            Gate.allows('company::invoice-download') &&
            <Column
              title={Lang.get('charges.charge.download')}
              key="download"
              width={150}
              render={this.renderActionsButton}
              align={'center'}
            />
          }
        </Table>
      </div>
    )
  }
}

const config = {
  charges: ChargesContainer
}

const mapStateToProps = (containers: any) => {
  return {
    charges: containers.charges.state
  }
}

export default connect(config, mapStateToProps)(ChargesTable);
