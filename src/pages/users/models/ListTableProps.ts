interface ListTableProps {
  data: [],
  httpBusy: boolean,
  onShow: any,
  onClose: any,
  visible: any,
  onSelect: any,
  disabledRejeted: any,
  disabledSuccess: any,
  motive: any,
  onHandleChange: any,
  success: any,
  rejected: any,
  containers: any
  nextProps?: {
    data: [],
    httpBusy: boolean,
    visible: any,
    onSelect: any,
    disabledRejeted: any,
    disabledSuccess: any,
    motive: any,
    onHandleChange: any,
    success: any,
    rejected: any,
  }
}

export default ListTableProps;