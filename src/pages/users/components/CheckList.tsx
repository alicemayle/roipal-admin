import React, { PureComponent } from 'react';
import { Popover, Icon } from 'antd';
import CheckListProps from '../models/CheckListProps'
import TimelineComponent from './Timeline';
import Lang from 'src/support/Lang';

const stepsUser = [
  {
    name: 'activation',
    description: Lang.get('users.user.activation')
  },
  {
    name: 'profile',
    description: Lang.get('users.user.profile')
  },
  {
    name: 'payment',
    description: Lang.get('users.user.payment')
  },
  {
    name: 'disc-assessment',
    description: Lang.get('users.user.disc-assessment')
  },
  {
    name: 'roipal-assessment',
    description: Lang.get('users.user.roipal-assessment')
  }
]

class CheckList extends PureComponent<CheckListProps> {
  constructor(props: CheckListProps) {
    super(props);
  }

  private renderTimeline(data: any) {
    const { type } = this.props;
    const steps = stepsUser;
    return (
      <TimelineComponent
        data={data}
        steps={steps}
        type={type} />
    );
  }

  public render() {
    const { data } = this.props;

    return (
      <Popover content={this.renderTimeline(data)} title={Lang.get('users.user.checklist')}>
        <Icon type="eye" style={{fontSize: 18, color: '#52bebb'}}  />
      </Popover>
    );
  }
}

export default CheckList;