import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Drawer, Divider, Tabs, Empty } from 'antd';
import DocumentsItem from './DocumentsItem';
import Text from 'antd/lib/typography/Text';
import Lang from 'src/support/Lang';

const { TabPane } = Tabs;

const pStyle = {
  fontSize: 25,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16,
};

class DocumentsTab extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  private renderItem(item: any, index: any) {
    const { onSelect = {} } = this.props

    return (
      <TabPane tab={item.name} key={index}>
        <DocumentsItem
          item={item}
          img={item.document_url}
          executive_uuid={onSelect.uuid}
          status={item.status}
        />
      </TabPane>
    )
  }

  public render() {
    const { visible, onClose, onSelect = {} } = this.props
    const images = onSelect.documents_profile
    let count = false

    if (onSelect.documents_profile) {
      if (onSelect.documents_profile.length > 0) {
        count = true
      }
    }

    return (
      <Drawer
        placement="right"
        width={'70%'}
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <p style={{ ...pStyle, marginBottom: 24 }}>{onSelect.name}</p>
        <Divider />
        {
          !count &&
          <Empty
            description={
              <span>
                <Text>{Lang.get('documents.document.empty_docs')}</Text>
              </span>
            }
          />
        }
        {
          count &&
          <Tabs defaultActiveKey="0" size={'small'}>
            {
              images.map(this.renderItem)
            }
          </Tabs>
        }
      </Drawer>
    )
  }
}

export default DocumentsTab;