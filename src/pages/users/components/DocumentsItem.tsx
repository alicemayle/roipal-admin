import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Radio, Button, Select, Alert, Form, message } from 'antd';
import '../docs.scss'
import Text from 'antd/lib/typography/Text';
import { connect } from 'unstated-enhancers';
import SignInContainer from 'src/auth/state/SignInContainer';
import UsersListContainer from '../containers/UsersListContainer';
import moment from 'moment';
import Lang from 'src/support/Lang';
import { statusDocuments } from '../../../support/statusDocuments';
import { Gate } from '@xaamin/guardian';

const { Option } = Select;

const title = {
  fontSize: 14,
  lineHeight: '22px',
  marginBottom: 3,
  color: 'rgba(0,0,0,0.65)',
};

const content = {
  marginRight: 8,
  display: 'inline-block',
  color: 'rgba(0,0,0,0.85)',
}

interface ListState {
  option: any,
  motive: string,
  field: any,
  success: boolean,
  rejected: boolean,
  validateRejected: any
}

class DocumentsItem extends Component<any, ListState> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    option: null,
    motive: '',
    field: null,
    success: false,
    rejected: false
  }

  private async getMotives() {
    const { users } = this.props.containers;
    await users.getMotives();
  }

  private async getStatusDocuments() {
    const { executive_uuid } = this.props
    const { users } = this.props.containers;

    await users.statusDocuments(executive_uuid)
  }

  public async componentDidMount() {
    await this.getStatusDocuments()
    this.getMotives()
  }

  private cambio(value: any) {
    const selectName = (value.target as any).value;

    this.setState({
      success: selectName === 'success' ? true : false,
      rejected: selectName === 'rejected' ? true : false,
      option: selectName
    })
  }

  private handleMotiveSelected(field: string) {
    this.setState({
      motive: field
    })
  }

  private getDate() {
    const date = new Date()
    const FormatDate = moment(new Date(date)).format('YYYY-MM-DD hh:mm:ss')

    this.documentCheck(FormatDate)
  }

  private async documentCheck(date: any) {
    if (Gate.allows('executive::document-edit')) {
      const { data = {} } = this.props.containers.signin.state
      const { users } = this.props.containers;
      const { item, executive_uuid } = this.props
      const { motive, option } = this.state;
      const user = data.profile.name

      const dataBuild = {
        executive: executive_uuid,
        uuid: item.uuid,
        name_user_check: user,
        reason_rejected: motive ? motive : '',
        date_check: date,
        document_type: item.document_type,
        document_url: item.document_url,
        status: option === 'rejected' ? -1 : 1
      }

      if (dataBuild) {
        await users.setData(dataBuild)
      }

      if (option === 'rejected') {
        this.setState({
          success: false,
          rejected: false
        })
      } else {
        this.setState({
          success: false,
          rejected: false
        })
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  public renderItem(item: any) {
    return (
      <Option key={item.id} value={item.value}>
        {item.label}
      </Option>
    )
  }

  private handleSubmit(e: any) {
    e.preventDefault();
    this.props.form.validateFields((err: any, values: any) => {
      if (!err) {
        this.getDate()
      }
    });
  };

  public render() {
    const { data = {} } = this.props.containers.signin.state
    const { httpBusy, motives } = this.props.containers.users.state;
    const user = data.profile.name
    const email = data.profile.email
    const { img, status } = this.props
    const { getFieldDecorator } = this.props.form;
    const { success, rejected } = this.state

    return (
      <div className="documents">
        <div className="box" style={{ overflow: 'scroll' }}>
          <img
            alt="example"
            src={img}
          />
        </div>
        {
          status === statusDocuments.MISSION_ON_HOLD &&
          Gate.allows('executive::document-edit') &&
          <div>
            <Radio.Group
              buttonStyle="solid"
              style={{ marginTop: 20, marginBottom: 25 }}
              onChange={this.cambio}
              defaultValue={null}
            >
              <Radio.Button value="success">
                {Lang.get('documents.document.approve_document')}
              </Radio.Button>
              <Radio.Button value="rejected">
                {Lang.get('documents.document.reject_document')}
              </Radio.Button>
            </Radio.Group>
          </div>
        }
        <br />
        {
          status === statusDocuments.MISSION_APPROVED &&
          <Alert
            message={Lang.get('documents.document.info')}
            type="success"
            showIcon={true} />
        }
        {
          success &&
          <div>
            <div style={{ ...title }}>
              <p style={{ ...content }}>{Lang.get('documents.document.reviewed')}</p>{user}
            </div>
            <div style={{ ...title }}>
              <p style={{ ...content }}>{Lang.get('documents.document.contact')}</p>{email}
            </div>
            <Button
              onClick={this.getDate}
              type="primary"
              loading={httpBusy}
            >
              <Text style={{ color: 'white' }}>{Lang.get('documents.button.save')}</Text>
            </Button>
          </div>
        }
        {
          status === statusDocuments.MISSION_REJECTED &&
          <Alert
            message={Lang.get('documents.document.info_rejected')}
            type="info"
            showIcon={true} />
        }
        {
          rejected &&
          Gate.allows('executive::document-edit') &&
          <div>
            <div style={{ ...title }}>
              <p style={{ ...content }}>{Lang.get('documents.document.reviewed')}</p>{user}
            </div>
            <div style={{ ...title }}>
              <p style={{ ...content }}>{Lang.get('documents.document.contact')}</p>{email}
            </div>
            <Form className="login-form"
              onSubmit={this.handleSubmit}>
              <Form.Item>
                {
                  getFieldDecorator('motive', {
                    rules: [{
                      required: true, message: Lang.get('documents.document.info_motive')
                    }],
                  })(
                    <Select
                      showSearch={true}
                      style={{ width: 500 }}
                      placeholder={Lang.get('documents.document.reason_rejection')}
                      onChange={this.handleMotiveSelected}
                    >
                      {
                        motives.map(this.renderItem)
                      }
                    </Select>
                  )
                }
              </Form.Item>
              <Form.Item>
                <Button
                  style={{ backgroundColor: '#cc3300', marginTop: 20, borderColor: '#cc3300' }}
                  type="primary"
                  htmlType="submit"
                  loading={httpBusy}
                >
                  <Text style={{ color: '#e6e6e6' }}>{Lang.get('documents.button.save')}</Text>
                </Button>
              </Form.Item>
            </Form>
          </div>
        }
      </div>
    )
  }
}

const container = {
  signin: SignInContainer,
  users: UsersListContainer
}

const mapStateToProps = (containers: any) => {
  return {
    signin: containers.signin.state,
    users: containers.users.state
  }
}

const WrappedDocumentsItem = Form.create()(DocumentsItem);

export default connect(container, mapStateToProps)(WrappedDocumentsItem);