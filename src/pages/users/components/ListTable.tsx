import React, { Component } from 'react';
import { Table, Icon, Spin } from 'antd';
import autobind from 'class-autobind';
import ListTableProps from '../models/ListTableProps';
import CheckList from './CheckList';
import FilterSearch from '../../../components/search/FilterSearch';
import DocumentsTab from './DocumentsTab';
import Pagination from 'src/components/pagination/Pagination';
import { connect } from 'unstated-enhancers';
import UsersListContainer from '../containers/UsersListContainer'
import Lang from 'src/support/Lang';
import SignInContainer from 'src/auth/state/SignInContainer';
import { statusDocuments } from '../../../support/statusDocuments';
import IconButton from 'src/components/buttons/IconButton';

const { Column } = Table;

interface FilterDropdDownInterface {
  setSelectedKeys: (selected: string[]) => {},
  selectedKeys: number[],
  clearFilters: () => {},
  confirm: any,
}

interface ListState {
  searchText: any,
  limit: number,
  oldDocs: any,
  page: number
}

class ListTable extends Component<ListTableProps, ListState> {
  public searchInput: any = null;
  constructor(props: ListTableProps) {
    super(props);
    autobind(this);
  }

  public state: ListState = {
    searchText: '',
    limit: 50,
    oldDocs: [],
    page: 1
  }

  private renderColumn(record: {}, data: { classification: string }) {
    return (
      <CheckList
        data={record}
        type={data.classification} />
    );
  }

  private renderActions(text: any) {
    const { onShow } = this.props;

    return (
      <IconButton
        item={text}
        onFuction={onShow}
        size={18}
        colorIcon={'#ff9900'}
        name={'folder-open'}
      />
    );
  }

  private setSearchInput(node: any) {
    this.searchInput = node;
  }

  private getColumnSearchProps = (dataIndex: any) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterDropdDownInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: any) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: any, record: any) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    }
  })

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  public async handleFetch(limit?: number, page?: number) {
    const { users } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    await users.fetch(meta);

    const { data } = this.props.containers.users.state;

    this.setState({
      oldDocs: data
    })

  }

  public componentDidMount() {
    this.handleFetch()
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  private async userVerification(event: any) {
    const { dataDocument } = this.props.containers.users.state;
    const { users } = this.props.containers;
    const id = (event.target as any).id;

    const newArray = dataDocument.filter((fil: any) => fil.executive === id)

    if (newArray.length > 0) {
      const hold = newArray.filter((item: any) => item.status === statusDocuments.MISSION_ON_HOLD)
      const rejected = newArray.filter((item: any) =>
        item.status === statusDocuments.MISSION_REJECTED)

      if (hold.length === 0 && rejected.length === 0) {
        const newData = {
          documents: newArray
        }

        await users.update(id, newData)

        const { userVerification, error } = users.state;

        if (!error) {
          const docsRejected = userVerification.find((rec: any) => rec.status === -1)
          const docUpdate = userVerification.find((upd: any) => upd.status === 0)

          if (docsRejected === undefined && docUpdate === undefined) {
            const data = {
              verification_status: 1
            }
            await users.updateExecutive(id, data)

            if (!error) {
              await users.setDataUpdate(id)
            }
          }
        }
      } else {
        const newData = {
          documents: newArray
        }
        await users.update(id, newData)

        const { error, userVerification } = users.state;

        if (!error) {
          const docsVerify = userVerification.filter((item: any) =>
            item.status !== statusDocuments.MISSION_ON_HOLD)
          const _dataDocs = dataDocument;

          docsVerify.map((item: any) => {
            const index = dataDocument.findIndex((doc: any) => doc.uuid === item.uuid)

            if (index >= 0) {
              _dataDocs.splice(index, 1)
            }
          })
          users.setDocsUpdate(_dataDocs)
        }
      }
    }
  }

  private renderActionsButton(item: any) {
    const { dataDocument, userBusy } = this.props.containers.users.state;

    const newArray = dataDocument.filter((fil: any) => fil.executive === item.uuid)
    const userStatus = item.profile.verification_status
    const long = newArray.length

    return (
      <div>
        <div>
          {
            long > 0 && userStatus !== 1 &&
            <span>
              <a id={item.uuid} onClick={this.userVerification}>
                {Lang.get('documents.document.submit_review')}
              </a>
              {userBusy &&
                <Spin size="small" />
              }
            </span>
          }
        </div>
        {userStatus === 1 &&
          <div>
            <Icon type="check-circle" style={{ color: 'green', fontSize: 15 }} />
          </div>
        }
        {
          userStatus !== 1 && long === 0 &&
          <div>
            <Icon type="close-circle" style={{ color: 'red', fontSize: 15 }} />
          </div>
        }
      </div>
    );
  }

  public render() {
    const { httpBusy, visible, onClose, onSelect } = this.props;
    const { limit } = this.state
    const { data, meta } = this.props.containers.users.state

    return (
      <div>
        <Table
          dataSource={data}
          loading={httpBusy}
          pagination={false}
          scroll={{ x: 420 }}
          className="users-table"
        >
          <Column
            title={Lang.get('users.user.date')}
            dataIndex="created_at"
            key="created_at"
            align={'center'}
          />
          <Column
            title={Lang.get('users.user.name')}
            dataIndex="name"
            key="uuid"
            {...this.getColumnSearchProps('name')}
          />
          <Column
            title={Lang.get('users.user.email')}
            dataIndex="email"
            {...this.getColumnSearchProps('email')}
          />
          <Column
            title={Lang.get('users.user.checklist')}
            dataIndex="checklist"
            render={this.renderColumn}
            align={'center'}
          />
          <Column
            title={Lang.get('users.user.document_status')}
            dataIndex="verification_executive"
            key="verification_executive"
            align={'center'}
          />
          <Column
            title={Lang.get('users.user.documents')}
            dataIndex=""
            key="0"
            render={this.renderActions}
            align={'center'}
          />
          <Column
            title={Lang.get('users.user.verify')}
            key="1"
            render={this.renderActionsButton}
            align={'center'}
          />
        </Table>
        {
          meta &&
          <Pagination
            limit={limit}
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
          />
        }
        <DocumentsTab
          visible={visible}
          onClose={onClose}
          onSelect={onSelect}
        />
      </div>
    );
  }
}

const container = {
  users: UsersListContainer,
  signin: SignInContainer
}

const mapStateToProps = (containers: any) => {
  return {
    users: containers.users.state,
    signin: containers.signin.state
  }
}

export default connect(container, mapStateToProps)(ListTable);