import React, { PureComponent } from 'react';
import { Timeline, Icon } from 'antd';
import TimelineProps from '../models/TimelineProps';
import Autobind from 'class-autobind';

class TimelineComponent extends PureComponent<TimelineProps> {
  public steps: any;
  constructor(props: TimelineProps) {
    super(props);
    Autobind(this);
  }

  private reset() {
    this.steps = this.props.steps;
    this.steps.map((step: any) => {
      step.status = false;
    })
  }

  private renderTimeLine(steps: any, index: number) {
    const { data } = this.props;
    data.map((step: any) => {
      if (step.step === steps.name) {
        this.steps[index].status = true;
      }
    })

    const icon = this.steps[index].status ? 'check-circle' : 'close-circle'

    return (
      <Timeline.Item
        key={index}
        dot={<Icon type={icon} style={{ fontSize: '13px' }} />}
        color={this.steps[index].status ? 'green' : 'red'}>
        {this.steps[index].description}
      </Timeline.Item>
    )
  }

  public render() {
    this.reset()
    const { data, steps } = this.props;

    return (
      <Timeline>
        {data && steps.map(this.renderTimeLine)}
      </Timeline>
    );
  }
}

export default TimelineComponent;