import { Container } from 'unstated';
import Users from '../services/Users';
import Lang from 'src/support/Lang';
import moment from 'moment';

interface UsersList {
  httpBusy: boolean,
  motiveBusy: boolean,
  data: any,
  error: any,
  message: string,
  __action?: string,
  meta: any,
  userVerification: any,
  motives: any,
  dataDocument: any,
  user_document: any,
  statusDocuments: any,
  updateExecutive: any,
  userBusy: boolean
}

class UsersListContainer extends Container<UsersList> {
  public state: UsersList = {
    httpBusy: false,
    motiveBusy: false,
    data: null,
    error: null,
    message: '',
    meta: null,
    userVerification: '',
    motives: '',
    dataDocument: [],
    user_document: '',
    statusDocuments: null,
    updateExecutive: [],
    userBusy: false
  }

  public async fetch(params: {}) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'USERS LIST STARTS'
      });

      const response = await Users.list(params);

      response.data.map((user: any, index: number) => {
        response.data[index].key = user.uuid;
        response.data[index].verification_executive =
          user.profile.verification_status === 1 ?
            Lang.get('documents.document.verified_user')
            : Lang.get('documents.document.missing_review')
        response.data[index].created_at =
          moment(new Date(user.created_at)).format('DD-MM-YYYY')
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        meta: response.meta,
        __action: 'USERS LIST SUCCESS'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'USERS LIST ERROR'
      })
    }
  }

  public async update(uuid: any, data: any) {
    try {
      this.setState({
        userBusy: true,
        __action: 'Documents Success STARTS'
      });

      const response = await Users.UpdateDocumento(uuid, data);

      this.setState({
        userBusy: false,
        userVerification: response.data,
        __action: 'Documents Success SUCCESS'
      });
    } catch (error) {
      this.setState({
        userBusy: false,
        error,
        __action: 'Documents Success ERROR'
      })
    }
  }

  public async statusDocuments(uuid: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Documents status Success STARTS'
      });

      const response = await Users.StatusDcument(uuid);

      this.setState({
        httpBusy: false,
        statusDocuments: response.data,
        __action: 'Documents status Success SUCCESS'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Documents status Success ERROR'
      })
    }
  }

  public async getMotives() {
    try {
      this.setState({
        motiveBusy: true,
        __action: 'Get Motives STARTS'
      });

      const response = await Users.getMotives();

      const data = response.data.map((item: any, index: any) => {
        const newData = {
          id: index,
          value: item,
          label: item
        }
        return newData
      })

      this.setState({
        motiveBusy: false,
        motives: data,
        __action: 'Get Motives SUCCESS'
      });
    } catch (error) {
      this.setState({
        motiveBusy: false,
        error,
        __action: 'Get Motives ERROR'
      })
    }
  }

  public async updateExecutive(uuid: any, data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update profile executive STARTS'
      });

      const response = await Users.UpdateExecutive(uuid, data);

      this.setState({
        httpBusy: false,
        updateExecutive: response.data,
        __action: 'Update profile executive SUCCESS'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'Update profile executive ERROR'
      })
    }
  }

  public async setData(data: any) {
    const newData = this.state.dataDocument;
    const originalData = this.state.data;

    const doc = await newData.find((pol: any) => pol.uuid === data.uuid);
    const index = newData.indexOf(doc);

    if (index !== -1) {
      newData[index] = data
    } else {
      newData.push(data)
    }

    const executive = await originalData.find((pol: any) => pol.uuid === data.executive);
    const originalExecutiveIndex = originalData.indexOf(executive);

    const originalDoc = await executive.documents_profile.find(
      (item: any) => item.uuid === data.uuid
    )
    const originalDocIndex = executive.documents_profile.indexOf(originalDoc)

    if (originalDocIndex !== -1) {
      executive.documents_profile[originalDocIndex] = {
        ...executive.documents_profile[originalDocIndex],
        status: data.status
      }
      originalData[originalExecutiveIndex] = executive
    }

    this.setState({
      dataDocument: newData,
      data: originalData
    })
  }

  public setDataUpdate(id: any) {
    const dataOriginal = this.state.data

    dataOriginal.map((find: any) => {
      if (find.key === id) {
        find.profile.verification_status = 1;
        find.verification_executive = Lang.get('documents.document.verified_user')
      }
      return find;
    });

    this.setState({
      data: dataOriginal
    })
  }

  public async setDocsUpdate(data: any) {
    this.setState({
      dataDocument: data
    })
  }

  public async setNewDocs(newDocs: any) {
    this.setState({
      data: newDocs
    })
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default UsersListContainer;
