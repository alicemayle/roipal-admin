import { Container } from 'unstated';
import UserService from '../services/UsersService';

interface UserState {
    httpBusy: boolean,
    data: any,
    error: any,
    message: string,
    __action?: string
}

const initialState = {
    httpBusy: true,
    data: null,
    error: null,
    message: ''
}

class UsersListContainer extends Container<UserState> {
    public state: UserState = {
        ...initialState
    }

    public async fetch(params:any) {
        try {
            this.setState({
                httpBusy: true,
                __action: 'Users list starts'
            });

            const response = await UserService.paginate(params);

            this.setState({
                httpBusy: false,
                ...response.data,
                __action: 'Users list success'
            });
        } catch (error) {
            this.setState({
                httpBusy: false,
                error,
                __action: 'Users list error'
            });
        }
    }

    public reset() {
        this.setState({
            ...initialState,
            __action: 'Users list reset'
        });
    }

    public clearError() {
        this.setState({
            ...this.state,
            message: '',
            error: null,
            __action: 'Users list clear error'
        })
    }
}

export default UsersListContainer;