import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import UsersListContainer from './containers/UsersListContainer';
import ListTable from './components/ListTable';
import ListProps from './models/ListProps';
import autobind from 'class-autobind';
import signinContainer from '../../auth/state/SignInContainer';
import ListState from '../users/models/ListState'
import Lang from 'src/support/Lang';
import { Divider } from 'antd';

class List extends Component<ListProps, ListState> {

  constructor(props: ListProps) {
    super(props);
    autobind(this)
  }

  public state: any = {
    visible: false,
    ExecutiveSelected: []
  }

  private async showDocumentsDetails(documents: any) {
    const { data } = this.props.containers.users.state;

    const ItemSelected = await data.find((item: any) => item.key === documents.key);

    this.setState({
      ExecutiveSelected: ItemSelected,
    })

    this.showDrawer()
  }

  private showDrawer() {
    this.setState({
      visible: true,
    });
  };

  private closeDrawer() {
    this.setState({
      visible: false,
    });
  };

  public shouldComponentUpdate(nextProps: ListProps, nextState: ListState) {
    return (nextProps.users.data !== this.props.users.data
      || nextProps.users.httpBusy !== this.props.users.httpBusy
      || nextState.ExecutiveSelected !== this.state.ExecutiveSelected
      || nextState.visible !== this.state.visible
      || nextState.selected !== this.state.selected)
  }

  public render() {
    const { httpBusy } = this.props.users;
    return (
      <div>
        <h1>{Lang.get('users.user.title_menu')}</h1>
        <Divider />
        <ListTable
          httpBusy={httpBusy}
          onShow={this.showDocumentsDetails}
          onClose={this.closeDrawer}
          visible={this.state.visible}
          onSelect={this.state.ExecutiveSelected}
        />
      </div>
    )
  }
}

const container = {
  users: UsersListContainer,
  signin: signinContainer
}

const mapStateToProps = (containers: any) => {
  return {
    users: containers.users.state,
    signin: containers.signin.state
  }
}

export default connect(container, mapStateToProps)(List);
