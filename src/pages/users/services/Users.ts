import { Http } from '../../../shared';

class Users {

  public list(params: {}): any {
    return Http.get(`/api/executives?includes=checklist,documents_profile,profile`, { params })
  }

  public UpdateDocumento(uuid: string, data: any) {
    return Http.put(`/api/executives/${uuid}/documentation`, data)
  }

  public getMotives() {
    return Http.get(`/api/translations/catalogs?namespace=catalogs&
                                group=documentation_reason_rejected`)
  }

  public UpdateExecutive(uuid: string, data: any) {
    return Http.put(`/api/executives/${uuid}`, data)
  }

  public StatusDcument(uuid: string) {
    return Http.get(`/api/executives/${uuid}/documentation`)
  }
}

export default new Users;