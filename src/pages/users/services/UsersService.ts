import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class UsersService {

    public paginate(params: any): Promise<AxiosResponse> {
        return Http.get(`api/users`,{params});
    }
}

export default new UsersService;