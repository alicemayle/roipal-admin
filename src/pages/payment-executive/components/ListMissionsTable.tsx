import React, { Component } from 'react';
import autobind from 'class-autobind';
import PaymentProps from '../models/PaymentProps'
import { Table, Icon } from 'antd';
import Lang from '../../../support/Lang'
import FilterInterface from 'src/components/search/FilterInterface';
import FilterSearch from 'src/components/search/FilterSearch';
import { statusPayment } from '../../../support/PaymentConstants'
import { Gate } from '@xaamin/guardian';

const { Column } = Table;

interface ListState {
  mission: any,
  searchText: string,
  fields: any,
  selectedRows: any
}

class ListMissionsTable extends Component<PaymentProps, ListState> {
  public searchInput: any = null;
  public searchField: string = '';

  constructor(props: PaymentProps) {
    super(props);
    autobind(this)
  }

  public state: any = {
    mission: [],
    searchText: '',
    fields: [],
    selectedRows: []
  }

  private async getData() {
    const { data } = this.props;

    const newData: {} = await data.map((item: any) => {
      const transformer = {
        key: item.uuid,
        name: item.mission_name,
        business: item.business_name,
        numStatus: item.status,
        status: item.status === 0 ? Lang.get('payments.payment.pending')
          : item.status === 1 ? Lang.get('payments.payment.process')
            : Lang.get('payments.payment.approved'),
        formatcost: item.symbol + ' ' + item.total + ' ' + item.currency,
        totalcost: item.total,
        concept: item.reason_payment === 1 ? Lang.get('payments.payment.providers')
          : Lang.get('payments.payment.bond')
      }
      return transformer
    })

    this.setState({
      mission: newData
    })
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  });

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  public componentDidMount() {
    this.getData()
  }

  public componentWillReceiveProps() {
    this.getData()
  }

  private onSelectChange(selectedRows: any) {
    const { OpenUpsert } = this.props;

    this.setState({
      selectedRows
    });

    setTimeout(() => {
      OpenUpsert(this.state.selectedRows)
    }, 500)
  }

  public render() {

    const rowSelection = {
      onChange: (selectedRowKeys: any, selectedRows: any) => {
        this.onSelectChange(selectedRows)
      },
      getCheckboxProps: (record: any) => ({
        disabled: record.numStatus === statusPayment.MISSION_APPROVED,
      }),
    };

    return (
      <div>
        <Table
          dataSource={this.state.mission}
          size={'middle'}
          rowSelection={ Gate.allows('executive::payment-edit')
            ? rowSelection
            : undefined
          }
        >
          <Column
            title={Lang.get('payments.payment.company')}
            dataIndex="business"
            key="business"
            width={50}
            {...this.getColumnSearchProps('business')}
          />
          <Column
            title={Lang.get('payments.payment.mision')}
            dataIndex="name"
            key="name"
            width={50}
            {...this.getColumnSearchProps('name')}
          />
          <Column
            title={Lang.get('payments.payment.concept')}
            dataIndex="concept"
            key="concept"
            width={50}
            align={'center'}
          />
          <Column
            title={Lang.get('payments.payment.total')}
            dataIndex="formatcost"
            key="totalcost"
            width={50}
            align={'right'}
          />
          <Column
            title={Lang.get('payments.payment.status')}
            dataIndex="status"
            key="status"
            width={50}
            {...this.getColumnSearchProps('status')}
            align={'center'}
          />
        </Table>
      </div>
    )
  }
}


export default ListMissionsTable;