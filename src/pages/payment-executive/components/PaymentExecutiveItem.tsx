import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Drawer, Row, Col, Divider, Button, message } from 'antd';
import PaymentExecutiveDesing from '../components/PaymentExecutiveDesing'
import ListMissionsTable from '../components/ListMissionsTable';
import Lang from '../../../support/Lang'
import Text from 'antd/lib/typography/Text';
import { connect } from 'unstated-enhancers';
import PaymentExecutiveContainer from '../state/PaymentExecutiveContainer';
import { Gate } from '@xaamin/guardian';

const pStyle = {
  fontSize: 18,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16,
};

interface ListState {
  paymentsSuccess: any,
}

class PaymentExecutiveItem extends Component<any, ListState> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    paymentsSuccess: [],
  }

  private async handleOpenUpsert(value: any) {
    const data = value.map((item: any) => {
      const newData = {
        uuid: item.key
      }
      return newData
    })

    this.setState({
      paymentsSuccess: data
    })
  }

  private async changeStatusMission() {
    if(Gate.allows('executive::payment-edit')) {
      const { onSelect } = this.props
      const payment = this.props.containers.payment;
      const data = this.state.paymentsSuccess

      const list = {
        payments_uuid: data
      }

      if (data.length > 0) {
        await payment.updateStatus(list, onSelect.uuid)
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  public render() {

    const { visible, onClose, onSelect } = this.props
    const { updateBusy } = this.props.containers.payment.state;
    const { paymentsSuccess } = this.state

    return (
      <Drawer
        placement="right"
        width={'55%'}
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <p style={{ ...pStyle, marginBottom: 24 }}>{Lang.get('payments.payment.profile')}</p>
        <Divider />
        <p style={pStyle}>{Lang.get('payments.payment.personal')}</p>
        <Row>
          <Col span={12}>
            <PaymentExecutiveDesing
              title={Lang.get('payments.payment.name')}
              content={onSelect.name}
            />
          </Col>
          <Col span={12}>
            <PaymentExecutiveDesing
              title={Lang.get('payments.payment.countmision')}
              content={onSelect.misions}
            />
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <PaymentExecutiveDesing
              title={Lang.get('payments.payment.email')}
              content={onSelect.email}
            />
          </Col>
          <Col span={12}>
            <PaymentExecutiveDesing
              title={Lang.get('payments.payment.total')}
              content={onSelect.formatcost}
            />
          </Col>
        </Row>
        <Divider />
        <div style={{
          display: 'flex',
          'justifyContent': 'space-between',
          marginBottom: 15
        }}>
          <Text style={{ color: 'rgba(0,0,0,0.85)', fontSize: 18 }}>
            {Lang.get('payments.payment.misions')}
          </Text>
          {
            paymentsSuccess.length > 0 &&
            Gate.allows('executive::payment-edit') &&
            <Button
              type="primary"
              onClick={this.changeStatusMission}
              loading={updateBusy}
            >
              <Text style={{ color: 'white' }}>
                {Lang.get('payments.payment.paid')}
              </Text>
            </Button>
          }
        </div>
        <ListMissionsTable
          data={onSelect.missionList}
          executiveUuid={onSelect.uuid}
          OpenUpsert={this.handleOpenUpsert}
        />
      </Drawer>
    )
  }
}

const container = {
  payment: PaymentExecutiveContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    payment: containers.payment.state,
  }
}

export default connect(container, mapStateToProps)(PaymentExecutiveItem);
