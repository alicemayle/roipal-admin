import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Table, Icon } from 'antd';
import PaymentExecutiveItem from './PaymentExecutiveItem';
import Lang from '../../../support/Lang'
import FilterSearch from 'src/components/search/FilterSearch';
import FilterDropdDownInterface from 'src/components/search/FilterDropDownInterface';
import IconButton from 'src/components/buttons/IconButton';
import { Gate } from '@xaamin/guardian';

const { Column } = Table;

class PaymentExecutiveList extends Component<any> {
  public searchField: string = '';
  public searchInput: any = null;

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    searchText: '',
    fields: [],
    selectedRows: []
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterDropdDownInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  })

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private renderActions(text: any) {
    const { onShow } = this.props;

    return (
      <IconButton
      item={text}
      onFuction={onShow}
      size={18}
      colorIcon={'#52bebb'}
      name={'info-circle'}
    />
    );
  }

  private onSelectChange(selectedRows: any) {
    const { onOpenUpsert } = this.props;

    this.setState({
      selectedRows
    });

    setTimeout(() => {
      onOpenUpsert(this.state.selectedRows)
    }, 500)
  }

  public render() {

    const { data, visible, onClose, onSelect, load } = this.props;

    const rowSelection = {
      onChange: (selectedRowKeys: any, selectedRows: any) => {
        this.onSelectChange(selectedRows)
      },
      getCheckboxProps: (record: any) => ({
        disabled: record.totalcost === 0,
      }),
    };

    return (
      <div>
        <Table
          dataSource={data}
          scroll={{ x: 800 }}
          loading={load}
          rowSelection={ Gate.allows('executive::payment-download') ?
            rowSelection
            : undefined
          }
        >
          <Column
            title={Lang.get('payments.payment.providers')}
            dataIndex="name"
            key="name"
            width={150}
            {...this.getColumnSearchProps('name')}
            align={'center'}
          />
          <Column
            title={Lang.get('payments.payment.email')}
            dataIndex="email"
            key="email"
            width={150}
            {...this.getColumnSearchProps('email')}
          />
          <Column
            title={Lang.get('payments.payment.countmision')}
            dataIndex="misions"
            key="1"
            width={150}
            align={'center'}
          />
          <Column
            title={Lang.get('payments.payment.total')}
            dataIndex="formatcost"
            key="2"
            width={150}
            align={'right'}
          />
          <Column
            title={Lang.get('generic.button.details')}
            key="3"
            width={150}
            render={this.renderActions}
            align={'center'}
          />
        </Table>
        <PaymentExecutiveItem visible={visible} onClose={onClose} onSelect={onSelect} />
      </div>
    )
  }
}


export default PaymentExecutiveList;