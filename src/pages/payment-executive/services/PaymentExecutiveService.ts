import { Http } from '../../../shared';
import { AxiosResponse } from 'axios';

class Payment {

  public list(): Promise<AxiosResponse> {
    return Http.get(`/api/payment/executive/missions`)
  }

  public export(data: any): Promise<AxiosResponse> {
    return Http.post(`api/export`, data);
  }

  public updatePayment(data: any, uuid: any) {
    return Http.put(`/api/executives/${uuid}/payment-status`, data);
  }

}

export default new Payment;