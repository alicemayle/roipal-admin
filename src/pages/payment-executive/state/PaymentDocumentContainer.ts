import { Container } from 'unstated';
import PaymentState from '../models/PaymentState';
import Payment from '../services/PaymentExecutiveService';
class PaymentDocumentContainer extends Container<PaymentState> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
  }

  public name: string = 'PaymentDocument Container';

  public async export(data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Charges excel Starts'
      })

      const response = await Payment.export(data);

      this.setState({
        httpBusy: false,
        data: response.data.url,
        __action: 'Charges excel Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Charges excel Error'
      })
    }
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default PaymentDocumentContainer;