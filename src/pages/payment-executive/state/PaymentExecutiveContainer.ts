import { Container } from 'unstated';
import PaymentState from '../models/PaymentState';
import Payment from '../services/PaymentExecutiveService';
import {Logger} from 'unstated-enhancers';
import Event from 'src/support/Event';

class PaymentExecutiveContainer extends Container<PaymentState> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
    updateBusy: false,
  }

  public name: string = 'PaymenExecutive Container';

  public async fetch() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get List Paymen Executives starts'
      });

      const response = await Payment.list();

      let symbolMoney: any
      let symbolCurrency: any

      const newResponse = response.data.map((item: any) => {
        if(!symbolMoney && !symbolCurrency) {
          const _mission = item.missions.find((mision: any) => mision.currency !== null)
          if (_mission) {
            symbolCurrency = _mission.currency;
            symbolMoney = _mission.currency
          }
        }

        const total = parseFloat(item.total.toFixed(2))
        const data = {
          key: item.number,
          name: item.name_executive,
          email: item.email,
          misions: item.missions.length,
          totalcost: total,
          formatcost: '$ ' + total + ' MXN',
          missionList: item.missions,
          uuid: item.uuid,
          // currency: symbolCurrency,
          // symbol: symbolMoney
        }
        return data
      })

      this.setState({
        httpBusy: false,
        data: newResponse,
        __action: 'Get List Paymen Executives success'
      });

    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Get List Paymen Executives error'
      });
    }
  }

  public async updateStatus(data: any, uuid: any) {
    let result;

    try {
      this.setState({
        updateBusy: true,
        __action: 'Update status missions Executives starts'
      });

      const response = await Payment.updatePayment(data, uuid);

      Logger.dispatch('data del response update container', response)

      this.setState({
        updateBusy: false,
        __action: 'Update status missions Executives success'
      });
      result = {
        ...response,
        action: 'Change status mission success'
      }
    } catch (error) {
      this.setState({
        updateBusy: false,
        error,
        message: error.message,
        __action: 'Update status missions Executives error'
      });
      result = {
        error,
        action: 'Change status mission error'
      }
    }
    Event.notify('mission::pay', result);
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default PaymentExecutiveContainer;