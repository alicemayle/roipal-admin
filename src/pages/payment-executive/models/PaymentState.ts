import { ListState } from '../../../roipal/models';

interface PaymentState extends ListState {
  __action?: string,
  dataCurrency: [],
  updateBusy: any
}

export default PaymentState;