import React, { Component } from 'react';
import { Typography, Divider, Button, message } from 'antd';
import PaymentExecutiveList from './components/PaymentExecutiveList'
import autobind from 'class-autobind';
import paymentContainer from '../payment-executive/state/PaymentExecutiveContainer'
import paymentDocument from '../payment-executive/state/PaymentDocumentContainer'
import { connect, Logger } from 'unstated-enhancers';
import Lang from '../../support/Lang';
import { status } from '../../support/MissionConstants'
import { Gate } from '@xaamin/guardian';
import Event from 'src/support/Event';
import Notifications from 'src/components/notifications/notifications';

const { Text } = Typography;

interface ListState {
  visible: boolean
  containers: any
  selected: any
}

class List extends Component<ListState> {

  constructor(props: any) {
    super(props);
    autobind(this)
    Event.listen('mission::pay', this.listeForUpsert);
  }

  public state: any = {
    visible: false,
    executiveSelected: [],
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    Logger.dispatch('entroa la funcion listeForUpsert')
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }
      this.closeDrawer()
      this.handleOnComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.handleOnComplete('error');
    }

    notification.openNotification(msj)
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.handleFetch();
    }
  }


  private async showAssestmentDetails(payment: any) {
    const { data } = this.props.containers.payment.state;

    const itemSelected = await data.find((item: any) => item.key === payment.key);

    this.setState({
      executiveSelected: itemSelected
    })

    this.showDrawer()
  }

  private showDrawer() {
    this.setState({
      visible: true,
    });
  };

  private closeDrawer() {
    this.setState({
      visible: false,
    });
  };

  private async handleOpenUpsert(value: any) {
    const data = value.map((item: any) => {
      const missionsUuids = item.missionList.map((mission: any) => {
        if (mission.status !== status.MISSION_APPROVED) {
          const missionUuid = mission.uuid
          return missionUuid;
        }
      })

      const filter = missionsUuids.filter((fil: any) => fil !== undefined)

      const newData = {
        executive_uuid: item.uuid,
        missions_uuids: filter,
        total: item.totalcost
      }
      return newData
    })

    const list = {
      executive_payment_list: [
        ...data
      ]
    }

    const count = data.length

    this.setState({
      payments: list,
      executiveSum: count
    })
  }

  private async dowload() {
    if (Gate.allows('executive::payment-download')) {
      const document = this.props.containers.document;

      await document.export(this.state.payments)

      const url = document.state.data;

      window.open(url)
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  public async handleFetch() {
    const { payment } = this.props.containers;

    await payment.fetch()

    if (payment.state.error) {
      Logger.dispatch('Error al obtener la lista de pagos')
    }
  }

  public async componentDidMount() {
    this.handleFetch()
  }

  public componentWillUnmount() {
    Event.remove('mission::pay', this.listeForUpsert);
  }


  public render() {
    const payment = this.props.containers.payment.state;
    const document = this.props.containers.document.state;
    const { executiveSum } = this.state

    return (
      <div>
        <h1>{Lang.get('payments.payment.title')}</h1>
        <Divider />
        <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
          <Text>{}</Text>
          {
            executiveSum > 0 &&
            Gate.allows('executive::payment-download') &&
            <Button
              type="primary"
              icon="download"
              loading={document.httpBusy}
              onClick={this.dowload}
            >
              {Lang.get('payments.payment.download')}
            </Button>
          }
        </div>
        <br />
        <div>
          <PaymentExecutiveList
            data={payment.data}
            onShow={this.showAssestmentDetails}
            onClose={this.closeDrawer}
            visible={this.state.visible}
            onSelect={this.state.executiveSelected}
            load={payment.httpBusy}
            onOpenUpsert={this.handleOpenUpsert}
          />
        </div>
      </div>
    )
  }
}

const container = {
  payment: paymentContainer,
  document: paymentDocument,
}

const mapStateToProps = (containers: any) => {
  return {
    payment: containers.payment.state,
    document: containers.document.state,
  }
}

export default connect(container, mapStateToProps)(List);