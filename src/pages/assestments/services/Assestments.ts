import { Http } from '../../../shared'
import { AxiosResponse } from 'axios';

class Assestments {

  public paginate(type: string, params: any = {}): Promise<AxiosResponse> {
    return Http.get(`api/assestments/${type}/evaluations`, { params })
  }

  public info(uuid: string, params: any = {}): Promise<AxiosResponse> {
    return Http.get(`api/evaluations/${uuid}`, { params })
  }

}

export default new Assestments;