import { ItemState } from '../../../roipal/models';

interface AssestmentState extends ItemState {
  modalVisible: boolean,
  evaluation: any,
  __action?: string
}

export default AssestmentState;