import React, { Component, MouseEvent } from 'react';
import { Table, message } from 'antd';
import { connect } from 'unstated-enhancers';

import './assestments.scss';
import AssestmentsContainer from './state/AssestmentsContainer';
import AssestmentsListState from './models/AssestmentsListState';
import Detail from './Detail';
import AssestmentContainer from './state/AssestmentContainer';

const { Column } = Table;

interface ListProps extends AssestmentsListState {
  containers: any
}

class List extends Component<ListProps> {

  public state: any = {
    visible: false
  }

  constructor(props: any) {
    super(props);

    this.renderActions = this.renderActions.bind(this);
    this.showAssestmentDetails = this.showAssestmentDetails.bind(this);
    this.renderName = this.renderName.bind(this);
  }

  private async showAssestmentDetails(event: MouseEvent) {
    const { assestment } = this.props.containers;
    const id = (event.target as any).id;

    const evaluation = this.props.data.find((item: any) => item.uuid === id)

    const meta = {
      includes: 'answers'
    }

    await assestment.info(id, meta, evaluation);

    const { error } = this.props.containers.assestment.state;

    if (error) {
      message.error(error.message);

      assestment.reset();
    }
  }

  private renderName(field: string) {
    return (text: any, record: any, index: number): React.ReactNode => {
      return (
        <span>{record[field].name}</span>
      )
    }
  }

  private renderActions(text: any, record: any) {
    return (
      <span>
        <a href="javascript:;" id={text.uuid} onClick={this.showAssestmentDetails}>View</a>
      </span>
    );
  }

  public componentDidMount() {
    const { assestments } = this.props.containers;

    const meta = {
      includes: 'seller,company'
    }

    assestments.fetch('disc', meta)
  }

  public render() {
    const { data, httpBusy } = this.props;

    return (
      <div className="AssestmentsList">
        <Table
          dataSource={data}
          loading={httpBusy}
          scroll={{ x: 1000, y: 300 }}
        >
          {/*           <Column
            title="Company"
            dataIndex="company"
            key="company"
            render={this.renderName('company')}
          />
          <Column
            title="Type"
            dataIndex="name"
            key="name"
            width={100}
          />
          <Column
            title="User"
            dataIndex="user"
            key="user"
            width={200}
            render={this.renderName('seller')}
          /> */}
          <Column
            title="Profile"
            dataIndex="profile"
            key="profile"
            width={100}
          />
          <Column
            title="Points"
            dataIndex="points"
            key="points"
            width={100}
          />
          <Column
            title="Action"
            key="operation"
            fixed="right"
            width={100}
            render={this.renderActions}
          />
        </Table>
        <Detail />
      </div>
    )
  }

}

const config = {
  assestments: AssestmentsContainer,
  assestment: AssestmentContainer
}

const mapStateToProps = (containers: any) => {
  return containers.assestments.state;
}

export default connect(config, mapStateToProps)(List);