import { Container } from 'unstated';

import { Queue } from '../../../shared';
import Assestments from '../services/Assestments';
import AssestmentsListState from '../models/AssestmentsListState';

const State = {
  httpBusy: false,
  message: null,
  data: null,
  error: null,
}

class AssestmentsContainer extends Container<AssestmentsListState> {
  public state: AssestmentsListState = {
    ...State
  };

  public name: string = 'AssestmentsList';

  public async fetch(type: string, meta: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Assestments list starts'
      })

      const response = await Assestments.paginate(type, meta)

      await this.setState({
        ...response,
        __action: 'Assestments list success'
      })
    } catch (error) {
      await this.setState({
        error,
        __action: 'Assestments list error'
      })
    } finally {
      Queue.next(() => {
        this.setState({
          httpBusy: false,
          error: null,
          message: null,
          __action: 'Assestments list ends'
        })
      })
    }
  }
}

export default AssestmentsContainer;