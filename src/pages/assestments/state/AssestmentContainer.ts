import { Container } from 'unstated';

import { Queue } from '../../../shared';
import Assestments from '../services/Assestments';
import AssestmentState from '../models/AssestmentState';

const State = {
  httpBusy: false,
  message: null,
  data: null,
  error: null,
  modalVisible: false,
  evaluation: {}
}

class AssestmentContainer extends Container<AssestmentState> {
  public state: AssestmentState = {
    ...State
  };

  public name: string = 'AssestmentsInfo';

  public async info(uuid: string, meta: any, evaluation: any) {
    try {
      this.setState({
        httpBusy: true,
        modalVisible: true,
        __action: 'Assestment info starts'
      })

      const response = await Assestments.info(uuid, meta)

      await this.setState({
        ...response,
        evaluation,
        __action: 'Assestment info success'
      })
    } catch (error) {
      await this.setState({
        error,
        __action: 'Assestment info error'
      })
    } finally {
      Queue.next(() => {
        this.setState({
          httpBusy: false,
          error: null,
          message: null,
          __action: 'Assestment info ends'
        })
      })
    }
  }

  public reset() {
    this.setState({
      ...State,
      __action: 'Assestment reset info'
    })
  }
}

export default AssestmentContainer;