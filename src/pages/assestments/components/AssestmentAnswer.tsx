import React, { Component } from 'react';

interface AssestmentProps {
  item: any
}

class AssestmentAnswer extends Component<AssestmentProps> {
  constructor(props: any) {
    super(props);

    this.renderAnswer = this.renderAnswer.bind(this);
  }

  private renderAnswer(answer: any, index: number) {
    const { options } = this.props.item;

    return (
        <p key={ `assestment-answer-${index}` }>
            <strong>{ answer }</strong> { options[answer] }
        </p>
    )
  }

  public render() {
    const { item } = this.props;

    const splited = item.answer.split('');

    return (
        <div>
            { splited.map(this.renderAnswer ) }
        </div>
    )
  }

}


export default AssestmentAnswer;