import React, { Component } from 'react';
import { Drawer, List, Popover } from 'antd';
import { connect } from 'unstated-enhancers';

import './assestments.scss';
import AssestmentContainer from './state/AssestmentContainer';
import AssestmentState from './models/AssestmentState';
import AssestmentAnswer from './components/AssestmentAnswer';


interface DetailsProps extends AssestmentState {
  containers: any
}

class Detail extends Component<DetailsProps> {
  constructor(props: any) {
    super(props);

    this.renderAnswer = this.renderAnswer.bind(this);
    this.renderTitle = this.renderTitle.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  private onClose() {
    const { assestment } = this.props.containers;

    assestment.reset();
  }

  private renderTitle(item: any) {
    const content = (
      <AssestmentAnswer item={ item } />
    )

    return (
      <Popover content={ content }>
        <a href="javascript:;">{ item.question }</a>
      </Popover>
    )
  }

  private renderAnswer(item: any) {
    const avatar = (<span>{ item.number }</span>);

    return (
      <List.Item>
        <List.Item.Meta
          avatar={ avatar }
          title={ this.renderTitle(item) }
          description={ `Answer: ${item.answer}`}
        />
        <div>
          { item.profile }{ item.points || 'None' }
        </div>
      </List.Item>
    )
  }

  public render() {
    const { modalVisible, httpBusy, data } = this.props;
    const { evaluation } = this.props;
    const answers = data ? data.answers : [];

    const seller = evaluation.seller ? evaluation.seller.name : '...';
    const points = (evaluation.profile || '') + (evaluation.points || '')

    return (
      <Drawer
        title={ `${seller} ${points}` }
        placement="right"
        width={ 300 }
        onClose={ this.onClose }
        visible={ modalVisible }
        >
       <List
        itemLayout="horizontal"
        loading= { httpBusy }
        dataSource={ answers }
        renderItem={ this.renderAnswer }
      />
      </Drawer>
    )
  }

}

const config = {
  assestment: AssestmentContainer
}

const mapStateToProps = (containers: any) => {
  return containers.assestment.state;
}

export default connect(config, mapStateToProps)(Detail);