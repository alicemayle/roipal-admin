import { Container } from 'unstated';
import Invitations from '../services/Invitations';
import moment from 'moment';

interface InvitationsList {
  httpBusy: boolean,
  data: any,
  error: any,
  message: string,
  __action?: string,
  meta: any
}
const initialState = {
  httpBusy: true,
  data: null,
  error: null,
  message: '',
  meta: null
}
class InvitationsListContainer extends Container<InvitationsList> {
  public state: InvitationsList = {
    ...initialState
  }

  public async fetch(params: {}) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'INVITATION LIST STARTS'
      });

      const response = await Invitations.list(params);

      response.data.map((invitation: any, index: number) => {
        response.data[index].key = invitation.uuid;
        response.data[index].created_at =
          moment(new Date(invitation.created_at)).format('DD-MM-YYYY')
        response.data[index].mission_name = invitation.mission.name
        response.data[index].company_name = invitation.company.name
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        meta: response.meta,
        __action: 'INVITATION LIST SUCCESS'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        __action: 'INVITATIONS LIST ERROR'
      })
    }
  }

  public reset() {
    this.setState({
      ...initialState
    });
  }

  public clearError() {
    this.setState({
      ...this.state,
      message: '',
      error: null
    })
  }
}

export default InvitationsListContainer;
