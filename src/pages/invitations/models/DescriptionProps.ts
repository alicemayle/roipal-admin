import MissionProps from './MissionProps';
import LocationProps from './LocationProps';

interface DescriptionProps {
    mission: MissionProps,
    location: LocationProps
}

export default DescriptionProps;