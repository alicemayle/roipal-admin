import CompanyProps from './CompanyProps';

interface CompanyComponentProps {
  company: CompanyProps
}

export default CompanyComponentProps;