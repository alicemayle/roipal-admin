interface MissionProps {
    profile: string,
    type: string,
    time: number,
    time_unit:string,
    total:number,
    description:string,
    currency: any
}
export default MissionProps;