interface CompanyProps {
    name: string,
    business_name: string,
    vertical: string,
    profile: {
      address:string,
    }
}

export default CompanyProps;