interface ExecutiveProps {
    name: string,
    email: string
}
export default ExecutiveProps;