interface ListTableProps {
  data: any,
  httpBusy: boolean,
  meta?: any,
  limit: number,
  onChangeLimit:(limit:number, page: number) =>void
}

export default ListTableProps;
