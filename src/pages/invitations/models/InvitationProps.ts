import MissionProps from './MissionProps';
import CompanyProps from './CompanyProps';
import LocationProps from './LocationProps';
import ExecutiveProps from './ExecutiveProps';

interface InvitationProps  {
  data: {
      company: CompanyProps,
      mission: MissionProps,
      location: LocationProps,
      executive: ExecutiveProps
  }
}

export default InvitationProps;