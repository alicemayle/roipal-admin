interface ListProps {
  containers: any,
  invitations: any,
  data: any,
  httpBusy: boolean,
  limit: number
}

export default ListProps;