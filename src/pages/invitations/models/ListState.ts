interface ListState {
  searchText: string,
  itemDetails?: any,
  showDetails: boolean
}

export default ListState;