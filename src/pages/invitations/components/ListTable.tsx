import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import ListTableProps from '../models/ListTableProps';
import ListState from '../models/ListState';
import FilterSearch from '../../../components/search/FilterSearch';
import Lang from '../../../support/Lang';
import autobind from 'class-autobind';
import DetailsDrawer from './DetailsDrawer';
import Pagination from 'src/components/pagination/Pagination';
import IconButton from 'src/components/buttons/IconButton';

const { Column } = Table;
interface FilterDropdDownInterface {
  setSelectedKeys: (selected: string[]) => {},
  selectedKeys: number[],
  clearFilters: () => {},
  confirm: any
}

class ListTable extends Component<ListTableProps, ListState> {
  public searchInput: any = null;
  constructor(props: ListTableProps) {
    super(props);
    autobind(this);
    this.state = {
      searchText: '',
      showDetails: false
    };
  }

  private setSearchInput(node: any) {
    this.searchInput = node;
  }

  private renderColumn(status: any) {
    switch (status) {
      case 1:
        return (<Icon className="status success" type="check-circle" />);
        break;
      case -1:
        return (<Icon className="status error" type="close-circle" />);
        break;
      case 0:
        return (<Icon className="status warning" type="clock-circle" />);
        break;
      default:
        return (<Icon className="status warning" type="clock-circle" />);
        break;
    }
  }

  private getColumnSearchProps = (dataIndex: any) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterDropdDownInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: any) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: any, record: any) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
  })

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    this.setState({ searchText: '' });
  }


  private handleOpenDetails(item: any) {
    this.setState({
      showDetails: true,
      itemDetails: item
    })
  }

  private renderActions(item: any) {
    return (
      <IconButton
        item={item}
        onFuction={this.handleOpenDetails}
        size={18}
        colorIcon={'#52bebb'}
        name={'info-circle'}
      />
    );
  }

  private closeDrawer() {
    this.setState({
      showDetails: false,
    });
  };

  public render() {
    const { data = {}, httpBusy, meta, limit, onChangeLimit } = this.props;

    return (
      <div>
        <Table
          dataSource={data}
          loading={httpBusy}
          pagination={false}
          scroll={{ x: 420 }}
          className="rwd-table"
        >
          <Column
            title={Lang.get('invitation.date')}
            dataIndex="created_at"
            key="created_at"
            align={'center'}
          />
          <Column
            title={Lang.get('invitation.mission')}
            dataIndex="mission.name"
            key="mission.uuid"
            {...this.getColumnSearchProps('mission_name')}
          />
          <Column
            title={Lang.get('invitation.company')}
            dataIndex="company.name"
            key="company.uuid"
            {...this.getColumnSearchProps('company_name')}
          />
          <Column
            title={Lang.get('invitation.status')}
            dataIndex="status"
            key="uuid"
            render={this.renderColumn}
            align={'center'}
          />
          <Column
            title={Lang.get('generic.button.details')}
            dataIndex=""
            key="0"
            render={this.renderActions}
            align={'center'}
          />
        </Table>
        {
          meta &&
          <Pagination
            limit={limit}
            meta={meta.cursor}
            onChange={onChangeLimit}
          />
        }
        <DetailsDrawer
          visible={this.state.showDetails}
          onClose={this.closeDrawer}
          item={this.state.itemDetails}
        />
      </div>
    );
  }

}

export default ListTable;

