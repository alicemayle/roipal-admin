import React, { PureComponent } from 'react';
import { Row, Col } from 'antd';
import Description from '../components/Description';
import InvitationProps from '../models/InvitationProps';
import Executive from './Executive';

class ListTableRowExpanded extends PureComponent<InvitationProps> {
  constructor(props: InvitationProps) {
    super(props);
  }

  public render() {
    const { data } = this.props;

    return (
      <Col>
        <Row>
          <Executive executive={data.executive} />
        </Row>
        <Row>
          <Description
          location={data.location}
          mission={data.mission}
            />
        </Row>
      </Col>
    );
  }
}

export default ListTableRowExpanded;