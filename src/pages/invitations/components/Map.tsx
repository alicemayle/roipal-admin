import React, { Component } from 'react';
import Autobind from 'class-autobind';
import GoogleMap from 'google-map-react';
import { Card, Row, Col } from 'antd';
import Marker from 'src/components/map/Marker';
import Text from 'antd/lib/typography/Text';
import Lang from 'src/support/Lang';


class Map extends Component<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public state = {
    center: {
      lat: 19.3665903,
      lng: -99.1832562
    }
  }

  public render() {
    const { position } = this.props

    const center = {
      lat: position.latitude,
      lng: position.longitude
    }

    return (
      <Card
        bordered={false}
        type="inner"
        title={Lang.get('company.location.title')}
      >
        <Row gutter={18}>
          <Col span={12}>
            <div style={{ height: '22vh' }}>
              <GoogleMap
                bootstrapURLKeys={{ key: 'AIzaSyCzTCLGCeoIPtGng_7IhV4aZRgL-dYyPaw' }}
                zoom={14}
                center={center}>
                <Marker
                  lat={center.lat}
                  lng={center.lng}
                  marker={{}}
                />
              </GoogleMap>
            </div>
          </Col>
          <Col span={12}>
            <Row>
              <Col>
                <Text><b>{Lang.get('company.location.address')}:</b> &nbsp;</Text>
                <Text>{position.address}</Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </Card>
    )
  }
}

export default Map