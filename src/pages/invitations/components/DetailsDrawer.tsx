import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Drawer } from 'antd';
import Lang from '../../../support/Lang';
import ListTableRowExpanded from './ListTableRowExpanded'

const pStyle = {
  fontSize: 18,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16,
};

class DetailsDrawer extends Component<any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public render() {

    const { visible, onClose, item} = this.props

    return (
      <Drawer
        placement="right"
        width={'55%'}
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <p style={pStyle}>{Lang.get('invitation.details')}</p>
        <ListTableRowExpanded data={item}/>
      </Drawer>
    )
  }
}

export default DetailsDrawer;