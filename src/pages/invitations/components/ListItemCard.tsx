import React, { PureComponent } from 'react';
import { Icon } from 'antd';

interface ListItemCardProps {
    data: {},
}

class ListItemCard extends PureComponent<any,ListItemCardProps> {
    constructor(props: any) {
        super(props);
    }

    public render() {
        const { label={}, icon={} } = this.props;

        return(
            <div >
              {
                label &&
                <div  style={{display:'flex',
                flexDirection: 'column',
                alignItems:'center'}}>
                    <Icon
                    className="status success"
                    type={ icon }
                    />
                    <p> {label}</p>
                  </div>
              }
            </div>
        );
    }
}

export default ListItemCard;