import React, { PureComponent } from 'react';
import { Card, Row, Col } from 'antd';
import DescriptionProps from '../models/DescriptionProps'
import ListIcons from '../../../components/ListIcons';
import Lang from '../../../support/Lang';
import ReadMore from 'src/components/readMore/ReadMore';
import Map from './Map'

class Description extends PureComponent<DescriptionProps> {
  constructor(props: DescriptionProps) {
    super(props);
  }

  public render() {
    const { mission, location } = this.props;

    return (
      <div>
        <Card
          type="inner"
          title={Lang.get('invitation.description')}
          bordered={false}>
          <ReadMore description={mission.description} />
          <Row gutter={16} type="flex" justify="center">
            <Col span={6}>
              <ListIcons
                type="vertical"
                colorIcon={'orange'}
                label={mission.type} icon={'solution'} />
            </Col>
            <Col span={6}>
              <ListIcons
                type="vertical"
                colorIcon={'#3babd4'}
                label={`${mission.time} ${mission.time_unit}`}
                icon={'clock-circle'}
              />
            </Col>
            <Col span={6}>
              <ListIcons
                type="vertical"
                colorIcon="green"
                label={`${mission.currency.symbol} ${mission.total} ${mission.currency.currency}`}
                icon={'dollar'}
              />
            </Col>
          </Row>
        </Card>
        <Map position={location} />
      </div>
    );
  }
}

export default Description;