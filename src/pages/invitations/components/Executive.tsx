import React, { PureComponent } from 'react';
import { Card, Avatar } from 'antd';
import Lang from '../../../support/Lang';
import Meta from 'antd/lib/card/Meta';
import logo from '../../../images/person.png'
import Text from 'antd/lib/typography/Text';

class Executive extends PureComponent<any> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    const { executive } = this.props;

    const avatar = executive.profile.photo_bio_url ?
      executive.profile.photo_bio_url : logo

    return (
      <div>
        <Card
          type="inner"
          title={Lang.get('invitation.executive')}
          bordered={false}>
          <Meta
            avatar={<Avatar src={avatar} style={{ height: 100, width: 100 }} />}
            title={executive.name}
            description={
            <div>
              <Text>{executive.email}</Text>
              <br />
              <Text>{executive.profile.phone}</Text>
              <br />
              <Text>{executive.profile.address}</Text>
            </div>
          }
          />
        </Card>
      </div>
    );
  }
}

export default Executive;