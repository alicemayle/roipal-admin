import { Http } from '../../../shared';
class Invitations {

  public list( params: any): any {
    return Http.get(`/api/invitations?includes=company.profile,mission,executive.profile,location`,
     { params })
  }

}

export default new Invitations;
