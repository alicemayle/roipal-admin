import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import InvitationsListContainer from './containers/InvitationListContainer';
import ListTable from './components/ListTable';
import ListProps from './models/ListProps';
import Lang from '../../support/Lang';
import autobind from 'class-autobind';

import './styles.scss';
import { Divider } from 'antd';
interface ListState {
  limit: number,
  page: number
}
class List extends Component<ListProps, ListState> {

  constructor(props: ListProps) {
    super(props);
    autobind(this);
  }

  public state: ListState = {
    limit: 50,
    page: 1
  }

  public handleFetch(limit?: number, page?: number) {
    const { invitations } = this.props.containers;

    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page,
    }

    invitations.fetch(meta);
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  public componentDidMount() {
    this.handleFetch()
  }

  public shouldComponentUpdate(nextProps: any) {
    return (nextProps.invitations.data !== this.props.invitations.data
      || nextProps.invitations.httpBusy !== this.props.invitations.httpBusy)
  }

  public render() {
    const { data, httpBusy, meta } = this.props.invitations;

    return (
      <div>
        <h1>{Lang.get('invitation.title')}</h1>
        <Divider />
        <ListTable
          data={data}
          httpBusy={httpBusy}
          meta={meta}
          onChangeLimit={this.handleChangeLimit}
          limit={this.state.limit}
        />
      </div>
    )
  }
}

const container = {
  invitations: InvitationsListContainer
}

const mapStateToProps = (containers: any) => {
  return {
    invitations: containers.invitations.state
  }
}

export default connect(container, mapStateToProps)(List);