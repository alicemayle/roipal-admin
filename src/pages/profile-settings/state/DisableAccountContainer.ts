import { Container } from 'unstated';
import AccountService from '../service/AccountService'

const initialState = {
  httpBusy: false,
  message: '',
  data: null,
  error: null,
  meta: null
}

class DisableAccountContainer extends Container<any> {
  public state: any = {
    ...initialState
  };

  public name: string = 'DisableAccountContainer';

  public async disableAccount(data: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Disable/Enable account starts'
      })

      const response = await AccountService.disableAccount(data);

      if (response) {
        await this.setState({
          httpBusy: false,
          account: response.data,
          __action: 'Disable/Enable account success'
        })
      }

    } catch (error) {
      await this.setState({
        error,
        httpBusy: false,
        __action: 'Disable/Enable account error'
      });
    }
  }

}

export default DisableAccountContainer;