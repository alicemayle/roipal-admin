import { Container } from 'unstated';
import update from '../service/UpdateProfileService';

class PaymentDocumentContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
  }

  public name: string = 'Update profile Container';

  public async updateProfile(data: any, company:any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update profile Starts'
      })

      const response = await update.update(data, company);

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Update profile Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Update profile Error'
      })
    }
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'Update profile clear error'
    });
  }
}

export default PaymentDocumentContainer;