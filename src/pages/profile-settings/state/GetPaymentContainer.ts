import { Container } from 'unstated';
import update from '../service/UpdateProfileService';

class GetPaymentContainer extends Container<any> {

  public state: any = {
    httpBusy: false,
    data: null,
    error: null,
    message: '',
  }

  public name: string = 'Get payment Container';

  public async getPayment(company:any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'get payment Starts'
      })

      const response = await update.getPayment(company);

      this.setState({
        httpBusy: false,
        data: response.data.payments_profiles[0],
        __action: 'get payment Success'
      })
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'get payment Error'
      })
    }
  }

  public clearError() {
    this.setState({
      ...this.state,
      error: null,
      message: '',
      __action: 'get payment clear error'
    });
  }
}

export default GetPaymentContainer;