import { Http } from '../../../shared';

class UpdateProfileService {

  public update(data:any, company:any): any {
    return Http.put(`/api/companies/${company}?includes=profile,payments_profiles,owner`, data)
  }

  public getPayment(company:any) {
    return Http.get(`/api/companies/${company}?includes=payments_profiles`);
  }
}

export default new UpdateProfileService;