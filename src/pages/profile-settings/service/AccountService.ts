import { Http } from '../../../shared';
import Auth from '../../../auth/User'
class Account {

    public disableAccount(data: any) {
        const uuid = Auth.getData().profile.uuid;
    
        return Http.put(`api/user/${uuid}/account-status/`, data)
      }

}

export default new Account;