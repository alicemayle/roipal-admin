import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Collapse, Divider } from 'antd';
import { connect, Logger } from 'unstated-enhancers';
import SignInContainer from 'src/auth/state/SignInContainer';
import EditProfile from './components/EditProfile';
import EditPasword from './components/EditPasword'
import EditPaymentMethod from './components/EditPaymentMethod';
import ChangeLanguage from './components/ChangeLanguage';
import DisableAccount from './components/DisableAccount';
import Lang from 'src/support/Lang';
import LanguageContainer from '../../auth/state/LanguageContainer'
import { Gate } from '@xaamin/guardian';
import Companies from '../companies/state/CompaniesListContainer';

const { Panel } = Collapse;

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden',
};

class Menu extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this);
  }

  public state: any = {
    profileCompany: null
  }

  private getCompany() {
    const { data } = this.props.signing

    this.setState({
      profileCompany: data.company,
      profileUser: data.profile
    })

    if(!data.company) {
      const { companies } = this.props.containers;

      const meta = {
        includes: 'profile,payments_profiles',
        limit: 100
      }
      companies.fetch(meta);
    }
  }

  public componentDidMount() {
    this.getCompany()
  }

  public render() {
    const companies = this.props.companies.data;

    Logger.dispatch('state settigns', this.state)
    return (
      <div>
        <h1>
          {Lang.get('setting.settings.title')}
        </h1>
        <Divider />
        <Collapse
          bordered={false}
        >
          {
            Gate.allows('company::profile-edit') &&
            <Panel
              header={Lang.get('setting.settings.personal')}
              key="1"
              style={customPanelStyle}>
              <EditProfile data={this.state.profileCompany} user={this.state.profileUser}/>
            </Panel>
          }
          {
            Gate.allows('company::payment-edit') &&
            <Panel
              header={Lang.get('setting.payment.title')}
              key="2"
              style={customPanelStyle}>
              <EditPaymentMethod company={this.state.profileCompany} companies={companies}/>
            </Panel>
          }
          {
            Gate.allows('company::password-edit') &&
            <Panel
              header={Lang.get('setting.password.title')}
              key="3"
              style={customPanelStyle}>
              <EditPasword company={this.state.profileCompany} />
            </Panel>
          }
          <Panel
            header={Lang.get('setting.language.title')}
            key="4"
            style={customPanelStyle}>
            <ChangeLanguage />
          </Panel>
          {
            Gate.allows('company::account-edit') &&
            <Panel
              header={Lang.get('setting.disable_account.title')}
              key="5"
              style={customPanelStyle}>
              <DisableAccount />
            </Panel>
          }
        </Collapse>
      </div>
    )
  }
}

const container = {
  signing: SignInContainer,
  language: LanguageContainer,
  companies: Companies
}

const mapStateToProps = (containers: any) => {
  return {
    language: containers.language.state,
    signing: containers.signing.state,
    companies: containers.companies.state
  }
}

export default connect(container, mapStateToProps)(Menu);