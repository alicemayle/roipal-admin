import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Form, Input, Button, message } from 'antd';
import { connect } from 'unstated-enhancers';
import UpdateProfileContainer from '../state/UpdateProfileContainer';
import Notifications from 'src/components/notifications/notifications';
import Text from 'antd/lib/typography/Text';
import '../settings.scss'
import Lang from 'src/support/Lang';
import { Gate } from '@xaamin/guardian';

class EditPasword extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    new_password: null,
    confirmPassword: null,
    password: null,
    flagerror: false
  }

  private changeText(e: any) {
    this.setState({
      flagerror: false,
      [e.target.name]: e.target.value
    })
  }

  private handleSubmit() {
    if (Gate.allows('company::password-edit')) {
    this.props.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        const { new_password, confirmPassword, password } = this.state;
        const { edit } = this.props.containers
        const { company } = this.props
        const NewPass = new_password;
        const regex = /(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}.+$)/;
        const valor = regex.test(NewPass);

        if (new_password === confirmPassword && password) {
          if (valor) {
            const dataUpdate = {
              user_profile: {
                new_password,
                password
              }
            }

            await edit.updateProfile(dataUpdate, company.uuid)
            const { error } = edit.state

            if (error) {
              const notification = new Notifications;
              const msj = {
                type: 'error',
                message: Lang.get('setting.password.update_error')
              }
              notification.openNotification(msj)
            } else {
              this.setState({
                new_password: null,
                confirmPassword: null,
                password: null
              })
            }
          } else {
            this.setState({
              flagerror: true
            })
            return;
          }
        } else {
          this.setState({
            flagerror: true
          })
        }
      }
    })
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  public render() {
    const { flagerror } = this.state
    const { httpBusy } = this.props.containers.edit.state
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Form>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: Lang.get('setting.password.current_pass')
                },
              ],
            })(
              <Input.Password
                placeholder={Lang.get('setting.password.curent_pass_small')}
                name="password"
                onChange={this.changeText}
                style={{ width: '50%' }}
              />
            )}
          </Form.Item>
          <Form.Item
            validateStatus={flagerror ? 'error' : undefined}
          >
            {getFieldDecorator('newPass', {
              rules: [
                {
                  required: true,
                  message: Lang.get('setting.password.new_pass')
                },
              ],
            })(
              <Input.Password
                placeholder={Lang.get('setting.password.new_pass_small')}
                name="new_password"
                onChange={this.changeText}
                style={{ width: '50%' }}
              />
            )}
          </Form.Item>
          <Form.Item
            validateStatus={flagerror ? 'error' : undefined}
          >
            {getFieldDecorator('confirmPass', {
              rules: [
                {
                  required: true,
                  message: Lang.get('setting.password.confirm_pass')
                },
              ],
            })(
              <Input.Password
                placeholder={Lang.get('setting.password.confirm_pass')}
                style={{ width: '50%' }}
                onChange={this.changeText}
                name="confirmPassword"
              />
            )}
          </Form.Item>
          <Form.Item>
            <Button
              onClick={this.handleSubmit}
              key="submit"
              loading={httpBusy}
              type="primary"
            >
              {Lang.get('setting.button.save')}
            </Button>
          </Form.Item>
          {flagerror &&
            <div className="message-error">
              <Text style={{ color: 'red' }}>
                {Lang.get('setting.password.info_pass6')}
                <br />
                <br />
                {Lang.get('setting.password.info_pass')}
                <br />
                {Lang.get('setting.password.info_pass2')}
                <br />
                {Lang.get('setting.password.info_pass3')}
                <br />
                {Lang.get('setting.password.info_pass4')}
                <br />
                {Lang.get('setting.password.info_pass5')}
              </Text>
            </div>
          }
        </Form>
      </div>
    )
  }
}

const config = {
  edit: UpdateProfileContainer
}

const mapStateToProps = (containers: any) => {
  return {
    edit: containers.edit.state
  }
}

const FormInputNumber: any = Form.create({ name: 'form_pass' })(EditPasword);

export default connect(config, mapStateToProps)(FormInputNumber);