import React, { Component } from 'react';
import autobind from 'class-autobind';
import '../settings.scss'
import { Upload } from 'antd';
import avatar from '../../../images/icon.png'

class UpdateAvatar extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
  };

  public render() {
    const { beforeUpload, onChange, imageUrl,
      uploadButton, cambio, bio_ima } = this.props

    return (
      <div>
        {
          !cambio &&
          <img
            alt="example"
            src={imageUrl ? imageUrl : bio_ima ? bio_ima : avatar}
            style={{ width: '100%', height: '100%' }}
          />
        }
        {
          cambio &&
          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={beforeUpload}
            onChange={onChange}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar"
              style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        }
      </div>
    )
  }
}

export default UpdateAvatar;