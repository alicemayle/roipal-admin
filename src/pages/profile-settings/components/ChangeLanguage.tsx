import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Select, Row, Col } from 'antd';
import Lang from 'src/support/Lang';
import { connect, Logger } from 'unstated-enhancers';
import LanguageContainer from '../../../auth/state/LanguageContainer';
import mexicoFlag from '../../../images/mexico.svg'
import franceFlag from '../../../images/france.svg'
import usaFlag from '../../../images/united-states.svg'
const { Option } = Select;

class ChangeLanguage extends Component<any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public handleChangeLanguage = (value: string) => {
    const { language } = this.props.containers;

    language.setLanguage(value)
  }

  public render() {
    const { selected } = this.props.language
    let language = null
    let image = null

    if (selected === 'es') {
      language = Lang.get('generic.language.spanish')
      image = mexicoFlag
    }

    if (selected === 'fr') {
      language = Lang.get('generic.language.french')
      image = franceFlag
    }


    if (selected === 'en') {
      language = Lang.get('generic.language.english')
      image = usaFlag
    }

    Logger.count(this)


    return (
      <div>
        <Row type="flex" justify={'center'} style={{ alignContent: 'center' }}>
          <Col span={3}>
            <p>{Lang.get('setting.language.selected')}: </p>
          </Col>
          <Col span={1}>
            <img
              src={image}
              width="25" height="25"
            />
          </Col>
          <Col span={6}>
            <p>{language}</p>
          </Col>

          <Col span={2}>
            <p>{Lang.get('setting.language.change')}: </p>

          </Col>
          <Col span={6}>
            <Select
              placeholder={Lang.get('generic.language.select_language')}
              defaultValue={Lang.getLocale()}
              onChange={this.handleChangeLanguage}
            >
              <Option value="en">{Lang.get('generic.language.english')}</Option>
              <Option value="es">{Lang.get('generic.language.spanish')}</Option>
              <Option value="fr">{Lang.get('generic.language.french')}</Option>
            </Select>
          </Col>
        </Row>
      </div>

    )
  }
}

const container = {
  language: LanguageContainer
}

const mapStateToProps = (containers: any) => {
  return {
    language: containers.language.state
  }
}

export default connect(container, mapStateToProps)(ChangeLanguage);

