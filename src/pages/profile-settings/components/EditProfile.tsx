import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Icon, Card, message, Col, Row, Form, Button, Input } from 'antd';
import '../settings.scss'
import Meta from 'antd/lib/card/Meta';
import UpdateAvatar from './UpdateAvatar';
import { connect } from 'unstated-enhancers';
import UpdateProfileContainer from '../state/UpdateProfileContainer'
import Notifications from 'src/components/notifications/notifications';
import Lang from 'src/support/Lang';
import SignInContainer from 'src/auth/state/SignInContainer';
import { Gate } from '@xaamin/guardian';

class EditProfile extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    loading: false,
    cambio: false
  };

  private handleChange(info: any) {
    if (info.file.status === 'uploading') {
      this.setState({
        loading: true
      });
      return;
    }
    if (info.file.status === 'done') {
      let fileList = [...info.fileList];
      fileList = fileList.slice(-1);
      fileList = fileList.map(file => {
        if (file.response) {
          this.setState({
            data: {
              ...this.state.data,
              avatar: file.response
            }
          })
        }
        return file;
      });
      this.getBase64(info.file.originFileObj, (imageUrl: any) =>
        this.setState({
          imageUrl,
          loading: false,
          cambio: false,
        }),
      );
    }
  };

  private getBase64(img: any, callback: any) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  private beforeUpload(file: any) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  }

  private set() {
    this.setState({
      cambio: true
    })
  }

  private async save() {
    if (Gate.allows('company::profile-edit')) {
      const { update, singing } = this.props.containers
      const { data } = this.props

      const dataUpdate = {
        user_profile: {
          name: this.state.name
        }
      }

      if (dataUpdate) {
        await update.updateProfile(dataUpdate, data.uuid)
        const { error } = update.state

        if (error) {
          const notification = new Notifications;

          const msj = {
            type: 'error',
            message: Lang.get('setting.settings.update_error')
          }

          notification.openNotification(msj)
        } else {
          const account = this.props.containers.update.state.data
          singing.setProfile(account.owner)
        }
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  private changeText(e: any) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  private setData() {
    const { user } = this.props

    this.setState({
      name: user.name
    })

  }

  public componentDidMount() {
    this.setData()
  }

  public render() {
    const { user } = this.props
    const { httpBusy } = this.props.containers.update.state
    const { imageUrl } = this.state;

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' :
          imageUrl ? 'plus' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    return (
      <div>
        <Row gutter={20} >
          <Col span={6} >
            <Card
              style={{ width: 300, margin: 25 }}
              cover={
                <UpdateAvatar
                  bio_ima={user.avatar}
                  beforeUpload={this.beforeUpload}
                  onChange={this.handleChange}
                  imageUrl={imageUrl}
                  uploadButton={uploadButton}
                  cambio={this.state.cambio}
                />
              }
              actions={[
                <Icon type="upload" key="upload" onClick={this.set} />
              ]}
            >
              <Meta
                title={user.email}
              />
            </Card>
          </Col>
          <Col span={7} >
            <Form style={{ marginTop: 50, marginLeft: 70 }}>
              <Form.Item label={Lang.get('setting.settings.name')}>
                <Input
                  addonAfter={<Icon type="edit" />}
                  defaultValue={user.name}
                  onChange={this.changeText}
                  name={'name'}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  onClick={this.save}
                  type="primary"
                  loading={httpBusy}
                >
                  {Lang.get('setting.button.save')}
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
}

const config = {
  update: UpdateProfileContainer,
  singing: SignInContainer
}

const mapStateToProps = (containers: any) => {
  return {
    update: containers.update.state,
    singing: containers.singing.state
  }
}

export default connect(config, mapStateToProps)(EditProfile);