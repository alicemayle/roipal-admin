import React, { Component } from 'react';
import autobind from 'class-autobind';
import Notifications from 'src/components/notifications/notifications';
import GetPaymentContainer from '../state/GetPaymentContainer';
import { connect, Logger } from 'unstated-enhancers';
import { Card, Row, Col, Form, Radio, Button, Select, message } from 'antd';
import Cards from 'react-credit-cards';
import creditCardType from 'credit-card-type';
import typeCard from '../../../support/TypeCardManagement';
import UpdatePrfileContainer from '../state/UpdateProfileContainer'
import EditPaymentForm from './EditPaymentForm';
import Lang from 'src/support/Lang';
import { Gate } from '@xaamin/guardian';

const { Option } = Select;
class EditPaymentMethod extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    flagName: false,
    flagCard1: false,
    flagCard2: false,
    flagCard3: false,
    flagCard4: false,
    disable: true,
    flagExp: null,
    flagCcv: null,
  }

  private async getDataPayment(business?: any) {
    const { payment } = this.props.containers;
    const { company } = this.props
    const _company = business 
      ? business : company 
      ? company.uuid : null

    if (_company) {
      await payment.getPayment(_company);
      const { error, data } = this.props.containers.payment.state

      if (error) {
        const notification = new Notifications;
        const msj = {
          type: 'error',
          message: Lang.get('setting.payment.error_payment')
        }
        notification.openNotification(msj)

        payment.clearError()
        
      } else {
        this.setState({
          payment: data,
        })
      }
    }
  }

  private async handleSubmit() {
    if (Gate.allows('company::payment-edit')) {
      const { name, cardNumber, ccv, exp } = this.state
      const { update } = this.props.containers
      const { company } = this.props

      this.setState({
        flagName: name ? false : true,
        flagCard1: cardNumber ? false : true,
        flagCard2: cardNumber ? false : true,
        flagCard3: cardNumber ? false : true,
        flagCard4: cardNumber ? false : true,
        flagExp: exp ? false : true,
        flagCcv: ccv ? false : true
      })

      if (name) {
        const data = {
          payment: {
            owner: name,
            exp: this.state.exp,
            ccv: this.state.ccv,
            card_number: cardNumber,
          }
        }

        const cardType = cardNumber === undefined
          || cardNumber === ''
          || creditCardType(cardNumber).length === 0 
            ? undefined : creditCardType(cardNumber)[0].type;

        if ((!typeCard.isAmericanExpressCard(cardType) && cardNumber.length === 16
          && ccv.length === 3)
          || (typeCard.isAmericanExpressCard(cardType) && cardNumber.length === 15
            && ccv.length === 4)) {
          this.setState({
            cardnumbervalidate: true
          })

          if ((!typeCard.isAmericanExpressCard(cardType) && this.state.ccv.length !== 3)
            || (typeCard.isAmericanExpressCard(cardType) && this.state.ccv.length !== 4)) {
            this.setState({
              ccvValidate: false
            })
            return;
          }
        }

        const validExp = await this.validateExp();

        if (validExp) {
          const _uuid = company ? company.uuid : this.state.companyUuid

          await update.updateProfile(data, _uuid);
          const { error } = update.state

          if (error) {
            const notification = new Notifications;

            const msj = {
              type: 'error',
              message: Lang.get('setting.payment.error_update')
            }

            notification.openNotification(msj)

            update.clearError()
          }
        }
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  private sendData(data: any) {
    this.setState({
      ...data
    }, () => Logger.dispatch('sendData', this.state))
  }

  private async validateExp() {
    const { exp } = this.state;

    const month = new Date().getMonth();
    const year = new Date().getFullYear();

    if (exp) {
      const arrayDate = exp.split('/');

      const mm = Number(arrayDate[0])
      const yy = Number(arrayDate[1])

      if (mm > 12 || mm < 1) {
        this.setState({
          invalidExp: true
        })
        return false;
      }

      if (yy < Number(year.toString().substr(2, 3))) {
        this.setState({
          invalidExp: true
        })
        return false;
      } else if (yy === Number(year.toString().substr(2, 3))) {
        if (mm <= (month + 1)) {
          this.setState({
            invalidExp: true
          })
          return false;
        } else {
          this.setState({
            invalidExp: false
          })
          return true;
        }
      } else {
        this.setState({
          invalidExp: false
        })
        return true;
      }
    }
    return false
  }

  private changeOption() {
    this.setState({
      disable: !this.state.disable
    })
  }

  private renderOptions(data: any) {
    const value = data.business_name

    return (
      <Option key={data.key} value={data.uuid}>
        {value}
      </Option>
    );
  }

  private onChangeCompany(value: string) {
    this.setState({
      companyUuid: value
    })

    this.getDataPayment(value)
  }

  public componentDidMount() {
    this.getDataPayment()
  }

  public render() {
    const { payment, name } = this.state
    const { httpBusy } = this.props.containers.update.state
    const { companies, company } = this.props

    let cardNumber = '';

    if(payment) {
      if (payment.card_number.length <= 4) {
        cardNumber = '············'.concat(payment.card_number)
      } else {
      cardNumber = payment.card_number
      }
    }

    return (
      <div>
        {
          !company && companies && 
          <Form.Item label={Lang.get('buses.mission.add.description.select_company.label')}>
            <Select
              showSearch={true}
              placeholder={Lang.get('buses.mission.add.description.select_company.label')}
              optionFilterProp="children"
              style={{width: '50%'}}
              onChange={this.onChangeCompany}
            >
              { companies.map(this.renderOptions) }
            </Select>
        </Form.Item>
        }

        {payment &&
          <Row gutter={20}>
            <Col span={11}>
              <Card style={{ marginTop: 10 }}>
                <Cards
                  number={cardNumber}
                  name={name ? name : payment.owner}
                  expiry={payment.exp}
                  cvc={payment.ccv}
                  focused={payment.name}
                  preview={true}
                />
              </Card>
            </Col>
            <Col span={11}>
              <Radio
                style={{ marginLeft: 10, marginBottom: 5 }}
                onChange={this.changeOption}
              >
                {Lang.get('setting.payment.title')}
              </Radio>
              <EditPaymentForm
                sendData={this.sendData}
                flagName={this.state.flagName}
                flagCard1={this.state.flagCard1}
                flagCard2={this.state.flagCard2}
                flagCard3={this.state.flagCard3}
                flagCard4={this.state.flagCard4}
                flagExp={this.state.flagExp}
                flagCcv={this.state.flagCcv}
                cardnumbervalidate={this.state.cardnumbervalidate}
                invalidExp={this.state.invalidExp}
                ccvValidate={this.state.ccvValidate}
                disable={this.state.disable}
              />
              <Button
                onClick={this.handleSubmit}
                style={{marginLeft: 10}}
                loading={httpBusy}
                disabled={this.state.disable}
                type="primary"
              >
                {Lang.get('setting.button.save')}
              </Button>
            </Col>
          </Row>
        }
      </div>
    )
  }
}

const container = {
  payment: GetPaymentContainer,
  update: UpdatePrfileContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    payment: containers.payment.state,
    update: containers.update.state,
  }
}

const WrapperEditPaymentForm: any = Form.create({
  name: 'EditPaymentForm'
})(EditPaymentMethod);

export default connect(container, mapStateToProps)(WrapperEditPaymentForm);