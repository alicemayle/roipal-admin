import React, { Component } from 'react';
import autobind from 'class-autobind';
// import Lang from 'src/support/Lang';
import { connect, Logger } from 'unstated-enhancers';
import DisableAccountContainer from '../state/DisableAccountContainer';
import SingInContainer from '../../../auth/state/SignInContainer';
import { Icon, Row, Col, Button, message } from 'antd';
import Lang from 'src/support/Lang';
import { Gate } from '@xaamin/guardian';

class ChangeLanguage extends Component<any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public handleChangeLanguage = (value: string) => {
    const { language } = this.props.containers;

    language.setLanguage(value)
    console.log(value)
  }

  private async disableAccount() {
    if (Gate.allows('company::account-edit')) {
      const { disable, singin } = this.props.containers
      const status = singin.state.data.profile.account_status;
      let data = {}

      if (status > 0) {
        data = {
          account_status: -1
        }
      } else {
        data = {
          account_status: 1
        }
      }

      await disable.disableAccount(data)

      const { error, account } = disable.state;

      if(!error) {
        singin.setProfile(account)
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  public render() {
    const { data } = this.props.singin
    const status = data.profile.account_status

    Logger.count(this)

    return (
      <div>
        <Row type="flex" justify="center">
          <Col span={2}>
            <Icon type="warning" style={{ fontSize: 60, color: '#5bc1be' }} />
          </Col>
          <Col span={20}>
            {
              status > 0 &&
                <p>
                  {Lang.get('setting.disable_account.message_one')}
                  <br />
                  <br />
                  {Lang.get('setting.disable_account.message_two')}
                </p>
            }
            {
              status < 0 &&
              <p>{Lang.get('setting.disable_account.enable_message')}
              </p>
            }
          </Col>
        </Row>
        <br />
        <Button
          type="primary"
          style={{marginLeft: '13%'}}
          onClick={this.disableAccount}
        >
          { status > 0
            ? Lang.get('setting.disable_account.disable_button')
            : Lang.get('setting.disable_account.enable.button')}
        </Button>
      </div>

    )
  }
}

const container = {
  disable: DisableAccountContainer,
  singin: SingInContainer
}

const mapStateToProps = (containers: any) => {
  return {
    disable: containers.disable.state,
    singin: containers.singin.state
  }
}

export default connect(container, mapStateToProps)(ChangeLanguage);

