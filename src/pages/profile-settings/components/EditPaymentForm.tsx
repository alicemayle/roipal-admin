import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Form, Input, Col } from 'antd';
import Lang from 'src/support/Lang';
import creditCardType from 'credit-card-type';
import typeCard from '../../../support/TypeCardManagement';


class EditPaymentForm extends Component<any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {

  }

  private changeText(e: any) {
    this.setState({
      flagName: false,
      [e.target.name]: e.target.value
    })
    this.sendData()
  }

  private sendData() {
    const { sendData } = this.props

    if (sendData) {
      sendData(this.state)
    }
  }

  private changeNumbCard(e: any) {
    const value = e.target.value

    if (value.length <= 4) {
      this.setState({
        flagCard1: false,
        flagCard2: false,
        flagCard3: false,
        flagCard4: false,
        [e.target.name]: value
      })
      if (this.state.ccv) {
        if (this.state.ccv.length === 4) {
          this.setState({
            ccvValidate: true
          })
        }
      }
    } else {
      if (this.state.ccv) {
        if (this.state.ccv.length === 4) {
          this.setState({
            ccvValidate: false
          })
        }
      }
    }
    this.sendData()
  }

  private changeTextDate(e: any) {
    this.setState({
      flagExp: false
    })
    let value = e.target.value
    const oldValue = this.state.oldValue ? this.state.oldValue : 0
    const { invalidExp } = this.state;

    if (invalidExp) {
      this.setState({
        invalidExp: false
      })
    }

    if (value.length > 1) {
      if (value.length === 2 && oldValue.length !== 3) {
        value += '/';
      }
    }

    if (value.length <= 5) {
      this.setState({
        [e.target.name]: value
      })
    } else {
      this.setState({
        oldValue: value
      })
    }
    this.sendData()
  }

  private changeCCV(e: any) {
    const { card_number, card_number2, card_number3, card_number4 } = this.state
    const value = e.target.value
    let card

    this.setState({
      flagCcv: false
    })

    if (card_number && card_number2 && card_number3 && card_number4) {
      card = card_number.concat(card_number2, card_number3, card_number4);
      this.setState({
        cardNumber: card
      })
    }

    if (card) {
      const cardType = card === undefined
        || card === ''
        || creditCardType(card).length === 0 ? undefined
        : creditCardType(card)[0].type;

      if (!typeCard.isAmericanExpressCard(cardType) ? value.length <= 3 : value.length <= 4) {
        this.setState({
          ccvValidate: true,
          [e.target.name]: value
        })
      }
    } else {
      if (value.length <= 4) {
        this.setState({
          ccvValidate: true,
          [e.target.name]: value
        })
      }
    }
    this.sendData()
  }

  public render() {
    const { flagName, flagCard1, flagCard2, flagCard3, flagCard4,
      flagExp, flagCcv, cardnumbervalidate, invalidExp, ccvValidate,
      disable } = this.props

    return (
      <Form>
        <Form.Item validateStatus={flagName ? 'error' : undefined}
          help={flagName ? Lang.get('setting.payment.required')
            : undefined}>
          <Input
            placeholder={Lang.get('setting.payment.name')}
            name="name"
            style={{ marginLeft: 10 }}
            disabled={disable}
            onChange={this.changeText}
          />
        </Form.Item>
        <Col span={6}>
          <Form.Item validateStatus={flagCard1 ? 'error' : undefined}
            help={cardnumbervalidate === false ?
              Lang.get('setting.payment.info_number') : flagCard1 ?
                Lang.get('setting.payment.required') : undefined}
          >
            <Input
              name="card_number"
              placeholder={'     XXXX'}
              style={{ width: 100 }}
              onChange={this.changeNumbCard}
              disabled={disable}
              value={this.state.card_number}
            />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item validateStatus={flagCard2 ? 'error' : undefined}>
            <Input
              name="card_number2"
              style={{ width: 100 }}
              placeholder={'     XXXX'}
              onChange={this.changeNumbCard}
              disabled={disable}
              value={this.state.card_number2}
            />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item validateStatus={flagCard3 ? 'error' : undefined}>
            <Input
              name="card_number3"
              style={{ width: 100 }}
              placeholder={'     XXXX'}
              onChange={this.changeNumbCard}
              value={this.state.card_number3}
              disabled={disable}
            />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item validateStatus={flagCard4 ? 'error' : undefined}>
            <Input
              style={{ width: 100, marginLeft: 5 }}
              name="card_number4"
              placeholder={'     XXXX'}
              onChange={this.changeNumbCard}
              disabled={disable}
              value={this.state.card_number4}
            />
          </Form.Item>
        </Col>
        <Col span={11}>
          <Form.Item validateStatus={flagExp ? 'error' : undefined}
            help={invalidExp ? Lang.get('setting.payment.invalid_exp') : flagExp ?
              Lang.get('setting.payment.required') : undefined}
          >
            <Input
              placeholder={Lang.get('setting.payment.exp')}
              name="exp"
              onChange={this.changeTextDate}
              value={this.state.exp}
              disabled={disable}
            />
          </Form.Item>
        </Col>
        <Col span={11}>
          <Form.Item validateStatus={flagCcv ? 'error' : undefined}
            help={!ccvValidate && this.state.ccv ? Lang.get('setting.payment.invalid_ccv')
              : flagCcv ? Lang.get('setting.payment.required') : undefined}
          >
            <Input
              placeholder={'ccv'}
              name="ccv"
              onChange={this.changeCCV}
              disabled={disable}
              value={this.state.ccv}
            />
          </Form.Item>
        </Col>
      </Form>
    )
  }
}

export default EditPaymentForm;