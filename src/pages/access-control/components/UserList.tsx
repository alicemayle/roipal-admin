import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';
import { Table, Icon, Tag, Button } from 'antd';
import Column from 'antd/lib/table/Column';
import FilterInterface from 'src/components/search/FilterInterface';
import FilterSearch from 'src/components/search/FilterSearch';
import UsersContainer from '../state/UsersContainer';
import IconButton from 'src/components/buttons/IconButton';
import UserUpsert from './UserUpsert';
import Pagination from 'src/components/pagination/Pagination';
import Lang from 'src/support/Lang';
import { Gate } from '@xaamin/guardian';

class UserList extends Component<any, any> {

  public searchInput: any = null;
  public searchField: string = '';

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    searchText: '',
    fields: [],
    limit: 50,
    page: 1
  }

  private getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: FilterInterface) => (
        <FilterSearch
          onSelectedKeys={setSelectedKeys}
          selectedKeys={selectedKeys}
          confirm={confirm}
          handleReset={this.handleReset}
          onSearchInput={this.setSearchInput}
          onSearch={this.handleSearch}
          clearFilters={clearFilters}
          dataIndex={dataIndex}
        />
      ),
    filterIcon: (filtered: string) =>
      <Icon type="search" style={{ color: filtered ? '#52bebb' : undefined }} />,
    onFilter: (value: string, record: []) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        this.searchField = dataIndex;
        setTimeout(() => this.searchInput.select());
      }
    }
  });

  private handleReset = (clearFilters: () => {}): void => {
    clearFilters();
    const data = this.state.fields;
    const newData = data.map((
      field: {
        name: string,
        status?: boolean
      }) => {

      if (field.name === this.searchField) {
        field = { ...field }
        field.status = false
      }
      return field;
    })

    this.setState({
      searchText: '',
      fields: newData
    });
  }

  private setSearchInput(node: {}) {
    this.searchInput = node;
  }

  private handleSearch = (selectedKeys: string[], confirm: () => void) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  private renderRoles(item: any) {
    return (
      <span>
        {
          item.roles.map((tag: any) => {
            return (
              <Tag color={'green'} key={tag.uuid}>
                {tag.name.toUpperCase()}
              </Tag>
            );
          })}
      </span>
    )
  }

  private renderActionAdd(record: any) {
    return (
      <IconButton
        item={record}
        onFuction={this.handleOpenUpsert}
        size={18}
        colorIcon={'#52bebb'}
        name={'plus-circle'}
      />
    )
  }

  private handleOpenUpsert(record: any = {}) {
    let _record;

    if (record && record.key) {
      _record = record;
    } else {
      _record = { ...this.state.record };
    }

    this.setState({
      record: _record,
      openUpsert: true
    })
  }

  private handleCloseUpsert() {
    this.setState({
      openUpsert: false,
      record: {
        key: '',
        uuid: '',
        email: '',
        name: '',
        roles: [],
        permissions: []
      }
    })
  }

  public handleChange(value: string, field: string) {
    const { record } = this.state;
    const _record = {
      ...record,
      [field]: value
    }
    this.setState({
      record: _record
    })
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.handleCloseUpsert();
      this.handleFetch();
    }
  }

  public handleChangeLimit(_limit: number, _page: number) {
    this.setState({
      limit: _limit,
      page: _page
    })

    this.handleFetch(_limit, _page)
  }

  public async handleFetch(limit?: number, page?: number) {
    const { users } = this.props.containers;
    const _limit = this.state.limit;
    const _page = this.state.page;

    const meta = {
      limit: limit || _limit.toString(),
      page: page || _page,
      includes: 'roles,permissions'
    }

    await users.fetch(meta);
  }

  public componentDidMount() {
    this.handleFetch()
  }

  public render() {
    const { data, httpBusy, meta } = this.props.containers.users.state
    const { limit, openUpsert, record } = this.state

    return (
      <div>
        <div style={{ display: 'flex', 'justifyContent': 'space-between' }}>
          <h1>{}</h1>
          {
            Gate.allows('user::create') &&
            <Button
              type="primary"
              style={{ marginBottom: 16 }}
              icon="plus"
              onClick={this.handleOpenUpsert}
            >
              {Lang.get('access.button.add')}
            </Button>
          }
        </div>
        <Table
          dataSource={data}
          loading={httpBusy}
          pagination={false}
        >
          <Column
            title={Lang.get('access.user.date')}
            dataIndex="created_at"
            key="created_at"
            align={'center'}
          />
          <Column
            title={Lang.get('access.user.name')}
            dataIndex="name"
            key="name"
            align={'center'}
            {...this.getColumnSearchProps('name')}
          />
          <Column
            title={Lang.get('access.user.email')}
            dataIndex="email"
            key="email"
            align={'center'}
            {...this.getColumnSearchProps('email')}
          />
          <Column
            title={Lang.get('access.user.roles')}
            key="rol"
            render={this.renderRoles}
            align={'center'}
          />
          {
            Gate.allows('user::edit') &&
            <Column
              title={Lang.get('access.button.edit')}
              key="permisos"
              align={'center'}
              render={this.renderActionAdd}
            />
          }
        </Table>
        {
          meta &&
          <Pagination
            limit={limit}
            meta={meta.cursor}
            onChange={this.handleChangeLimit}
          />
        }
        <UserUpsert
          record={record}
          openUpsert={openUpsert}
          onClose={this.handleCloseUpsert}
          onChange={this.handleChange}
          onComplete={this.handleOnComplete}
        />
      </div>
    )
  }
}

const container = {
  users: UsersContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    users: containers.users.state,
  }
}

export default connect(container, mapStateToProps)(UserList);