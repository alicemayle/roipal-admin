import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Checkbox } from 'antd';
import { Logger } from 'unstated-enhancers';

class PermissionCheckbox extends Component<any, any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  private change(e: any) {
    const { onChange, item } = this.props

    onChange(item, e.target.checked)
  }

  public render() {
    const { item } = this.props
    Logger.count(this)

    return (
      <Checkbox
        key={item.value}
        onClick={this.change}
        defaultChecked={item.check}
        checked={item.check}
      >
        {item.label}
      </Checkbox>
    )
  }
}

export default (PermissionCheckbox);