import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Modal, Button, Divider, Row, Col, message } from 'antd';
import RoleCreateForm from './RoleCreateForm';
import { connect } from 'unstated-enhancers';
import RoleCheckbox from './RoleCheckbox';
import Language from '../../languages/containers/LanguageListLanguageContainer';
import UpdateRolesContainer from '../state/UpdateRolesContainer';
import PermissionsCatalogContainer from '../state/PermissionsCatalogContainer';
import Lang from 'src/support/Lang';
import Notifications from 'src/components/notifications/notifications';
import RolesGroupContainer from '../state/RolesGroupContainer';
import { Gate } from '@xaamin/guardian';

const pStyle = {
  fontSize: 16,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16
};

class CreateRole extends Component<any, any> {
  public form: any = null;
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    addPermissions: [],
    addRolName: [],
    translations: [],
    nameRol: ''
  }

  private cancel() {
    const { onChange } = this.props

    this.setState({
      addPermissions: [],
    })

    if (onChange) {
      onChange('createVisible')
    }
  }

  private changeRoleName(name: any, trans: any) {
    const { addRolName } = this.state

    const newForm = {
      ...addRolName,
      translation: trans
    }

    this.setState({
      addRolName: newForm,
      nameRol: name
    })
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  private setGroup(value: any) {
    this.setState({
      group: value
    })
  }

  public async handleSubmit() {
    const { addRolName, nameRol } = this.state
    const { update } = this.props.containers

    this.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        if (Gate.allows('role::create')) {
          const data = {
            name: nameRol,
            group: this.state.group,
            ...addRolName
          }
          if (data) {
            await update.createRole(data)
            const { error, createData } = update.state

            if (!error) {
              this.addPermissions(createData)
            } else {
              const notification = new Notifications;

              const msj = {
                type: 'error',
                message: Lang.get('access.role.error_permissions')
              }

              notification.openNotification(msj)
            }
          }
        } else {
          message.error(Lang.get('generic.policies.denied'));
        }
      }
    });
  }

  private async addPermissions(data: any) {
    const { update, group } = this.props.containers
    const { addPermissions } = this.state

    const newData = {
      permissions: addPermissions
    }

    if (addPermissions) {
      await update.sendPermissions(data.uuid, newData)

      const { error } = this.props.containers

      if (!error) {
        const { sendPermissions } = update.state
        await group.addDataCreate(sendPermissions)
        this.cancel()
      }
    }
  }

  private renderPermissions(item: any) {
    const { group } = this.state
    return (
      <Col span={12} key={item.value}>
        <RoleCheckbox
          item={item}
          onChange={this.selectedPermissions}
          disable={group ? false : true}
        />
      </Col>
    )
  }

  private selectedPermissions(item: any, check: any) {
    if (check) {
      const add = this.state.addPermissions

      const format = {
        permission_uuid: item.uuid,
        status: 1
      }

      add.push(format)

      this.setState({
        addPermissions: add
      })
    }
    if (!check) {
      const _delete = this.state.addPermissions
      const findB = _delete.find((fin: any) => fin.permission_uuid === item.uuid)
      const index = _delete.indexOf(findB)

      if (index > -1) {
        _delete.splice(index, 1)

        this.setState({
          addPermissions: _delete
        })
      }
    }
  }

  private getLanguages() {
    const { language } = this.props.containers;
    const meta = {
      limit: '1'
    }
    language.fetch(meta);
  }

  public shouldComponentUpdate(nextProps: any, nextState: any) {
    return (
      nextProps.visible !== this.props.visible ||
      nextState.group !== this.state.group ||
      nextState.addPermissions !== this.state.addPermissions ||
      nextProps.language.data !== this.props.language.data ||
      nextProps.httpBusy !== this.props.httpBusy
    )
  }

  public componentDidMount() {
    this.getLanguages()
  }

  public render() {
    const { visible, permissions, groups } = this.props
    const { data } = this.props.language
    const { group } = this.state
    const { httpBusy } = this.props.containers.update.state

    return (
      <Modal
        title={Lang.get('access.role.new_role')}
        visible={visible}
        destroyOnClose={true}
        centered={true}
        width={'60%'}
        onCancel={this.cancel}
        footer={[
          <Button
            key="cancel"
            onClick={this.cancel}>
            {Lang.get('access.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={this.handleSubmit}
            loading={httpBusy}
          >
            {Lang.get('access.button.save')}
          </Button>
        ]}
      >
        <RoleCreateForm
          onValidate={this.handleValidateForm}
          onChangeRoleName={this.changeRoleName}
          onSelectPermissions={this.selectedPermissions}
          languages={data}
          groups={groups}
          onSelectGroup={this.setGroup}
          groupSelected={group}
        />
        <Divider />
        <p style={{ ...pStyle, marginBottom: 24 }}>
          {Lang.get('access.role.select_permission')}
        </p>
        <Row>
          {permissions &&
            permissions.map(this.renderPermissions)
          }
        </Row>
      </Modal>
    )
  }
}

const container = {
  language: Language,
  update: UpdateRolesContainer,
  addPer: PermissionsCatalogContainer,
  group: RolesGroupContainer,
}

const mapStateToProps = (containers: any) => {
  return {
    language: containers.language.state,
    update: containers.update.state,
    addPer: containers.addPer.state,
    group: containers.group.state,
  }
}

export default connect(container, mapStateToProps)(CreateRole);