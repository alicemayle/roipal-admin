import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import { Form, Input, Select } from 'antd';
import Lang from 'src/support/Lang';

const { Option } = Select;

class RoleCreateForm extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
  }

  public state: any = {
  }

  private setText(value: any) {
    const { groupSelected, languages } = this.props
    const con = groupSelected.concat('_').concat(value.target.value)
    const newData = languages.map((item: any) => {

      const form = {
        id: item.id,
        namespace: '*',
        group: groupSelected,
        item: con,
        text: value.target.value
      }
      return form
    })

    this.props.onChangeRoleName(value.target.value, newData)
  }

  private renderGroup(item: any) {
    return (
      <Option
        value={item}
        key={item}
      >
        {item}
      </Option>
    )
  }

  private setGroup(find: string) {
    const { onSelectGroup } = this.props

    if (onSelectGroup) {
      onSelectGroup(find)
    }
  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { groups, groupSelected } = this.props

    return (
      <Form>
        <Form.Item label={Lang.get('access.role.group')}>
          {getFieldDecorator('group', {
            rules: [{ required: true, message: Lang.get('access.role.select_group') }],
          })(
            <Select style={{ width: 200 }}
              placeholder={Lang.get('access.role.select_group')}
              onChange={this.setGroup}
            >
              {
                groups &&
                groups.map(this.renderGroup)
              }
            </Select>
          )}
        </Form.Item>
        <Form.Item label={Lang.get('access.role.name_rol')}>
          {getFieldDecorator('name', {
            rules: [{
              required: true,
              message: Lang.get('access.role.select_rol')
            }],
          })(
            <Input
              placeholder={Lang.get('access.role.select_rol')}
              name="name"
              style={{ width: 300 }}
              disabled={groupSelected ? false : true}
              onChange={this.setText}
            />
          )}
        </Form.Item>
      </Form>
    );
  }
}

const WrapperRoleCreateForm: any = Form.create({
  name: 'RoleCreateForm'
})(RoleCreateForm);

export default WrapperRoleCreateForm;