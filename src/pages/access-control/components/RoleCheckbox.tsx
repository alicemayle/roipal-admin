import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Checkbox } from 'antd';
import { Logger } from 'unstated-enhancers';

class RoleCheckbox extends Component<any, any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
  }

  private change(e: any) {
    const { onChange } = this.props

    if (onChange) {
      onChange(this.props.item, e.target.checked)
    }
  }

  public render() {
    const { item, disable } = this.props
    Logger.count(this)

    return (
      <Checkbox
        key={item.value}
        onClick={this.change}
        defaultChecked={item.check}
        disabled={disable}
      >
        {item.label}
      </Checkbox>
    )
  }
}

export default (RoleCheckbox);