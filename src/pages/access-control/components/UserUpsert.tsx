import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Modal, Button, Row, Col, Collapse, message } from 'antd';
import { connect, Logger } from 'unstated-enhancers';
import UpdateUserContainer from '../state/UpdateUserContainer';
import RoleCatalogContainer from '../state/RoleCatalogContainer';
import PermissionsCatalogContainer from '../state/PermissionsCatalogContainer';
import RoleCheckbox from './RoleCheckbox'
import Lang from 'src/support/Lang';
import UserUpsertForm from './UserUpsertForm';
import Notifications from '../../../components/notifications/notifications';
import Event from '../../../support/Event';
import { Gate } from '@xaamin/guardian';
import PermissionCheckbox from './PermissionCheckbox'

const { Panel } = Collapse;
class UserUpsert extends Component<any, any> {
  public form: any = null;

  constructor(props: any) {
    super(props);
    autobind(this)
    Event.listen('access::user.edit', this.listeForUpsert);
  }

  public state: any = {
    updateRoles: [],
    roleCatalog: [],
    permissionCatalog: [],
    updatePermissions: []
  }

  private handleChange(value: string, field: string) {
    this.props.onChange(value, field);
  }

  public handleValidateForm(form: any) {
    this.form = form;
  }

  private async handleChangeRole(item: any, checked: boolean) {
    const _updateRoles = [...this.state.updateRoles]
    const _roleCatalog = [...this.state.roleCatalog]
    const _permissionCatalog = [...this.state.permissionCatalog]
    const _updatePermissions = [...this.state.updatePermissions]
    const _initialPermissions = this.props.record.permissions 
      ? [...this.props.record.permissions] : []

    const found = _roleCatalog.find((role: any) =>
      role.uuid === item.uuid && role.check === true
    )
    const index = _updateRoles.findIndex((ele: any) => ele.uuid === item.value)

    const _updatePermissionsCatalog = _permissionCatalog.map((elem: any) => {
      let permission = { ...elem }

      if (item.permissions) {
        const find = item.permissions.find((perm: any) => perm.uuid === elem.uuid)

        if (find) {
          permission = {
            ...permission,
            check: checked
          }
        }
      }

      return permission
    })

    if (checked) {
      const _role = {
        ...item,
        status: 1
      }

      if (!found) {
        if (index > -1) {
          _updateRoles.splice(index, 1, _role)
        } else {
          _updateRoles.push(_role)
        }
      } else {
        if (index > -1) {
          _updateRoles.splice(index, 1)
        }
      }

      if (item.permissions) {
        item.permissions.map((elem: any) => {
          const _permission = {
            ...elem,
            status: 1
          }
          const initialFound = _initialPermissions.find((ele: any) => 
            ele.uuid === elem.uuid
          )
          const indexUpdate = _updatePermissions.findIndex((ele: any) =>
            ele.uuid === elem.uuid
          )
    
          if(!initialFound) {
            if (indexUpdate > -1) {
              _updatePermissions.splice(indexUpdate, 1, _permission)
            } else {
              _updatePermissions.push(_permission)
            }
          } else {
            if (indexUpdate > -1) {
              _updatePermissions.splice(indexUpdate, 1)
            }
          }
        })
      }
    } else {
      const _role = {
        ...item,
        status: 0
      }

      if (found) {
        if (index > -1) {
          _updateRoles.splice(index, 1, _role)
        } else {
          _updateRoles.push(_role)
        }
      } else {
        if (index > -1) {
          _updateRoles.splice(index, 1)
        }
      }

      if (item.permissions) {
        item.permissions.map((elem: any) => {
          const _permission = {
            ...elem,
            status: 0
          }
          const initialFound = _initialPermissions.find((ele: any) => 
            ele.uuid === elem.uuid
          )
          const indexUpdate = _updatePermissions.findIndex((ele: any) =>
            ele.uuid === elem.uuid
          )
    
          if (initialFound) {
            if (indexUpdate > -1) {
              _updatePermissions.splice(indexUpdate, 1, _permission)
            } else {
              _updatePermissions.push(_permission)
            }
          } else {
            if (indexUpdate > -1) {
              _updatePermissions.splice(indexUpdate, 1)
            }
          }
        })
      }
    }
    this.setState({
      ...this.state,
      updateRoles: _updateRoles,
      permissionCatalog: _updatePermissionsCatalog,
      updatePermissions: _updatePermissions,
    }, () => Logger.dispatch('changes role', this.state))
  }

  private async handleChangePermission(item: any, checked: boolean) {
    const _updatePermissions = [...this.state.updatePermissions]
    const _permissionCatalog = [...this.state.permissionCatalog]
    const _initialPermissions = this.props.record.permissions 
      ? [...this.props.record.permissions] : []

    const initialFound = _initialPermissions.find((ele: any) => 
      ele.uuid === item.uuid
    )
    const indexUpdate = _updatePermissions.findIndex((ele: any) =>
      ele.uuid === item.value
    )
    const indexCat = _permissionCatalog.findIndex((perm: any) => 
      perm.uuid === item.uuid
    )

    if (indexCat) {
      const permission = {
        ...item,
        check: checked
      }
      _permissionCatalog.splice(indexCat, 1, permission)
    }

    if (checked) {
      const _permission = {
        ...item,
        status: 1
      }

      if(!initialFound) {
        if (indexUpdate > -1) {
          _updatePermissions.splice(indexUpdate, 1, _permission)
        } else {
          _updatePermissions.push(_permission)
        }
      } else {
        if (indexUpdate > -1) {
          _updatePermissions.splice(indexUpdate, 1)
        }
      }

    } else {
      const _permission = {
        ...item,
        status: 0
      }

      if (initialFound) {
        if (indexUpdate > -1) {
          _updatePermissions.splice(indexUpdate, 1, _permission)
        } else {
          _updatePermissions.push(_permission)
        }
      } else {
        if (indexUpdate > -1) {
          _updatePermissions.splice(indexUpdate, 1)
        }
      }
    }

    this.setState({
      ...this.state,
      updatePermissions: _updatePermissions,
      permissionCatalog: _permissionCatalog
    }, () => Logger.dispatch('changes permission', this.state))
  }

  private renderItemRole(item: any) {
    return (
      <Col span={12} key={item.uuid}>
        <RoleCheckbox
          item={item}
          key={item.uuid}
          onChange={this.handleChangeRole}
        />
      </Col>
    )
  }

  private renderItemPermission(item: any) {
    return (
      <Col span={12} key={item.uuid}>
        <PermissionCheckbox
          item={item}
          onChange={this.handleChangePermission}
        />
      </Col>
    )
  }

  private async handleSubmit() {
    const { record } = this.props
    const { updatePermissions, updateRoles } = this.state
    const { update } = this.props.containers

    this.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        if ((record && record.uuid && Gate.allows('user::edit'))
          || Gate.allows('user::create')) {
          let data = {}

          data = {
            name: values.name,
            email: values.email,
            classification: 'admin',
          }

          if (updatePermissions.length > 0) {
            data = {
              ...data,
              permissions: updatePermissions
            }
          }

          if (updateRoles.length > 0) {
            data = {
              ...data,
              roles: updateRoles
            }
          }

            if (record.uuid) {
              if (updatePermissions.length > 0 || updateRoles.length > 0) {
                update.updateUser(record.uuid, data)
              }
            } else {
              update.createUser(data)
            }
        } else {
          message.error(Lang.get('generic.policies.denied'));
        }
      }
    });
  }

  private async getRoleCatalog(record?: any) {
    const { roleCatalog } = this.props.containers;

    if (roleCatalog.state.data.length < 1) {
        const meta = {
          includes: 'permissions'
        }

        await roleCatalog.getRoles(meta)

        const { error, data } = roleCatalog.state;

        if (!error) {
          if (record.uuid) {
            const _roles = record.roles || []
            const _checkRoles = data

            _roles.map((role: any) => {
              const index = data.findIndex((element: any) => element.uuid === role.uuid)
              if (index > -1) {
                const _role = {
                  ...data[index],
                  label: role.name,
                  value: role.uuid,
                  check: true
                }
                _checkRoles.splice(index, 1, _role)
              }
            })

            this.setState({
              roleCatalog: _checkRoles
            })
          } else {
            this.setState({
              roleCatalog: data
            })
          }
        }
    } else {
      if (record.uuid) {
        const { data } = roleCatalog.state;
        const _roles = record.roles || []
        const _checkRoles = data

        _roles.map((role: any) => {
          const index = data.findIndex((element: any) => element.uuid === role.uuid)
          if (index > -1) {
            const _role = {
              ...data[index],
              label: role.name,
              value: role.uuid,
              check: true
            }
            _checkRoles.splice(index, 1, _role)
          }
        })

        this.setState({
          roleCatalog: _checkRoles
        })
      }
    }
  }

  private handleClose() {
    const { roleCatalog, permissionCatalog } = this.props.containers

    this.props.onClose();

    setTimeout(() => {
      permissionCatalog.reset()
      roleCatalog.reset()

      this.setState(({
        roleCatalog: [],
        permissionCatalog: []
      }))
    }, 500)
  }

  private async getPermissionCatalog(record: any) {
    const { permissionCatalog } = this.props.containers

    if (permissionCatalog.state.data.length < 1) {

      const params = {
        limit: 50
      }

      await permissionCatalog.getPermissionsCatalog(params)
    }

    const { error, data } = permissionCatalog.state;

    if (!error && data) {
      if (record.uuid) {
        const _permissions = record.permissions || []
        const _checkPermissions = data

        _permissions.map((per: any) => {
          const index = data.findIndex((element: any) => element.uuid === per.uuid)

          if (index > -1) {
            const _permission = {
              ...per,
              label: per.name,
              value: per.uuid,
              check: true
            }
            _checkPermissions.splice(index, 1, _permission)
          }
        })

        this.setState({
          permissionCatalog: _checkPermissions
        })
      } else {
        this.setState({
          permissionCatalog: data
        })
      }
    }
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.props.onComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.props.onComplete('error');
    }

    notification.openNotification(msj)
  }

  public componentWillUnmount() {
    Event.remove('access::user.edit', this.listeForUpsert);
  }

  public async componentWillReceiveProps(nextProps: any) {
    if (nextProps.record !== this.props.record) {
      this.getRoleCatalog(nextProps.record)
      this.getPermissionCatalog(nextProps.record)
    }
  }

  public render() {
    const { openUpsert, record } = this.props
    const { httpBusy } = this.props.update;
    const { roleCatalog, permissionCatalog } = this.state
    Logger.count(this)

    return (
      <div>
        <Modal
          title={record && record.uuid
            ? Lang.get('access.button.edit')
            : Lang.get('access.button.add')}
          style={{ top: 20 }}
          width={'70%'}
          visible={openUpsert}
          centered={true}
          onCancel={this.handleClose}
          destroyOnClose={true}
          footer={[
            <Button
              key="cancel"
              onClick={this.handleClose}>
              {Lang.get('access.button.cancel')}
            </Button>,
            <Button
              key="submit"
              type="primary"
              onClick={this.handleSubmit}
              loading={httpBusy}
            >
              {Lang.get('access.button.save')}
            </Button>
          ]}
        >
          <UserUpsertForm
            record={record}
            onChange={this.handleChange}
            onValidate={this.handleValidateForm}
          />
          {roleCatalog && roleCatalog.length > 0 &&
            <div>
              <Collapse activeKey="1">
                <Panel
                  header={Lang.get('access.user.roles')}
                  key="1"
                >
                  <Row>
                    {roleCatalog.map(this.renderItemRole)}
                  </Row>
                </Panel>
              </Collapse>
            </div>
          }
          <br />
          {permissionCatalog && permissionCatalog.length > 0 &&
            <div>
              <Collapse>
                <Panel
                  header={Lang.get('access.user.permissions')}
                  key="1"
                >
                  <Row>
                    {permissionCatalog.map(this.renderItemPermission)}
                  </Row>
                </Panel>
              </Collapse>

            </div>
          }
        </Modal>
      </div >
    )
  }
}

const container = {
  roleCatalog: RoleCatalogContainer,
  update: UpdateUserContainer,
  permissionCatalog: PermissionsCatalogContainer
}

const mapStateToProps = (containers: any) => {
  return {
    roleCatalog: containers.roleCatalog.state,
    update: containers.update.state,
    permissionCatalog: containers.permissionCatalog.state
  }
}

export default connect(container, mapStateToProps)(UserUpsert);