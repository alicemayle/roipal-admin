import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Row, Col, Modal, Button, message } from 'antd';
import { connect } from 'unstated-enhancers';
import PermissionsCatalogContainer from '../state/PermissionsCatalogContainer';
import RoleCheckbox from './RoleCheckbox';
import Lang from 'src/support/Lang';
import RolesGroupContainer from '../state/RolesGroupContainer';
import Notifications from '../../../components/notifications/notifications';
import Event from 'src/support/Event';
import UpdateRolesContainer from '../state/UpdateRolesContainer';
import { Gate } from '@xaamin/guardian';

const pStyle = {
  fontSize: 18,
  color: 'rgba(0,0,0,0.85)',
  lineHeight: '24px',
  display: 'block',
  marginBottom: 16
};

class RoleDetails extends Component<any, any> {

  constructor(props: any) {
    super(props);
    autobind(this)
    Event.listen('access::role.edit', this.listeForUpsert);
  }

  public state: any = {
    newData: [],
    modifiedPermissions: []
  }

  private selectedPermissions(item: any, check: any) {
    if (check) {
      const _add = this.state.modifiedPermissions
      const original = this.props.data.permissions
      const find = original.find((fin: any) => fin.uuid === item.uuid)

      if (!find) {
        const format = {
          permission_uuid: item.uuid,
          status: 1
        }

        _add.push(format)

        this.setState({
          modifiedPermissions: _add
        })
      }
    }
    if (!check) {
      const _delete = this.state.modifiedPermissions
      const original = this.props.data.permissions
      const find = original.find((fin: any) => fin.uuid === item.uuid)

      if (find) {
        const format = {
          permission_uuid: item.uuid,
          status: 0
        }
        _delete.push(format)

        this.setState({
          modifiedPermissions: _delete
        })
      } else {
        const findB = _delete.find((fin: any) => fin.permission_uuid === item.uuid)
        const index = _delete.indexOf(findB)

        if (index > -1) {
          _delete.splice(index, 1)
          this.setState({
            modifiedPermissions: _delete
          })
        }
      }
    }
  }

  private getCheckPermissions(newPermission?: any) {
    const data = newPermission ? newPermission : this.props.data
    const permissions = this.props.containers.permissions.state.data

    const newFormat = permissions.map((item: any) => {
      const find = data.permissions.find((fin: any) => fin.uuid === item.value)
      if (find) {
        const format = {
          ...item,
          check: true
        }
        return format
      } else {
        const format = {
          ...item,
        }
        return format
      }
    })

    this.setState({
      newData: newFormat
    })
  }

  private async editPermissions() {
    if (Gate.allows('role::edit')) {
      const { close, data } = this.props
      const { upsert } = this.props.containers
      const { modifiedPermissions } = this.state

      const newData = {
        permissions: modifiedPermissions
      }

      if (modifiedPermissions) {
        await upsert.sendPermissions(data.uuid, newData)
        const { error } = upsert.state

        if (!error) {
          close('visible')
        }
      }
    } else {
      message.error(Lang.get('generic.policies.denied'));
    }
  }

  private listeForUpsert(result: {
    error?: {
      message: string
    },
    message: string,
    action: string
  }
  ) {
    let msj = {}
    const notification = new Notifications;

    if (!result.error) {
      msj = {
        type: 'success',
        message: result.message
      }

      this.props.onComplete('success');
    } else {
      msj = {
        type: 'error',
        message: result.error.message
      }

      this.props.onComplete('error');
    }

    notification.openNotification(msj)
  }

  private cancel() {
    const { close } = this.props

    this.setState({
      modifiedPermissions: []
    })

    if (close) {
      close('visible')
    }
  }

  private renderPermissions(item: any) {
    return (
      <Col span={12} key={item.value}>
        <RoleCheckbox
          item={item}
          onChange={this.selectedPermissions}
        />
      </Col>
    )
  }

  public componentWillUnmount() {
    Event.remove('access::role.edit', this.listeForUpsert);
  }

  public componentDidMount() {
    this.getCheckPermissions()
  }

  public componentWillReceiveProps(nextProps: any) {
    if (this.props.data !== nextProps.data) {
      this.getCheckPermissions(nextProps.data)
    }
  }

  public render() {
    const { visible, data } = this.props
    const { newData } = this.state
    const { httpBusy } = this.props.containers.permissions.state

    return (
      <Modal
        title={data.name}
        destroyOnClose={true}
        onCancel={this.cancel}
        visible={visible}
        centered={true}
        width={'60%'}
        footer={[
          <Button
            key="cancel"
            onClick={this.cancel}>
            {Lang.get('access.button.cancel')}
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={this.editPermissions}
            loading={httpBusy}
          >
            {Lang.get('access.button.save')}
          </Button>
        ]}
      >
        <p style={{ ...pStyle, marginBottom: 24 }}>{Lang.get('access.role.select_permission')}</p>
        <Row>
          {
            newData.map(this.renderPermissions)
          }
        </Row>
      </Modal>
    )
  }
}

const container = {
  permissions: PermissionsCatalogContainer,
  group: RolesGroupContainer,
  upsert: UpdateRolesContainer
}

const mapStateToProps = (containers: any) => {
  return {
    permissions: containers.permissions.state,
    group: containers.group.state,
    upsert: containers.upsert.state
  }
}

export default connect(container, mapStateToProps)(RoleDetails);