import React, { PureComponent } from 'react';
import Autobind from 'class-autobind';
import Lang from '../../../support/Lang';
import { Form, Input } from 'antd';

class UserUpsertForm extends PureComponent<any> {
  constructor(props: any) {
    super(props);
    Autobind(this);
    this.state = {
    }
  }

  private handleChange(e: any) {
    const value = e.target.value;
    const field = e.target.name;

    this.props.onChange(value, field)
  }

  public componentDidMount() {
    this.props.onValidate(this.props.form)
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const { record } = this.props;

    return (
      <Form  >
        <Form.Item label={Lang.get('access.user.email')}>
          {getFieldDecorator('email', {
            initialValue: record.uuid ? record.email : '',
            rules: [{
              required: true,
              type: 'email',
              message: Lang.get('access.user.validations.email')
            }],
          })(
            <Input
              placeholder={Lang.get('access.user.email')}
              name="email"
              disabled={record.uuid ? true : false}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>

        <Form.Item label={Lang.get('access.user.name')}>
          {getFieldDecorator('name', {
            initialValue: record.uuid ? record.name : '',
            rules: [{
              required: true,
              message: Lang.get('access.user.validations.name')
            }],
          })(
            <Input
              placeholder={Lang.get('access.user.name')}
              name="name"
              disabled={record.uuid ? true : false}
              onChange={this.handleChange}
            />
          )}
        </Form.Item>
      </Form>
    );
  }
}

const WrapperUserUpsertForm: any = Form.create({ name: 'UserUpsertForm' })(UserUpsertForm);

export default WrapperUserUpsertForm;