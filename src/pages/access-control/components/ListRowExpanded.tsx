import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Row, Col, List } from 'antd';
import Text from 'antd/lib/typography/Text';
import Lang from 'src/support/Lang';

class ListRowExpanded extends Component<any, any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
  }

  private renderItem(item: any) {

    return (
      <List.Item>
        <Row>
          <Col>
            <Text>{item.definition}</Text>
          </Col>
        </Row>
      </List.Item>
    )
  }

  public render() {
    const { data } = this.props

    return (
      <div>
        <p style={{ fontWeight: 'bold' }}>
          {Lang.get('access.role.list_permissions')}
        </p>
        <br />
        <List
          grid={{ column: 3 }}
          dataSource={data.permissions}
          renderItem={this.renderItem}
        />
        </div>
    )
  }
}

export default (ListRowExpanded);