import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';
import RolesGroupContainer from '../state/RolesGroupContainer';
import { Button, Table, Row, Col } from 'antd';
import Column from 'antd/lib/table/Column';
import SignInContainer from 'src/auth/state/SignInContainer';
import IconButton from 'src/components/buttons/IconButton';
import RoleDetails from './RoleDetails';
import PermissionsCatalogContainer from '../state/PermissionsCatalogContainer';
import CreateRolModal from './CreateRole';
import Lang from 'src/support/Lang';
import Notifications from 'src/components/notifications/notifications';
import ListRowExpanded from './ListRowExpanded';
import { Gate } from '@xaamin/guardian';

// const { Option } = Select;

class RoleList extends Component<any, any> {

  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state: any = {
    pass: false,
    visible: false,
    roleSelected: null,
    createVisible: false
  }

  private async getRolesUser(find?: string) {
    const { group } = this.props.containers
    // const rolesGroup = signing.state.data.groups

    // const findGroup = find ? find : rolesGroup[0]

    // if (findGroup) {
      const params = {
        includes: 'permissions',
        // group: findGroup
      }
      await group.getRolesGroup(params)
      const { error } = this.props.containers.group.state

      if (error) {
        const notification = new Notifications;

        const msj = {
          type: 'error',
          message: Lang.get('access.role.error_group')
        }

        notification.openNotification(msj)
      }
    // }
  }

  private async getPermissions() {
    const { permissions, error } = this.props.containers
    const params = {
      limit: 50
    }

    await permissions.getPermissionsCatalog(params)

    if (error) {
      const notification = new Notifications;

      const msj = {
        type: 'error',
        message: Lang.get('access.role.error_permissions')
      }

      notification.openNotification(msj)
    }
    this.getRolesUser()
  }

  private renderItem(record: any) {
    return (
      <IconButton
        item={record}
        onFuction={this.showDetails}
        size={18}
        colorIcon={'#52bebb'}
        name={'edit'}
      />
    )
  }

  private showDetails(data: any) {
    this.setState({
      roleSelected: data
    })

    this.changeVisible('visible')
  }

  private changeVisible(option: any) {
    this.setState({
      [option]: !this.state[option]
    })
  }

  // private renderGroup(item: any) {
  //   return (
  //     <Option
  //       value={item}
  //       key={item}>
  //       {item}
  //     </Option>
  //   )
  // }

  private renderRowExpanded(record: any) {
    return (
      <ListRowExpanded
        data={record} />
    );
  }

  private createRol() {
    this.changeVisible('createVisible')
  }

  public handleOnComplete(type: string) {
    if (type === 'success') {
      this.getPermissions();
    }
  }

  public componentDidMount() {
    this.getPermissions()
  }

  public render() {
    const { data = {}, httpBusy } = this.props.containers.group.state
    const permissions = this.props.containers.permissions.state.data
    const { roleSelected } = this.state
    // const { signing } = this.props.containers
    // const rolesGroup = signing.state.data.groups

    return (
      <div>
        <Row gutter={16}>
          <Col className="gutter-row" span={16}>
            <h1>{}</h1>
          </Col>
          <Col className="gutter-row" span={8} style={{ textAlign: 'right' }}>
            {
              Gate.allows('role::create') &&
              <Button
                type="primary"
                onClick={this.createRol}
                style={{ marginBottom: 16, marginRight: 10 }}
              >
                {Lang.get('access.button.add')}
              </Button>
            }
            {/* <Select style={{ width: 200 }}
              placeholder={Lang.get('access.role.select_group')}
              defaultValue={rolesGroup[0]}
              onChange={this.getRolesUser}>
              {
                rolesGroup &&
                rolesGroup.map(this.renderGroup)
              }
            </Select> */}
          </Col>
        </Row>
        <Table
          dataSource={data}
          pagination={false}
          loading={httpBusy}
          className="access-control-roles-table"
          expandedRowRender={this.renderRowExpanded}
        >
          <Column
            title={Lang.get('access.role.date')}
            dataIndex="created_at"
            key="created_at"
            align={'center'}
          />
          <Column
            title={Lang.get('access.role.name')}
            dataIndex="name"
            key="name"
            align={'center'}
          />
          <Column
            title={Lang.get('access.role.group')}
            dataIndex="group"
            key="group"
            align={'center'}
          />
          {
            Gate.allows('role::edit') &&
            <Column
              title={Lang.get('access.role.edit')}
              key="edit"
              render={this.renderItem}
              align={'center'}
            />
          }
        </Table>
        {
          roleSelected &&
          <RoleDetails
            data={this.state.roleSelected}
            close={this.changeVisible}
            visible={this.state.visible}
            onComplete={this.handleOnComplete}
          />
        }
        {
          permissions &&
          <CreateRolModal
            visible={this.state.createVisible}
            onChange={this.changeVisible}
            permissions={permissions}
            // groups={rolesGroup}
          />
        }
      </div>
    )
  }
}

const container = {
  group: RolesGroupContainer,
  signing: SignInContainer,
  permissions: PermissionsCatalogContainer
}

const mapStateToProps = (containers: any) => {
  return {
    group: containers.group.state,
    signing: containers.signing.state,
    permissions: containers.permissions.state
  }
}

export default connect(container, mapStateToProps)(RoleList);