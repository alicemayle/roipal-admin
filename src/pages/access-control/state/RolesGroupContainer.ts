import { Container } from 'unstated';
import ControlService from '../services/ControlService'
import moment from 'moment';

const initialState = {
  httpBusy: false,
  message: '',
  data: null,
  error: null,
}

class RolesGroupContainer extends Container<any> {
  public state: any = {
    ...initialState
  };

  public name: string = 'Get Rol By Group Containert';

  public async getRolesGroup(group: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Roles by group starts'
      });

      const response = await ControlService.getRolesGroup(group);

      response.data.map((item: any) => {
        item.key = item.uuid
        item.created_at = moment(new Date(item.created_at)).format('DD-MM-YYYY')
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Get Roles by group success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Get Roles by group error'
      });
    }
  }

  public addDataCreate(item: any) {
    const original = this.state.data

    const format = {
      ...item,
      key: item.uuid,
      created_at: moment(new Date(item.created_at)).format('DD-MM-YYYY')
    }

    original.unshift(format)

    this.setState({
      data: original
    })
  }
}

export default RolesGroupContainer;