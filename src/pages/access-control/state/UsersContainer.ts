import { Container } from 'unstated';
import ControlService from '../services/ControlService'
import moment from 'moment';

const initialState = {
  httpBusy: false,
  message: '',
  data: null,
  error: null,
  meta: null,
}

class UsersContainer extends Container<any> {
  public state: any = {
    ...initialState
  };

  public name: string = 'Users List Containert';

  public async fetch(params: any) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Users List starts'
      });

      const response = await ControlService.getUsers(params);

      const newData = response.data.map((item: any) => {
        const date = moment(new Date(item.created_at)).format('DD-MM-YYYY')
        const format = {
          ...item,
          created_at: date,
          key: item.uuid
        }
        return format
      })

      this.setState({
        httpBusy: false,
        data: newData,
        meta: response.meta,
        __action: 'Users List success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Users List error'
      });
    }
  }
}

export default UsersContainer;