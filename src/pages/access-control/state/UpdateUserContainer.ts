import { Container } from 'unstated';
import ControlService from '../services/ControlService'
import Event from '../../../support/Event';
import Lang from 'src/support/Lang';

const initialState = {
  httpBusy: false,
  message: '',
  data: null,
  error: null,
}

class UpdateUserContainer extends Container<any> {
  public state: any = {
    ...initialState
  };

  public name: string = 'Update User Container';

  public async updateUser(uuid:any, data:any) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Update User starts'
      });

      const response = await ControlService.updateUser(uuid, data);

      result = {
        message: Lang.get('access.user.success_update')
      }

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Update User success'
      });
    } catch (error) {
      result = {
        error,
      }

      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Update User error'
      });
    }
    Event.notify('access::user.edit', result);
  }

  public async createUser(data:any) {
    let result;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Create user starts'
      });

      const response = await ControlService.createUser(data);

      result = {
        message: Lang.get('access.user.success_create')
      }

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Create user success'
      });
    } catch (error) {
      result = {
        error,
      }

      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Create user error'
      });
    }
    Event.notify('access::user.edit', result);
  }
}

export default UpdateUserContainer;