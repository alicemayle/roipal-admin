import { Container } from 'unstated';
import ControlService from '../services/ControlService'

const initialState = {
  httpBusy: false,
  message: '',
  data: [],
  error: null,
}

class RoleCatalogContainer extends Container<any> {
  public state: any = {
    ...initialState
  };

  public name: string = 'Get Rol Containert';

  public async getRoles(params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Roles starts'
      });

      const response = await ControlService.getRoleCatalog(params);

      const catalog = response.data.map((item: any) => {
          const format = {
            ...item,
            label: item.name,
            value: item.uuid,
          }
          return format
      })

      const newData = this.state.data.concat(catalog)

      this.setState({
        httpBusy: false,
        data: newData,
        __action: 'Get Roles success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Get Roles error'
      });
    }
  }

  public reset() {
    this.setState({
      ...initialState,
      __action: 'Reset Roles Container'
    });
  }
}

export default RoleCatalogContainer;