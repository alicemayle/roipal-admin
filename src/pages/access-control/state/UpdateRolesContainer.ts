import { Container } from 'unstated';
import ControlService from '../services/ControlService';
import Event from '../../../support/Event';
import Lang from 'src/support/Lang';

const initialState = {
  httpBusy: false,
  message: '',
  data: null,
  error: null,
  dataRevoke: null,
  createData: null
}

class UpdateRolesContainer extends Container<any> {
  public state: any = {
    ...initialState
  };

  public name: string = 'Create Rol Containert';

  public async createRole(data:any) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Create Roles  starts'
      });

      const response = await ControlService.createRole(data);

      this.setState({
        httpBusy: false,
        createData: response.data,
        __action: 'Create Roles  success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Create Roles  error'
      });
    }
  }

  public async sendPermissions(uuid:any, data:any){
    let result;
    try {
      this.setState({
        httpBusy: true,
        __action: 'Send Permissions starts'
      });

      const response = await ControlService.sendPermissions(uuid, data);

      result = {
        message: Lang.get('access.role.success'),
      }

      this.setState({
        httpBusy: false,
        sendPermissions: response.data,
        __action: 'Send Permissions success'
      });
    } catch (error) {
      result = {
        error,
      }

      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Send Permissions error'
      });
    }
    Event.notify('access::role.edit', result);
  }
}

export default UpdateRolesContainer;