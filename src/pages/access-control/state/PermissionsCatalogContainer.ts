import { Container } from 'unstated';
import ControlService from '../services/ControlService'

const initialState = {
  httpBusy: false,
  message: '',
  data: [],
  error: null,
  sendPermissions: null
}

class PermissionsCatalogContainer extends Container<any> {
  public state: any = {
    ...initialState
  };

  public name: string = 'Get Permissions Containert';

  public async getPermissionsCatalog(params: any) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Permissions starts'
      });

      const response = await ControlService.getPermissionsCatalog(params);

      response.data.map((item:any)=>{
        item.label = item.name,
        item.value = item.uuid
      })

      this.setState({
        httpBusy: false,
        data: response.data,
        __action: 'Get Permissions success'
      });
    } catch (error) {
      this.setState({
        httpBusy: false,
        error,
        message: error.message,
        __action: 'Get Permissions error'
      });
    }
  }

  public reset() {
    this.setState({
      ...initialState,
      __action: 'Reset Permissions Container'
    });
  }
}

export default PermissionsCatalogContainer;