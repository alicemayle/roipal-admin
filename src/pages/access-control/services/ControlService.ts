import { Http } from '../../../shared';

class ControlService {
  public getUsers(params: any): any {
    return Http.get(`api/user`, {params});
  }

  public getRoleCatalog(params: any) {
    return Http.get(`api/role`, {params});
  }

  public updateUser(uuid:any, data:any){
    return Http.put(`/api/user/${uuid}/roles?includes=roles,permissions`, data);
  }

  public createUser(data:any){
    return Http.post(`/api/user?includes=roles,permissions`, data);
  }

  public getRolesGroup(params:any) {
    return Http.get(`/api/rol`, {params});
  }

  public getPermissionsCatalog(params: any) {
    return Http.get(`/api/permission`, {params});
  }

  public sendPermissions(uuid: any, data: any){
    return Http.put(`/api/rol/${uuid}?includes=permissions`, data);
  }

  public createRole(data:any){
    return Http.post(`/api/rol`, data);
  }
}


export default new ControlService;