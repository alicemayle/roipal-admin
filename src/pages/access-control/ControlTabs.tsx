import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Tabs, Divider } from 'antd';
import RoleList from './components/RoleList';
import UserList from './components/UserList';
import Lang from 'src/support/Lang';
import { Gate } from '@xaamin/guardian';

const { TabPane } = Tabs;

class ControlTabs extends Component<any, any> {
  constructor(props: any) {
    super(props);
    autobind(this)
  }

  public state = {
    pass: false,
    visible: false,
    rolesUser: []
  }

  public render() {
    return (
      <div>
        <h1>{Lang.get('access.title')}</h1>
        <Divider />
        <Tabs defaultActiveKey="1">
          {
            Gate.allows('role::page-access') &&
            <TabPane tab={Lang.get('access.role.title')} key="1">
              <RoleList />
            </TabPane>
          }
          {
            Gate.allows('user::page-access') &&
            <TabPane tab={Lang.get('access.user.title')} key="2">
              <UserList />
            </TabPane>
          }
        </Tabs>
      </div>
    )
  }
}

export default (ControlTabs);