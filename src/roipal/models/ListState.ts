import ResponseError from './ResponseError'

interface ListState {
  httpBusy?: boolean,
  message: string | null,
  data?: any,
  error?: ResponseError | null,
  meta?: any | null
}

export default ListState;