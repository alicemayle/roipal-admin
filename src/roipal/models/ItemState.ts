import ResponseError from './ResponseError'

interface ItemState {
  httpBusy?: boolean,
  message: string | null,
  data?: any,
  error?: ResponseError | null,
  meta?: any | null
}

export default ItemState;