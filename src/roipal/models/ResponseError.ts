interface ResponseError {
  code?: number,
  message: string | null,
  errors?: any[] | null
}

export default ResponseError;